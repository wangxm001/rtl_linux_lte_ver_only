show_usage(){
    echo "Usage:"
    echo "    $0 -h, --help         Show this usage"
    echo "    $0 -l, --list         List available projects"
    echo "    $0 project_name       Apply a custom"
}


list_projects(){
    echo "+--------------------------------------------+"
    echo "|               Project List                 |"
    echo "+--------------------------------------------+"
	new_version=`lsb_release -a 2>/dev/null | grep Release  | grep 10.04`
	if [ "$new_version" == "" ];then
    ls trunk/uClinux-release-2.4.0/vendors/Realtek/RTL8670/web/ -l | grep '^d' | awk '{printf "| %-42s |\n", $9}'
	else
    ls trunk/uClinux-release-2.4.0/vendors/Realtek/RTL8670/web/ -l | grep '^d' | awk '{printf "| %-42s |\n", $8}'	
	fi

    echo "+--------------------------------------------+"
}

do_custom(){
    if [ ! -d "trunk/uClinux-release-2.4.0/vendors/Realtek/RTL8670/web/$1" ]; then
        echo "[ERROR] Project name = $1 is invalid!"

        list_projects

        exit 1
    fi

    echo "+--------------------------------------------+"
    printf "| Custom Project: %-27s|\n" $1
    echo "+--------------------------------------------+"
    cd trunk
    for dir in *
    do
    if [ "$dir" != "uClinux-release-2.4.0" ]; then
        echo "[ERROR]: DIR $dir not support custom"
        exit 1
    fi
    done
}


################################
# Main                         #
################################

cd $(dirname $0)

case $1 in
    "")
        echo "[ERROR] Project name can't be empty."
        echo "[ERROR] You can specify the project name by 'export LCT_CUSTOM_PROJECT=xxx' before"
        echo "[ERROR] bitbake something or build the project directly with 'build9615 <project_name>'"
        exit 1
        ;;

    -h | --help)
        show_usage
        ;;

    -h | --list)
        list_projects
        ;;

    *)
        do_custom $1
        ;;
esac

