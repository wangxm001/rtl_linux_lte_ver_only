#ifndef _PRMT_IPERF_H_
#define _PRMT_IPERF_H_

#include "prmt_igd.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef CONFIG_USER_IPERF

extern struct CWMP_LEAF tIperfLeaf[];

int getIperf(char *name, struct CWMP_LEAF *entity, int *type, void **data);
int setIperf(char *name, struct CWMP_LEAF *entity, int type, void *data);

#endif /*CONFIG_USER_IPERF*/
	
#ifdef __cplusplus
}
#endif
#endif /*_PRMT_IPERF_H_*/
