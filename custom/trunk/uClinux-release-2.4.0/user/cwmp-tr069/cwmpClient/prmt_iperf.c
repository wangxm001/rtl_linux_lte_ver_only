#include "prmt_iperf.h"
#ifdef CONFIG_USER_IPERF
#include <pthread.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/errno.h>
#include <sys/wait.h>
#include  <dirent.h>
#define READ_BUF_SIZE 128

void remove_delimitor( char *s)
{
    char *p1, *p2;

    p1 = p2 = s;
    while ( *p1 != '\0' || *(p1+1) != '\0') {
        if (*p1 != '\0') {
           *p2 = *p1;
           p2++;
        }
        p1++;
    }
    *p2='\0';

}

pid_t * find_pid_by_name( char* pidName)
{
    DIR *dir;
    struct dirent *next;
    pid_t* pidList=NULL;
    int i=0;

    /*FILE *status */
    FILE *cmdline;
    char filename[READ_BUF_SIZE];
    char buffer[READ_BUF_SIZE];
    /* char name[READ_BUF_SIZE]; */
            
    dir = opendir("/proc");
    if (!dir) {
        printf("cfm:Cannot open /proc");
        return NULL;
    }

    while ((next = readdir(dir)) != NULL) {
        /* re-initialize buffers */
        memset(filename, 0, sizeof(filename));
        memset(buffer, 0, sizeof(buffer));  

        /* Must skip ".." since that is outside /proc */
        if (strcmp(next->d_name, "..") == 0)
            continue;

        /* If it isn't a number, we don't want it */
        if (!isdigit(*next->d_name))
            continue;

        /* sprintf(filename, "/proc/%s/status", next->d_name); */
        /* read /porc/<pid>/cmdline instead to get full cmd line */
        sprintf(filename, "/proc/%s/cmdline", next->d_name);
        if (! (cmdline = fopen(filename, "r")) ) {
            continue;
        }
        if (fgets(buffer, READ_BUF_SIZE-1, cmdline) == NULL) {
            fclose(cmdline);
           continue;
        }
        fclose(cmdline);

        /* Buffer should contain a string like "Name:   binary_name" */
        /*sscanf(buffer, "%*s %s", name);*/
        /* buffer contains full commandline params separted by '\0' */
        remove_delimitor(buffer);
        if (strstr(buffer, pidName) != NULL) {
            pidList=realloc( pidList, sizeof(pid_t) * (i+2));
            if (!pidList) {
               printf("Out of memeory!\n");
               closedir(dir);
               return NULL;
            }
            pidList[i++]=strtol(next->d_name, NULL, 0);
            //printf(" %u", strtol(next->d_name, NULL, 0));
        }
    }
    closedir(dir);

    if (pidList)
       pidList[i]=0;
    else if ( strcmp(pidName, "init")==0) {
        /* If we found nothing and they were trying to kill "init",
         * guess PID 1 and call it good...  Perhaps we should simply
         * exit if /proc isn't mounted, but this will do for now. */
        pidList=realloc( pidList, sizeof(pid_t));
        if (!pidList) {
           printf("Out of memeory!\n");
           return NULL;
        }
        pidList[0]=1;
    } else {
        pidList=realloc( pidList, sizeof(pid_t));
        if (!pidList) {
            printf("Out of memeory!\n");
            return NULL;
        }
        pidList[0]=-1;
    }
    return pidList;
}

int GetPid(char * command)
{
    char cmdline[128], *p1, *p2;
    pid_t *pid = NULL;
    int ret = 0;

    p1 = command;
    p2 = cmdline;
    while ( *p1 != '\0') {
        if (*p1 != ' ') {
           *p2 = *p1;
           p2++;
         }
         p1++;
    }
    *p2='\0';

    pid = find_pid_by_name(cmdline);
    if ( pid != NULL ) {
       ret = (int)(*pid);
       free(pid);
    }
    return ret;
}
struct CWMP_OP tIperfLeafOP = { getIperf,	setIperf };
struct CWMP_PRMT tIperfLeafInfo[] =
{
{"LastMeasurementDateTime",		eCWMP_tSTRING,	CWMP_READ,	&tIperfLeafOP},
#if 1
{"Mode",		                  eCWMP_tSTRING,	CWMP_WRITE|CWMP_READ,	&tIperfLeafOP},
#endif
{"ServerAddress",		          eCWMP_tSTRING,	CWMP_WRITE|CWMP_READ,	&tIperfLeafOP},
{"ServerPort",		            eCWMP_tUINT,	  CWMP_WRITE|CWMP_READ,	&tIperfLeafOP},
{"DurationTime",		          eCWMP_tUINT,	  CWMP_WRITE|CWMP_READ,	&tIperfLeafOP},
{"Protocol",		              eCWMP_tSTRING,	CWMP_WRITE|CWMP_READ,	&tIperfLeafOP},
{"threadsNumber",		          eCWMP_tUINT,	  CWMP_WRITE|CWMP_READ,	&tIperfLeafOP},
{"Status",		                eCWMP_tSTRING,	CWMP_READ,	&tIperfLeafOP},
{"UplinkSpeed",		            eCWMP_tSTRING,	CWMP_READ,	&tIperfLeafOP},
{"DownlinkSpeed",		          eCWMP_tSTRING,	CWMP_READ,	&tIperfLeafOP},
};

enum eIperfLeaf
{
	eLastMeasurementDateTime,
#if 1
	eMode,
#endif
	eServerAddress,
	eServerPort,
	eDurationTime,
	eProtocol,
	ethreadsNumber,
	eStatus,
	eUplinkSpeed,
	eDownlinkSpeed,
};

struct CWMP_LEAF tIperfLeaf[] =
{
{ &tIperfLeafInfo[eLastMeasurementDateTime] }, 
#if 1
{ &tIperfLeafInfo[eMode] }, 
#endif
{ &tIperfLeafInfo[eServerAddress] }, 
{ &tIperfLeafInfo[eServerPort] }, 
{ &tIperfLeafInfo[eDurationTime] }, 
{ &tIperfLeafInfo[eProtocol] }, 
{ &tIperfLeafInfo[ethreadsNumber] }, 
{ &tIperfLeafInfo[eStatus] }, 
{ &tIperfLeafInfo[eUplinkSpeed] }, 
{ &tIperfLeafInfo[eDownlinkSpeed] }, 
{ NULL }
};

void restartIperf()
{
  char tmpBuf[100];
  char cmd[256];
  MIB_IPERF_TBL_T entry,*p;
  int pid;
  char port[8];
  char nthread[8];
  char serverIP[64];
  char duration[16];
  
  p=&entry;
  if(!mib_chain_get(MIB_IPERF_TBL, 0, (void*)p))
  {
    printf("mib_chain_get error\n");
    return;
  }
  
	if((pid = GetPid("/bin/iperf")) > 0)
	{
	  sprintf(tmpBuf, "Stop Test");
	  sprintf(cmd, "kill %d && sleep 1", pid);
	  system(cmd);
	}
  
	mib_set(MIB_IPERF_STATUS, (void *)"");
	mib_set(MIB_IPERF_UPLINK, (void *)"");
	mib_set(MIB_IPERF_DOWNLINK, (void *)"");
	
	if(p->mode == 1) /*server*/
	{
		sprintf(port, "%d", p->port);
		
		if( p->protocol == 1)
		{
			va_cmd("/bin/iperf", 8, 0, "-s", "-p", port, "-P", "0", "-i", "2", "-d");
		}
		else
		{
			va_cmd("/bin/iperf", 9, 0, "-s", "-p", port, "-P", "0", "-i", "2", "-d", "-u");
		}
	}
	else /*client*/
	{
		sprintf(port, "%d", p->port);
		sprintf(nthread, "%d", p->clientNum);
		sprintf(duration, "%d", p->duration);
		sprintf(serverIP, "%s", p->serverAddr);
		
		if( p->protocol == 1)
		{
			va_cmd("/bin/iperf", 11, 0, "-c", serverIP, "-p", port, "-t", duration, "-P", nthread, "-i", "2", "-d");
		}
		else
		{
			va_cmd("/bin/iperf", 14, 0, "-c", serverIP, "-p", port, "-t", duration, "-P", nthread, "-i", "2", "-d", "-u", "-b", "100m");
		}
	}
  getSYS2Str(SYS_DATE, cmd);
  strncpy(p->lastTime, cmd,  sizeof(p->lastTime)-1);	
  
  if(!mib_chain_update( MIB_IPERF_TBL, (void*)p, 0))
  {
    printf("mib_chain_update error\n");
  }
}

int getIperf(char *name, struct CWMP_LEAF *entity, int *type, void **data)
{
	char	*lastname = entity->info->name;
	unsigned char vChar=0;
	struct in_addr ipAddr;
	char tmpBuf[256]={0};
	char status[64] = "\0";
	MIB_IPERF_TBL_T entry,*p;
	
	if( (name==NULL) || (type==NULL) || (data==NULL) || (entity==NULL))
		return -1;
		
	p=&entry;
  if(!mib_chain_get(MIB_IPERF_TBL, 0, (void*)p))
 		return -1;
 		
	*type = entity->info->type;
	*data = NULL;
	
	if( strcmp( lastname, "LastMeasurementDateTime" )==0)
	{
		*data = strdup(p->lastTime);
	}
#if 1
	else if( strcmp( lastname, "Mode" )==0)
	{
		*data = strdup(p->mode ? "Server" : "Client");
	}
#endif
	else if( strcmp( lastname, "ServerAddress" )==0)
	{
		*data = strdup(p->serverAddr);
	}
	else if( strcmp( lastname, "ServerPort" )==0)
	{
		*data = intdup(p->port);
	}
	else if( strcmp( lastname, "DurationTime" )==0)
	{
		*data = intdup(p->duration);
	}
	else if( strcmp( lastname, "Protocol" )==0)
	{
		*data = strdup(p->protocol == 1 ? "TCP" : "UDP");
	}
	else if( strcmp( lastname, "threadsNumber" )==0)
	{
		*data = intdup(p->clientNum);
	}
	else if( strcmp( lastname, "Status" )==0)
	{
		mib_get(MIB_IPERF_STATUS, (void *)status);
		
		if(GetPid("/bin/iperf") > 0)
		{
			if(status[0] != '\0')
			{
				sprintf(tmpBuf, "%s", status);
			}
			else
			{
			  sprintf(tmpBuf, "Running");
			}
		}
		else
		{
			if(status[0] != '\0')
			{
				sprintf(tmpBuf, "%s", status);
			}
			else
			{
			  sprintf(tmpBuf, "Not Run");
			}
		}
		*data = strdup(tmpBuf);
	}
	else if( strcmp( lastname, "UplinkSpeed" )==0)
	{
		mib_get(MIB_IPERF_UPLINK, (void *)tmpBuf);
		*data = strdup(tmpBuf);
	}
	else if( strcmp( lastname, "DownlinkSpeed" )==0)
	{
		mib_get(MIB_IPERF_DOWNLINK, (void *)tmpBuf);
		*data = strdup(tmpBuf);
	}
	else
	{
		return ERR_9005;
	}

	return 0;
}

int setIperf(char *name, struct CWMP_LEAF *entity, int type, void *data)
{
	char	*lastname = entity->info->name;
	char	*buf=data;
	unsigned char vChar=0;
	MIB_IPERF_TBL_T entry,*p;

	if( (name==NULL) || (data==NULL) || (entity==NULL)) return -1;
	if( entity->info->type!=type ) return ERR_9006;

	p=&entry;
  if(!mib_chain_get(MIB_IPERF_TBL, 0, (void*)p))
 		return -1;
#if 1 	
	if( strcmp( lastname, "Mode" )==0)
	{
		char *buf = data;
		
		if(strlen(buf) != 0)
		{
			if(!strcmp(buf, "Server"))
			{
				p->mode = 1;
			}
			else if(!strcmp(buf, "Client"))
			{
				p->mode = 0;
			}
			if(!mib_chain_update( MIB_IPERF_TBL, (void*)p, 0))
  		{
    		printf("mib_chain_update error\n");
 			}
		}
		apply_add( CWMP_PRI_N, apply_Iperf, CWMP_RESTART, 0, NULL, 0 );
		return CWMP_APPLIED;
	}
	else 
#endif
	if( strcmp( lastname, "ServerAddress" )==0)
	{
		char *buf = data;
		
		if(strlen(buf) != 0)
		{
			strncpy(p->serverAddr, buf,  sizeof(p->serverAddr)-1);
			 
			if(!mib_chain_update( MIB_IPERF_TBL, (void*)p, 0))
  		{
    		printf("mib_chain_update error\n");
 			}
 		}
 		apply_add( CWMP_PRI_N, apply_Iperf, CWMP_RESTART, 0, NULL, 0 );
 		return CWMP_APPLIED;
	}
	else if( strcmp( lastname, "ServerPort" )==0)
	{
		int *i = data;
		
		if(i==NULL) return ERR_9007;
		if( *i> 65535) return ERR_9007;
		if(*i == 0) return ERR_9001;	//cannot set to 0 currently
		
		p->port = *i;
		
		if(!mib_chain_update( MIB_IPERF_TBL, (void*)p, 0))
  	{
    	printf("mib_chain_update error\n");
 		}
 		
 		apply_add( CWMP_PRI_N, apply_Iperf, CWMP_RESTART, 0, NULL, 0 );
 		return CWMP_APPLIED;
	}
	else if( strcmp( lastname, "DurationTime" )==0)
	{
		int *i = data;
		
		if(i==NULL) return ERR_9007;
		if(*i == 0) return ERR_9001;	//cannot set to 0 currently
		
		p->duration = *i;
		
		if(!mib_chain_update( MIB_IPERF_TBL, (void*)p, 0))
  	{
    	printf("mib_chain_update error\n");
 		}
 		
 		apply_add( CWMP_PRI_N, apply_Iperf, CWMP_RESTART, 0, NULL, 0 );
 		return CWMP_APPLIED;
	}
	else if( strcmp( lastname, "Protocol" )==0)
	{
		char *buf = data;
		
		if(strlen(buf) != 0)
		{
			if(!strcmp(buf, "TCP"))
			{
				p->protocol = 1;
			}
			else
			{
				p->protocol = 2;
			}
			if(!mib_chain_update( MIB_IPERF_TBL, (void*)p, 0))
  		{
    		printf("mib_chain_update error\n");
 			}
		}
		apply_add( CWMP_PRI_N, apply_Iperf, CWMP_RESTART, 0, NULL, 0 );
		return CWMP_APPLIED;
	}
	else if( strcmp( lastname, "threadsNumber" )==0)
	{
		int *i = data;
		
		if(i==NULL) return ERR_9007;
		if(*i == 0) return ERR_9001;	//cannot set to 0 currently
		
		p->clientNum = *i;
		
		if(!mib_chain_update( MIB_IPERF_TBL, (void*)p, 0))
  	{
    	printf("mib_chain_update error\n");
 		}
 		
 		apply_add( CWMP_PRI_N, apply_Iperf, CWMP_RESTART, 0, NULL, 0 );
 		return CWMP_APPLIED;
	}
	else
	{
		return ERR_9005;
	}

	return 0;
}

#endif /*CONFIG_USER_IPERF*/
