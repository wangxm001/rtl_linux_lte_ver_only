#ifndef _PRMT_LTE_H_
#define _PRMT_LTE_H_

#include "prmt_igd.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef CONFIG_USER_LTE_TRICHEER

int getLTEInfoByName(const char *node, char *strInfo);

extern struct CWMP_NODE tLTESystemObject[];

extern struct CWMP_NODE tLTEObject[];

extern struct CWMP_LEAF tPeakDownloadLeaf[];


int getLTE_StatusSystem(char *name, struct CWMP_LEAF *entity, int *type, void **data);

int getLTE_StatusRX(char *name, struct CWMP_LEAF *entity, int *type, void **data);

int getLTE_StatusTX(char *name, struct CWMP_LEAF *entity, int *type, void **data);

int getLTE_StatusPDN(char *name, struct CWMP_LEAF *entity, int *type, void **data);

int getAuthentication_Type(char *name, struct CWMP_LEAF *entity, int *type, void **data);
int setAuthentication_Type(char *name, struct CWMP_LEAF *entity, int type, void *data);

int getNetwork_Attach(char *name, struct CWMP_LEAF *entity, int *type, void **data);
int setNetwork_Attach(char *name, struct CWMP_LEAF *entity, int type, void *data);

int getLTE_Cell_Selection(char *name, struct CWMP_LEAF *entity, int *type, void **data);
int setLTE_Cell_Selection(char *name, struct CWMP_LEAF *entity, int type, void *data);

int getLTE_Multi_PDN(char *name, struct CWMP_LEAF *entity, int *type, void **data);
int setLTE_Multi_PDN(char *name, struct CWMP_LEAF *entity, int type, void *data);

int getLTE_Change_PIN(char *name, struct CWMP_LEAF *entity, int *type, void **data);
int setLTE_Change_PIN(char *name, struct CWMP_LEAF *entity, int type, void *data);

int getLTE_Enable_PIN(char *name, struct CWMP_LEAF *entity, int *type, void **data);
int setLTE_Enable_PIN(char *name, struct CWMP_LEAF *entity, int type, void *data);

int getPeakDownload(char *name, struct CWMP_LEAF *entity, int *type, void **data);
int setPeakDownload(char *name, struct CWMP_LEAF *entity, int type, void *data);

#endif /*CONFIG_USER_LTE_TRICHEER*/
	
#ifdef __cplusplus
}
#endif
#endif /*_PRMT_LTE_H_*/
