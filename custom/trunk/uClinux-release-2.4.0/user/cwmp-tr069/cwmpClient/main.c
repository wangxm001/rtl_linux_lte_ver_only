#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <netinet/in.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#include <signal.h>
#include <sys/wait.h>
#include <cwmp_rpc.h>
#include "prmt_igd.h"
#include "prmt_wancondevice.h"
#include "parameter_api.h"
#ifdef CONFIG_MIDDLEWARE
#include <rtk/midwaredefs.h>
#endif
#ifdef CONFIG_USER_CWMP_UPNP_DM
#include <sys/epoll.h>
#include "prmt_ctcom_proxy_dev_list.h"
#endif
#ifdef CONFIG_CWMP_TR181_SUPPORT
#include "tr181/prmt_device2.h"
#endif

void cwmp_show_help( void )
{
	fprintf( stderr, "cwmpClient:\n" );
	fprintf( stderr, "	-SendGetRPC:	send GetRPCMethods to ACS\n" );
	fprintf( stderr, "	-SSLAuth:	ACS need certificate the CPE\n" );
	fprintf( stderr, "	-SkipMReboot:	do not send 'M Reboot' event code\n" );
	fprintf( stderr, "	-Delay: 	delay some seconds to start\n" );
	fprintf( stderr, "	-NoDebugMsg: 	do not show debug message\n" );
#ifdef CONFIG_CWMP_TR181_SUPPORT
	fprintf( stderr, "	-TR181:		Use Device:2 as root node\n" );
#endif
	fprintf( stderr, "	-h or -help: 	show help\n" );
	fprintf( stderr, "\n" );
	fprintf( stderr, "	if no arguments, read the setting from mib\n" );
	fprintf( stderr, "\n" );
}

/*refer to climenu.c*/
#define CWMP_RUNFILE	"/var/run/cwmp.pid"
static void log_pid()
{
	FILE *f;
	pid_t pid;
	char *pidfile = CWMP_RUNFILE;

	pid = getpid();
	if((f = fopen(pidfile, "w")) == NULL)
		return;
	fprintf(f, "%d\n", pid);
	fclose(f);
}

static void clr_pid()
{
	unlink(CWMP_RUNFILE);
#ifdef CONFIG_MIDDLEWARE
	unlink(CWMP_MIDPROC_RUNFILE);
#endif
}

#ifdef CONFIG_E8B
#ifdef CONFIG_MIDDLEWARE
extern void handle_alarm(int sig, siginfo_t *si, void *uc);
extern void updateMidprocTimer();
void cwmp_handle_alarm(int sig, siginfo_t *si, void *uc)
{
	handle_alarm( sig, si, uc );
	updateMidprocTimer();
}

void handle_x_ct_event(int sig)
{
	unsigned char vChar;
	unsigned int vUint=0;

	printf("\n%s\n",__FUNCTION__);
	mib_get(CWMP_TR069_ENABLE,(void*)&vChar);
	if(vChar == 1 || vChar == 2){
		if( mib_get(CWMP_INFORM_EVENTCODE, &vUint)!=0 ){
			mib_get(MIB_MIDWARE_INFORM_EVENT,(void *)&vChar);
			switch(vChar){
			case CTEVENT_SEND_INFORM:
				vUint = vUint|(EC_PERIODIC);
				break;
			case CTEVENT_ACCOUNTCHANGE:
				vUint = vUint|(EC_X_CT_COM_ACCOUNT);
				break;
#ifdef _PRMT_X_CT_COM_USERINFO_
			case CTEVENT_BIND:
				vUint = vUint|(EC_X_CT_COM_BIND);
				break;
#endif
			default:
				break;
			}
			mib_set(CWMP_INFORM_EVENTCODE, &vUint);
		}
	}
}
#else
void handle_x_ct_account(int sig)
{
	unsigned int vUint=0;

	warn("%s():%d ", __FUNCTION__, __LINE__);
	if (mib_get(CWMP_INFORM_EVENTCODE, &vUint)) {
		vUint = vUint | EC_X_CT_COM_ACCOUNT;
		mib_set(CWMP_INFORM_EVENTCODE, &vUint);
	}
	cwmp_reset_retry_timer();
}

#ifdef _PRMT_X_CT_COM_USERINFO_
void handle_x_ct_bind(int sig)
{
	unsigned int vUint;

	warn("%s():%d ", __FUNCTION__, __LINE__);
	if (mib_get(CWMP_INFORM_EVENTCODE, &vUint)) {
		vUint = vUint | EC_X_CT_COM_BIND;
		mib_set(CWMP_INFORM_EVENTCODE, &vUint);
	}
}
#endif
#endif //#ifdef CONFIG_MIDDLEWARE
#endif // end of CONFIG_E8B


/*star:20091229 START send signal to cwmp process to let it know that wan connection ip changed*/
void sigusr1_handler()
{
	notify_set_wan_changed();
/*star:20100305 START add qos rule to set tr069 packets to the first priority queue*/
#if defined(IP_QOS) || defined(NEW_IP_QOS_SUPPORT)
	setTr069QosFlag(0);
#endif
/*star:20100305 END*/
}
/*star:20091229 END*/

void sigusr2_handler()
{
	struct cwmp_message msg = {0};
	int cwmp_msgid = msgget((key_t)1234,  0666);

	if(cwmp_msgid != -1)
	{
		msg.msg_type = MSG_PRINT_PRMT;
		msgsnd(cwmp_msgid, (void *)&msg, MSG_SIZE, 0);
	}
}

void clear_child(int i)
{
	int status;
	pid_t childpid = 0;

	//childpid=wait( &status );
#ifdef _PRMT_TR143_
#ifdef CONFIG_USER_FTP_FTP_FTP
	//if(childpid!=-1)
	checkPidforFTPDiag( childpid );
#endif //CONFIG_USER_FTP_FTP_FTP
#endif //_PRMT_TR143_
#ifdef _SUPPORT_TRACEROUTE_PROFILE_
//	if(childpid!=-1)
	checkPidforTraceRouteDiag( childpid );
#endif //_SUPPORT_TRACEROUTE_PROFILE_
#ifdef _PRMT_NSLOOKUP_
	checkPidforNSLookupDiag(childpid);
#endif
	/* clear the rest child */
	childpid = waitpid(-1, &status, WNOHANG);

	return;
}

void handle_term()
{
	clr_pid();
	exit(0);
}

static void* ip_link_listener(void *data)
{
	struct sockaddr_nl addr;
	int sock, len, rtl;
	char buffer[4096] = {0};
	struct nlmsghdr *nlh;
	struct ifaddrmsg *ifa;
	struct rtattr *rth;

	if ((sock = socket(PF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) == -1)
		perror ("socket failure\n");

	memset (&addr,0,sizeof(addr));
	addr.nl_family = AF_NETLINK;
	addr.nl_groups = RTMGRP_IPV4_IFADDR | RTMGRP_LINK;

	if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1)
		perror ("bind failure\n");

	while ((len = recv(sock, buffer, sizeof(buffer), 0)) > 0)
	{
		nlh = (struct nlmsghdr *)buffer;
		for (;(NLMSG_OK (nlh, len)) && (nlh->nlmsg_type != NLMSG_DONE); nlh = NLMSG_NEXT(nlh, len))
		{
			char cwmp_ifname[IFNAMSIZ] = {0};
			char *ifname = NULL;
			struct in_addr *ipaddr;

			if (nlh->nlmsg_type != RTM_NEWADDR)
				continue; /* some other kind of announcement */

			ifa = (struct ifaddrmsg *) NLMSG_DATA (nlh);

			if(ifa->ifa_family != AF_INET)
				continue;

			rth = IFA_RTA (ifa);
			rtl = IFA_PAYLOAD (nlh);
			for (;rtl && RTA_OK (rth, rtl); rth = RTA_NEXT (rth,rtl))
			{
				switch(rth->rta_type)
				{
				case IFA_ADDRESS:
					ipaddr = (struct in_addr *)RTA_DATA(rth);
					//fprintf(stderr, "Address got: %s\n", inet_ntoa(*ipaddr));
					break;
				case IFA_LABEL:
					ifname = (char *)RTA_DATA(rth);
					//fprintf(stderr, "Interface got: %s\n", ifname);
					break;
				default:
					//fprintf(stderr, "Ignored rta_type: %d\n", rth->rta_type);
					break;
				}
			}

			port_get_tr069_ifname(cwmp_ifname);
			if(ifname && strcmp(cwmp_ifname, ifname) == 0)
				cwmp_reset_retry_timer();
		}
	}

	if(sock != -1)
		close(sock);

	return NULL;
}


void start_ip_link_listener()
{
	pthread_t id;

	pthread_create(&id, NULL, ip_link_listener, NULL);
	pthread_detach(id);
}

#ifdef CONFIG_YUEME
#include <sys/un.h>
#include <sys/epoll.h>

static int tr069_listener_sock = -1;
#define TR069_LISTENER_FILE "/tmp/tr069_listener.sock"
#define MAX_TR069_IPC_BUF 2048

enum tr069_cmd
{
	TR069_CMD_GET_NAME = 1,
	TR069_CMD_GET_VALUE,
	TR069_CMD_SET_VALUE,
	TR069_CMD_ADD_OBJECT,
	TR069_CMD_DEL_OBJECT,
	TR069_CMD_MAX
};

struct tr069_ipc_msg
{
	unsigned int cmd;
	char name[256];
	char value[256];
};

struct tr069_ParameterValueStruct
{
	char Name[256];
	int type;
	char Value[256];
};

struct tr069_ParameterNameStruct
{
	char Name[256];
	int writable;
};

struct tr069_FaultStruct
{
	unsigned int FaultCode;
	char FaultString[128];
};

struct tr069_AddObjectResponse
{
	unsigned int InstanceNumber;
};

struct tr069_ResultHeaderStruct
{
	int data_len;
	int status;
};


static int make_socket_non_blocking (int sfd)
{
	int flags, s;

	flags = fcntl (sfd, F_GETFL, 0);
	if (flags == -1)
	{
		perror ("<tr069_ipc> fcntl");
		return -1;
	}

	flags |= O_NONBLOCK;
	s = fcntl (sfd, F_SETFL, flags);
	if (s == -1)
	{
		perror ("<tr069_ipc> fcntl");
		return -1;
	}

	return 0;
}

int init_tr069_ipc_socket(int epollfd)
{
	struct epoll_event ev;
	struct sockaddr_un bind_addr = {0};

	if ((tr069_listener_sock = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
	{
		perror("<tr069_ipc> socket");
		return -1;
	}

	bind_addr.sun_family = AF_UNIX;
	strcpy(bind_addr.sun_path, TR069_LISTENER_FILE);

	/* unlink before bind to avoid failure */
	unlink(TR069_LISTENER_FILE);
	if (bind(tr069_listener_sock, (struct sockaddr *)&bind_addr, sizeof(bind_addr)) < 0)
	{
		perror("<tr069_ipc> bind");
		goto fail;
	}

	if (make_socket_non_blocking(tr069_listener_sock) == -1)
	{
		goto fail;
	}

	if (listen(tr069_listener_sock, 5) == -1) {
		perror("<tr069_ipc> listen");
		goto fail;
	}

	ev.data.fd = tr069_listener_sock;
	ev.events = EPOLLIN;

	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, tr069_listener_sock, &ev) == -1) 
	{
		perror("<tr069_ipc> epoll_ctl");
		goto fail;
	}

	CWMPDBP1("Start to listening tr069_ipc event on socket %d\n", tr069_listener_sock);
	return 0;

fail:
	close(tr069_listener_sock);
	return -1;
}

int get_ParameterCount(int type, char *prefix, int next_level)
{
	char *name = NULL;
	int err = 0, n = 0, v = 0;

	CWMPDBG(3, (stderr, "<%s:%d>\n", __FUNCTION__, __LINE__));
	if (prefix == NULL || strlen(prefix) == 0)
		err = get_ParameterName("", next_level, &name);
	else
		err = get_ParameterName(prefix, next_level, &name);

	if ((err < 0) & (next_level == 1))	//object but no instances, return 0, not error code
	{
		struct CWMP_LEAF *entity = NULL;
		if (get_ParameterEntity(prefix, &entity) == 0)
			return 0;
	}

	do {
		if (name) {
			n++;
			int type;
			void *data = NULL;
			if (get_ParameterValue(name, &type, &data) == 0) {
				v++;
			}

			if(data) {
				get_ParameterValueFree(type, data);
			}
		}
	} while (get_ParameterName(NULL, next_level, &name) == 0);
	
	return type == 0 ? n:v;
}

void tr069_ipc_GetParameterName(int fd, struct tr069_ipc_msg *msg)
{
	char *name = NULL;
	int count = 0, result_size = 0;
	char *pDATA = NULL;
	struct tr069_ResultHeaderStruct *rh = NULL;
	count =	get_ParameterCount(0, msg->name, 0);
	if (count > 0)
	{
		result_size = sizeof(struct tr069_ResultHeaderStruct) + (sizeof(struct tr069_ParameterNameStruct) * count);
		pDATA = malloc(result_size);
		if (pDATA == NULL) {
			fprintf(stderr, "Out of memory: %s, line %d\n", __FILE__, __LINE__);
			return;
		}
		memset(pDATA, 0, result_size);
		rh = (struct tr069_ResultHeaderStruct *)pDATA;
		rh->data_len = sizeof(struct tr069_ParameterNameStruct) * count;
	}

	if (strlen(msg->name) == 0)
		get_ParameterName("", 0, &name);
	else
		get_ParameterName(msg->name, 0, &name);

	count = 0;
	do {
		if (name)
		{
			struct tr069_ParameterNameStruct *pn = NULL;
			int iswritable = 0, len = 0;
			get_ParameterIsWritable(name, &iswritable);

			pn = (struct tr069_ParameterNameStruct *)(pDATA + sizeof(struct tr069_ResultHeaderStruct) + (count * sizeof(struct tr069_ParameterNameStruct)));
			sprintf(pn->Name, "%s", name);
			pn->writable = iswritable ? 1:0;

			count++;
		}
	} while (get_ParameterName(NULL, 0, &name) == 0);

	char *sendTmp = NULL;
	sendTmp = pDATA;
	while (result_size > 0)
	{
		count = send(fd, (char *)sendTmp, (result_size >= MAX_TR069_IPC_BUF) ? MAX_TR069_IPC_BUF:result_size, 0);
		//fprintf(stderr, "send count = %d\n", count);
		sendTmp += MAX_TR069_IPC_BUF;
		result_size -= MAX_TR069_IPC_BUF;
	}

	if (pDATA) {
		free(pDATA);
	}
}

void tr069_ipc_GetParameterValue(int fd, struct tr069_ipc_msg *msg)
{
	char *name = NULL;
	int count = 0, result_size = 0;
	char *pDATA = NULL;
	struct tr069_ResultHeaderStruct *rh = NULL;
	count =	get_ParameterCount(1, msg->name, 0);

	if (count > 0)
	{
		result_size = sizeof(struct tr069_ResultHeaderStruct) + (sizeof(struct tr069_ParameterValueStruct) * count);
		pDATA = malloc(result_size);
		if (pDATA == NULL) {
			fprintf(stderr, "Out of memory: %s, line %d\n", __FILE__, __LINE__);
			return;
		}
		memset(pDATA, 0, result_size);
		rh = (struct tr069_ResultHeaderStruct *)pDATA;
		rh->data_len = sizeof(struct tr069_ParameterValueStruct) * count;
	}

	if (strlen(msg->name) == 0)
		get_ParameterName("", 0, &name);
	else
		get_ParameterName(msg->name, 0, &name);

	count = 0;
	do {
		if (name)
		{
			int type = 0;
			void *data = NULL;
			
			if (get_ParameterValue(name, &type, &data) == 0)
			{
				struct tr069_ParameterValueStruct *pv = NULL;
				pv = (struct tr069_ParameterValueStruct *)(pDATA + sizeof(struct tr069_ResultHeaderStruct) + (count * sizeof(struct tr069_ParameterValueStruct)));
				sprintf(pv->Name, "%s", name);
				pv->type = type;

				switch (type)
				{
					case SOAP_TYPE_int:
					{
						int *pInt = (int*)data;
						sprintf(pv->Value, "%d", pInt ? *pInt : 0);
					}
						break;
					case SOAP_TYPE_xsd__boolean:
					{
						int *pBool = (int*)data;
						sprintf(pv->Value, "%d", pBool ? *pBool : 0);
					}
						break;
					case SOAP_TYPE_unsignedInt:
					{
						unsigned int *pUint = (unsigned int *)data;
						sprintf(pv->Value, "%d", pUint ? *pUint : 0);
					}
						break;
					case SOAP_TYPE_time:
					{
						time_t *pTime = (time_t *)data;
						struct soap soap = {0};
						if(pTime)
							sprintf(pv->Value, "%s", *pTime == LONG_MAX ? "9999-12-31T23:59:59Z" : soap_dateTime2s(&soap, *pTime));
						else
							sprintf(pv->Value, "%s", "0001-01-01T00:00:00Z");
					}
						break;
					case eCWMP_tMICROSECTIME:
					{
						struct timeval *pTime = (struct timeval *)data;
						if (pTime && pTime->tv_sec)
						{
							struct tm m;
							localtime_r(&pTime->tv_sec, &m);
							sprintf(pv->Value, "%04d-%02d-%02dT%02d:%02d:%02d.%06d", m.tm_year+1900, m.tm_mon+1, m.tm_mday, m.tm_hour, m.tm_min, m.tm_sec, pTime->tv_usec);
						}
						else
							sprintf(pv->Value, "%s", "0001-01-01T00:00:00.000000");
					}
						break;
					case SOAP_TYPE_string:
					{
						if (data)
							strcpy(pv->Value, (char *)data);
					}
						break;
					case eCWMP_tFILE:
					{
						if (data)
							strcpy(pv->Value, (char *)data);
					}
						break;
					case SOAP_TYPE_SOAP_ENC__base64:
					{
						struct SOAP_ENC__base64 *pBase64 = data;
						char tmpoutput[256] = {0};

						if (pBase64 && pBase64->__ptr && (pBase64->__size > 0))
						{
							static const char strbase64[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
							unsigned char *s;
							int n;
							int i;
							unsigned long m;
							char tmpchar[5];

							s = pBase64->__ptr;
							n = pBase64->__size;
							for (; n > 2; n -= 3, s += 3)
							{
								m = s[0];
								m = (m << 8) | s[1];
								m = (m << 8) | s[2];
								for (i = 4; i > 0; m >>= 6)
									tmpchar[--i] = strbase64[m & 0x3F];
								tmpchar[4] = 0;
								sprintf(tmpoutput, "%s%s", tmpoutput, tmpchar);
							}

							if (n > 0)
							{
								m = 0;
								for (i = 0; i < n; i++)
									m = (m << 8) | *s++;
								for (; i < 3; i++)
									m <<= 8;
								for (i++; i > 0; m >>= 6)
									tmpchar[--i] = strbase64[m & 0x3F];
								for (i = 3; i > n; i--)
									tmpchar[i] = '=';
								tmpchar[4] = 0;
								sprintf(tmpoutput, "%s%s", tmpoutput, tmpchar);
							}
						}
						strcpy(pv->Value, tmpoutput);
					}
						break;
					case SOAP_TYPE_unsignedLong:
					{
						unsigned long *pUlong = (unsigned long *)data;
						sprintf(pv->Value, "%u", pUlong ? *pUlong : 0);
					}
						break;
					case SOAP_TYPE_xsd__hexBinary:
					{
						struct xsd__hexBinary *pHexBin = data;

						if (pHexBin && pHexBin->__ptr && (pHexBin->__size > 0))
						{
							int n;
							int i;
							char *value = NULL;

							n = pHexBin->__size;
							value = malloc(n*2 + 1);
							if (value == NULL)
							{
								fprintf(stderr, "Out of memory: %s, line %d\n", __FILE__, __LINE__);
								break;
							}

							for (i = 0; i < n; i++)
								sprintf(&value[i*2], "%02X", pHexBin->__ptr[i]);
							value[n*2] = '\0';
							sprintf(pv->Value, "%s", value);
							free(value);
						}
					}
						break;
					default:
						break;
				}
				count++;
			}

			if(data) {
				get_ParameterValueFree(type, data);
			}
		}
				
	} while (get_ParameterName(NULL, 0, &name) == 0);

	char *sendTmp = NULL;
	sendTmp = pDATA;
	while (result_size > 0)
	{
		count = send(fd, (char *)sendTmp, (result_size >= MAX_TR069_IPC_BUF) ? MAX_TR069_IPC_BUF:result_size, 0);
		//fprintf(stderr, "send count = %d\n", count);
		sendTmp += MAX_TR069_IPC_BUF;
		result_size -= MAX_TR069_IPC_BUF;
	}
	
	if (pDATA) {
		free(pDATA);
	}
}

void tr069_ipc_SetParameterValue(int fd, struct tr069_ipc_msg *msg)
{
	int err = 0, result_size = 0;
	char *pDATA = NULL;
	struct tr069_ResultHeaderStruct *rh = NULL;
	struct tr069_FaultStruct *fs = NULL;

	if (strlen(msg->name) == 0 || strlen(msg->value) == 0 )
	{
		struct tr069_FaultStruct *fs = NULL;
		result_size = sizeof(struct tr069_ResultHeaderStruct) + sizeof(struct tr069_FaultStruct);
		pDATA = malloc(result_size);
		if (pDATA == NULL) {
			fprintf(stderr, "Out of memory: %s, line %d\n", __FILE__, __LINE__);
			return;
		}
		memset(pDATA, 0, result_size);
		rh = (struct tr069_ResultHeaderStruct *)pDATA;
		rh->data_len = sizeof(struct tr069_FaultStruct);
		rh->status = -1;

		fs = (struct tr069_FaultStruct *)(pDATA + sizeof(struct tr069_ResultHeaderStruct));

		char *s = NULL;
		if (strlen(msg->name) == 0)
		{
			fs->FaultCode = 9005;
		}
		else if (strlen(msg->value) == 0)
		{
			fs->FaultCode = 9007;
		}

		s = cwmp_ErrorString(fs->FaultCode);
	  	if (s == NULL)
	  	{
			fs->FaultCode = 9002;
	  		s = strERR_default;
	  	}

		sprintf(fs->FaultString, "%s", s);
	}
	else
	{
		struct CWMP_LEAF *entity;
		int type;

		port_backup_config();

		if (get_ParameterEntity(msg->name, &entity) == 0)
		{
			char *s = msg->value;
			void *v = NULL;
			switch (entity->info->type)
			{
				case SOAP_TYPE_string:
				{
					type = SOAP_TYPE_string;
					v = s;
				}
					break;

				case SOAP_TYPE_int:
				{
					int *p = NULL;
					type = SOAP_TYPE_int;
					p = malloc(sizeof(int));

					if (p) {
						*p = (int)strtol(s, NULL, 10);

						if (p && isdigit(*p)) {
							v = p;
						}
						else {
							free(p);
						}
					}
				}
					break;

				case SOAP_TYPE_unsignedInt:
				{
					unsigned int *p = NULL;
					type = SOAP_TYPE_unsignedInt;
					p = malloc(sizeof(unsigned int));

					if (p) {
						*p = (unsigned int)strtol(s, NULL, 10);

						if (p && isdigit(*p)) {
							v = p;
						}
						else {
							free(p);
						}
					}
				}
					break;

				case SOAP_TYPE_xsd__boolean:
				{
					int *p = NULL;
					type = SOAP_TYPE_xsd__boolean;
					p = malloc(sizeof(int));

					if (p) {
						*p = (int)strtol(s, NULL, 10);

						if (p && (*p == 0 || *p == 1)) {
							v = p;
						}
						else {
							free(p);
						}
					}
				}
					break;
				
			}

			if (v) {
				free(v);
			}
		}
	}
}

void tr069_ipc_AddObject(int fd, struct tr069_ipc_msg *msg)
{
	int err = 0, num = 0, result_size = 0;
	char *pDATA = NULL;
	struct tr069_ResultHeaderStruct *rh = NULL;

	err = add_ParameterObject(msg->name, &num);

	if (err < 0)
	{
		struct tr069_FaultStruct *fs = NULL;
		result_size = sizeof(struct tr069_ResultHeaderStruct) + sizeof(struct tr069_FaultStruct);
		pDATA = malloc(result_size);
		if (pDATA == NULL) {
			fprintf(stderr, "Out of memory: %s, line %d\n", __FILE__, __LINE__);
			return;
		}
		memset(pDATA, 0, result_size);
		rh = (struct tr069_ResultHeaderStruct *)pDATA;
		rh->data_len = sizeof(struct tr069_FaultStruct);
		rh->status = -1;

		fs = (struct tr069_FaultStruct *)(pDATA + sizeof(struct tr069_ResultHeaderStruct));

		char *s = NULL;
		if (err == -1)
		{
			fs->FaultCode = 9002;
		}
		else
		{
			fs->FaultCode = -err;
		}

		s = cwmp_ErrorString(fs->FaultCode);
	  	if (s == NULL)
	  		s = strERR_default;

		sprintf(fs->FaultString, "%s", s);
	}
	else
	{
		struct tr069_AddObjectResponse *ao = NULL;
		result_size = sizeof(struct tr069_ResultHeaderStruct) + sizeof(struct tr069_AddObjectResponse);
		pDATA = malloc(result_size);
		if (pDATA == NULL) {
			fprintf(stderr, "Out of memory: %s, line %d\n", __FILE__, __LINE__);
			return;
		}
		memset(pDATA, 0, result_size);
		rh = (struct tr069_ResultHeaderStruct *)pDATA;
		rh->data_len = sizeof(struct tr069_AddObjectResponse);
		rh->status = err;

		ao = (struct tr069_AddObjectResponse *)(pDATA + sizeof(struct tr069_ResultHeaderStruct));
		ao->InstanceNumber = num;
	}

	send(fd, (char *)pDATA, result_size, 0);

	if (pDATA) {
		free(pDATA);
	}
}

void tr069_ipc_DeleteObject(int fd, struct tr069_ipc_msg *msg)
{
	int err = 0, result_size = 0;
	char *pDATA = NULL;
	struct tr069_ResultHeaderStruct *rh = NULL;

	err = del_ParameterObject(msg->name);

	if (err < 0)
	{
		struct tr069_FaultStruct *fs = NULL;
		result_size = sizeof(struct tr069_ResultHeaderStruct) + sizeof(struct tr069_FaultStruct);
		pDATA = malloc(result_size);
		if (pDATA == NULL) {
			fprintf(stderr, "Out of memory: %s, line %d\n", __FILE__, __LINE__);
			return;
		}
		memset(pDATA, 0, result_size);
		rh = (struct tr069_ResultHeaderStruct *)pDATA;
		rh->data_len = sizeof(struct tr069_FaultStruct);
		rh->status = -1;

		fs = (struct tr069_FaultStruct *)(pDATA + sizeof(struct tr069_ResultHeaderStruct));

		char *s = NULL;
		if (err == -1)
		{
			fs->FaultCode = 9002;
		}
		else
		{
			fs->FaultCode = -err;
		}

		s = cwmp_ErrorString(fs->FaultCode);
		if (s == NULL)
			s = strERR_default;

		sprintf(fs->FaultString, "%s", s);
	}
	else
	{
		result_size = sizeof(struct tr069_ResultHeaderStruct);
		pDATA = malloc(result_size);
		if (pDATA == NULL) {
			fprintf(stderr, "Out of memory: %s, line %d\n", __FILE__, __LINE__);
			return;
		}
		memset(pDATA, 0, result_size);
		rh = (struct tr069_ResultHeaderStruct *)pDATA;
		rh->data_len = 0;
		rh->status = err;
	}

	send(fd, (char *)pDATA, result_size, 0);

	if (pDATA) {
		free(pDATA);
	}
}


int handle_tr069_ipc_event(int fd, char *event)
{
	struct tr069_ipc_msg *msg = NULL;

	msg = (struct tr069_ipc_msg *)event;

	switch (msg->cmd)
	{
		case TR069_CMD_GET_NAME:
		{
			//fprintf(stderr, "\033[1;31m[%s:%s@%d]\033[0m TR069_CMD_GET_NAME: msg->name = %s\n", __FILE__, __FUNCTION__, __LINE__, msg->name);
			tr069_ipc_GetParameterName(fd, msg);
			break;
		}

		case TR069_CMD_GET_VALUE:
		{
			//fprintf(stderr, "\033[1;31m[%s:%s@%d]\033[0m TR069_CMD_GET_VALUE: msg->name = %s\n", __FILE__, __FUNCTION__, __LINE__, msg->name);
			tr069_ipc_GetParameterValue(fd, msg);
			break;
		}

		case TR069_CMD_SET_VALUE:
		{
			//fprintf(stderr, "\033[1;31m[%s:%s@%d]\033[0m TR069_CMD_SET_VALUE: msg->name = %s\n", __FILE__, __FUNCTION__, __LINE__, msg->name);
			tr069_ipc_SetParameterValue(fd, msg);
			break;
		}

		case TR069_CMD_ADD_OBJECT:
		{
			//fprintf(stderr, "\033[1;31m[%s:%s@%d]\033[0m TR069_CMD_ADD_OBJECT: msg->name = %s\n", __FILE__, __FUNCTION__, __LINE__, msg->name);
			tr069_ipc_AddObject(fd, msg);
			break;
		}

		case TR069_CMD_DEL_OBJECT:
		{
			//fprintf(stderr, "\033[1;31m[%s:%s@%d]\033[0m TR069_CMD_DEL_OBJECT: msg->name = %s\n", __FILE__, __FUNCTION__, __LINE__, msg->name);
			tr069_ipc_DeleteObject(fd, msg);
			break;
		}

		default:
			CWMPDBP1("Unknown command %d\n", msg->cmd);
			break;
	}

	return 0;
}

static void* tr069_ipc_listener(void *data)
{
#define MAX_EPOLL_EVENT 5
	struct epoll_event ev, events[MAX_EPOLL_EVENT];
	int epollfd;

	// initialize epoll
	epollfd = epoll_create(MAX_EPOLL_EVENT);
	if(epollfd < 0)
	{
		perror("<cwmpClient> epoll_create\n");
		return NULL;
	}

	init_tr069_ipc_socket(epollfd);

	while(1)
	{
		int num_ev;
		int i;

		num_ev = epoll_wait(epollfd, events, MAX_EPOLL_EVENT, -1);
		if (num_ev > 0)
		{
			for (i = 0 ; i < num_ev ; i++)
			{
				if (tr069_listener_sock == events[i].data.fd)
				{
					while (1)
					{
						struct sockaddr in_addr;
						socklen_t in_len;
						int infd;
						char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];

						in_len = sizeof(in_addr);
						infd = accept(tr069_listener_sock, &in_addr, &in_len);
						if (infd == -1)
						{
							if ((errno == EAGAIN) || (errno == EWOULDBLOCK))
							{
								break;
							}
							else
							{
								perror("<tr069_ipc> accept");
								break;
							}
						}

						getnameinfo(&in_addr, in_len, hbuf, sizeof(hbuf), sbuf, sizeof(sbuf), NI_NUMERICHOST | NI_NUMERICSERV);

						make_socket_non_blocking(infd);

						ev.data.fd = infd;
						ev.events = EPOLLIN;
						if (epoll_ctl(epollfd, EPOLL_CTL_ADD, infd, &ev) == -1)
						{
							perror("<tr069_ipc> epoll_ctl");
						}

					}
					continue;
				}
				else
				{
					int count = 0;
					char buf[MAX_TR069_IPC_BUF] = {0};
					count = recv(events[i].data.fd, buf, sizeof(buf), 0);

					if (count > 0)
					{
						handle_tr069_ipc_event(events[i].data.fd, buf);
						close(events[i].data.fd);
					}
				}
			}
		}
	}
	return NULL;
}

void start_tr069_ipc_listener()
{
	pthread_t id;

	pthread_create(&id, NULL, tr069_ipc_listener, NULL);
	pthread_detach(id);
}
#endif

static void* event_listener(void *data)
{
#ifdef CONFIG_USER_CWMP_UPNP_DM
#define MAX_EPOLL_EVENT 5
	struct epoll_event ev, events[MAX_EPOLL_EVENT];
	int epollfd;

	// initialize epoll
	epollfd = epoll_create(MAX_EPOLL_EVENT);
	if(epollfd < 0)
	{
		perror("<cwmpClient> epoll_create\n");
		return NULL;
	}

	init_upnpdm_socket(epollfd);

	while(1)
	{
		int num_ev;
		int i;

		num_ev = epoll_wait(epollfd, events, MAX_EPOLL_EVENT, -1);
		if(num_ev > 0)
		{
			for(i = 0 ; i < num_ev ; i++)
			{
				handle_upnpdm_event(events[i].data.fd);
			}
		}
	}

#endif
	return NULL;
}


void start_event_listener()
{
	pthread_t id;

	pthread_create(&id, NULL, event_listener, NULL);
	pthread_detach(id);
}


int main(int argc, char **argv)
{
	struct net_device_stats nds;
#ifdef CONFIG_MIDDLEWARE
	unsigned char vChar;
	struct CWMP_NODE * selRoot;
	struct sigaction act;
	struct itimerspec its;
	struct sigevent te;
	timer_t timer;
#endif

	log_pid();

#ifdef CONFIG_USER_LTE_TRICHEER
	sleep(30);
#endif

	get_net_device_stats("br0", &nds);
	srand(nds.rx_bytes + nds.tx_bytes);
#ifdef CONFIG_E8B
#ifdef CONFIG_MIDDLEWARE
	signal(SIGUSR1, handle_x_ct_event);		//xl_yue: SIGUSR2 is used by midware
#else
	signal(SIGUSR1, handle_x_ct_account);
#ifdef _PRMT_X_CT_COM_USERINFO_
	signal(SIGUSR2, handle_x_ct_bind);
#endif	//end of CONFIG_MIDDLEWARE
#endif	//end of CONFIG_MIDDLEWARE
#else	//CONFIG_E8B
	signal(SIGUSR1, sigusr1_handler);
	signal(SIGUSR2, sigusr2_handler);
#endif

	signal(SIGCHLD, clear_child);	//set this signal process function according to CWMP_TR069_ENABLE below if MIDDLEWARE is defined
	signal(SIGTERM, handle_term);

#ifdef CONFIG_MIDDLEWARE
	act.sa_flags = SA_SIGINFO;
	act.sa_sigaction = cwmp_handle_alarm;
	sigemptyset(&act.sa_mask);
	sigaction(SIGALRM, &act, NULL);

	te.sigev_notify = SIGEV_SIGNAL;
	te.sigev_signo = SIGALRM;
	te.sigev_value.sival_int = 0;
	timer_create(CLOCK_REALTIME, &te, &timer);

	its.it_interval.tv_sec = 1;
	its.it_interval.tv_nsec = 0;
	its.it_value.tv_sec = 1;
	its.it_value.tv_nsec = 0;
	timer_settime(timer, 0, &its, NULL);
#endif

/*star:20100305 START add qos rule to set tr069 packets to the first priority queue*/
#if defined(IP_QOS) || defined(NEW_IP_QOS_SUPPORT)
	setTr069QosFlag(0);
#endif
/*star:20100305 END*/

	if( argc >= 2 )
	{
		int i;
		for(i=1;i<argc;i++)
		{
			if( strcmp( argv[i], "-SendGetRPC" )==0 )
			{
				cwmpinit_SendGetRPC(1);
				fprintf( stderr, "<%s>Send GetPRCMethods to ACS\n",__FILE__ );
			}else if( strcmp( argv[i], "-SSLAuth" )==0 )
			{
				cwmpinit_SSLAuth(1);
				fprintf( stderr, "<%s>Set using certificate auth.\n",__FILE__ );
			}else if( strcmp( argv[i], "-SkipMReboot" )==0 )
			{
				cwmpinit_SkipMReboot(1);
				fprintf( stderr, "<%s>Set skipping MReboot event code\n",__FILE__ );
			}else if( strcmp( argv[i], "-Delay" )==0 )
			{
				cwmpinit_DelayStart(30);
				fprintf( stderr, "<%s>Set Delay!\n", __FILE__ );
			}else if( strcmp( argv[i], "-NoDebugMsg" )==0 )
			{
				cwmpinit_NoDebugMsg(1);
				fprintf( stderr, "<%s>Set No Debug Message!\n", __FILE__ );

#ifdef CONFIG_CWMP_TR181_SUPPORT
			}else if( strcmp( argv[i], "-TR181" )==0 )
			{
				cwmpinit_UseTR181();
#endif
			}else if( strcmp( argv[i], "-h" )==0 || strcmp( argv[i], "-help" )==0 )
			{
				cwmp_show_help();
				exit(0);
			}else
			{
				fprintf( stderr, "<%s>Error argument: %s\n", __FILE__,argv[i] );
			}
		}
	}else{
		unsigned char cwmp_flag=0;
		//read the flag, CWMP_FLAG, from mib
		if ( mib_get( CWMP_FLAG, (void *)&cwmp_flag)!=0 )
		{
			printf("\ncwmp_flag=%x\n",cwmp_flag);
			if( (cwmp_flag&CWMP_FLAG_DEBUG_MSG)==0 )
			{
				fprintf( stderr, "<%s>Set No Debug Message!\n", __FILE__ );
				cwmpinit_NoDebugMsg(1);
			}

			if( cwmp_flag&CWMP_FLAG_CERT_AUTH )
			{
				fprintf( stderr, "<%s>Set using certificate auth.\n",__FILE__ );
				cwmpinit_SSLAuth(1);
			}

			if( cwmp_flag&CWMP_FLAG_SENDGETRPC )
			{
				fprintf( stderr, "<%s>Send GetPRCMethods to ACS\n",__FILE__ );
				cwmpinit_SendGetRPC(1);
			}

			if( cwmp_flag&CWMP_FLAG_SKIPMREBOOT )
			{
				fprintf( stderr, "<%s>Set skipping MReboot event code\n",__FILE__ );
				cwmpinit_SkipMReboot(1);
			}

			if( cwmp_flag&CWMP_FLAG_DELAY )
			{
				fprintf( stderr, "<%s>Set Delay!\n", __FILE__ );
				cwmpinit_DelayStart(30);
			}

			if( cwmp_flag&CWMP_FLAG_SELFREBOOT)
			{
				fprintf( stderr, "<%s>Set SelfReboot!\n", __FILE__ );
				cwmpinit_SelfReboot(1);
			}

		}

		if ( mib_get( CWMP_FLAG2, (void *)&cwmp_flag)!=0 )
		{
			if( cwmp_flag&CWMP_FLAG2_DIS_CONREQ_AUTH)
			{
				fprintf( stderr, "<%s>Set DisConReqAuth!\n", __FILE__ );
				cwmpinit_DisConReqAuth(1);
			}

			if( cwmp_flag&CWMP_FLAG2_DEFAULT_WANIP_IN_INFORM)
				cwmpinit_OnlyDefaultWanIPinInform(1);
			else
				cwmpinit_OnlyDefaultWanIPinInform(0);
#ifdef CONFIG_CWMP_TR181_SUPPORT
			if( cwmp_flag&CWMP_FLAG2_USE_TR181)
				cwmpinit_UseTR181();
#endif

			if(cwmp_flag & CWMP_FLAG2_HTTP_503)
				cwmpinit_Return503(1);
		}
	}

#ifdef TELEFONICA_DEFAULT_CFG
//#ifdef WLAN_SUPPORT
	cwmpinit_BringLanMacAddrInInform(1);
//#endif //WLAN_SUPPORT
	cwmpinit_SslSetAllowSelfSignedCert(0);
#endif //TELEFONICA_DEFAULT_CFG

#ifdef CONFIG_USER_TR104
	cwmp_solarOpen();
#endif
//startRip();

	start_ip_link_listener();

	start_event_listener();

#ifdef CONFIG_YUEME
	start_tr069_ipc_listener();
#endif

	printf("\nenter cwmp_main!\n");

#ifdef CONFIG_MIDDLEWARE
	cwmp_main(mw_tROOT);
#else

#ifdef CONFIG_CWMP_TR181_SUPPORT
	if(gUseTR181)
	{
		printf("Root is Device:2.\n");
		cwmp_main(device_tROOT);
	}
	else
	{
		printf("Root is InternetGatewayDevice.\n");
		cwmp_main(tROOT);
	}
#else	//CONFIG_CWMP_TR181_SUPPORT
	cwmp_main(tROOT);
#endif	//CONFIG_CWMP_TR181_SUPPORT
#endif

	return 0;
}
