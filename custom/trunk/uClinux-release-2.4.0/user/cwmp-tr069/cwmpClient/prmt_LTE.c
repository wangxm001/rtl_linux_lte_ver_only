#include "prmt_LTE.h"

#ifdef CONFIG_USER_LTE_TRICHEER

typedef struct {
	char *node;
	char * path;
	char * value;
} LTE_INFO_PATH;

LTE_INFO_PATH get_info_file_list[] = {
	{ "Frequency_Range",    NULL ,                                  "3400~3800 MHz"},
	{ "ICCID",              NULL ,                                   ""},
	{ "IMEI",               "/tmp/nv/wm_wcdma_imei_nv" ,             ""},
	{ "IMSI",               "/tmp/nv/wm_wcdma_imsi_nv" ,             ""},
	{ "Service_Provider",   "/tmp/nv/wm_wcdma_operatorname_tmp" ,    ""},
	{ "Bandwidth",          "/tmp/nv/wm_wcdma_bndwidth_value_tmp" ,  ""},
	{ "CINR0",              "/tmp/nv/wm_wcdma_cinr0_value_tmp" ,     ""},
	{ "CINR1",              "/tmp/nv/wm_wcdma_cinr1_value_tmp" ,     ""},
	{ "CellID",             "/tmp/nv/wm_wcdma_cellid_value_tmp" ,    ""},
	{ "ConnectTime",        "/tmp/nv/wm_wcdma_volume_value_tmp" ,    ""},
	{ "DL_Frequency",       "/tmp/nv/wm_wcdma_dlfqcy_value_tmp" ,    ""},
	{ "Network_Operator",   "/tmp/nv/wm_wcdma_plmn" ,                ""},
	{ "PCI",                "/tmp/nv/wm_wcdma_pci_value_tmp" ,       ""},
	{ "RSRP0",              "/tmp/nv/wm_wcdma_rsrp0_value_tmp" ,     ""},
	{ "RSRP1",              "/tmp/nv/wm_wcdma_rsrp1_value_tmp" ,     ""},
	{ "RSRQ",               "/tmp/nv/wm_wcdma_rsrq_value_tmp" ,      ""},
	{ "SINR0",              "/tmp/nv/wm_wcdma_sinr0_value_tmp" ,     ""},	
	{ "SINR1",              "/tmp/nv/wm_wcdma_sinr1_value_tmp" ,     ""},
	{ "State",              "/tmp/nv/wm_wcdma_status_tmp" ,          ""},	
	{ "TXPower",            "/tmp/nv/wm_wcdma_txpusch_value_tmp" ,   ""},
	{ "Technology",          NULL ,                                 "LTE"},
	{ "UL_Frequency",        "/tmp/nv/wm_wcdma_ulfqcy_value_tmp" ,   ""},
	{ "RDataRate",           "/tmp/nv/wm_wcdma_volume_value_tmp" ,   ""},	
	{ "RPackets",            "/tmp/nv/wm_wcdma_volume_value_tmp" ,   ""},
	{ "RRxBytes",            "/tmp/nv/wm_wcdma_volume_value_tmp" ,   ""},
	{ "TDataRate",           "/tmp/nv/wm_wcdma_volume_value_tmp" ,   ""},
	{ "TPackets",            "/tmp/nv/wm_wcdma_volume_value_tmp" ,   ""},
	{ "TRxBytes",            "/tmp/nv/wm_wcdma_volume_value_tmp" ,   ""},	
	{ "LTE_PDN_Status",       NULL ,                         ""},
	{ NULL,                   NULL ,                         ""},
};

int getLTEInfoByName(const char *node, char *strInfo)
{
	char *path  = NULL;
	FILE *fp;
	int idx;
	char tmp[11][64];
	
	strcpy(strInfo, "");
	
	for (idx=0; get_info_file_list[idx].node != NULL; idx++)
	{
		if (!strcmp(node, get_info_file_list[idx].node))
		{
			if(get_info_file_list[idx].path == NULL)
			{
				strcpy(strInfo, get_info_file_list[idx].value);
				return;
			}
			else
			{
				path = get_info_file_list[idx].path;
				break;
			}
		}
	}
	if(path != NULL)
	{
		if((fp = fopen(path, "r")) != NULL)
		{
			if(!strcmp(node, "RDataRate") || !strcmp(node, "RPackets") || !strcmp(node, "RRxBytes") ||
				 !strcmp(node, "TDataRate") || !strcmp(node, "TPackets") || !strcmp(node, "TRxBytes") ||
				 !strcmp(node, "ConnectTime"))
			{
				memset ((void *)(tmp), 0, sizeof(tmp));
				
				fscanf(fp, "%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,]", tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], tmp[7], tmp[8], tmp[9], tmp[10]);
				
				if(!strcmp(node, "RRxBytes"))
				{
					strcpy(strInfo, tmp[0]);
				}
				else if(!strcmp(node, "RPackets"))
				{
					strcpy(strInfo, tmp[1]);
				}
				else if(!strcmp(node, "RDataRate"))
				{
					strcpy(strInfo, tmp[2]);
				}
				else if(!strcmp(node, "TRxBytes"))
				{
					strcpy(strInfo, tmp[4]);
				}
				else if(!strcmp(node, "TPackets"))
				{
					strcpy(strInfo, tmp[5]);
				}
				else if(!strcmp(node, "TDataRate"))
				{
					strcpy(strInfo, tmp[6]);
				}
				else if(!strcmp(node, "ConnectTime"))
				{
					strcpy(strInfo, tmp[8]);
				}
			}
			else
			{
				fscanf(fp, "%s", strInfo);
			}
			fclose(fp);	
		}
	}
	
	return 0;
}

struct CWMP_OP tPeakDownloadLeafOP = { getPeakDownload,	setPeakDownload };

struct CWMP_PRMT tPeakDownloadLeafInfo[] =
{
{"Max_Data_Rate_Downlink",		eCWMP_tSTRING,	CWMP_READ,	&tPeakDownloadLeafOP},
{"Max_Data_Rate_Uplink",		eCWMP_tSTRING,	CWMP_READ,	&tPeakDownloadLeafOP},
{"Max_Data_Rate_Reset",		eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,	&tPeakDownloadLeafOP},
{"Max_Data_Rate_Period",		eCWMP_tUINT,	CWMP_READ|CWMP_WRITE,	&tPeakDownloadLeafOP},
{"Current_Data_Rate_Downlink",		eCWMP_tSTRING,	CWMP_READ,	&tPeakDownloadLeafOP},
{"Current_Data_Rate_Uplink",		eCWMP_tSTRING,	CWMP_READ,	&tPeakDownloadLeafOP},
};

enum ePeakDownloadLeaf
{
	eMax_Data_Rate_Downlink,
	eMax_Data_Rate_Uplink,
	eMax_Data_Rate_Reset,
	eMax_Data_Rate_Period,
	eCurrent_Data_Rate_Downlink,
	eCurrent_Data_Rate_Uplink,
};
struct CWMP_LEAF tPeakDownloadLeaf[] =
{
{ &tPeakDownloadLeafInfo[eMax_Data_Rate_Downlink] },
{ &tPeakDownloadLeafInfo[eMax_Data_Rate_Uplink] },
{ &tPeakDownloadLeafInfo[eMax_Data_Rate_Reset] },
{ &tPeakDownloadLeafInfo[eMax_Data_Rate_Period] },
{ &tPeakDownloadLeafInfo[eCurrent_Data_Rate_Downlink] },
{ &tPeakDownloadLeafInfo[eCurrent_Data_Rate_Uplink] },
{ NULL }
};

struct CWMP_PRMT tLTESatusObjectInfo[] =
{
{"System",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
{"LTE_RX",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
{"LTE_TX",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
{"PDN",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
};

enum eLTEStatusObject
{
	eLTE_StatusSystem,
	eLTE_StatusLTE_RX,
	eLTE_StatusLTE_TX,
	eLTE_StatusPDN
};

enum eLTE_StatusSystemLeaf
{
	eFrequency_Range,
	eICCID,
	eIMEI,
	eIMSI,
	eService_Provider,
	eBandwidth,
	eCINR0,
	eCINR1,
	eCellID,
	eConnectTime,
	eDL_Frequency,
	eNetwork_Operator,
	ePCI,
	eRSRP0,
	eRSRP1,
	eRSRQ,
	eSINR0,
	eSINR1,
	eState,
	eTXPower,
	eTechnology,
	eUL_Frequency
};

enum etLTE_StatusLTE_RXLeaf
{
	eDataRate,
	ePackets,
	RxBytes
};

enum etLTE_StatusPDNLeaf
{
	ePDN,
};

struct CWMP_OP tLTE_StatusSystemLeafOP = { getLTE_StatusSystem, NULL };
struct CWMP_OP tLTE_StatusRXLeafOP = { getLTE_StatusRX, NULL };
struct CWMP_OP tLTE_StatusTXLeafOP = { getLTE_StatusTX, NULL };
struct CWMP_OP tLTE_StatusPDNLeafOP = { getLTE_StatusPDN, NULL };

struct CWMP_PRMT tLTE_StatusSystemInfo[] =
{
{"Frequency_Range",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"ICCID",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"IMEI",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"IMSI",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"Service_Provider",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"Bandwidth",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"CINR0",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"CINR1",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"CellID",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"ConnectTime",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"DL_Frequency",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"Network_Operator",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"PCI",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"RSRP0",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"RSRP1",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"RSRQ",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"SINR0",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"SINR1",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"State",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"TXPower",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"Technology",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
{"UL_Frequency",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusSystemLeafOP},
};

struct CWMP_LEAF tLTE_StatusSystemLeaf[] =
{
{ &tLTE_StatusSystemInfo[eFrequency_Range] },
{ &tLTE_StatusSystemInfo[eICCID] },
{ &tLTE_StatusSystemInfo[eIMEI] },
{ &tLTE_StatusSystemInfo[eIMSI] },
{ &tLTE_StatusSystemInfo[eService_Provider] },
{ &tLTE_StatusSystemInfo[eBandwidth] },
{ &tLTE_StatusSystemInfo[eCINR0] },
{ &tLTE_StatusSystemInfo[eCINR1] },
{ &tLTE_StatusSystemInfo[eCellID] },
{ &tLTE_StatusSystemInfo[eConnectTime] },
{ &tLTE_StatusSystemInfo[eDL_Frequency] },
{ &tLTE_StatusSystemInfo[eNetwork_Operator] },
{ &tLTE_StatusSystemInfo[ePCI] },
{ &tLTE_StatusSystemInfo[eRSRP0] },
{ &tLTE_StatusSystemInfo[eRSRP1] },
{ &tLTE_StatusSystemInfo[eRSRQ] },
{ &tLTE_StatusSystemInfo[eSINR0] },
{ &tLTE_StatusSystemInfo[eSINR1] },
{ &tLTE_StatusSystemInfo[eState] },
{ &tLTE_StatusSystemInfo[eTXPower] },
{ &tLTE_StatusSystemInfo[eTechnology] },
{ &tLTE_StatusSystemInfo[eUL_Frequency] },
{ NULL }
};

struct CWMP_PRMT tLTE_StatusLTE_RXLeafInfo[] =
{
{"DataRate",	eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusRXLeafOP},
{"Packets",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusRXLeafOP},
{"RxBytes",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusRXLeafOP},
};


struct CWMP_LEAF tLTE_StatusLTE_RXLeaf[] =
{
{ &tLTE_StatusLTE_RXLeafInfo[eDataRate] },
{ &tLTE_StatusLTE_RXLeafInfo[ePackets] },
{ &tLTE_StatusLTE_RXLeafInfo[RxBytes] },
{ NULL }
};

struct CWMP_PRMT tLTE_StatusLTE_TXLeafInfo[] =
{
{"DataRate",	eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusTXLeafOP},
{"Packets",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusTXLeafOP},
{"TxBytes",		eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusTXLeafOP},
};


struct CWMP_LEAF tLTE_StatusLTE_TXLeaf[] =
{
{ &tLTE_StatusLTE_TXLeafInfo[eDataRate] },
{ &tLTE_StatusLTE_TXLeafInfo[ePackets] },
{ &tLTE_StatusLTE_TXLeafInfo[RxBytes] },
{ NULL }
};

struct CWMP_PRMT tLTE_StatusLTE_PDNLeafInfo[] =
{
{"LTE_PDN_Status",	eCWMP_tSTRING,	CWMP_READ,	&tLTE_StatusPDNLeafOP},
};


struct CWMP_LEAF tLTE_StatusLTE_PDNLeaf[] =
{
{ &tLTE_StatusLTE_PDNLeafInfo[ePDN] },
{ NULL }
};

struct CWMP_NODE tLTESystemObject[] =
{
{&tLTESatusObjectInfo[eLTE_StatusSystem],	tLTE_StatusSystemLeaf,	NULL},
{&tLTESatusObjectInfo[eLTE_StatusLTE_RX],	tLTE_StatusLTE_RXLeaf,	NULL},
{&tLTESatusObjectInfo[eLTE_StatusLTE_TX],	tLTE_StatusLTE_TXLeaf,	NULL},
{&tLTESatusObjectInfo[eLTE_StatusPDN],	tLTE_StatusLTE_PDNLeaf,	NULL},

{NULL,							NULL,				NULL}
};

int getLTE_StatusSystem(char *name, struct CWMP_LEAF *entity, int *type, void **data)
{
	char	*lastname = entity->info->name;
	char LTE_info[128];
	int bfound = 0;

	if( (name==NULL) || (type==NULL) || (data==NULL) || (entity==NULL))
		return -1;

	*type = entity->info->type;
	*data = NULL;
	if( strcmp( lastname, "Frequency_Range" )==0 )
	{
		bfound = 1;
	}else if( strcmp( lastname, "ICCID" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "IMEI" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "IMSI" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "Service_Provider" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "Bandwidth" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "CINR0" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "CINR1" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "CellID" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "ConnectTime" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "DL_Frequency" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "Network_Operator" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "PCI" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "RSRP0" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "RSRP1" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "RSRQ" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "SINR0" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "SINR1" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "State" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "TXPower" )==0 )
	{
		bfound = 1;
	}	
	else if( strcmp( lastname, "Technology" )==0 )
	{
		bfound = 1;
	}
	else if( strcmp( lastname, "UL_Frequency" )==0 )
	{
		bfound = 1;
	}	
	else{
		return ERR_9005;
	}
	
	if(bfound)
	{
		getLTEInfoByName(lastname, LTE_info);
		*data = strdup(LTE_info);
	}

	return 0;
}

int getLTE_StatusRX(char *name, struct CWMP_LEAF *entity, int *type, void **data)
{
	char	*lastname = entity->info->name;
	char LTE_info[128];
	
	if( (name==NULL) || (type==NULL) || (data==NULL) || (entity==NULL))
		return -1;

	*type = entity->info->type;
	*data = NULL;
	if( strcmp( lastname, "DataRate" )==0 )
	{
		 getLTEInfoByName("RDataRate", LTE_info);
		 *data=strdup(LTE_info);
	}else if( strcmp( lastname, "Packets" )==0 )
	{
		 getLTEInfoByName("RPackets", LTE_info);
		 *data=strdup(LTE_info);
	}else if( strcmp( lastname, "RxBytes" )==0 )
	{
		 getLTEInfoByName("RRxBytes", LTE_info);
		 *data=strdup(LTE_info);
	}else{
		return ERR_9005;
	}
	
	return 0;
}

int getLTE_StatusTX(char *name, struct CWMP_LEAF *entity, int *type, void **data)
{
	char	*lastname = entity->info->name;
	char LTE_info[128];
	
	if( (name==NULL) || (type==NULL) || (data==NULL) || (entity==NULL))
		return -1;

	*type = entity->info->type;
	*data = NULL;
	if( strcmp( lastname, "DataRate" )==0 )
	{
		getLTEInfoByName("TDataRate", LTE_info);
	  *data=strdup(LTE_info);
	}else if( strcmp( lastname, "Packets" )==0 )
	{
		getLTEInfoByName("TPackets", LTE_info);
	  *data=strdup(LTE_info);
	}else if( strcmp( lastname, "TxBytes" )==0 )
	{
		getLTEInfoByName("TRxBytes", LTE_info);
	  *data=strdup(LTE_info);
	}else{
		return ERR_9005;
	}

	return 0;
}

int getLTE_StatusPDN(char *name, struct CWMP_LEAF *entity, int *type, void **data)
{
	char	*lastname = entity->info->name;
	char LTE_info[128];
	
	if( (name==NULL) || (type==NULL) || (data==NULL) || (entity==NULL))
		return -1;

	*type = entity->info->type;
	*data = NULL;
	if( strcmp( lastname, "LTE_PDN_Status" )==0 )
	{
		getLTEInfoByName(lastname, LTE_info);
	  *data=strdup(LTE_info);
	}else{
		return ERR_9005;
	}

	return 0;
}

enum eLTEObject
{
	eLTE_Default_PDN,
	eLTE_Cell_Selection,
	eLTE_Multi_PDN,
	eLTE_PIN,
};

struct CWMP_PRMT tLTEObjectInfo[] =
{
{"Default_PDN",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
{"LTE_Cell_Selection",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
{"Multi_PDN",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
{"PIN",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
};

struct CWMP_PRMT DefaultPDNObjectInfo[] =
{
{"Authentication_Type",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
{"Network_Attach",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
};

enum eLTEDefaultPDNObject
{
	eAuthentication_Type,
	eNetwork_Attach,
};

enum eAuthentication_TypeLeaf
{
	ePassword,
	eType,
	eUsername
};

enum eNetwork_AttachLeaf
{
	eAPN_Name,
	eAttach,
};

struct CWMP_OP tAuthentication_TypeLeafOP = { getAuthentication_Type, setAuthentication_Type };

struct CWMP_PRMT tLTEAuthentication_TypeLeafInfo[] =
{
{"Password",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tAuthentication_TypeLeafOP},
{"Type",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tAuthentication_TypeLeafOP},
{"Username",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tAuthentication_TypeLeafOP},
};

struct CWMP_OP tNetwork_AttachLeafOP = { getNetwork_Attach, setNetwork_Attach };
struct CWMP_PRMT tLTENetwork_AttachLeafInfo[] =
{
{"APN_Name",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tNetwork_AttachLeafOP},
{"Attach",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tNetwork_AttachLeafOP},
};

struct CWMP_LEAF tLTEAuthentication_TypeLeaf[] =
{
{ &tLTEAuthentication_TypeLeafInfo[ePassword] },
{ &tLTEAuthentication_TypeLeafInfo[eType] },
{ &tLTEAuthentication_TypeLeafInfo[eUsername] },
{ NULL }
};

struct CWMP_LEAF tLTENetwork_AttachLeafLeaf[] =
{
{ &tLTENetwork_AttachLeafInfo[eAPN_Name] },
{ &tLTENetwork_AttachLeafInfo[eAttach] },
{ NULL }
};

struct CWMP_NODE tLTEDefaultPDNObject[] =
{
{&DefaultPDNObjectInfo[eAuthentication_Type],	tLTEAuthentication_TypeLeaf,	NULL},
{&DefaultPDNObjectInfo[eNetwork_Attach],	tLTENetwork_AttachLeafLeaf,	NULL},
{NULL,							NULL,				NULL}
};

enum eCell_SelectionObject
{
	eCell_Selection,
};

struct CWMP_PRMT tLTE_Cell_SelectionobjectInfo[] =
{
{"LTE_Cell_Selection",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
};

enum eLTE_Cell_SelectionLeaf
{
	eBand,
	eEarfcn_Frequency_Setting,
	eMode,
	eScan_Mode,
	eSelectionType,
};

struct CWMP_OP tLTE_Cell_SelectionLeafOP = { getLTE_Cell_Selection, setLTE_Cell_Selection };
struct CWMP_PRMT tLTE_Cell_SelectionLeafInfo[] =
{
{"Band",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tLTE_Cell_SelectionLeafOP},
{"Earfcn_Frequency_Setting",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tLTE_Cell_SelectionLeafOP},
{"Mode",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tLTE_Cell_SelectionLeafOP},
{"Scan_Mode",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tLTE_Cell_SelectionLeafOP},
{"Type",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tLTE_Cell_SelectionLeafOP},
};

struct CWMP_LEAF tLTE_Cell_SelectionLeaf[] =
{
{ &tLTE_Cell_SelectionLeafInfo[eBand] },
{ &tLTE_Cell_SelectionLeafInfo[eEarfcn_Frequency_Setting] },
{ &tLTE_Cell_SelectionLeafInfo[eMode] },
{ &tLTE_Cell_SelectionLeafInfo[eScan_Mode] },
{ &tLTE_Cell_SelectionLeafInfo[eSelectionType] },
{ NULL }
};

struct CWMP_NODE tLTE_Cell_SelectionObject[] =
{
{&tLTE_Cell_SelectionobjectInfo[eCell_Selection],	tLTE_Cell_SelectionLeaf,	NULL},
{NULL,							NULL,				NULL}
};


enum eMulti_PDNObject
{
	eMulti_PDN,
};

struct CWMP_PRMT tLTEMulti_PDNobjectInfo[] =
{
{"Multi_PDN",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
};

enum eMulti_PDNLeaf
{
	eAdd_Example,
	eAdd_New_Cid,
	eCid_2,
	eCid_3,
	eCid_4,
	eCid_5,
	eCid_6,
	eCid_7,
	eCid_8
};

struct CWMP_OP LTE_Multi_PDNLeafOP = { getLTE_Multi_PDN, setLTE_Multi_PDN };
struct CWMP_PRMT tLTE_Multi_PDNLeafInfo[] =
{
{"Add_Example",	eCWMP_tSTRING,	CWMP_READ,		&LTE_Multi_PDNLeafOP},
{"Add_New_Cid",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&LTE_Multi_PDNLeafOP},
{"Cid_2",	eCWMP_tSTRING,	CWMP_READ,		&LTE_Multi_PDNLeafOP},
{"Cid_3",	eCWMP_tSTRING,	CWMP_READ,		&LTE_Multi_PDNLeafOP},
{"Cid_4",	eCWMP_tSTRING,	CWMP_READ,		&LTE_Multi_PDNLeafOP},
{"Cid_5",	eCWMP_tSTRING,	CWMP_READ,		&LTE_Multi_PDNLeafOP},
{"Cid_6",	eCWMP_tSTRING,	CWMP_READ,		&LTE_Multi_PDNLeafOP},
{"Cid_7",	eCWMP_tSTRING,	CWMP_READ,		&LTE_Multi_PDNLeafOP},
{"Cid_8",	eCWMP_tSTRING,	CWMP_READ,		&LTE_Multi_PDNLeafOP},
};

struct CWMP_LEAF tLTE_Multi_PDNLeaf[] =
{
{ &tLTE_Multi_PDNLeafInfo[eAdd_Example] },
{ &tLTE_Multi_PDNLeafInfo[eAdd_New_Cid] },
{ &tLTE_Multi_PDNLeafInfo[eCid_2] },
{ &tLTE_Multi_PDNLeafInfo[eCid_3] },
{ &tLTE_Multi_PDNLeafInfo[eCid_4] },
{ &tLTE_Multi_PDNLeafInfo[eCid_5] },
{ &tLTE_Multi_PDNLeafInfo[eCid_6] },
{ &tLTE_Multi_PDNLeafInfo[eCid_7] },
{ &tLTE_Multi_PDNLeafInfo[eCid_8] },
{ NULL }
};

struct CWMP_NODE tLTEMulti_PDNObject[] =
{
{&tLTEMulti_PDNobjectInfo[eMulti_PDN],	tLTE_Multi_PDNLeaf,	NULL},
{NULL,							NULL,				NULL}
};

struct CWMP_PRMT tLTEPINObjectInfo[] =
{
{"LTE_Change_PIN",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
{"LTE_Enable_PIN",	eCWMP_tOBJECT,	CWMP_READ,		NULL},
};

enum eLTEPINObject
{
	eLTE_Change_PIN,
	eLTE_Enable_PIN,
};

enum eLTE_Change_PINLeaf
{
	eEnable_Change_PIN_Code,
	eNew_PIN_Code,
	eOld_PIN_Code,
	eRemaining_Attempts
};

enum eLTE_Enable_PINLeaf
{
	eEnable_PIN,
	ePIN_Code,
};

struct CWMP_OP tLTE_Change_PINLeafOP = { getLTE_Change_PIN, setLTE_Change_PIN };

struct CWMP_PRMT tLTE_Change_PINLeafInfo[] =
{
{"Enable_Change_PIN_Code",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tLTE_Change_PINLeafOP},
{"New_PIN_Code",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tLTE_Change_PINLeafOP},
{"Old_PIN_Code",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tLTE_Change_PINLeafOP},
{"Remaining_Attempts",	eCWMP_tINT,	CWMP_READ,		&tLTE_Change_PINLeafOP},
};

struct CWMP_OP tLTE_Enable_PINLeafOP = { getLTE_Enable_PIN, setLTE_Enable_PIN };
struct CWMP_PRMT tLTE_Enable_PINLeafInfo[] =
{
{"Enable_PIN",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tLTE_Enable_PINLeafOP},
{"PIN_Code",	eCWMP_tSTRING,	CWMP_READ|CWMP_WRITE,		&tLTE_Enable_PINLeafOP},
};

struct CWMP_LEAF tLTE_Change_PINLeaf[] =
{
{ &tLTE_Change_PINLeafInfo[eEnable_Change_PIN_Code] },
{ &tLTE_Change_PINLeafInfo[eNew_PIN_Code] },
{ &tLTE_Change_PINLeafInfo[eOld_PIN_Code] },
{ &tLTE_Change_PINLeafInfo[eRemaining_Attempts] },
{ NULL }
};

struct CWMP_LEAF tLTE_Enable_PINLeaf[] =
{
{ &tLTE_Enable_PINLeafInfo[eEnable_PIN] },
{ &tLTE_Enable_PINLeafInfo[ePIN_Code] },
{ NULL }
};

struct CWMP_NODE tLTEPINObject[] =
{
{&tLTEPINObjectInfo[eLTE_Change_PIN],	tLTE_Change_PINLeaf,	NULL},
{&tLTEPINObjectInfo[eLTE_Enable_PIN],	tLTE_Enable_PINLeaf,	NULL},
{NULL,							NULL,				NULL}
};

struct CWMP_NODE tLTEObject[] =
{
{&tLTEObjectInfo[eLTE_Default_PDN],	NULL,	tLTEDefaultPDNObject},
{&tLTEObjectInfo[eLTE_Cell_Selection],	NULL,	tLTE_Cell_SelectionObject},
{&tLTEObjectInfo[eLTE_Multi_PDN],	NULL,	tLTEMulti_PDNObject},
{&tLTEObjectInfo[eLTE_PIN],	NULL,	tLTEPINObject},
{NULL,							NULL,				NULL}
};

int getAuthentication_Type(char *name, struct CWMP_LEAF *entity, int *type, void **data)
{
	char	*lastname = entity->info->name;
	FILE *fp;
	char tmp[6][32];
	char *auth_type[] = {"None", "PAP", "CHAP", "AUTO", "Deny"};
	int i;
	
	if( (name==NULL) || (type==NULL) || (data==NULL) || (entity==NULL))
		return -1;

	memset ((void *)(tmp), 0, sizeof(tmp));
	if((fp = fopen("/tmp/nv/wm_wcdma_connection_active_tmp", "r")) != NULL)
	{
		fscanf(fp, "%[^','],%[^','],%[^','],%[^','],%[^','],%s", tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5]);
		fclose(fp);
	}
  
	*type = entity->info->type;
	*data = NULL;
	if( strcmp( lastname, "Password" )==0 )
	{
		 *data=strdup(tmp[3]);
	}
	else if( strcmp( lastname, "Type" )==0 )
	{
		i = atoi(tmp[4]);
		
		if(i < 0 || i > 4)
		{
			*data=strdup("");
		}
		else
		{
			*data=strdup(auth_type[i]);
		}
	}
	else if( strcmp( lastname, "Username" )==0 )
	{
		 *data=strdup(tmp[2]);
	}
	else{
		return ERR_9005;
	}
	return 0;
}

int setAuthentication_Type(char *name, struct CWMP_LEAF *entity, int type, void *data)
{
	char	*lastname = entity->info->name;
	char	*buf=data;
	unsigned int *vUInt=data;

	if( (name==NULL) || (data==NULL) || (entity==NULL)) return -1;
	if( entity->info->type!=type ) return ERR_9006;

	if( strcmp( lastname, "Password" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else if( strcmp( lastname, "Type" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else if( strcmp( lastname, "Username" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else{
		return ERR_9005;
	}
	return 0;
}

int getNetwork_Attach(char *name, struct CWMP_LEAF *entity, int *type, void **data)
{
	char	*lastname = entity->info->name;
	MIB_WAN_LTE_T Entry_LTE;
	FILE *fp;
	char tmp[6][64];
	char *attach[] = {"", "IPV4", "IPV6", "IPV4V6"};
	int i;

	if( (name==NULL) || (type==NULL) || (data==NULL) || (entity==NULL))
		return -1;
	
	memset ((void *)(tmp), 0, sizeof(tmp));
	if((fp = fopen("/tmp/nv/wm_wcdma_connection_active_tmp", "r")) != NULL)
	{	
		fscanf(fp, "%[^','],%[^','],%[^','],%[^','],%[^','],%s", tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5]);
		fclose(fp);
	}

	*type = entity->info->type;
	*data = NULL;
	if( strcmp( lastname, "APN_Name" )==0 )
	{
#if 0
		if(!mib_chain_get( MIB_WAN_LTE_TBL, 0, (void*)&Entry_LTE))
		{
			*data=strdup("");
		}
		else
		{
			*data=strdup(Entry_LTE.apn_list);
		}
#endif
		*data=strdup(tmp[1]);

	}
	else if( strcmp( lastname, "Attach" )==0 )
	{
		i = atoi(tmp[5]);
		
		if(i < 0 || i > 3)
		{
			*data=strdup("");
		}
		else
		{
			*data=strdup(attach[i]);
		}
	}
	else{
		return ERR_9005;
	}
	return 0;
}

int setNetwork_Attach(char *name, struct CWMP_LEAF *entity, int type, void *data)
{
	char	*lastname = entity->info->name;
	char	*buf=data;
	unsigned int *vUInt=data;

	if( (name==NULL) || (data==NULL) || (entity==NULL)) return -1;
	if( entity->info->type!=type ) return ERR_9006;

	if( strcmp( lastname, "APN_Name" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else if( strcmp( lastname, "Attach" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else{
		return ERR_9005;
	}
	return 0;
}

int getLTE_Cell_Selection(char *name, struct CWMP_LEAF *entity, int *type, void **data)
{
	char	*lastname = entity->info->name;

	if( (name==NULL) || (type==NULL) || (data==NULL) || (entity==NULL))
		return -1;

	*type = entity->info->type;
	*data = NULL;
	if( strcmp( lastname, "Band" )==0 )
	{
		 *data=strdup("42,43");
	}
	else if( strcmp( lastname, "Earfcn_Frequency_Setting" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "Mode" )==0 )
	{
		 *data=strdup("TDD");
	}
	else if( strcmp( lastname, "Scan_Mode" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "Type" )==0 )
	{
		 *data=strdup("");
	}
	else{
		return ERR_9005;
	}
	return 0;
}

int setLTE_Cell_Selection(char *name, struct CWMP_LEAF *entity, int type, void *data)
{
	char	*lastname = entity->info->name;
	char	*buf=data;
	unsigned int *vUInt=data;

	if( (name==NULL) || (data==NULL) || (entity==NULL)) return -1;
	if( entity->info->type!=type ) return ERR_9006;

	if( strcmp( lastname, "Band" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else if( strcmp( lastname, "Earfcn_Frequency_Setting" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else if( strcmp( lastname, "Mode" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else if( strcmp( lastname, "Scan_Mode" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else if( strcmp( lastname, "Type" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else{
		return ERR_9005;
	}
	return 0;
}

int getLTE_Multi_PDN(char *name, struct CWMP_LEAF *entity, int *type, void **data)
{
	char	*lastname = entity->info->name;

	if( (name==NULL) || (type==NULL) || (data==NULL) || (entity==NULL))
		return -1;

	*type = entity->info->type;
	*data = NULL;
	if( strcmp( lastname, "Add_Example" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "Add_New_Cid" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "Cid_2" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "Cid_3" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "Cid_4" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "Cid_5" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "Cid_6" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "Cid_7" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "Cid_8" )==0 )
	{
		 *data=strdup("");
	}
	else{
		return ERR_9005;
	}
	return 0;
}

int setLTE_Multi_PDN(char *name, struct CWMP_LEAF *entity, int type, void *data)
{
	char	*lastname = entity->info->name;
	char	*buf=data;
	unsigned int *vUInt=data;

	if( (name==NULL) || (data==NULL) || (entity==NULL)) return -1;
	if( entity->info->type!=type ) return ERR_9006;

	if( strcmp( lastname, "Add_New_Cid" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else{
		return ERR_9005;
	}
	return 0;
}

int getLTE_Change_PIN(char *name, struct CWMP_LEAF *entity, int *type, void **data)
{
	char	*lastname = entity->info->name;

	if( (name==NULL) || (type==NULL) || (data==NULL) || (entity==NULL))
		return -1;

	*type = entity->info->type;
	*data = NULL;
	if( strcmp( lastname, "Enable_Change_PIN_Code" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "New_PIN_Code" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "Old_PIN_Code" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "Remaining_Attempts" )==0 )
	{
		 *data= uintdup(3);
	}
	else{
		return ERR_9005;
	}
	return 0;
}

int setLTE_Change_PIN(char *name, struct CWMP_LEAF *entity, int type, void *data)
{
	char	*lastname = entity->info->name;
	char	*buf=data;
	unsigned int *vUInt=data;

	if( (name==NULL) || (data==NULL) || (entity==NULL)) return -1;
	if( entity->info->type!=type ) return ERR_9006;

	if( strcmp( lastname, "Enable_Change_PIN_Code" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else if( strcmp( lastname, "New_PIN_Code" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else if( strcmp( lastname, "Old_PIN_Code" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else{
		return ERR_9005;
	}
	return 0;
}

int getLTE_Enable_PIN(char *name, struct CWMP_LEAF *entity, int *type, void **data)
{
	char	*lastname = entity->info->name;

	if( (name==NULL) || (type==NULL) || (data==NULL) || (entity==NULL))
		return -1;

	*type = entity->info->type;
	*data = NULL;
	if( strcmp( lastname, "Enable_PIN" )==0 )
	{
		 *data=strdup("");
	}
	else if( strcmp( lastname, "PIN_Code" )==0 )
	{
		 *data=strdup("");
	}
	else{
		return ERR_9005;
	}
	return 0;
}

int setLTE_Enable_PIN(char *name, struct CWMP_LEAF *entity, int type, void *data)
{
	char	*lastname = entity->info->name;
	char	*buf=data;
	unsigned int *vUInt=data;

	if( (name==NULL) || (data==NULL) || (entity==NULL)) return -1;
	if( entity->info->type!=type ) return ERR_9006;

	if( strcmp( lastname, "Enable_PIN" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else if( strcmp( lastname, "PIN_Code" )==0 )
	{
		if(1)
		{
			return 0;
		}
		return ERR_9007;
	}
	else{
		return ERR_9005;
	}
	return 0;
}

int getPeakDownload(char *name, struct CWMP_LEAF *entity, int *type, void **data)
{
	char	*lastname = entity->info->name;
	FILE *fp;
	char tmp[11][64];
	MIB_WAN_LTE_T Entry_LTE;
	
	if( (name==NULL) || (type==NULL) || (data==NULL) || (entity==NULL))
		return -1;

	*type = entity->info->type;
	*data = NULL;
	
	if( strcmp( lastname, "Max_Data_Rate_Reset" )==0 )
	{
		 *data=booldup("");
	}
	else if( strcmp( lastname, "Max_Data_Rate_Period" )==0 )
	{
		if(!mib_chain_get( MIB_WAN_LTE_TBL, 0, (void*)&Entry_LTE))
		{
			*data=strdup("");
		}
		else
		{
			*data=uintdup(Entry_LTE.peak_time);
		}
	}
	else if( strcmp( lastname, "Max_Data_Rate_Downlink" )==0  ||strcmp( lastname, "Max_Data_Rate_Uplink" )==0 ||
		 strcmp( lastname, "Current_Data_Rate_Downlink" )==0  || strcmp( lastname, "Current_Data_Rate_Uplink" )==0)
	{
		 *data=strdup("");
	
	  if((fp = fopen("/tmp/nv/wm_wcdma_volume_value_tmp_bak", "r")) != NULL)
		{
			memset ((void *)(tmp), 0, sizeof(tmp));
			
			fscanf(fp, "%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,],%[^,]", tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5], tmp[6], tmp[7], tmp[8], tmp[9], tmp[10]);
			
			if(!strcmp(lastname, "Max_Data_Rate_Downlink"))
			{
				*data=strdup(tmp[9]);
			}
			else if(!strcmp(lastname, "Max_Data_Rate_Uplink"))
			{
				char *p;
				
				if((p = strstr(tmp[10], "\n")) != NULL)
				{
					*p = '\0';
				}
				*data=strdup(tmp[10]);
			}
			else if(!strcmp(lastname, "Current_Data_Rate_Downlink"))
			{
				*data=strdup(tmp[3]);
			}
			else if(!strcmp(lastname, "Current_Data_Rate_Uplink"))
			{
				*data=strdup(tmp[7]);
			}
			fclose(fp);
	  }
	}
	else{
		return ERR_9005;
	}
	return 0;
}

int setPeakDownload(char *name, struct CWMP_LEAF *entity, int type, void *data)
{
	char	*lastname = entity->info->name;
	char	*buf=data;
	unsigned int *vUInt=data;
	MIB_WAN_LTE_T Entry_LTE;

	if( (name==NULL) || (data==NULL) || (entity==NULL)) return -1;
	if( entity->info->type!=type ) return ERR_9006;

	if( strcmp( lastname, "Max_Data_Rate_Reset" )==0 )
	{
		buf = data;

		if(!strcmp(buf, "enable"))
		{
			system("/bin/wanmanage -z");
		}
		
		return 0;
	}
	else if( strcmp( lastname, "Max_Data_Rate_Period" )==0 )
	{
		if(!mib_chain_get( MIB_WAN_LTE_TBL, 0, (void*)&Entry_LTE))
		{
			return ERR_9007;
		}
		else
		{
			Entry_LTE.peak_time = *vUInt;
			if(!mib_chain_update(MIB_WAN_LTE_TBL, &Entry_LTE, 0))
			{
				printf("set Max_Data_Rate_Period error\n");
				return ERR_9007;
			}
		}
		return 0;
	}
	else{
		return ERR_9005;
	}
	return 0;
}
#endif