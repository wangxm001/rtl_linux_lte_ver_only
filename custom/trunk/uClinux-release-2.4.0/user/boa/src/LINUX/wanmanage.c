#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "utility.h"
#include "lte_sdk.h"

#define LTE_INTERFACE "4g0"
#define ETH_INTERFACE "nas0"
static int SendMsgToTricheer(int msg);

void usage(void)
{
   fprintf(stderr, "  -s             set status of this interface, 1 up, 0 down\n");
   fprintf(stderr, "  -t             set tpye of this interface, support MEDIA_ETH:1, MEDIA_4G:8\n");
   fprintf(stderr, "  -p             set ip address of this interface\n");
   fprintf(stderr, "  -g             set gateway of this interface\n");
   fprintf(stderr, "  -m             set netmask of this interface\n");
   fprintf(stderr, "  -a             set dns1 of this interface\n");
   fprintf(stderr, "  -b             set dns2 of this interface\n");
   fprintf(stderr, "  -M             send msg, Eth Wan up or down\n");
   fprintf(stderr, "  -z             reset Max_Data_Rate_Reset\n");
   fprintf(stderr, "  -h             help\n");

   exit(0);
}

typedef struct wan_info
{
   int status;
   int type;
   char wan_ip[IP_LEN_MAX];
   char gateway[IP_LEN_MAX];
   char dns1[IP_LEN_MAX];
   char dns2[IP_LEN_MAX];
   char mask[IP_LEN_MAX];
}Wan_info;

static void modify_LTE_WanEntry(Wan_info *info, int id, MIB_CE_ATM_VC_Tp pEntry)
{
  g_accessMib = 1;
  char ipaddr[32];

  if(info->status)
  {
    inet_aton(info->wan_ip, (struct in_addr *)pEntry->ipAddr);
    inet_aton(info->gateway, (struct in_addr *)pEntry->remoteIpAddr);
    inet_aton(info->mask, (struct in_addr *)pEntry->netMask);
    inet_aton(info->dns1, (struct in_addr *)pEntry->v4dns1);
    inet_aton(info->dns2, (struct in_addr *)pEntry->v4dns2);

    mib_chain_update(MIB_ATM_VC_TBL, pEntry, id);
    restartWAN(CONFIGONE, pEntry);

    printf("4G wan up, ip : %s/%s, gw :%s, dns : %s|%s\n", info->wan_ip, info->mask, info->gateway, info->dns1, info->dns2);
  }
  else
  {
    stopConnection(pEntry);

    strcpy(ipaddr, inet_ntoa(*((struct in_addr *)pEntry->remoteIpAddr)));

    if(strcmp(ipaddr, "0.0.0.0"))
    {
        va_cmd(ROUTE, 4, 1, "del", "default", "gw", ipaddr);
    }

    inet_aton("0.0.0.0", (struct in_addr *)pEntry->ipAddr);
    inet_aton("0.0.0.0", (struct in_addr *)pEntry->remoteIpAddr);
    inet_aton("0.0.0.0", (struct in_addr *)pEntry->netMask);
    inet_aton("0.0.0.0", (struct in_addr *)pEntry->v4dns1);
    inet_aton("0.0.0.0", (struct in_addr *)pEntry->v4dns2);

    mib_chain_update(MIB_ATM_VC_TBL, pEntry, id);
    cmd_set_dns_config(LTE_INTERFACE);

    printf("4G wan down!\n");
  }

  return;
}

static int checkInterfaceRuning(const char *devname)
{
   int  skfd;
   int  ret = FALSE;
   struct ifreq intf;
   if (devname == NULL || (skfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
   {
      return ret;
   }
   strcpy(intf.ifr_name, devname);
   if (ioctl(skfd, SIOCGIFFLAGS, &intf) != -1)
   {
      if ((intf.ifr_flags & IFF_RUNNING) != 0)
         ret = TRUE;
   }
   close(skfd);
   return ret;
}
static int processWanLTE(Wan_info *info, int id, void *ptr)
{
   modify_LTE_WanEntry(info, id, ptr);
}

static int processWanETH(Wan_info *info, int id, void *ptr)
{
  if(info->status)
  {
    if(!checkInterfaceRuning(ETH_INTERFACE))
    {
      SendMsgToTricheer(3);  /*eth is down*/
      printf("Eth wan down! just return\n");
      return;
    }
    restartWAN(CONFIGONE, ptr);
    printf("Eth wan up\n");
  }
  else
  {
    stopConnection(ptr);
    printf("Eth wan down!\n");
  }

}

static void Update_LTE_WanStatus(Wan_info *info)
{
  int i, total;
  MIB_CE_ATM_VC_T Entry;

  total = mib_chain_total(MIB_ATM_VC_TBL);

  for (i = 0; i < total; i++) {
    if (mib_chain_get(MIB_ATM_VC_TBL, i, &Entry) != 1)
      continue;

    if (MEDIA_INDEX(Entry.ifIndex) == info->type)
    {
      if(MEDIA_INDEX(Entry.ifIndex) == MEDIA_4G)
      {
        processWanLTE(info, i, &Entry);
        break;
      }
      else if(MEDIA_INDEX(Entry.ifIndex) == MEDIA_ETH)
      {
        processWanETH(info, i, &Entry);
      }
    }
  }
  return;
}

static int Tricheer_SDK_init()
{
  lct_config_s Tricheer_SDK_config;

  sprintf(Tricheer_SDK_config.name, "Tricheer_sdk");
  Tricheer_SDK_config.msg_func = NULL;
  Tricheer_SDK_config.log_enable = 1;

  if(lct_module_init(&Tricheer_SDK_config) < 0)
  {
      printf("lte sdk init error\n");
      return -1;
  }
  return 0;
}

static int SendMsgToTricheer(int msg)
{
  iface_status_evt_t req;

  if(msg == -1)
  {
    printf("No msg to send...\n");
  }
  else
  {
    if(Tricheer_SDK_init() == 0)
    {
      req.link.iface_id = WM_ETH;

      if(msg > 2)
      {
        req.link.link_status = WM_LINK_STA_CLEAR;
        req.link.iface_status = (msg == 4 ? WM_READY: WM_NOT_READY);
      }
      else
      {
        req.link.iface_status = WM_WARNING_CLEAR;
        req.link.link_status = (msg == 1 ? WM_CONNECTED: WM_DISCONNECTED);
      }

      if(lct_inface_status_req(&req) < 0)
      {
        printf("send interface status %d error\n", msg);
      }
      else
      {
        printf("send interface status %d ok\n",  msg);
      }

      lct_module_fin();
    }

    exit(0);
  }
}

static int resetPeakData()
{
    lct_peak_clear();
    exit(0);
}

int main(int argc, char *argv[])
{
  Wan_info wan;
  int opt_ret = 0;
  int lop;
  int nMsg = -1;

  memset(&wan, 0, sizeof(Wan_info));

  struct option longopts[] =
  {
    {NULL, 0, 0, 0}
  };

  while( opt_ret != -1 )
  {
    opt_ret = getopt_long(argc, argv, "hs:p:g:m:a:b:t:M:z", longopts, &lop);
    switch(opt_ret)
    {
      case 't':
        wan.type = atoi(optarg);
        break;
      case 's':
        wan.status = atoi(optarg);
        break;
      case 'p':
        strncpy(wan.wan_ip, optarg, IP_LEN_MAX -1);
        break;
      case 'g':
        strncpy(wan.gateway, optarg, IP_LEN_MAX -1);
        break;
      case 'm':
        strncpy(wan.mask, optarg, IP_LEN_MAX -1);
        break;
      case 'a':
        strncpy(wan.dns1, optarg, IP_LEN_MAX -1);
        break;
      case 'b':
        strncpy(wan.dns2, optarg, IP_LEN_MAX -1);
        break;
      case 'M':
        nMsg = atoi(optarg);
        break;
      case 'z':
        if(Tricheer_SDK_init() == 0)
        {
            //resetPeakData();
            lct_peak_clear();
            lct_module_fin();
        }
        exit(0);
        break;
      case 'h':
        usage();
    }
  }

  SendMsgToTricheer(nMsg);

  Update_LTE_WanStatus(&wan);

  return 0;
}
