/*
 *      Web server handler routines for get info and index (getinfo(), getindex())
 *      Authors: David Hsu	<davidhsu@realtek.com.tw>
 *      Authors: Dick Tam	<dicktam@realtek.com.tw>
 *
 */

#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/sysinfo.h>
#ifdef EMBED
#include <linux/config.h>
#include <config/autoconf.h>
#else
#include "../../../../include/linux/autoconf.h"
#include "../../../../config/autoconf.h"
#endif

#include "include/lte_sdk.h"
#include "include/wm_log.h"

#include "../webs.h"
#include "mib.h"
#include "adsl_drv.h"
#include "utility.h"
//added by xl_yue
#include "../defs.h"
#include "include/nvmanager.h"
// Added by davian kuo
#include "multilang.h"
#include "multilang_set.h"

// remote config status flag: 0: disabled, 1: enabled
int g_remoteConfig=0;
int g_remoteAccessPort=51003;

// Added by Mason Yu
extern char suName[MAX_NAME_LEN];
extern char usName[MAX_NAME_LEN];
// Mason Yu on True
extern unsigned char g_login_username[MAX_NAME_LEN];

#ifdef WLAN_SUPPORT
void translate_control_code(char *buffer)
{
	char tmpBuf[200], *p1 = buffer, *p2 = tmpBuf;


	while (*p1) {
		if (*p1 == '"') {
			memcpy(p2, "&quot;", 6);
			p2 += 6;
		}
		else if (*p1 == '\x27') {
			memcpy(p2, "&#39;", 5);
			p2 += 5;
		}
		else if (*p1 == '\x5c') {
			memcpy(p2, "&#92;", 5);
			p2 += 5;
		}
		else if (*p1 =='<'){
			memcpy(p2, "&lt;", 4);
			p2 += 4;
		}
		else if (*p1 =='>'){
			memcpy(p2, "&gt;", 4);
			p2 += 4;
		}
		else
			*p2++ = *p1;
		p1++;
	}
	*p2 = '\0';

	strcpy(buffer, tmpBuf);
}
void translate_web_code(char *buffer)
{
	char tmpBuf[200], *p1 = buffer, *p2 = tmpBuf;


	while (*p1) {
		if (*p1 == '"') {
			strcpy(p2, "\\\"");
			p2 += 2;
		}
		else if (*p1 == '\'') {
			strcpy(p2, "\\'");
			p2 += 2;
		}
		else if (*p1 == '\\') {
			strcpy(p2, "\\\\");
			p2 += 2;
		}
		else
			*p2++ = *p1;
		p1++;
	}
	*p2 = '\0';

	strcpy(buffer, tmpBuf);
}

#endif

// Kaohj
typedef enum {
	INFO_MIB,
	INFO_SYS
} INFO_T;

typedef struct {
	char *cmd;
	INFO_T type;
	int id;
} web_get_cmd;

typedef struct {
	char *cmd;
	int (*handler)(int , request * , int , char **, char *);
} web_custome_cmd;

typedef struct
{
	unsigned int            nCode;
	const char              *nVar;
	unsigned char           nType;
} PLMN_LIST;

web_get_cmd get_info_list[] = {
	{"lan-ip", INFO_MIB, MIB_ADSL_LAN_IP},
	{"lan-subnet", INFO_MIB, MIB_ADSL_LAN_SUBNET},
	{"lan-ip2", INFO_MIB, MIB_ADSL_LAN_IP2},
	{"lan-subnet2", INFO_MIB, MIB_ADSL_LAN_SUBNET2},
	// Kaohj
	#ifndef DHCPS_POOL_COMPLETE_IP
	{"lan-dhcpRangeStart", INFO_MIB, MIB_ADSL_LAN_CLIENT_START},
	{"lan-dhcpRangeEnd", INFO_MIB, MIB_ADSL_LAN_CLIENT_END},
	#else
	{"lan-dhcpRangeStart", INFO_MIB, MIB_DHCP_POOL_START},
	{"lan-dhcpRangeEnd", INFO_MIB, MIB_DHCP_POOL_END},
	#endif
	{"lan-dhcpSubnetMask", INFO_MIB, MIB_DHCP_SUBNET_MASK},
	{"dhcps-dns1", INFO_MIB, MIB_DHCPS_DNS1},
	{"dhcps-dns2", INFO_MIB, MIB_DHCPS_DNS2},
	{"dhcps-dns3", INFO_MIB, MIB_DHCPS_DNS3},
	{"lan-dhcpLTime", INFO_MIB, MIB_ADSL_LAN_DHCP_LEASE},
	{"lan-dhcpDName", INFO_MIB, MIB_ADSL_LAN_DHCP_DOMAIN},
	{"elan-Mac", INFO_MIB, MIB_ELAN_MAC_ADDR},
	{"wlan-Mac", INFO_MIB, MIB_WLAN_MAC_ADDR},
	{"wan-dns1", INFO_MIB, MIB_ADSL_WAN_DNS1},
	{"wan-dns2", INFO_MIB, MIB_ADSL_WAN_DNS2},
	{"wan-dns3", INFO_MIB, MIB_ADSL_WAN_DNS3},
#ifdef CONFIG_USER_DHCP_SERVER
	{"wan-dhcps", INFO_MIB, MIB_ADSL_WAN_DHCPS},
#endif
#ifdef DMZ
	{"dmzHost", INFO_MIB, MIB_DMZ_IP},
#endif
#ifdef CONFIG_USER_SNMPD_SNMPD_V2CTRAP
	{"snmpSysDescr", INFO_MIB, MIB_SNMP_SYS_DESCR},
	{"snmpSysContact", INFO_MIB, MIB_SNMP_SYS_CONTACT},
	{"snmpSysLocation", INFO_MIB, MIB_SNMP_SYS_LOCATION},
	{"snmpSysObjectID", INFO_MIB, MIB_SNMP_SYS_OID},
	{"snmpTrapIpAddr", INFO_MIB, MIB_SNMP_TRAP_IP},
	{"snmpCommunityRO", INFO_MIB, MIB_SNMP_COMM_RO},
	{"snmpCommunityRW", INFO_MIB, MIB_SNMP_COMM_RW},
	{"name", INFO_MIB, MIB_SNMP_SYS_NAME},
#endif
	{"snmpSysName", INFO_MIB, MIB_SNMP_SYS_NAME},
	{"name", INFO_MIB, MIB_SNMP_SYS_NAME},
#ifdef TIME_ZONE
	{"ntpTimeZoneDBIndex", INFO_MIB, MIB_NTP_TIMEZONE_DB_INDEX},
/*ping_zhang:20081217 START:patch from telefonica branch to support WT-107*/
//#ifdef _PRMT_WT107_
	{"ntpServerHost1", INFO_MIB, MIB_NTP_SERVER_HOST1},
	{"ntpServerHost2", INFO_MIB, MIB_NTP_SERVER_HOST2},
//#endif
/*ping_zhang:20081217 END*/
#endif
	{"uptime", INFO_SYS, SYS_UPTIME},
	{"date", INFO_SYS, SYS_DATE},
	{"year", INFO_SYS, SYS_YEAR},
	{"month", INFO_SYS, SYS_MONTH},
	{"day", INFO_SYS, SYS_DAY},
	{"hour", INFO_SYS, SYS_HOUR},
	{"minute", INFO_SYS, SYS_MINUTE},
	{"second", INFO_SYS, SYS_SECOND},
	{"fwVersion", INFO_SYS, SYS_FWVERSION},
#ifdef CPU_UTILITY	
	{"cpu_util", INFO_SYS, SYS_CPU_UTIL},
#endif
#ifdef MEM_UTILITY
	{"mem_util", INFO_SYS, SYS_MEM_UTIL},
#endif
	{"dhcplan-ip", INFO_SYS, SYS_DHCP_LAN_IP},
	{"dhcplan-subnet", INFO_SYS, SYS_DHCP_LAN_SUBNET},
	{"dslstate", INFO_SYS, SYS_DSL_OPSTATE},
	{"bridge-ageingTime", INFO_MIB, MIB_BRCTL_AGEINGTIME},
#if defined(CONFIG_RTL_8676HWNAT) || defined(CONFIG_USER_VLAN_ON_LAN)
	{"lan1-vid", INFO_SYS, SYS_LAN1_VID},
	{"lan2-vid", INFO_SYS, SYS_LAN2_VID},
	{"lan3-vid", INFO_SYS, SYS_LAN3_VID},
	{"lan4-vid", INFO_SYS, SYS_LAN4_VID},
	{"lan1-status", INFO_SYS, SYS_LAN1_STATUS},
	{"lan2-status", INFO_SYS, SYS_LAN2_STATUS},
	{"lan3-status", INFO_SYS, SYS_LAN3_STATUS},
	{"lan4-status", INFO_SYS, SYS_LAN4_STATUS},
#endif
#ifdef CONFIG_USER_IGMPPROXY
	{"igmp-proxy-itf", INFO_MIB, MIB_IGMP_PROXY_ITF},
#endif
//#ifdef CONFIG_USER_UPNPD
#if defined(CONFIG_USER_UPNPD)||defined(CONFIG_USER_MINIUPNPD)
	{"upnp-ext-itf", INFO_MIB, MIB_UPNP_EXT_ITF},
#endif
#ifdef TIME_ZONE
	{"ntp-ext-itf", INFO_MIB, MIB_NTP_EXT_ITF},
#endif
#ifdef CONFIG_IPV6
#ifdef CONFIG_USER_ECMH
	{"mldproxy-ext-itf", INFO_MIB, MIB_MLD_PROXY_EXT_ITF}, 		// Mason Yu. MLD Proxy
#endif
#endif
#ifdef IP_PASSTHROUGH
	{"ippt-itf", INFO_MIB, MIB_IPPT_ITF},
	{"ippt-lease", INFO_MIB, MIB_IPPT_LEASE},
	{"ippt-lanacc", INFO_MIB, MIB_IPPT_LANACC},
#endif
#ifdef WLAN_SUPPORT
	{"ssid", INFO_SYS, SYS_WLAN_SSID},
	{"channel", INFO_MIB, MIB_WLAN_CHAN_NUM},
	{"chanwid", INFO_MIB, MIB_WLAN_CHANNEL_WIDTH},
	{"fragThreshold", INFO_MIB, MIB_WLAN_FRAG_THRESHOLD},
	{"rtsThreshold", INFO_MIB, MIB_WLAN_RTS_THRESHOLD},
	{"beaconInterval", INFO_MIB, MIB_WLAN_BEACON_INTERVAL},
	{"wlanDisabled",INFO_SYS,SYS_WLAN_DISABLED},
	{"hidden_ssid",INFO_SYS,SYS_WLAN_HIDDEN_SSID},
	{"pskValue", INFO_SYS, SYS_WLAN_PSKVAL},
#ifdef USER_WEB_WIZARD
	{"pskValue_Wizard", INFO_SYS, SYS_WLAN_PSKVAL_WIZARD},
#endif
	{"WiFiTest", INFO_MIB, MIB_WIFI_TEST},
#ifdef WLAN_1x
	{"rsPort",INFO_SYS,SYS_WLAN_RS_PORT},
	{"rsIp",INFO_SYS,SYS_WLAN_RS_IP},
	{"rsPassword",INFO_SYS,SYS_WLAN_RS_PASSWORD},
	{"enable1X", INFO_SYS,SYS_WLAN_ENABLE_1X},
#endif
	{"wlanMode",INFO_SYS,SYS_WLAN_MODE_VAL},
	{"encrypt",INFO_SYS,SYS_WLAN_ENCRYPT_VAL},
	{"wpa_cipher",INFO_SYS,SYS_WLAN_WPA_CIPHER_SUITE},
	{"wpa2_cipher",INFO_SYS,SYS_WLAN_WPA2_CIPHER_SUITE},
	{"wpaAuth",INFO_SYS,SYS_WLAN_WPA_AUTH},
	{"networkType",INFO_MIB,MIB_WLAN_NETWORK_TYPE},
	{"lockdown_stat",INFO_SYS,SYS_WLAN_WPS_LOCKDOWN},

#ifdef CONFIG_WIFI_SIMPLE_CONFIG // WPS
	{"wscDisable",INFO_SYS,SYS_WSC_DISABLE},
	//{"wscConfig",INFO_MIB,MIB_WSC_CONFIGURED},
	{"wps_auth",INFO_SYS,SYS_WSC_AUTH},
	{"wps_enc",INFO_SYS,SYS_WSC_ENC},
	{"wscLoocalPin", INFO_MIB, MIB_WSC_PIN},
	{"wpsUseVersion", INFO_MIB, MIB_WSC_VERSION},
#endif

#ifdef WLAN_WDS
	{"wlanWdsEnabled",INFO_MIB,MIB_WLAN_WDS_ENABLED},
	{"wdsPskValue",INFO_MIB,MIB_WLAN_WDS_PSK},
#endif
#ifdef WLAN_UNIVERSAL_REPEATER
	{"repeaterEnable", INFO_MIB, MIB_REPEATER_ENABLED1},
	{"repeaterSSID", INFO_MIB, MIB_REPEATER_SSID1},
#endif
#endif // of WLAN_SUPPORT
	//{ "dnsServer", INFO_SYS, SYS_DNS_SERVER},
	{"maxmsglen",INFO_MIB,MIB_MAXLOGLEN},
#ifdef _CWMP_MIB_
	{"acs-url", INFO_MIB, CWMP_ACS_URL},
	{"acs-username", INFO_MIB, CWMP_ACS_USERNAME},
	{"acs-password", INFO_MIB, CWMP_ACS_PASSWORD},
	{"inform-interval", INFO_MIB, CWMP_INFORM_INTERVAL},
	{"conreq-name", INFO_MIB, CWMP_CONREQ_USERNAME},
	{"conreq-pw", INFO_MIB, CWMP_CONREQ_PASSWORD},
	{"cert-pw", INFO_MIB, CWMP_CERT_PASSWORD},
	{"conreq-path", INFO_MIB, CWMP_CONREQ_PATH},
	{"conreq-port", INFO_MIB, CWMP_CONREQ_PORT},
	{"tr069_wan_intf", INFO_MIB, CWMP_WAN_INTERFACE},
#endif
#ifdef DOS_SUPPORT
	{"syssynFlood", INFO_MIB, MIB_DOS_SYSSYN_FLOOD},
	{"sysfinFlood", INFO_MIB, MIB_DOS_SYSFIN_FLOOD},
	{"sysudpFlood", INFO_MIB, MIB_DOS_SYSUDP_FLOOD},
	{"sysicmpFlood", INFO_MIB, MIB_DOS_SYSICMP_FLOOD},
	{"pipsynFlood", INFO_MIB, MIB_DOS_PIPSYN_FLOOD},
	{"pipfinFlood", INFO_MIB, MIB_DOS_PIPFIN_FLOOD},
	{"pipudpFlood", INFO_MIB, MIB_DOS_PIPUDP_FLOOD},
	{"pipicmpFlood", INFO_MIB, MIB_DOS_PIPICMP_FLOOD},
	{"blockTime", INFO_MIB, MIB_DOS_BLOCK_TIME},
#endif
	{"lan-dhcp-gateway", INFO_MIB, MIB_ADSL_LAN_DHCP_GATEWAY},
#ifdef ADDRESS_MAPPING
#ifndef MULTI_ADDRESS_MAPPING
	{"local-s-ip", INFO_MIB, MIB_LOCAL_START_IP},
	{"local-e-ip", INFO_MIB, MIB_LOCAL_END_IP},
	{"global-s-ip", INFO_MIB, MIB_GLOBAL_START_IP},
	{"global-e-ip", INFO_MIB, MIB_GLOBAL_END_IP},
#endif //!MULTI_ADDRESS_MAPPING
#endif
#ifdef CONFIG_USER_RTK_SYSLOG
	{"log-level", INFO_MIB, MIB_SYSLOG_LOG_LEVEL},
	{"display-level", INFO_MIB, MIB_SYSLOG_DISPLAY_LEVEL},
#ifdef CONFIG_USER_RTK_SYSLOG_REMOTE
	{"syslog-mode", INFO_MIB, MIB_SYSLOG_MODE},
	{"syslog-server-ip", INFO_MIB, MIB_SYSLOG_SERVER_IP},
	{"syslog-server-port", INFO_MIB, MIB_SYSLOG_SERVER_PORT},
#endif
#ifdef SEND_LOG
	{"log-server-ip", INFO_MIB, MIB_LOG_SERVER_IP},
	{"log-server-username", INFO_MIB, MIB_LOG_SERVER_NAME},
#endif
#endif
#ifdef TCP_UDP_CONN_LIMIT
	{"connLimit-tcp", INFO_MIB, MIB_CONNLIMIT_TCP},
	{"connLimit-udp", INFO_MIB, MIB_CONNLIMIT_UDP},
#endif
#ifdef WEB_REDIRECT_BY_MAC
	{"landing-page-time", INFO_MIB, MIB_WEB_REDIR_BY_MAC_INTERVAL},
#endif
	{"super-user", INFO_MIB, MIB_SUSER_NAME},
#ifdef USER_WEB_WIZARD
	{"super-pass", INFO_MIB, MIB_SUSER_PASSWORD},
#endif
	{"normal-user", INFO_MIB, MIB_USER_NAME},
#ifdef DEFAULT_GATEWAY_V2
	{"wan-default-gateway", INFO_MIB, MIB_ADSL_WAN_DGW_IP},
	{"itf-default-gateway", INFO_MIB, MIB_ADSL_WAN_DGW_ITF},
#endif
//ql 20090119
#ifdef IMAGENIO_IPTV_SUPPORT
	{"stb-dns1", INFO_MIB, MIB_IMAGENIO_DNS1},
	{"stb-dns2", INFO_MIB, MIB_IMAGENIO_DNS2},
/*ping_zhang:20090930 START:add for Telefonica new option 240*/
#if 0
	{"opch-addr", INFO_MIB, MIB_OPCH_ADDRESS},
	{"opch-port", INFO_MIB, MIB_OPCH_PORT},
#endif
/*ping_zhang:20090930 END*/
#endif

#ifdef CONFIG_IPV6
#ifdef CONFIG_USER_RADVD
	{"V6MaxRtrAdvInterval", INFO_MIB, MIB_V6_MAXRTRADVINTERVAL},
	{"V6MinRtrAdvInterval", INFO_MIB, MIB_V6_MINRTRADVINTERVAL},
	{"V6AdvCurHopLimit", INFO_MIB, MIB_V6_ADVCURHOPLIMIT},
	{"V6AdvDefaultLifetime", INFO_MIB, MIB_V6_ADVDEFAULTLIFETIME},
	{"V6AdvReachableTime", INFO_MIB, MIB_V6_ADVREACHABLETIME},
	{"V6AdvRetransTimer", INFO_MIB, MIB_V6_ADVRETRANSTIMER},
	{"V6AdvLinkMTU", INFO_MIB, MIB_V6_ADVLINKMTU},
	{"V6prefix_ip", INFO_MIB, MIB_V6_PREFIX_IP},
	{"V6prefix_len", INFO_MIB, MIB_V6_PREFIX_LEN},
	{"V6ValidLifetime", INFO_MIB, MIB_V6_VALIDLIFETIME},
	{"V6PreferredLifetime", INFO_MIB, MIB_V6_PREFERREDLIFETIME},
	{"V6RDNSS1", INFO_MIB, MIB_V6_RDNSS1},
	{"V6RDNSS2", INFO_MIB, MIB_V6_RDNSS2},
	{"V6ULAPrefixEnable", INFO_MIB, MIB_V6_ULAPREFIX_ENABLE},
	{"V6ULAPrefix", INFO_MIB, MIB_V6_ULAPREFIX},
	{"V6ULAPrefixlen", INFO_MIB, MIB_V6_ULAPREFIX_LEN},
	{"V6ULAPrefixValidLifetime", INFO_MIB, MIB_V6_ULAPREFIX_VALID_TIME},
	{"V6ULAPrefixPreferredLifetime", INFO_MIB, MIB_V6_ULAPREFIX_PREFER_TIME},
#endif

#ifdef CONFIG_USER_DHCPV6_ISC_DHCP411
	{"dhcpv6s_prefix_length", INFO_MIB, MIB_DHCPV6S_PREFIX_LENGTH},
	{"dhcpv6s_range_start", INFO_MIB, MIB_DHCPV6S_RANGE_START},
	{"dhcpv6s_range_end", INFO_MIB, MIB_DHCPV6S_RANGE_END},
	{"dhcpv6s_default_LTime", INFO_MIB, MIB_DHCPV6S_DEFAULT_LEASE},
	{"dhcpv6s_preferred_LTime", INFO_MIB, MIB_DHCPV6S_PREFERRED_LIFETIME},
	{"dhcpv6_mode", INFO_SYS, SYS_DHCPV6_MODE},
	{"dhcpv6_relay_itf", INFO_SYS, SYS_DHCPV6_RELAY_UPPER_ITF},
	{"dhcpv6s_renew_time", INFO_MIB, MIB_DHCPV6S_RENEW_TIME},
	{"dhcpv6s_rebind_time", INFO_MIB, MIB_DHCPV6S_REBIND_TIME},
	{"dhcpv6s_clientID", INFO_MIB, MIB_DHCPV6S_CLIENT_DUID},
#endif
	{"ip6_ll", INFO_SYS, SYS_LAN_IP6_LL},
	{"ip6_global", INFO_SYS, SYS_LAN_IP6_GLOBAL},
#endif // of CONFIG_IPV6

#ifdef CONFIG_RTL_WAPI_SUPPORT
	{ "wapiUcastReKeyType", INFO_MIB, MIB_WLAN_WAPI_UCAST_REKETTYPE},
	{ "wapiUcastTime", INFO_MIB, MIB_WLAN_WAPI_UCAST_TIME},
	{ "wapiUcastPackets", INFO_MIB, MIB_WLAN_WAPI_UCAST_PACKETS},
	{ "wapiMcastReKeyType", INFO_MIB, MIB_WLAN_WAPI_MCAST_REKEYTYPE},
	{ "wapiMcastTime", INFO_MIB, MIB_WLAN_WAPI_MCAST_TIME},
	{ "wapiMcastPackets", INFO_MIB, MIB_WLAN_WAPI_MCAST_PACKETS},
#endif
#ifdef CONFIG_IPV6
	{"wan-dnsv61", INFO_MIB, MIB_ADSL_WAN_DNSV61},
	{"wan-dnsv62", INFO_MIB, MIB_ADSL_WAN_DNSV62},
	{"wan-dnsv63", INFO_MIB, MIB_ADSL_WAN_DNSV63},
#endif
	{"wan_mode", INFO_MIB, MIB_WAN_MODE},
	{"dhcp_port_filter", INFO_MIB, MIB_DHCP_PORT_FILTER},
#ifdef CONFIG_USER_SAMBA
#ifdef CONFIG_USER_NMBD
	{"samba-netbios-name", INFO_MIB, MIB_SAMBA_NETBIOS_NAME},
#endif
	{"samba-server-string", INFO_MIB, MIB_SAMBA_SERVER_STRING},
#endif
#ifdef CONFIG_RTK_RG_INIT
	{"mac_based_tag_decision", INFO_MIB, MIB_MAC_BASED_TAG_DECISION},
	{"lan_vlan_id1", INFO_MIB, MIB_LAN_VLAN_ID1},
	{"lan_vlan_id2", INFO_MIB, MIB_LAN_VLAN_ID2},
#endif
	{"loid", INFO_MIB, MIB_LOID},
#ifdef CONFIG_GPON_FEATURE
	{"omci_sw_ver1", INFO_MIB, MIB_OMCI_SW_VER1},
	{"omci_sw_ver2", INFO_MIB, MIB_OMCI_SW_VER2},
	{"omcc_ver", INFO_MIB, MIB_OMCC_VER},
	{"omci_tm_opt", INFO_MIB, MIB_OMCI_TM_OPT},
	{"omci_eqid", INFO_MIB, MIB_OMCI_EQID},
	{"omci_ont_ver", INFO_MIB, MIB_OMCI_ONT_VER},
	{"omci_vendor_id", INFO_MIB, MIB_OMCI_VENDOR_ID},
#endif	
#ifdef CONFIG_USER_Y1731
	{"y1731_mode", INFO_MIB, Y1731_MODE},
	{"y1731_megid", INFO_MIB, Y1731_MEGID},
	{"y1731_myid", INFO_MIB, Y1731_MYID},
	{"y1731_meglevel", INFO_MIB, Y1731_MEGLEVEL},
	{"y1731_loglevel", INFO_MIB, Y1731_LOGLEVEL},
	{"y1731_ccminterval", INFO_MIB, Y1731_CCM_INTERVAL},
#endif	
	{"rtk_manufacturer", INFO_MIB, RTK_DEVID_MANUFACTURER},	
	{"rtk_oui", INFO_MIB, RTK_DEVID_OUI},	
	{"rtk_productclass", INFO_MIB, RTK_DEVID_PRODUCTCLASS},	
	{"rtk_serialno", INFO_MIB, MIB_HW_SERIAL_NUMBER},	
#ifdef CONFIG_USER_CWMP_TR069
	{"cwmp_provisioningcode", INFO_MIB, CWMP_PROVISIONINGCODE},	
#endif
	{"rtk_specver", INFO_MIB, RTK_DEVINFO_SPECVER},	
	{"rtk_swver", INFO_MIB, RTK_DEVINFO_SWVER},	
	{"rtk_hwver", INFO_MIB, RTK_DEVINFO_HWVER},
#if defined(CONFIG_GPON_FEATURE)
	{"gpon_sn",INFO_MIB,MIB_GPON_SN},
#endif
	{"elan_mac_addr", INFO_MIB, MIB_ELAN_MAC_ADDR},
	{NULL, 0, 0}
};


const PLMN_LIST sPLMNList[]=
{
	/* Ruler:		"00000000011111111112" */
	/* 				"12345678901234567890" */

	/* Test networks */
	0x00101F,	(char *)"Test Net 001 01"	, 0,	            /* 	TEST NETWOEK			*/
	0x00102F,	(char *)"Test Net 001 02"	, 0,	            /* 	TEST NETWOEK			*/
	0x07865F,	(char *)"INFONET-VZ" 		, 0,	            /* ?????					*/

	/* Greece				  */
	0x20201F,	(char *)"COSMOTE"  	        , 0,	            /* COSMOTE					*/
	0x20205F,	(char *)"vodafone GR"		, 0,	/* Vodafone-Panafon			*/
	0x20209F,	(char *)"Q-TELCOM"		    , 0,	/* Info Quest - Commercial	*/
	0x20210F,	(char *)"WIND Hellas" 	    , 0,	/* STET HELLAS				*/

	/* Netherlands 		 */
	0x20404F,	(char *)"voda NL"			, 0,	/* Vodafone Libertel N.V  	*/
	0x20408F,	(char *)"NL KPN" 			, 0,	/* KPN Mobile The Netherl-	*/
	0x20412F,	(char *)"NL Tlfrt"		    , 0,	/* TELFORT B.V.		   		*/
	0x20416F,	(char *)"T-Mobile NL" 		, 0,	/* T-Mobile Netherlands    	*/
	0x20420F,	(char *)"Orange" 			, 0,	/* Orange Nederland N.V.    */

	/* Belgium 			 */
	0x20601F,	(char *)"PROXI"			    , 0,	/* Belgacom Mobile	   		*/
	0x20610F,	(char *)"mobil*"			    , 0,	/* Mobistar S.A 		   	*/
	0x20620F,	(char *)"BASE"			    , 0,	/* BASE NV/SA		   		*/

	/* France				 */ 
	0x20801F,	(char *)"Orange F"			, 0,	/* Orange France	   		*/
	0x20810F,	(char *)"SFR"				, 0,	/* SFR				   		*/
	0x20820F,	(char *)"BYTEL"			    , 0,	/* Bouygues Telecom 	   	*/

	/* Andorra 			 */
	0x21303F,	(char *)"M-AND"			    , 0,	/* S.T.A			   		*/

	/* Spain				 */
	0x21401F,	(char *)"vodafone ES"		, 0,	/* Airtel Movil S.A 	   	*/
	0x21402F,	(char *)"movistar"			    , 0,	/* Retevision Movil S.A    	*/
	0x21403F,	(char *)"MASmovil"			    , 0,	/* Retevision Movil S.A    	*/
	0x21404F,	(char *)"Yoigo"			    , 0,	/* Retevision Movil S.A    	*/	
    0x21405F,	(char *)"movistar"			    , 0,	/* Retevision Movil S.A    	*/
	0x21406F,   (char *)"EUSKALTEL"         , 0,    /* EUSKALTEL                */
	0x21407F,	(char *)"Movistar"		        , 0,
    0x21408f,   (char *)"EUSKALTEL"         , 0,    /* EUSKALTEL                */
	0x21418F,	(char *)"ONO VPN"	        , 0,

	/* Hungary 			 */
	0x21601F,	(char *)"PANNON" 		    , 0,	/* Pannon GSM Telecoms		*/
	0x21630F,	(char *)"Telekom LTE" 			    , 0,	/* Westel900 GSM Mobile    	*/
	0x21670F,	(char *)"vodafone HU"	    , 0,	/* V.R.A.M. Telecommunica-	*/

	/* Bosnia-Herzegovina	 */
	0x21803F,	(char *)"BA-ERONET" 			, 0,	/* PEronet Mobile Communi-	*/
	0x21805F,	(char *)"MOBI'S"			    , 0,	/* JODP for Telecomms of R-	*/
	0x21890F,	(char *)"GSMBIH" 			, 0,	/* PE PTT BIH		   		*/

	/* Croatia 			 */
	0x21901F,	(char *)"T-Mobile HR"	    , 0,	/* HT Mobile Communications, LLC */
	0x21902F,	(char *)"TELE2"			    , 0,	/* Tele2 Croatia */
	0x21910F,	(char *)"HR VIP" 		    , 0,	/* VIPnet d.o.o.	   		*/

	/* Serbia and Montenegro */
	0x22001F,	(char *)"MOBTEL"			    , 0,	/* MOBTEL			   		*/
	0x22002F,	(char *)"promonte"		    , 0,	/* ProMonte GSM 		   	*/
	0x22003F,	(char *)"SCGTS"			    , 0,	/* Telekom Srbija a.d.		*/
	0x22004F,	(char *)"MONET"			    , 0,	/* Monet D.O.O		   		*/

	/* Italy				 */
	0x22201F,	(char *)"I TIM"			, 0,	/* Telecom Italia Mobile  	*/
    0x22210F,	(char *)"Vodafone IT"		, 0,	/* Vodafone Omnitel N.V.  	*/
	0x22288F,	(char *)"I WIND"			    , 0,	/* Wind Telecomunicazioni 	*/
	0x22299F,	(char *)"3 ITA"				, 0,	/* H3G	 (planned)	   		*/

	/* Romania 			 */
	0x22601F,	(char *)"CONNEX" 			, 0,	/* MobiFon S.A				*/
	0x22603F,	(char *)"Cosmorom"			, 0,	/* Cosmorom 				*/
	0x22610F,	(char *)"RO ORANGE" 			, 0,	/* Orange Romania SA		*/

	/* Switzerland 		 */
	0x22801F,	(char *)"Swisscom"			, 0,	/* Swisscom Mobile Ltd		*/
	0x22802F,	(char *)"sunrise"			, 0,	/* TDC Switzerland AG		*/
	0x22803F,	(char *)"Orange" 			, 0,	/*Orange Communications S- 	*/
	0x22807F,	(char *)"In&Phone" 		    , 0,	/*In &Phone SA 	*/
	0x22808F,	(char *)"T2"				    , 0,	/*Tele2 Telecommunications Services AG 	*/
	
	/* Czech Republic		 */
	0x23001F,	(char *)"TMO CZ" 			, 0,	/* T-Mobile Czech Republic a.s.	*/
	0x23002F,	(char *)"ET - CZ"			, 0,	/* Eurotel Praha, spol. s.r.o.	*/
	0x23003F,	(char *)"OSKAR"			    , 0,	/* Cesky Mobil a.s	   			*/

	/* Slovakia			 */
	0x23101F,	(char *)"Orange" 			, 0,	/* Orange Slovensko a.s    	*/
	0x23102F,	(char *)"ET-SK"			    , 0,	/* EuroTel Bratislava as  	*/

	/* Austria 			 */
	0x23201F,	(char *)"A1" 				, 0,	/* MobilKom Austria AG &CO KG	*/
	0x23203F,	(char *)"TMO A"			    , 0,	/* T-Mobile Austria GmbH		*/
	0x23205F,	(char *)"one"				, 0,	/* ONE GMBH	  */
	0x23207F,	(char *)"telering"		    , 0,	/* Tele.ring Telekom Service GmbH		*/
	0x23210F,	(char *)"3AT"			    , 0,	/* Hutchison 3G Austria GmbH (planned)	*/

	/* United Kingdom		 */
    0x23401F,	(char *)"Vectone" 		    , 0,	/* O2 (UK) Limited	   		*/
	0x23410F,	(char *)"O2" 		    , 0,	/* O2 (UK) Limited	   		*/
	0x23415F,	(char *)"Vodafone"			, 0,	/* Vodafone Limited 	   	*/
    0x23416F,	(char *)"Talk Talk"			, 0,	/* Vodafone Limited 	   	*/
	0x23420F,	(char *)"3UK"			    , 0,	/* Hutchison 3G UK Ltd		*/
    0x23426F,	(char *)"Lycamobile"			    , 0,	/* Hutchison 3G UK Ltd		*/
	0x23430F,	(char *)"EE" 			, 0,	/* T-Mobile (UK) Limited   	*/
	0x23431F,	(char *)"TMO UK"			    , 0,	/* T-Mobile (UK) Limited    */
	0x23432F,	(char *)"TMO UK" 			, 0,	/* T-Mobile (UK) Limited    */
	0x23433F,	(char *)"EE" 			, 0,	/* Orange PCS Ltd	   		*/
    0x23438F,	(char *)"Virgin" 			, 0,	/* Orange PCS Ltd	   		*/
	0x23450F,	(char *)"JT" 		    , 0,	/* Jersey Telecoms	   		*/
	0x23451F,	(char *)"Relish" 		    , 0,	/* 	   		*/
	0x23455F,	(char *)"C&W"				, 0,	/* Cable & Wireless Guernsey Ltd	*/
	0x23458F,	(char *)"Manx Telecom" 		, 0,	/* Manx Telecom 		  	*/

	/* Denmark 			 */
	0x23801F,	(char *)"TDC MOBIL" 			, 0,	/* TDC Mobil A/S	   		*/
	0x23802F,	(char *)"SONO"			    , 0,	/* Sonofon			   		*/
	0x23806F,	(char *)"3 DK"			    , 0,	/* Hi3G Denmark ApS			*/
	0x23820F,	(char *)"TELIA DK"			, 0,	/* Telia A/S Denmark		*/
	0x23830F,	(char *)"Orange" 			, 0,	/* Orange A/S				*/
	0x23877F,	(char *)"TELE2" 			, 0,	/* TELE2			*/

	/* Sweden				 */
	0x24001F,	(char *)"TELIA"			    , 0,	/* TeliaSonera Mobile		*/
	0x24002F,	(char *)"3 SE"			    , 0,	/* HI3G Access AB (3) (planned)	*/
	0x24003F,   (char *)"Orange"			    , 0,	/* ????? */
	0x24004F,	(char *)"SWE"				, 0,	/* Hi3G Access AB	*/
	0x24005F,	(char *)"S COMVIQ"		    , 0,	/* Tele 2 AB	*/
	0x24006F,	(char *)"Telenor"		    , 0,	/* Telenor	*/
	0x24007F,	(char *)"S COMVIQ" 			, 0,	/* Tele 2 AB		   		*/
	0x24008F,	(char *)"Telenor"			, 0,	/* Vodafone Sverige AB		*/
	0x24010F,	(char *)"Spring" 			, 0,	/* Swefour AB		   		*/

	/* Norway				 */
	0x24201F,	(char *)"TELENOR"			, 0,	/* Telenor Mobil	   		*/
	0x24202F,	(char *)"NetCom"			    , 0,	/* NETCOM AS		   		*/
	0x24203F, 	(char *)"T3"				    , 0,	/* Teletopia Mobile Communications AS				*/
	
	/* Finland 			 */
	0x24403F,	(char *)"FINNET" 			, 0,	/* Finnet Networks Ltd 		*/
	0x24405F,	(char *)"FI elisa" 			, 0,	/* Radiolinja Origo Oy 		*/
	0x24410F,	(char *)"TDC"				, 0,	/* TDC	   					*/
	0x24412F,	(char *)"FINNET" 			, 0,	/* Finnet Networks Ltd 		*/
	0x24414F,	(char *)"FI AMT" 			, 0,	/* Alands Mobiltelefon Ab 	*/
	0x24421F,	(char *)"Saunalahti" 		, 0,	/* Saunalahti 	*/
	0x24491F,	(char *)"SONERA" 			, 0,	/* Sonera Mobile Networks Limited 	*/

	/* Lithuania			 */
	0x24601F,	(char *)"OMNITEL LT"			, 0,	/* OMNITEL			   		*/
	0x24602F,	(char *)"LT BITE GSM"		, 0,	/* UAB Bite GSM 		   	*/
	0x24603F,	(char *)"TELE2"			    , 0,	/* UAB "TELE2" (TELE2)		*/

	/* Latvia				 */
	0x24701F,	(char *)"LMT GSM"			, 0,	/* Latvian Mobile Tel. Co-	*/
	0x24702F,	(char *)"TELE2"			    , 0,	/* TELE2			   		*/
	0x24705F,	(char *)"BITE LV"			, 0,	/* SIA Bite Latvija	   		*/
	
	/* Estonia 			 */
	0x24801F,	(char *)"EE EMT GSM"			, 0,	/* AS EMT			   		*/
	0x24802F,	(char *)"EE elisa" 			, 0,	/* Radiolinja Eesti AS		*/
	0x24803F,	(char *)"TELE2"				, 0,	/* Tele2 Eesti AS	   		*/

	/* Russia				 */
	0x25001F,	(char *)"MTS-RUS"			, 0,	/* Mobile Telesystems		*/
	0x25002F,	(char *)"MegaFon"			, 0,	/* ZAO Ural GSM (MegaFon) 	*/
	0x25003F,	(char *)"RUS NCC"			, 0,	/* Nizhegorodskaya Cellular Communications 		*/
	0x25004F,	(char *)"RUS_SCN"			, 0,	/* SIBCHALLENGE LTD 	   	*/
	0x25005F,	(char *)"RUS 05" 			, 0,	/* Siberian Cellular Syst 	*/
	0x25007F,	(char *)"BMT"				, 0,	/* BM Telecom		   		*/
	0x25010F,	(char *)"RUS DTC"			, 0,	/* Don Telecom		   		*/
	0x25011F,	(char *)"ORENSOT"			, 0,	/* Joint-stock company		*/
	0x25012F,	(char *)"RUS 12" 			, 0,	/* Far-Eastern Cellular    	*/
	0x25013F,	(char *)"KUGSM"			    , 0,	/* Kuban -GSM closed JSC  	*/
	0x25014F,	(char *)"Di-ex"			    , 0,	/* Teleset Ltd  			*/
	0x25015F,	(char *)"SMARTS" 			, 0,	/* ZAO SMARTS		   		*/
	0x25016F,	(char *)"NTC"				, 0,	/* New Telephone Company    */
	0x25017F,	(char *)"ERMAK"			    , 0,	/* Ermak RMS		   		*/
	0x25019F,	(char *)"BASHCELL"		    , 0,	/* Bashcell, CJSC	   		*/
	0x25020F,	(char *)"TELE2"			    , 0,	/* Cellular Communications of Udmurtia 	*/
	0x25028F,	(char *)"Beeline"			, 0,	/* JSC Extel (Extel)					*/
	0x25039F,	(char *)"Uraltel"			, 0,	/* South Ural Cellular Tel- */
	0x25044F,	(char *)"NC-GSM" 			, 0,	/* StavTeleSot				*/
	0x25092F,	(char *)"Primtel"			, 0,	/* Primtelefone 			*/
	0x25099F,	(char *)"Beeline"			, 0,	/* "KB Impuls"				*/

	/* Ukraine 			 */
	0x25501F,	(char *)"UA UMC"				, 0,	/* Ukrainian Mobile Comms 	*/
	0x25502F,	(char *)"WellCOM"			, 0,	/* Ukrainian Radio System-	*/
	0x25503F,	(char *)"UA-KS"			    , 0,	/* "Kyivstar GSM" JSC		*/
	0x25505F,	(char *)"UA-GT"				, 0,	/* Golden Telecom LLC		*/
	0x25506F,	(char *)"life:)"			    , 0,	/* Astelit LLC				*/

	/*Belarus,Republic of	  */		  
	0x25701F,	(char *)"VELCOM" 			, 0,	/* Mobile Digital Communica-*/ 
	0x25702F,	(char *)"MTS BY"				, 0,	/* JLLC Mobile TeleSystems  */

	/* Moldova 			 */
	0x25901F,	(char *)"VOXTEL" 			, 0,	/* Voxtel SA				*/
	0x25902F,	(char *)"MDCELL" 			, 0,	/* Moldcell SA				*/

	/* Poland				 */
	0x26001F,	(char *)"Plus GSM"			, 0,	/* Polkomtel SA 			*/
	0x26002F,	(char *)"Era"				, 0,	/* Polska Telefonia Cyfro-	*/
	0x26003F,	(char *)"IDEA"			    , 0,	/* PTK Centertel		 	*/
	0x26006F,	(char *)"Netia P4"			, 0,	/* Polkomtel SA			 	*/
	/* Germany 			 */
	0x26201F,	(char *)"TMO D"			    , 0,	/* T-Mobile Deutschland GmbH*/
	0x26202F,	(char *)"Vodafone"		    , 0,	/* Vodafone D2 GmbH			*/
	0x26203F,	(char *)"E-Plus" 			, 0,	/* E-Plus Mobilfunk GmbH  	*/
	0x26207F,	(char *)"o2 - de"			, 0,	/* O2 (Germany) GmbH & Co.-	*/
	0x26208F,	(char *)"o2 - de" 		    , 0,	/* O2 (Germany) GmbH & Co.- */

	/* Gibraltar			 */
	0x26601F,	(char *)"GIBTEL"			    , 0,	/* Gibraltar Telecoms Int-	*/

	/* Portugal			 */
	0x26801F,	(char *)"voda P" 			, 0,	/* Vodafone Portugal		*/
	0x26803F,	(char *)"OPTIM"			    , 0,	/* Optimus Telecomunicaco-	*/
	0x26806F,	(char *)"P TMN"				, 0,	/* Telecom Moveis Nac.		*/

	/* Luxembourg			 */
	0x27001F,	(char *)"LUXGSM"			    , 0,	/* P+T Luxembourg	   		*/
	0x27077F,	(char *)"L TANGO"			, 0,	/* Tango S.A 			   	*/
	0x27099F,	(char *)"VOX.LU" 			, 0,	/* LuXcommunications S.A.  	*/

	/* Ireland 			 */
	0x27201F,	(char *)"vodafone IE"		, 0,	/* Vodafone Ireland Plc    	*/
	0x27202F,	(char *)"02 -IRL" 			, 0,	/* O2 Communications (Irel-	*/
	0x27203F,	(char *)"IRL-METEOR" 		, 0,	/*	METEOR			   		*/
	0x27205F,	(char *)"3 IRL" 			    , 0,	/* Hutchison 3G Ireland limited	*/
	/* Iceland 			 */
	0x27401F,	(char *)"IS SIMINN"			, 0,	/* Iceland Telecom Ltd		*/
	0x27402F,	(char *)"Vodafone"		    , 0,	/* Og fjarskipti hf			*/
	0x27403F,	(char *)"Vodafone"		    , 0,	/* Og fjarskipti hf	   		*/
	0x27404F,	(char *)"Viking" 			, 0,	/* IMC Island ehf (Viking -	*/

	/* Albania 			 */
	0x27601F,	(char *)"AMC-AL" 			, 0,	/* Albanian Mobile Comms  	*/
	0x27602F,	(char *)"voda AL"			, 0,	/* Vodafone Albania 	   	*/

	/* Malta				 */
	0x27801F,	(char *)"voda MT"			, 0,	/* Vodafone Malta Limited  	*/
	0x27821F,	(char *)"go mobile"		    , 0,	/* Mobisle Communications - */

	/* Cyprus				 */
	0x28001F,	(char *)"CY CYTAGSM" 		, 0,	/* CYTAGSM					*/
	0x28010F,	(char *)"areeba" 			, 0,	/* Areeba LTD					*/
	
	/* Georgia 			 */
	0x28201F,	(char *)"GCELL"			    , 0,	/* Geocell Limited			*/
	0x28202F,	(char *)"MAGTI"			    , 0,	/* Magti Com				*/

	/* Armenia 			 */
	0x28301F,	(char *)"RA-ARMGSM" 			, 0,	/* ArmenTel 				*/
	0x28304F,	(char *)"RA 04"			    , 0,	/* Karabakh Telecom 	   	*/
	0x28305F,	(char *)"RA 05"			    , 0,	/* K Telecom CJSC 	   	*/
	/* Bulgaria			 */
	0x28401F,	(char *)"M-TEL"			    , 0,	/* MobilTEL AD				*/
    0x28403F,   (char *)"vivatel"           , 0,    /* vivatel                  */
	0x28405F,	(char *)"BG GLOBUL " 		, 0,	/* Cosmo Bulgaria Mobile EAD*/

	/* Turkey				 */
	0x28601F,	(char *)"TCELL"			    , 0,	/* Turkcell Iletisim Hizm -	*/
	0x28602F,	(char *)"TELSIM GSM"		    , 0,	/* TELSIM Mobil Telekomun- 	*/
	0x28603F,	(char *)"AVEA"			    , 0,	/* IS-TIM Telekomunikasyon- */
	0x28604F,	(char *)"AYCELL" 			, 0,	/* AYCELL Haberlesme ve 	*/

	/*Faroe Islands		  */
	0x28801F,	(char *)"FT-GSM" 			, 0,	/* Faroese Telecom GSM 900  	*/
	0x28802F,	(char *)"KALL"			    , 0,	/* P/F Kall (KALL-GSM)(planned)	*/

	/* Greenland			 */
	0x29001F,	(char *)"TELE GRL"		    , 0,	/* Tele Greenland A/S		*/

	/* Slovenia			 */
	/*0x29331F,	(char *)"MOBITEL"			, 0,	*//* Mobitel d.d.		   		*/
	0x29340F,	(char *)"SI voda"			, 0,	/* SIMobil d.d		   		*/
	0x29341F,	(char *)"MOBITEL" 		    , 0,	/* Mobitel d.d. 			*/
	/*0x29351F,	(char *)"MOBITEL"			, 0,  *//* Mobitel d.d. 			*/
	0x29370F,	(char *)"VEGA 070"		    , 0,	/* Western Wireless Inter- 	*/

	/* Macedonia			 */
	0x29401F,	(char *)"MOBI-M" 			, 0,	/* MOBIMAK AD			 	*/
	0x29402F,	(char *)"MKD COSMOFON" 		, 0,	/* MTS Uslugi na mobilna (planned)-	*/

	/*Liechtenstein		 */    
	0x29501F,	(char *)"FL GSM"		        , 0,	/* Swisscom Mobile Ltd		*/
	0x29502F,	(char *)"Orange FL"			, 0,	/* Orange (Liechtenstein) AG */
	0x29505F,	(char *)"FL1"				, 0,	/* Mobilkom (Liechtenstein)	*/
	0x29577F,	(char *)"LI TANGO "			, 0,	/* Tele 2 Aktiengesellschaft */

    /* Serbia and Montenegro New*/
    0x29701F,	(char *)"promonte"		    , 0,	/* ProMonte GSM 		   	*/
    
	/* New code for north American oprators: */

	/* Canada				 */
	0x302370,	(char *)"Fido"			    , 0,	/* Microcell 				*/
	0x302720,	(char *)"CAN72"			    , 0,	/* Rogers Wireless 			*/
	
	/* SPM */
	0x30801F,	(char *)"AMERIS"			    , 0,	/* SPM Telecom  			*/
	
	/* United States		 */
	0x310000,	(char *)"Mid-Tex"			, 0,	/* Mid-Tex Cellular, Ltd.	*/
	0x310020,	(char *)"UnionTel"		    , 0,	/* Union Telephone Company	*/
	0x310026,	(char *)"T-Mobile"			, 0,	/* T-Mobile USA, Inc		*/
	0x310030,	(char *)"CENT USA"		    , 0,	/* Centennial Communications*/
	0x310031,	(char *)"T-Mobile"			, 0,	/* T-Mobile USA, Inc		*/
	0x310032,	(char *)"IT&E"			    , 0,	/* IT&E Overseas, Inc		*/
	0x310040,	(char *)"CONCHO "			, 0,	/* Concho Cellular Telephone Co., Inc. (planned)*/
	0x310046,	(char *)"SIMMETRY "		    , 0,	/* TMP Corp	*/
	0x310070,	(char *)"HIGHLAND"		    , 0,	/* Highland Cellular, Inc (planned)				*/
	0x310080,	(char *)"Corr"			    , 0,	/* Corr Wireless Communications*/
	0x310090,	(char *)"Edge"			    , 0,	/* Edge Wireless	*/
	0x310100,	(char *)"PLATEAU"			, 0,	/* E.N.M.R. Telephone Cooperative(planned)		*/
	0x310150,	(char *)"AT&T"		        , 0,	/* AT&T         							    */
	0x310160,	(char *)"T-Mobile"			, 0,	/* T-Mobile USA, Inc.							*/	
	0x31016F,	(char *)"TMO"				, 0,	/* T-Mobile USA, Inc.							*/
	0x310170,	(char *)"AT&T"		        , 0,	/* Cingular Wireless							*/
	0x31017F,	(char *)"Cingular"		    , 0,	/* Cingular Wireless	- OLD					*/
	0x310180,	(char *)"Cingular"		    , 0,	/* Cingular Wireless						*/
	0x31018F,	(char *)"Cingular"		    , 0,	/* Cingular Wireless	- OLD					*/
	0x310190,	(char *)"D-HARBOR"		    , 0,	/* Alaska Wireless Communications, LLC		 	*/
	0x310200,	(char *)"T-Mobile"			, 0,	/* T ?Mobile USA								*/
	0x31020F,	(char *)"TMO"				, 0,	/* T-Mobile USA, Inc.							*/
	0x310210,	(char *)"T-Mobile"			, 0,	/* T ?Mobile USA								*/
	0x31021F,	(char *)"TMO"				, 0,	/* T-Mobile USA, Inc.							*/
	0x310220,	(char *)"T-Mobile"			, 0,	/* T ?Mobile USA								*/
	0x31022F,	(char *)"TMO"				, 0,	/* T-Mobile USA, Inc.							*/
	0x310230,	(char *)"T-Mobile"			, 0,	/* T ?Mobile USA								*/
	0x31023F,	(char *)"TMO"				, 0,	/* T-Mobile USA, Inc.							*/
	0x310240,	(char *)"T-Mobile"			, 0,	/* T ?Mobile USA								*/
	0x31024F,	(char *)"TMO"				, 0,	/* T-Mobile USA, Inc.							*/
	0x310250,	(char *)"T-Mobile"			, 0,	/* T ?Mobile USA								*/
	0x31025F,	(char *)"TMO"				, 0,	/* T-Mobile USA, Inc.							*/
	0x310260,	(char *)"T-Mobile"			, 0,	/* Cook Inlet Voicestream PCS 					*/
	0x31026F,	(char *)"T-MOBILE"			, 0,	/* T-Mobile USA, Inc.							*/
	0x310270,	(char *)"T-Mobile"			, 0,	/* T ?Mobile USA								*/
	0x31027F,	(char *)"TMO"				, 0,	/* T-Mobile USA, Inc.							*/
	0x310311,	(char *)"FARMERS"			, 0,	/* Farmers Cellular Telephone Inc 				*/
	0x31031F,	(char *)"TMO"				, 0,	/* T-Mobile USA, Inc.							*/
	0x310320,	(char *)"Cell"			    , 0,	/* Smith Bagley, Inc. (planned)					*/
	0x310340,	(char *)"WestLink"		    , 0,	/* High Plains Midwest LLC dba Westlink 		*/
	0x310380,	(char *)"AT&T"		        , 0,	/* AT&T Wireless								*/
	0x310400,   (char *)"iCAN_GSM"		    , 0,    /* Wave Runner LLC Mariana Islands				*/
	0x310410,	(char *)"AT&T"		        , 0,	/* Cingular Wireless							*/
	0x310420,	(char *)"Cincinnati Bell USA"	, 0,	/* Cingular Wireless							*/
	0x310450,	(char *)"NECCI" 			, 0,	/* North East Colorado Cellular, Inc (NECCI)	*/
	0x310460,	(char *)"ONELINK"			, 0,	/* TMP Corp 									*/
	0x310500,	(char *)"PSC"				, 0,	/* Public Service Cellular, Inc					*/
	0x310530,	(char *)"W V W"			    , 0,	/* Key Communication LLC						*/
	0x310560,	(char *)"Cell One"		    , 0,	/* Dobson Communications Corporation			*/
	0x31058F,	(char *)"TMO"				, 0,	/* T-Mobile USA, Inc.							*/
	0x310590,	(char *)"ROAMING"			, 0,	/* Western Wireless Corporation 				*/
	0x310630,	(char *)"AMERLINK" 		    , 0,	/* Choice Wireless L.C							*/
	0x310640,	(char *)"ARDGMC" 			, 0,	/* Airadigm Airadigm Communica-					*/
	0x31066F,	(char *)"TMO"				, 0,	/* T-Mobile USA, Inc.							*/
	0x310660,	(char *)"T-Mobile" 			, 0,	/* T-Mobile USA, Inc.							*/	
	0x31068F,	(char *)"NPI"				, 0,	/* NPI Wireless 								*/
	0x310690,	(char *)"IMMIX"			    , 0,	/* Keystone Wireless LLC						*/
	0x310740,	(char *)"TLXT"			    , 0,	/* Tracy Corporation II 						*/
	0x310770,	(char *)"IWS"				, 0,	/* Iowa Wireless Services LP		 			*/
	0x310790,	(char *)"Pinpoint"		    , 0,	/* PinPoint Wireless Inc		  				*/
	0x31080F,	(char *)"TMO"				, 0,	/* T-Mobile USA, Inc.		 				 	*/
	0x310870,	(char *)"PACE"			    , 0,	/* Kaplan Telephone Company,Inc 				*/
	0x310880,	(char *)"ACSIAC" 			, 0,	/* Advantage Cellular Systems Advantage Cellular Systems Inc */
	0x310900,	(char *)"TXCELL"			, 0,	/* Taylor Telecommunications, LTD				*/
	0x310910,	(char *)"FCSI"			    , 0,	/* Southern Illinois RSA Partnership 			*/
	0x310940,	(char *)"DCT"				, 0,	/* Poka Lambro Telecommunications, Ltd. 		*/
	0x310950,	(char *)"XIT"				, 0,	/* XIT Cellular 			 	 				*/
	0x310980,	(char *)"AT&T"				, 0,	/* USA 			 	 				*/
	0x311000,	(char *)"Mid-Tex"			, 0,		/* Mid-Tex Cellular, Ltd.						*/
	0x31100F,	(char *)"WILKES" 			, 0,	/* Wilkes Cellular Inc		  					*/
	0x31101F,	(char *)"WILKES" 			, 0,	/* Wilkes Cellular Inc		  					*/
	0x311030,	(char *)"Indigo" 			, 0,	/* Indigo Wireless, Inc 	  					*/
	0x31105F,	(char *)"WILKES" 			, 0,	/* Wilkes Cellular Inc		  					*/
	0x311070,	(char *)"EASTER" 			, 0,	/* Easterbrooke Cellular Corporation 			*/
	0x311080,	(char *)"PINECell"		    , 0,	/* Pine Telephone Company	  					*/
	0x311090,	(char *)"SXLP"			    , 0,	/* Siouxland PCS, Inc.  	  					*/
	0x311110,	(char *)"HPW"				, 0,	/* High Plains Wireless, L.P	  				*/
	0x311130,	(char *)"C1AMARIL"		    , 0,	/* Amarillo License, L.P	  					*/
	0x311160,	(char *)"EMW"				, 0,	/* Endless Mountains Wireless, LLC				*/
	0x311170,	(char *)"PetroCom"		    , 0,	/* Petrocom LLC 			  					*/
	0x311180,	(char *)"AT&T"		        , 0,	/* Petrocom LLC 			  					*/
	0x311190,	(char *)"C1ECI"			    , 0,	/* Cellular Properties, Inc			  			*/
	0x311210,	(char *)"FARMERS"			, 0,	/* Farmers Cellular Telephone Inc				*/
	0x311250,	(char *)"iCAN_GSM"		    , 0,	/* Wave Runner LLC (Guam) 			  			*/
	0x311310,	(char *)"LamarCel"		    , 0,	/* Lamar County Cellular, Inc					*/
	0x330110,	(char *)"PR Claro"		    , 0,	/* Lamar County Cellular, Inc					*/
	/*Mexico				*/	
	0x334020,	(char *)"TELCEL GSM"	    , 0,	/* Radiomovil Dipsa SA de CV		*/
	0x33403F,	(char *)"movistar"		    , 0,	/* Pegaso Comunicaciones y	  	*/
	0x338050,   (char *)"DIGICEL"           , 0,    /* Digicel Caribean Islands*/

	/*Jamaica			  */   
	0x33805F,	(char *)"DIGICEL"			, 0,	/* Mossel (Jamaica) Ltd		   	*/
	0x338070,	(char *)"Claro JAM"			, 0,	/* Jamaica		   	*/
	0x338180,	(char *)"bmobile"			, 0,	/* Cable & Wireless Jamaica Limited(planned)	*/

	/*French Antilles		 */
	0x34001F,	(char *)"F-Orange" 			, 0,	/* Orange Caraibe			   	*/
	0x340020,	(char *)"BOUYG-C" 			, 0,	/* Caribean Islands			   	*/
	0x34002F,	(char *)"OMT" 			    , 0,		/* Outremer Telecom			   	*/
	/*0x34003F,	(char *)"TELCELL"			, 0,	*//* Saint Martin et Saint Barthelemy Tel Cell SARL*/
	0x34008F,	(char *)"AMIGO"			    , 0,	/* Dauphin Telecom				*/
	0x34020F,	(char *)"BOUYGTEL-C"		, 0,	/* Bouygues Telecom Caraibe 	*/
	
	/*Barbados 			*/			
	/*0x342050,	(char *)"Digicel"			, 0,	*//* Digicel (Barbados) Limited	*/
	0x342600,	(char *)"bmobile"			, 0,	/* Cable & Wireless Barbados Ltd*/
	0x342750,	(char *)"DIGICEL"			, 0,	/* Digicel (Barbados) Limited*/
	0x342810,	(char *)"Cingular"		    , 0,	/* Cellular Communications Barbados*/
	
	/*Antigua & Barbuda	*/		
	0x344030,	(char *)"APUA-PCS"		    , 0,	/* APUA PCS 					*/
	0x34403F,	(char *)"APUA-PCS"		    , 0,	/* APUA PCS 					*/
	0x344920,	(char *)"bmobile"			, 0,	/* Cable & Wireless Caribbean Cellular (Antigua) Limited */
	0x344930,	(char *)"Cingular"		    , 0,		/* Antigua Wireless Ventures Limited */
	
	/*Cayman Islands		 */ 	   
	0x346140,	(char *)"bmobile"			, 0,	/* Cable & Wireless (Cayman Is-(planned)		*/

	/* VGB */
	0x348170,	(char *)"bmobile"		    , 0,		/* Caribbean Cellular Telephone 		*/
	0x348570,	(char *)"CCT Boatphone"		, 0,		/* Caribbean Cellular Telephone 		*/
	
	/*Bermuda				 */ 		 
	/*0x35001F,	(char *)"TELE BDA"		, 0,	*//* Telecommunications (Bermud- 	*/
	0x35002F,	(char *)"MOBILITY"		    , 0,	/* Mobility Limited    (planned)*/
	0x35010F,	(char *)"Cingular"		    , 0,	/* Telecommunications (Bermuda & West Indies) Ltd */
	
	/*Grenada				 */ 
	0x352030,	(char *)"DIGICEL"			, 0,	/* Digicel Grenada Ltd. 				*/
	0x352110,	(char *)"bmobile"			, 0,	/* Cable & Wireless Grenada Ltd.*/
	0x352130,	(char *)"TWTCGN" 			, 0,	/* Trans-World Telecom Caribbean Ltd 	*/

	/*MS	  */		  
	0x354860, (char *)"bmobile"			    , 0,	/* Cable & Wireless St Kitts & Nevis Limited	*/
	
	/*St Kitts & Nevis 	  */		  
	0x356110,	(char *)"bmobile"			, 0,	/* Cable & Wireless St Kitts & Nevis Limited 	*/

	/*St Lucia 			  */	
	0x358030,	(char *)"Cingular"		    , 0,	/* Wireless Ventures (St. Lucia) Limited		*/
	0x358050,	(char *)"DIGICEL"			, 0,	/* Digicel (St Lucia) Limited					*/
	0x358110,	(char *)"bmobile"			, 0,	/* Cable & Wireless Caribbean Cellular- 		*/

	/*St Vincent & The Grenadines */	
	0x360010,	(char *)"Cingular"		    , 0,	/* Wireless Ventures (St. Lucia) Limited 		*/
	0x360070,	(char *)"DIGICEL"			, 0,	/* Digicel (St Lucia) Limited					*/
	0x360110,	(char *)"bmobile"			, 0,	/* Cable & Wireless Caribbean Cellular- 		*/

	/*Netherlands Antilles  */		  
	0x36251F,	(char *)"telcell"			, 0,	/* Telcell N.V. 				*/
	0x36269F,	(char *)"DIGICEL" 			, 0,	/* Curacao Telecom N.V. 		*/
	0x36291F,	(char *)"UTS"				, 0,	/* Setel NV (UTS Wireless Curacao) (planned)	*/
	0x362630,	(char *)"Cingular"		    , 0,	/* Communications Systems Curacao N.V.			*/
	0x362951,	(char *)"CHIPPIE"			, 0,	/* Setel NV (UTS Wireless Curacao) (planned)	*/
	
	/*Aruba				 */ 		 
	0x36301F,	(char *)"SETARGSM"		    , 0,	/* SETAR (Servicio di Teleco-  */
	0x363020,	(char *)"DIGICEL"			, 0,	/* Aruba  */
	0x36320F,	(char *)"DIGICEL"			, 0,	/* New Millenium Telecom Services (NMTS)  */
	
	/*Bahamas				  */		  
	0x36439F,	(char *)"BaTelCel"		    , 0,	/* The Bahamas Telecommunications-				*/

	/*Anguilla 			  */		  
	0x365840,	(char *)"bmobile"			, 0,	/* Cable & Wireless (West Indies)-				*/

	/*Dominica 			  */
	0x366020,	(char *)"Cingular"		    , 0,	/* Wireless Ventures (Dominica) Ltd.			*/
	0x366110,	(char *)"bmobile"			, 0,	/* Cable & Wireless Dominica Ltd.-				*/

	/*Cuba				  */		  
	0x36801F,	(char *)"C_Com"			    , 0,	/* C_Com						*/

	/*Dominican Republic	  */		  
	0x370011,  	(char *)"orange"			, 0,	/* Orange Dominicana S.A.		*/	
	0x37001F,  	(char *)"Orange"			, 0,	/* Orange Dominicana S.A.		*/
	0x37002F,  	(char *)"Claro DOM"			, 0,	/* Claro Dominicana S.A.		*/

	/* HTI	  */		  
	0x37201F, 	(char *)"VOILA"			    , 0,	/* Communication Cellulaire d'Haiti SA		*/
	0x372020, 	(char *)"DIGICEL"		    , 0,	/* HIT dIGICEL		*/

	/*Trinidad and Tobago	  */		  
	0x37412F,	(char *)"TSTT"			    , 0,	/* Telcommunications Services of- (planned)		*/
	0x374130,	(char *)"DIGICEL"		    , 0,	/* Trinidad and Tobago		*/

	/*Turks & Caicos Islands */		   
	0x376350,	(char *)"C&W"				, 0,	/* Cable & Wireless West Indies -				*/

	/* Azerbaijan			  */
	0x40001F,	(char *)"ACELL"			    , 0,	/* Azercell Telekom B.M.		*/
	0x40002F,	(char *)"BAKCELL GSM 2000"	, 0,		/* J.V.Bakcell			   		*/

	/*Kazakhstan 		  */ 
	0x40101F,	(char *)"K-MOBILE"		    , 0,	/* KaR-TeL LLC (K-MOBILE)		*/
	0x40102F,	(char *)"KZ KCELL"		    , 0,	/* GSM Kazakhstan Ltd (K'CELL) 	*/

	/*Bhutan				  */		  
	0x40211F,	(char *)"B-Mobile"		, 0,	/* B-Mobile				   		*/

	/* India				   */
	0x40401F,	(char *)"Vodafone"			    , 0,	/* Aircel Digilink India -		*/
	0x40402F,	(char *)"Airtel" 			, 0,	/* Bharti Mobile Ltd (planned)	*/
	0x404030,	(char *)"Vodafone"			    , 0,	/* Hutchison Telecom East -		*/
	0x40403F,	(char *)"Airtel" 			, 0,	/* Bharti Telenet Ltd	   		*/
	0x40404F,	(char *)"IDEA"			    , 0,	/* IDEA Cellular Limited (planned)*/
	0x40405F,	(char *)"Vodafone"			    , 0,	/* Fascel Limited (CELFORCE )   */
	/* 0x40406F,  (char *)"AIRTEL"		    , 0,*/	/* KARNATKA 					*/
	0x40407F,	(char *)"IDEA"			    , 0,	/* IDEA Cellular Limited		*/
	0x40409F,	(char *)"Reliance"		    , 0,	/* Reliance Telecom Private- 	*/
	0x40410F,	(char *)"Airtel" 			, 0,	/* Bharti Cellular Teleco-		*/
	0x40411F,	(char *)"Vodafone"			, 0,	/* Hutchison Essar Telecom Limited */
	0x40412F,	(char *)"IDEA"			    , 0,	/* Escotel Mobile Comms L-		*/
	0x40413F,	(char *)"Vodafone"			    , 0,	/* Hutchison Essar South Limited*/
	0x40414F,	(char *)"Spice"			    , 0,	/* Spice Communications Limited	*/
	0x40415F,	(char *)"Vodafone"			    , 0,	/* upe*/
	0x40416F,	(char *)"Airtel" 			, 0,	/* Bharti Cellular Teleco-		*/
	0x40417F,	(char *)"Aircel"			    , 0,	/* Dishnet Wireless Limited*/
	0x40418F,	(char *)"Reliance"		    , 0,	/* Reliance Telecom Private-	*/
	0x40419F,	(char *)"IDEA"			    , 0,	/* Escotel Mobile Comms L-		*/
	0x40420F,	(char *)"Vodafone" 			, 0,	/* Hutchison Max Telecom -		*/
	0x40421F,	(char *)"BPL Mobile" 		, 0,	/* BPL MOBILE		   			*/
	0x40422F,	(char *)"IDEA"			    , 0,	/* MAHARASHTRA		   			*/
	0x40424F,	(char *)"IDEA"			    , 0,	/* GUJARAT			   			*/
	0x40425F,	(char *)"Aircel"			    , 0,	/* Dishnet Wireless Limited		*/
	0x40427F,	(char *)"Vodafone" 		, 0,	/* BPL USWEST Cellular			*/
	0x40428F,	(char *)"Aircel"			    , 0,	/* Dishnet Wireless Limited		*/
	0x40429F,	(char *)"Aircel"			    , 0,	/* Dishnet Wireless Limited		*/
	0x40430F,	(char *)"Vodafone"			    , 0,	/* Usha Martin Telekom Lt-		*/
	0x40431F,	(char *)"Airtel" 			, 0,	/* BHARTI MOBITEL LIMITED 		*/
	0x40433F,	(char *)"Aircel"			    , 0,	/* Dishnet Wireless Limited		*/
	0x40434F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam - 		*/
	0x40435F,	(char *)"Aircel"			    , 0,	/* Dishnet Wireless Limited 	*/
	0x40436F,	(char *)"Reliance"		    , 0,	/* Reliance Telecom Private Ltd	*/
	0x40437F,   (char *)"Aircel"			    , 0,	/* Dishnet Wireless Limited 	*/
	0x40438F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam-  		*/
	0x40440F,	(char *)"Airtel" 			, 0,	/* Bharti Cellular Ltd			*/
	0x40441F,	(char *)"Aircel"			, 0,	/* RPG Cellular Services  		*/
	0x40442F,	(char *)"Aircel" 			, 0,	/* Aircel Limited	  -			*/
	0x40443F,	(char *)"Vodafone" 		, 0,	/* BPL USWEST Cellular			*/
	0x40444F,	(char *)"Spice"			    , 0,	/* Spice Communications Limited	*/
	0x40445F,	(char *)"Airtel" 			, 0,	/* Bharti Mobile Ltd 	   		*/
	0x40446F,	(char *)"Vodafone" 		    , 0,	/* BPL USWEST Cellular			*/
	0x40449F,	(char *)"Airtel" 			, 0,	/* Bharti Mobile Ltd			*/
	0x40450F,	(char *)"Reliance"		    , 0,	/* Reliance Telecom Private Ltd */
	0x40451F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40452F,	(char *)"Reliance"		    , 0,	/* Reliance Telecom Private Ltd	*/
	0x40453F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40454F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40455F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40456F,	(char *)"IDEA"			    , 0,	/* Escotel Mobile Comms L-		*/
	0x40457F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40458F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40459F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40460F,	(char *)"Vodafone"			    , 0,	/* Aircel Digilink India -		*/
	0x40462F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40464F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40466F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40467F,	(char *)"Reliance"		    , 0,	/* RMADHYA PARDESH				*/
	0x40468F,	(char *)"Dolphin"			, 0,	/* Mahanagar Telephone Nigam Ltd- Delhi*/
	0x40469F,	(char *)"Dolphin"			, 0,	/* Mahanagar Telephone Nigam Ltd- Mumbai*/
	0x40470F,	(char *)"Airtel"			    , 0,	/* Hexacom India Limited (Oasis Cellular)*/
	0x40471F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40472F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40473F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40474F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40475F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40476F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40477F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40478F,	(char *)"IDEA"			    , 0,	/* IDEA - Gujarat Circle  		*/
	0x40479F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40480F,	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40481F,  	(char *)"Cellone"			, 0,	/* Bharat Sanchar Nigam Limited	*/
	0x40482F,  	(char *)"IDEA"				, 0,	/* Escotel Mobile Comms L-		*/
	0x40483F,   (char *)"Reliance"		    , 0,	/* RMADHYA PARDESH				*/
	0x40484F,	(char *)"Vodafone"			    , 0,	/* Hutchison Essar Telecom Limited*/

	0x40485F,  (char *)"Reliance" 			, 0,	/* west bengal					*/
	0x40486F,	(char *)"Vodafone"			    , 0,	/* Hutchison Essar Telecom Limited*/
	0x40487F,  	(char *)"IDEA"				, 0,	/* Escotel Mobile Comms L-		*/
	0x40488F,	(char *)"Vodafone"			    , 0,	/* Hutchison Essar South Limited*/
	0x40489F,  	(char *)"IDEA"				, 0,	/* Escotel Mobile Comms L-		*/
	0x40490F,	(char *)"Airtel" 		    , 0,	/* Bharti Cellular Ltd (planned)*/
	0x40492F,	(char *)"Airtel" 		    , 0,	/* Bharti Cellular Ltd (planned)*/
	0x40493F,	(char *)"Airtel" 		    , 0,	/* Bharti Cellular Ltd (planned)*/
	0x40494F,	(char *)"Airtel" 		    , 0,	/* Bharti Cellular Ltd (planned)*/
	0x40495F,	(char *)"Airtel" 		    , 0,	/* Bharti Cellular Ltd (planned)*/
	0x40496F,	(char *)"Airtel" 		    , 0,	/* Bharti Cellular Ltd (planned)*/
	0x40497F,	(char *)"Airtel" 		    , 0,	/* Bharti Cellular Ltd (planned)*/
	0x40498F,	(char *)"Airtel" 		    , 0,	/* Bharti Cellular Ltd (planned)*/
	0x40551F, 	(char *)"Airtel"			, 0,	/* Bharti Cellular Ltd (planned)*/
	0x40552F,	(char *)"Airtel"			, 0,	/* Bharti Cellular Ltd (planned)*/
	0x40553F, 	(char *)"Airtel"			, 0,	/* Bharti Cellular Ltd (planned)*/
	0x40554F, 	(char *)"Airtel"			, 0,	/* Bharti Cellular Ltd (planned)*/
	0x40555F, 	(char *)"Airtel"			, 0,	/* Bharti Cellular Ltd (planned)*/
	0x40556F, 	(char *)"Airtel"			, 0,	/* Bharti Cellular Ltd (planned)*/
	0x40566F, 	(char *)"Vodafone"			, 0,	/* Bharti Cellular Ltd (planned)*/
	0x40567F, 	(char *)"Vodafone"			, 0,	/* Bharti Cellular Ltd (planned)*/
	0x405800, 	(char *)"Aircel"			, 0,	/* Aircel*/
	0x405801, 	(char *)"Aircel"			, 0,	/* Aircel*/
	0x405803, 	(char *)"Aircel"			, 0,	/* Aircel*/
	0x405805, 	(char *)"Aircel"			, 0,	/* Aircel*/
	0x405809, 	(char *)"Aircel"			, 0,	/* Aircel*/	
	0x405810, 	(char *)"Aircel"			, 0,	/* Aircel*/	
	0x405811, 	(char *)"Aircel"			, 0,	/* Aircel*/		
	
	/* Pakistan			  */
	0x41001F,	(char *)"Mobilink"		    , 0,	/* Mobilink			   			*/
	0x41003F,	(char *)"PK-UFONE"		    , 0,	/* Pak Telecom Mobile Ltd 		*/
	0x41004F, (char *)"PAKTEL"			    , 0,	/* Paktel Limited					*/
	0x41006F, (char *)"TELENOR"			    , 0,	/* Telenor Pakistan (Pvt) Ltd.		*/
	0x41007F, (char *)"WaridTel" 		    , 0,	/* Warid Telecom (PVT) Ltd			*/

	/* Afghanistan 		  */		  
	0x41201F,	(char *)"AF AWCC"		    , 0,	/* Telephone Systems Inter-		*/
	0x41220F,	(char *)"ROSHAN"			    , 0,	/* Telecom Development Comp-	*/

	/* Sri Lanka			  */
	0x41301F,	(char *)"Mobitel"			, 0,	/* Mobitel (Pvt) Limited    	*/
	0x41302F,	(char *)"DIALOG" 			, 0,	/* MTN Networks (Pvt) Ltd 		*/
	0x41303F,	(char *)"CELLTEL"			, 0,	/*Celltel Lanka Limited    		*/
	0x41308F,	(char *)"Hutch"			    , 0,	/*Hutchison Telecommunications Lanka (Pte) Limited  */
	
	/*Myanmar				 */ 		 
	0x41401F,	(char *)"MPTGSM"			    , 0,	/* Myanmar Posts and Telecommunications */

	/* Lebanon 			  */
	0x41501F,	(char *)"alfa"			    , 0,	/* FTML 				  	 	*/
	0x41503F,	(char *)"MTC LIBAN"		    , 0,	/* LibanCell		   			*/

	/* Jordan				  */
	0x41601F,	(char *)"FSTLNK"			    , 0,	/* Jordan Mobile Telephone Services (JMTS)*/
	0x41603F,	(char *)"UMNIAH"			    , 0,	/* Jordan Mobile Telephone Services (JMTS)*/
	0x41677F,	(char *)"JO MobCom"		    , 0,	/* Petra Jordanian Mobile- 		*/

	/* Syria				  */
	0x41701F,	(char *)"SYRIATEL"		    , 0,	/* Syriatel Mobile Teleco-  		*/
	0x41702F,	(char *)"areeba"			    , 0,	/* Spacetel Syria (94)			*/
	0x41709F,	(char *)"MOBILE"			    , 0,	/* Syrian Telecom Est.			*/

	/* Iraq				 */
	0x41800F,	(char *)"ASIACELL"		    , 0,	/* Asia Cell Telecommunications */
	0x41802F,	(char *)"SanaTel" 		    , 0,	/* Sanatel				 		*/
	0x41805F,	(char *)"ASIACELL"		    , 0,	/* Asia Cell Telecommunications */
	0x41808F,	(char *)"SanaTel" 		    , 0,	/* Sanatel				 		*/
	0x41820F,	(char *)"ATHEER" 			, 0,	/* Atheer Telecom Iraq	 		*/
	0x41830F,	(char *)"IRAQNA" 			, 0,	/* Orascom Telecom Iraq Corporation	*/

	/* Kuwait 			   */
	/*0x41902F,	(char *)"MTC"				, 0,	*//* Mobile Telecoms Co.	 		*/
	0x41902F,	(char *)"Zain"				, 0,
	0x41903F,	(char *)"Wataniya"		    , 0,	/* National Mobile Telecom- 	*/
	0x41904F,	(char *)"VIVA"				, 0,
	/* Saudi Arabia		  */
	0x42001F,	(char *)"STC"				, 0,	/* Saudi Telecom Company (ST-	*/
	0x42003F,	(char *)"Mobily"	, 0,		/* Etihad Etisalat Company. */
	0x42004F,	(char *)"Zain"				, 0,		/* Zain SA */
	/*0x42007F,  (char *)"EAE-ALJAWWAL" 	, 0, */ 	/* Electronics App' Est.   		*/

	/*Yemen				 */ 		  
	0x42101F,	(char *)"SabaFon"			, 0,	/* Yemen Mobile Phone Company "-*/
	0x42102F,	(char *)"Spacetel"		    , 0,	/* Spacetel - Yemen		   		*/

	/* Oman				 */
	0x42202F,	(char *)"OMAN"			    , 0,	/*Oman Telecommunications Company*/
	0x42203F,	(char *)"nawras"			    , 0,	/*Omani Qatari Telecommunications Company SAOC */
	
	/* United Arab Emirates */
	0x42402F,	(char *)"Etisalat"			    , 0,	/* Etisalat 					*/
	0x42403F,	(char *)"DU"			    , 0,
	/*Israel				*/
	0x42501F,	(char *)"IL ORANGE" 			, 0,	/* Partner Communications Compa-*/
	0x42502F,	(char *)"Cellcom"			, 0,	/* Cellcom Israel Ltd		 	*/

	/*Palestinian Authority */
	0x42505F,	(char *)"JAWWAL" 			, 0,	/* Palestine Telecoms Co Plc 	*/

	/* Bahrain 			 */
	0x42601F,	(char *)"BATELCO" 		    , 0,	/* Batelco					 	*/
	0x42602F,	(char *)"MTC-VFBH"		    , 0,	/* MTC Vodafone (Bahrain) B.S.C.(planned)*/

	/* Qatar				 */
	0x42701F,	(char *)"Q-tel"			    , 0,	/* Q-Tel					 	*/
	0x42702F,	(char *)"Vodafone"			    , 0,	/* Q-Tel                    */

	/*Mongolia			 */
	0x42899F,	(char *)"MobiCom"			, 0,	/* MobiCom					 	*/

	/*Nepal				 */ 		 
	0x42901F,	(char *)"Nepal Telcom" 	    , 0,	/* Nepal Telecommunications Cor- */

	/* Iran 				 */
	0x43211F,	(char *)"432 11" 			, 0,	/* TCI							*/
	0x43214F,	(char *)"KIFZO"			    , 0,	/* KIFZO.					 	*/
	0x43219F,	(char *)"MTCE   "			, 0,	/* Mobile Telecommunicatio- 	*/
	0x43232F,	(char *)"Taliya"			    , 0,	/* Mobile Telecommunicatio- 	*/
	
	/* Uzbekistan			 */
	/*0x43401F,  (char *)"Buztel"			, 0,*/	/* Buztel					 	*/
	0x43402F,	(char *)"UZMGSM" 			, 0,	/* Uzmacom					 	*/
	0x43404F,	(char *)"DW-GSM" 			, 0,	/* Daewoo Central Paging -	 	*/
	0x43405F,	(char *)"COSCOM" 			, 0,	/* Coscom					 	*/
	0x43407F,	(char *)"UZDGSM" 			, 0,	/* Uzdunrobita JV   (planned)	*/

	/*Tajikistan			 */   
	0x43601F,	(char *)"Somoncom"		    , 0,	/* JSC Somoncom				 	*/
	0x43602F,	(char *)"Indigo-T" 			, 0,	/* Indigo Tajikistan 			*/
	0x43603F,	(char *)"MLT"				, 0,	/* TT Mobile, Closed joint-stock*/
	0x43604F,	(char *)"Babilon"			, 0,	/* JOSA Babilon-Mobile		 	*/
	0x43605F,	(char *)"TajikTel"		    , 0,	/* Tajik Tel 					*/

	/* Kyrgyzstan			 */
	0x43701F,	(char *)"BITEL"			    , 0,	/* Bitel Limited			 	*/

	/*Turkmenistan 		 */ 		  
	0x43801F,	(char *)"BCTI"			    , 0,	/* Barash Communication Technolog-*/

	/*Japan				 */ 		  
    0x44000F,   (char *)"EMOBILE"           , 0,    /* EMobile                      */
	0x44010F,	(char *)"docomo" 			, 0,	/* NTT DoCoMo, Inc			 	*/
	0x44020F,	(char *)"SoftBank"			, 0,	/* Vodafone K.K. 			 	*/

	/*South Korea				 */ 		  
	0x45002F,	(char *)"KR KTF"				, 0,	/* KT Freetel Co., Ltd		 	*/
	0x45005F,	(char *)"SKT"				, 0,	/* SK Telecom		 			*/
    0x45006F,	(char *)"LGUPLUS"				, 0,	/* SK Telecom		 			*/
	0x45008F,	(char *)"KTF"				, 0,	/* KT Freetel Co., Ltd		 	*/

	/* Vietnam 			 */
	0x45201F,	(char *)"VMS" 			    , 0,	/* Vietnam Mobile Telecom-	 	*/
	0x45202F,	(char *)"GPC"				, 0,	/* Vietnam Telecoms Services	*/
	0x45204F,	(char *)"VIETTEL"			, 0,	/* Vietel Corporation		 	*/

	/* ------------ Start chinese network operator names ------------ */

	/* Hong Kong			 */
	0x45400F,	(char *)"CSL" 			    , 0,	/* Hong Kong CSL Limited  		*/
	0x45402F,	(char *)"CSL" 			    , 0,	/* Hong Kong CSL Limited  		*/
	0x45403F,	(char *)"3"				    , 0,	/* Hutchison Telecom (HK) 		*/
	0x45404F,	(char *)"3(2G)" 			    , 0,	/* Hutchison Telecom (HK) 		*/
	0x45406F,	(char *)"SMC-Voda"		    , 0,	/* SmarTone Mobile Comms -		*/
	0x45410F,	(char *)"NWPCS"			    , 0,	/* New World PCS	   			*/
	0x45412F,	(char *)"PEOPLES" 		    , 0,	/* China Resources Peoples Telephone Company-*/
	0x45415F,	(char *)"454-15"			    , 0,	/* SmarTone Mobile Communications-*/
	0x45416F,	(char *)"SUNDAY"		        , 0,	/* Sunday Communications-		*/
	0x45418F,  (char *)"CSL"				    , 0,	 /* Hong Kong CSL Limited		*/
	0x45419F,	(char *)"SUNDAY"			    , 0,	/* add by lzy 050201			*/
	/*0x45420F,  (char *)"HK HUTCHISON" 	    , 0, */ 	/* Hutchison Telecom (HK)-		*/
	/*0x45422F,  (char *)"HK P PLUS"		    , 0, */ 	/* P Plus Communications  		*/

	/* Macao				 */
	0x45500F,	(char *)"SmarTone"		    , 0,	/* SmarTone Mobile Commun-		*/
	0x45501F,	(char *)"CTM" 			    , 0,	/* C.T.M.			   			*/
	0x45503F,	(char *)"HT Macau"		    , 0,	/* Hutchison Telephone			*/

	/* Kampuchea (Cambodia) */
	0x45601F,	(char *)"MT-KHM" 			, 0,	/* CamGSM			   			*/
	0x45602F,	(char *)"KHM-SM" 			, 0,	/* Cambodia Samart Communi-		*/
	0x45618F,	(char *)"CAMSHIN"			, 0,	/* Cambodia Shinawatra			*/

	/* Laos 				*/
	0x45701F,	(char *)"LAO GSM"			, 0,	/* Lao Shinawatra	   			*/
	0x45702F,	(char *)"ETLMNW" 			, 0,	/* Enterprise of Telecommunications Lao */
	0x45703F,	(char *)"LATMOBIL"		    , 0,	/* Lao Asia Telecommunication-	*/
	0x45708F,	(char *)"TANGO LAO"			, 0,	/* Millicom Lao Co Ltd			*/

	/* China				 */
	0x46000F,	(char *)"China Mobile"       , 0,	/* China Mobile Communication Corp */
	0x46001F,	(char *)"China Unicom"       , 0,	/* China Unicom 		   		*/
	0x46002F,   (char *)"China Mobile" 	     , 0, 	/* China Mobile Communication Corp 	*/
    0x46003F,   (char *)"China Telecom"       , 0,	/* China Telecom  */
	0x46006F,   (char *)"China Unicom"       , 0,	/* China Unicom  */
	0x46007F,	(char *)"China Mobile"       , 0,	/* China Mobile 		   		*/
  
	/* Taiwan				 */
	0x46601F,	(char *)"Far EasTone"        , 0,	/* Far EasTone Telecoms    		*/
	/*0x46606F,  (char *)"TWN Tuntex GSM 1800" ,0,*/	/* TUNTEX Telecom 				*/
	0x46668F,   (char *)"ACeS"               , 0,  	/* ACeS International Limited (AIL)	*/
	0x46688F,	(char *)"KGT"                , 0,	/* KG Telecom		   			*/
	0x46689F,	(char *)"VIBO"               , 0,	/* Taiwan 3G Mobile Netwo- (planned) */
	0x46692F,	(char *)"Chunghwa"           , 0,	/* Chunghwa Telecom LDM    		*/
	0x46693F,	(char *)"Mobitai"            , 0,	/* Mobitai Communications-		*/
	0x46697F,	(char *)"TWN"            , 0,	/* Pacific Cellular Corp -		*/
	0x46699F,	(char *)"Trans Asia"         , 0,/* TransAsia		   			*/
	
	/* ------------ End  chinese network operator names ------------- */

	/*North Korea			 */ 		 
	0x467193,  (char *)"SUNNET"			, 0,	/*NEAT&T Ltd.		   			*/

	/* Bangladesh			 */
	0x47001F,	(char *)"BGD-GP" 		, 0,	/* GrameenPhone Ltd 		 	*/
	0x47002F,	(char *)"BGD AKTEL"		, 0,	/* TM Int'l (Bangladesh) Ltd 	*/
	0x47003F,	(char *)"Banglalink"	, 0,	/* Sheba Telecom (PVT) Ltd		*/
	0x47004F,	(char *)"bMobile"		, 0,	/* Sheba Telecom (PVT) Ltd		*/
	/*0x47019F,  (char *)"M2K"			, 0, */ /* Sheba Telecom (PVT) Lt-	 	*/

	/*Maldives			 */    
	0x47201F,	(char *)"D-MOBILE"		, 0,	/* Dhivehi Raajjeyge Gulhun P-	*/ 
	0x47202F,	(char *)"WMOBILE"		, 0,	/* Dhivehi Raajjeyge Gulhun P-	*/
	
	/* Malaysia			 */
	0x50212F,	(char *)"Maxis"		, 0,	/* Maxis Communications Berh-	*/
	0x50213F,	(char *)"CELCOM"		, 0,	/* Maxis Communications Berh-	*/
	0x50216F,	(char *)"DiGi"			, 0,	/* Digi Telecommunications Sd-	*/
	0x50218F,   (char *)"U mobile"      , 0,    /* U mobile                     */
	0x50219F,	(char *)"Celcom"		, 0,	/* Celcom (Malaysia) Sdn - 		*/

	/* Australia			 */
	0x50501F,	(char *)"Telstra Mobile", 0,	/* Telstra Mobile Comms 	  	*/
	0x50502F,	(char *)"OPTUS"			, 0,	/* Singtel Optus Limited   		*/
	0x50503F,	(char *)"voda AU"		, 0,	/* Vodafone Pacific Limited 		*/
	0x50506F,	(char *)"3TELSTRA"		, 0,	/* Hutchison 3G Australia- (planned)*/

	/* Indonesia			 */
	0x51000F,  (char *)"ACeS" 			, 0, 	/* ACeS International Limited (AIL)	*/
	0x51001F,	(char *)"INDOSAT"	    , 0,	/* PT. Satelindo			 	*/
	0x51008F,	(char *)"AXIS"		    , 0,	/* PT Natrindo Telepon Seluler	changed*/	
    0x51010F,	(char *)"TELKOMSEL"		, 0,   	/* PT Telekomunikasi Selular 	*/
	0x51011F,	(char *)"XL"			, 0,	/* PT. Excelcomindo 			*/
	0x51021F,	(char *)"ISAT 3G"		, 0,	/* PT Indonesian Satellite Cor- *//*"INDOSAT"*/
	0x51089F,	(char *)"3"				, 0,	/* 3 Indonesia,GSM&UMTS */

	/* East Timor	 */
	0x51402F,	(char *)"TT" 			, 0,	/* Timor Telecom				*/

	/* Philippines 		 */
	0x51501F,	(char *)"ISLACOM"		, 0,	/* Isla Comms Co. Inc.			*/
	0x51502F,	(char *)"GLOBE"			, 0,	/* Globe Telecom, GMCR Inc 		*/
	0x51503F,	(char *)"Smart"			, 0,	/* Smart Communications Inc		*/
	0x51505F,	(char *)"SUN"			, 0,	/* Digital Telecommunicat-(planned)*/
    0x51509F,	(char *)"Smart"			, 0,	/* Smart Communications Inc		*/
	0x51511F,   (char *)"ACeS" 			, 0,  	/* ACeS International Limited (AIL)	*/

	/* Thailand			 */
	0x52001F,	(char *)"AIS" 		, 0,	/* Advanced Info Service - 		*/
	0x52010F,  (char *)"WCS IQ"			, 0,  /* WCS						 	*/
	0x52015F,	(char *)"TOT"		, 0,	/* TOT			*/
	0x52018F,	(char *)"DTAC"		, 0,	/* DTAC  		*/
	0x52020F,   (char *)"ACeS" 			, 0,  	/* ACeS International Limited (AIL)	*/
	0x52023F,	(char *)"GSM 1800"		, 0,	/* Digital Phone Co Ltd 		*/
	0x52099F,	(char *)"TRUE" 	, 0,	/* TA Orange Company Ltd  (planned)*/

	/* Singapore			  */
	0x52501F,	(char *)"SingTel 4G" 		, 0,	/* Singapore Telecom GSM -		*/
	0x52502F,	(char *)"SingTel"		, 0,	/* Singapore Telecom GSM -		*/
	0x52503F,	(char *)"M1-3GSM" 		, 0,	/* MobileOne (Asia) Pte L-		*/
	0x52505F,	(char *)"STARHUB"		, 0,	/* STARHUB Singapore GSM- 		*/	  

	/* Brunei Darussalam	  */
	/*0x52801F,  (char *)"528 01"			, 0, */ /* Jabatan Telecom		   		*/
	0x52802F,	(char *)"b-mobile" 		, 0,	/* DST Communications  			*/
	0x52811F,	(char *)"DSTCom" 		, 0,	/* DST Communications  			*/

	/* New Zealand 		  */
	0x53001F,	(char *)"voda NZ"		, 0,	/* Vodafone New Zealand L		*/

	/*Papua New Guinea 	 */
	0x53701F,	(char *)"BMobile" 		, 0,	/* Pacific Mobile Comms 		*/

	/*Tonga				  */
	0x53901F,	(char *)"U-CALL" 		, 0,	/* Tonga Communications Co-		*/
	0x53943F,	(char *)"Shoreline"		, 0,	/* Shoreline Communications-	*/

	/*Vanuatu				   */
	0x54101F,	(char *)"SMILE"			, 0,	/* Telecom Vanuatu Ltd	 		*/

	/* Fiji				   */
	0x54201F,	(char *)"VODAFONE"		, 0,	/* Vodafone Fiji Limited  		*/

	/*American Samoa		 */ 		 
	/*0x54411F,  (char *)"Blue Sky" 	, 0, */ 	/* Blue Sky Communications		*/

	/* Kiribati			*/
	0x54509F,	(char *)"KI-FRIG"		, 0,	/* Telecom Services Kiribati Limited */

	/* New Caledonia		   */
	0x54601F,	(char *)"MOBNCL"		, 0,	/* OPT New Caledonia			*/

	/* French Polynesia	   */
	0x54720F,	(char *)"F-VINI"		, 0,	/* Tikiphone SA 				*/

	/* Cook Islands		   */
	0x54801F,	(char *)"KOKANET"		, 0,	/* Telecom Cook Islands 		*/

	/*Micronesia,the Federated States of	 */ 		 
	0x55001F,	(char *)"FSMTC"			, 0,	/* FSM Telecommunications Corp- */

	/* Egypt				   */
	0x60201F,	(char *)"MobiNiL"		, 0,	/* The Egyptian Company for Mobile Services */
	0x60202F,	(char *)"voda EG"		, 0,	/* Vodafone Egypt Telecommunications S.A.E.-*/

	/* Algeria 			   */
	0x60301F,	(char *)"Mobilis"		, 0,	/* Algerie Telecom	   			*/
	0x60302F,	(char *)"Djezzy" 		, 0,	/* Orascom Telecom Algerie-		*/
	0x60303F,	(char *)"DZA-WTA" 		, 0,	/* Wataniya Telecom Algerie		*/
	
	/* Morocco 				*/
	0x60400F,	(char *)"MEDITEL"		, 0,	/* Medi Telecom (Meditel) 		*/
	0x60401F,	(char *)"IAM" 			, 0,	/* Itissalat Al-Maghrib S-		*/

	/* Tunisia 				*/
	0x60502F,	(char *)"TUNTEL "		, 0,	/* Tunisie Telecom	   			*/
	0x60503F,	(char *)"TUNSIANA"		, 0,	/* Orascom Telecom Tunisie (planned)	*/

	/* Libya	*/
	0x60600F,	(char *)"LIBYANA"		, 0,	/* El Madar Telephone Co. 		*/	
	0x60601F,	(char *)"El Madar"		, 0,	/* El Madar Telephone Co. 		*/

	/*Gambia					*/		  
	0x60701F,	(char *)"GAMCEL"		, 0,	/* Gambia Telecommunication-	*/
	0x60702F,	(char *)"AFRICELL"		, 0,	/* Africell (Gambia) Ltd 		*/

	/* Senegal 				*/
	0x60801F,	(char *)"ALIZE"			, 0,	/* Sonatel						*/
	0x60802F,	(char *)"SENTEL" 		, 0,	/* Sentel GSM					*/

	/*Mauritania				*/		  
	0x60901F,	(char *)"MATTEL" 		, 0,	/* MATTEL						*/
	0x60910F,	(char *)"MAURITEL"		, 0,	/* MAURITEL						*/

	/*Mali					*/
	0x61001F,	(char *)"MALITEL ML"	, 0,	/* Malitel-SA					*/
	0x61002F,	(char *)"IKATEL" 		, 0,	/* IKATEL SA 	 (planned)		*/

	/* Guinea					 */
	0x61101F,	(char *)"Mobilis"		, 0,	/* Spacetel Guinee SA			*/
	0x61102F,	(char *)"LAGUI"			, 0,	/* Sotelgui SA					*/

	/* Cote d'Ivoire			 */
	0x61201F,	(char *)"CORA"			, 0,	/* CORA de COMSTAR				*/
	0x61203F,	(char *)"Orange CI" 	, 0,	/* Orange Cote D'Ivoire-   		*/
	0x61205F,	(char *)"TELECEL"		, 0,	/* Loteny Telecom (SA)			*/

	/* Burkina Faso			 */
	/*0x61301F,  (char *)"Onatel"			, 0, */ /* Onatel					 	*/ 				  
	0x61302F,	(char *)"Zain" 			, 0,	/* Celtel Burkina Faso			*/
	0x61303F,	(char *)"Telecel Faso SA"	, 0,	/* Telecel Faso SA				*/

	/*Niger					 */ 	
	0x61401F,	(char *)"SAHELCOM"		, 0,	/* Celtel Niger	(planned)  		*/
	0x61402F,	(char *)"CELTEL"		, 0,	/* Celtel Niger	(planned)  		*/
	0x61403F,	(char *)"TELECEL"		, 0,	/* TELECEL Niger SA	   			*/

	/* Togo					 */
	0x61501F,	(char *)"TGCELL"		, 0,	/* Togo Telecom 				*/

	/*Benin					 */ 	  
	0x616004,  	(char *)"BBCOM"			, 0,	/* Bell Benin Communications*/
	0x61601F,	(char *)"LIBERCOM"		, 0,	/* LIBERCOM						*/
	0x61602F,	(char *)"TLCL-BEN"		, 0,	/* Telecel Benin Ltd 			*/
	0x61603F,	(char *)"BENCELL"		, 0,	/* Spacetel-Benin				*/
	0x61604F,	(char *)"BBCOM"			, 0,	/* Bell Benin Communications (BBCOM)				*/

	/* Mauritius			   */
	0x61701F,	(char *)"CELL +"		, 0,	/* Cellplus Mobile Comms   		*/
	0x61710F,	(char *)"EMTEL"			, 0,	/* Emtel Ltd					*/

	/* Liberia 			   */
	0x61801F,	(char *)"LoneStar"		, 0,	/* Lonestar Communications Corporation*/
	0x61802F,	(char *)"LIBERCEL"		, 0,	/* Atlantic Wireless (Liberia) Inc. 	*/
	0x61803F,	(char *)"Celcom"		, 0,	/* Celcom Telecommunications, Inc		*/
	
	/*Sierra Leone 		   */		   
	0x61901F,	(char *)"CELTEL" 		, 0,	/* Celtel (SL) Limited			*/
	0x61902F,	(char *)"MILLICOM" 		, 0,		/* Millicom SL				*/
	
	/* Ghana				  */
	0x62001F,	(char *)"SPACE"			, 0,	/* ScanCom Ltd					*/
	0x62002F,	(char *)"GH ONEtouch"	, 0,	/* Ghana Telecommunicatio- 		*/
	0x62003F,	(char *)"mobitel"		, 0,	/* Millicom Ghana Limited  		*/

	/*Nigeria				*/			
	0x62120F,	(char *)"VMOBILE" 		, 0,	/* Econet Wireless Nigeria -		*/
	0x62126F,	(char *)"SWIFT NETWORKS" 		, 0,	/* SWIFT 4G BROADBAND		*/
	0x62130F,	(char *)"MTN-NG" 		, 0,	/* MTN Nigeria Communication-	*/
	0x62140F,	(char *)"Mtel"			, 0,	/* Nigerian Telecommunicatio-	*/
	0x62150F,	(char *)"GLO NG"		, 0,	/* Globacom Ltd. 			 	*/

	/*Chad 				 */ 	   
	0x62201F,	(char *)"Zain" 			, 0,	/* CelTel Tchad SA		 		*/
	0x62202F,	(char *)"LIBERTIS"		, 0,	/* Tchad Mobile SA			 	*/

	/* Cameroon			 */ 
	0x62401F,	(char *)"62401"			, 0,	/* MTN Cameroon Ltd 		 	*/
	0x62402F,	(char *)"Orange" 		, 0,	/* Orange Cameroun S.A. 	 	*/

	/* Cape Verde			 */
	0x62501F,	(char *)"CPV MOVEL"		, 0,	/* Cabo Verde Telecom	 		*/

	/*Sao Tome  and Principe*/ 		 
	0x62601F,	(char *)"CSTmovel"		, 0,	/* Companhia Santomense de Te-	*/ 

	/*Equatorial Guinea	 */ 		 
	0x62701F,	(char *)"GETESA" 		, 0,	/* GETESA				  		*/

	/*Gabon,Republic of	  */		  
	0x62801F,	(char *)"LIBERTIS"		, 0,	/* Libertis S.A. 			  	*/
	0x62802F,	(char *)"TELECEL"		, 0,	/* Telecel Gabon SA. 		  	*/
	0x62803F,	(char *)"CELTEL GA" 	, 0,	/* Celtel Gabon SA		  		*/

	/*Congo				  */ 
	0x62901F,	(char *)"CELTEL" 		, 0,	/* CelTel Congo SA		   		*/
	0x62910F,	(char *)"LIBERTIS"		, 0,	/* Libertis Telecom		   		*/

	/*Congo,democratic Republic of */			
	0x63001F,	(char *)"VODACOM CD"	, 0,	/* Vodacom Congo (RDC) sprl		*/
	0x63002F,	(char *)"CELTEL" 		, 0,	/* Celtel Congo			   		*/
	0x63004F,	(char *)"CELLCO" 		, 0,	/* CELLCO Sarl  (planned)		*/
	0x63089F,	(char *)"OASIS"			, 0,	/* SAIT Telecom SPRL (OASIS)    */

	/*Angola				 */ 		 
	0x63102F,	(char *)"UNITEL" 		, 0,	/* UNITEL S.a.r.l.		   		*/
	
	/* GNB */
	0x63207F,	(char *)"GTM" 			, 0,	/* UNITEL S.a.r.l.		   		*/

	/* Seychelles			 */
	0x63301F,	(char *)"C&W SEY"		, 0,	/* Cable + Wireless 			*/
	0x63302F,	(char *)"SmartC"		, 0,	/* Cable + Wireless 			*/
	0x63310F,	(char *)"AIRTEL" 		, 0,	/* Telecom (Seychelles Li-		*/

	/* Sudan				 */
	0x63401F,	(char *)"MobiTel"		, 0,	/* Mobile Telephone Compa		*/
	0x63402F,	(char *)"areeba"		, 0,	/* Mobile Telephone Compa		*/
	
	/* Rwanda				 */
	0x63510F,	(char *)"RCELL"			, 0,	/* Rwandacell SARL				*/

	/* Ethiopia			 */
	0x63601F,	(char *)"ET-MTN"		, 0,	/* Ethiopian Telecoms Aut-		*/

	/*Somalia				*/			
	0x63701F,	(char *)"TELESOM"		, 0,	/* Telesom Company				*/
	0x63704F,	(char *)"SOMAFONE" 		, 0,	/* Somafone FZLLC 				*/
	0x63710F,	(char *)"Nationlink" 	, 0,	/* Nationlink					*/	
	0x63730F,	(char *)"Golis"			, 0,	/* Golis Telecommunications Company Ltd	*/
	0x63782F,	(char *)"Telsom "		, 0,	/* Telsom Mobile Somalia 		*/

	/*Djibouti, Republic of */ 
	0x63801F,	(char *)"EVATIS" 		, 0,	/* Djibouti Telecom SA			*/

	/*Kenya			   */ 
	0x63902F,	(char *)"SAF-COM"		, 0,	/* Safaricom Limited 			*/
	0x63903F,	(char *)"CELTEL"		, 0,	/* Kencell Communications Ltd   	*/

	/* Tanzania			 */
	/*0x64001F,  (char *)"Tritel-TZ"	, 0, */ 	/* Tritel (T) Ltd			 		*/
	0x64002F,	(char *)"MOBITEL"		, 0,	/* MIC Tanzania Ltd 			*/
	0x64003F,	(char *)"ZANTEL"		, 0,	/* Zanzibar Telecom (ZANTEL)   	*/
	0x64004F,	(char *)"VodaCom"		, 0,	/* Vodacom Tanzania Ltd 		*/
	0x64005F,	(char *)"CELTEL" 		, 0,	/* Celtel Tanzania Limited		*/

	/* Uganda				*/
	0x64101F,	(char *)"CELTEL"		, 0,	/* Clovergem Celtel 			*/
	0x64110F,	(char *)"MTN-UG" 		, 0,	/* MTN Uganda					*/
	0x64111F,	(char *)"UTL-Mango"		, 0,	/* Uganda Telecom Ltd (UTL Tel-	*/

	/*Burundi			   */		 
	0x64201F,	(char *)"SPACETEL"		, 0,	/* Spacetel - Burundi		 	*/
	0x64202F,	(char *)"SAFARIS"		, 0,	/* Africell PLC Company		 	*/
	0x64203F, 	(char *)"ONATEL"		, 0,	/* ONATEL						*/
	0x64282F, 	(char *)"BDITL"			, 0,	/* Telecel-Burundi Company 		*/
	
	/* Mozambique			 */
	0x64301F,	(char *)"MOZ-mCel"		, 0,	/* Mocambique Celular Ltd	 	*/
	0x64304F,	(char *)"VodaCom-MZ"	, 0,	/* VM, S.A.R.L. 				*/

	/* Zambia				 */
	0x64501F,	(char *)"CELTEL" 		, 0,	/* Celtel Zambia Limited 	 	*/
	0x64502F,	(char *)"TELECEL" 		, 0,	/* Telecel Zambia Limited	 	*/

	/* Madagascar			 */
	0x64601F,	(char *)"Madacom"		, 0,	/* Madacom SA				 	*/
	0x64602F,	(char *)"MG ANTARIS"	, 0,	/* Orange Madagascar S.A 		*/
	/*0x64603F,  (char *)"Sacel"			, 0, */ 	/* Sacel Madagascar S.A.	  	*/

	/* Reunion (La)		 */
	0x64700F,	(char *)"Orange"		, 0,	/* Orange Reunion			 	*/
	0x64702F,	(char *)"Outremer Telecom", 0,	/* Outremer Telecom (planned)	*/
	0x64710F,	(char *)"SFR RU"		, 0,	/* Societe Reunionnaise 		*/

	/* Zimbabwe			 */
	0x64801F,	(char *)"NETONE"		, 0,	/* Net*One Cellular (Pvt) Ltd	*/
	0x64803F,	(char *)"TELECEL"		, 0,	/* Telecel Zimbabwe (PVT)-	 	*/
	0x64804F,	(char *)"ECONET"		, 0,	/* Econet Wireless (Privat-	 	*/

	/* Namibia 			 */
	0x64901F,	(char *)"MTC" 		, 0,	/* MTC						 	*/

	/* Malawi				 */
	0x65001F,	(char *)"MW CP 900"		, 0,	/* Telekom Network Ltd		 	*/
	0x65010F,	(char *)"CELTEL" 		, 0,	/* CelTel Limited			 	*/

	/* Lesotho 			 */
	0x65101F,	(char *)"Vodacom-LS" 	, 0,	/* Vodacom Lesotho (Pty) -	 	*/
	0x65102F,	(char *)"ETL"		    , 0,	/* Econet Ezi Cel Lesotho	 	*/

	/* Botswana			 */
	0x65201F,	(char *)"MASCOM" 		, 0,	/* Mascom Wireless			 	*/
	0x65202F,	(char *)"Orange"		, 0,	/* Orange (Botswana) Pty Limited */

	/*Swaziland			 */ 		 
	0x65310F,	(char *)"SwaziMTN"		, 0,	/* Swazi MTN (Ltd)			 	*/

	/*Comoros				 */ 		 
	0x654001,  	(char *)"HURI" 			, 0,	/* Societe Nationale des Postes- */

	/* South Africa		 */
	0x65501F,	(char *)"VODA"			, 0,	/* Vodacom (Pty) Ltd		 	*/
	0x65507F,	(char *)"Cell C" 		, 0,	/* Cell C (Pty) Ltd 			*/
	0x65510F,	(char *)"MTN" 		, 0,	/* MTN (Pty) Ltd			 	*/

	/*Belize				 */ 	
	0x70267F,	(char *)"BTL"			, 0,	/* Belize Telecommunications Ltd (planned)	*/ 
	0x70268F,	(char *)"INTELCO"		, 0,	/* International Telecommunication Limited	*/ 

	/*Guatemala			 */ 
	0x70401F,	(char *)"Claro GTM" 	, 0,	/* SERCOM S.A. (Guatemala)	  			*/ 
	0x70402F,	(char *)"TIGO" 		    , 0,	/* COMCEL-Communicaciones Celulares- 	  	*/ 
	0x70403F, 	(char *)"movistar"		, 0,	/* Telefonica Moviles Guatamala, SA		*/ 
	
	/*El Salvador 		 */ 		 
	0x70601F,	(char *)"Claro SLV"		, 0,	/* CTE Telecom Personal SA de-	*/
	0x70602F,	(char *)"Digicel"		, 0,	/* DIGICEL, S.A. de C.V. 	  	*/
	0x70603F,	(char *)"TIGO"			, 0,	/* Telemovil EL Salvador S.A   	*/
	0x70604F,   (char *)"movistar" 		, 0,	/* Telefonica Moviles El Salvador, S.A de c.v	*/

	/*Honduras			*/			
	0x708001,  	(char *)"Claro HND"		, 0,	/* Megatel S.V. de C.V.	  		*/
	0x70802F,	(char *)"TIGO" 		    , 0,	/* Telefonica Celular S.A   	*/
	0x70830F, 	(char *)"HT - 200"		, 0,	/* Empresa Hondurena de Telecomunicaciones HONDUTEL	*/

	/*Nicaragua			*/			
	0x710210,	(char *)"Claro" 		, 0,	/* Claro Nicaraguense de Te-	*/
	0x71021F,	(char *)"CLARO NICARAGUA" 	, 0,	/* Claro Nicaraguense de Te-	*/
	0x710300,	(char *)"MOVISTARNI" 		, 0,	/* movistar Nicaraguense de Te-	*/
	0x71030F,	(char *)"movistar" 		, 0,	/* movistar Nicaraguense de Te-	*/
	0x71073F,	(char *)"CLARO NICARAGUA" 	, 0,	/* Claro Nicaraguense de Te-	*/

	/*Costa Rica			*/	
    0x71200F,	(char *)"ICE Costa Rica" 		, 0,	/* I.C.E. (Instituto Costarr-	*/
	0x71201F,	(char *)"I.C.E." 		, 0,	/* I.C.E. (Instituto Costarr-	*/
	0x71202F,	(char *)"I.C.E." 		, 0,	/* I.C.E. (Instituto Costarr-	*/

	/*Panama,Republic of	*/			
	0x71401F,	(char *)"PANCW"			, 0,	/* Cable & Wireless Panama		*/
    0x714020,	(char *)"Movistar"		, 0,	/* movistar Panama		*/
	0x71402F,	(char *)"movistar"		, 0,	/* movistar Panama		*/
	0x71403F,   (char *)"Claro PA"      , 0,    /* Panama               */
  	0x71420F,	(char *)"movistar"		, 0,	/* TELEFONICA MOVILES PANAMA, S.A.	*/

	/*Peru				*/		
	0x71600F,	(char *)"Nextel 4G"		, 0,	/* movistar Peru S.A.C.		 		*/
    0x71606F,	(char *)"Movistar 4G LTE"		, 0,	/* movistar Peru S.A.C.		 		*/
	0x716100,	(char *)"Claro Pe"		, 0,	/* TIM Peru S.A.C.		 		*/
	0x71610F,	(char *)"Claro Pe"		, 0,	/* TIM Peru S.A.C.		 		*/

	/*Argentina		  */		  
	0x72207F,	(char *)"Movistar" 		, 0,	/* Telefonica Comunicaciones-	*/
	0x722310,	(char *)"Claro"		, 0,	/* CTI Compania de Telefonos-	*/
	0x72234F,	(char *)"Personal"		, 0,	/* Telecom Personal SA	 		*/
	0x722340,	(char *)"Personal Argentina"		, 0,	/* Telecom Personal SA	 		*/
	0x72235F,	(char *)"PORTHABLE"		, 0,	/* Hutchison Telecommunicati- (planned)*/
    0x72270F,	(char *)"Movistar"		, 0,	/* Hutchison Telecommunicati- (planned)*/
	/*Brazil				*/			
	0x72402F,	(char *)"TIM BR 02" 			, 0,	/* TIM Celular S.A.		  		*/
	0x72403F,	(char *)"TIM BR 03" 			, 0,	/* TIM Celular S.A.		  		*/
	0x72404F,	(char *)"TIM BR 04" 			, 0,	/* TIM Celular S.A.		  		*/
	0x72405F,	(char *)"Claro BR"			, 0,	/* ATL - Algar Telecom Leste- 	*/
    0x72406F,	(char *)"VIVO BR 06" 			, 0,	/* Vivo		  		*/
	0x72410F,	(char *)"VIVO BR 10" 			, 0,	/* Vivo	  		    */
	0x72411F,	(char *)"VIVO BR 11" 			, 0,	/* Vivo		  		*/
	0x72415F,	(char *)"BRA SCTL"		, 0,	/* Sercomtel Celular S/A 	  	*/
	0x72416F,	(char *)"BrT GSM" 		, 0,	/* Brasil Telecom Celular S.A 	*/ 
	0x72423F,	(char *)"VIVO BR 23"	, 0,	/* Brasil Telecom Celular S.A	*/ 
	0x72424F,	(char *)"AMAZONIA"		, 0,	/* Brasil Telecom Celular S.A	*/ 	
	0x72431F,	(char *)"Oi" 			, 0,	/* TNL PCS S.A.			  		*/
	0x72432F, 	(char *)"CTBC"			, 0,	/* Triangulo Celular S.A		*/
	0x72433F, 	(char *)"CTBC" 			, 0,	/* Triangulo Celular S.A		*/
	0x72434F, 	(char *)"CTBC" 			, 0,	/* Triangulo Celular S.A		*/
    0x72439F, 	(char *)"Nextel 3G" 			, 0,	/* Nextel Brasil 3G		*/

	/* Chile			   */
	0x73000F,	(char *)"Claro(4G)"		, 0,	/* Entel Telefonia Movil -		*/
	0x73001F,	(char *)"Entel"		, 0,	/* Entel Telefonia Movil -		*/
	0x73002F,	(char *)"Movistar" 		, 0,	/* Telefonica Movil de Chile  	*/
    0x73003F,   (char *)"Claro CHL"     , 0,    /* Claro CHL */
	0x73010F,	(char *)"ENTEL PCS CHILE"		, 0,	/* Entel PCS Telecomunica-		*/

	/* Colombia			 */
	0x732101,  	(char *)"Comcel 3GSM"   , 0,	/* Occidente y Caribe Celular- 	*/
	0x732103,  	(char *)"TIGO"		    , 0,	/* Colombia Movil SA			*/
	0x732111, 	(char *)"TIGO"			, 0,	/* Colombia Movil SA			*/
	0x732123, 	(char *)"movistar"		, 0,	/* Telefonica Moviles Colombia S.A.	*/
    0x732142, 	(char *)"UNE"		, 0,	/* Telefonica Moviles Colombia S.A.	*/

	/* Venezuela		   */
	0x73401F,	(char *)"DIGITEL"		, 0,	/* INFONET Redes de Infor-		*/
	0x73402F,	(char *)"DIGITEL"		, 0,	/* Corporacion Digitel C.A		*/
	0x73403F,	(char *)"DIGITEL"		, 0,	/* DIGICEL C.A. 				*/
    0x73404F,	(char *)"movistar"		, 0,	/* movistar Digitel C.A		*/
	0X73406F,   (char *)"Movilnet"      , 0,    /* Movilnet Digitel*/

	/*Bolivia 		   */  
	0x73601F,	(char *)"NUEVATEL"		, 0,	/* Nuevatel PCS De Bolivia SA   */
	0x73602F,	(char *)"Entel(4G)"		, 0,	/* Entel SA						*/
	0x736030,	(char *)"TIGO"		, 0,	/* Tigo Bolivia						*/
	0x73603F,	(char *)"TIGO"		, 0,	/* Tigo Bolivia						*/

	/*Guyana			   */  
	0x738001,  	(char *)"TWTGUY"		, 0,	/* Cel*Star Guyana Inc		 	*/
	0x73801F,   (char *)"U MOBILE"      , 0,    /* U Movile*/
	0x73802F, 	(char *)"CLNK PLS"		, 0,	/* Guyana Telephone & Telegraph Co.			*/
	
	/*Ecuador 			*/	
	0x74000F,	(char *)"Movistar"		, 0,	/* OTECEL S.A.				 	*/
	0x74001F,	(char *)"PORTA GSM"		, 0,	/* Conecel S.A.				 	*/
	0x74002F,	(char *)"Alegro"		, 0,	/* Conecel S.A.				 	*/

	/*Paraguay		   */		   
	0x74401F,	(char *)"VOX" 		    , 0,	/* Hola Paraguay S.A (VOX)	 	*/
	0x74402F,	(char *)"Claro PY"		, 0,	/* CTI Movil Paraguay*/
	0x74404F,	(char *)"TIGO"		    , 0,	/* Telefonica Celular Del Paragu-*/
	0x74405F, 	(char *)"Personal Paraguay"		, 0,	/* Nucleo S.A					*/

	/*Suriname 		   */		   
	/*0x74601F,  (char *)"ICMS" 			, 0,*/		/* NV ICMS (ICMS)				*/
	0x74602F,	(char *)"TeleG"			, 0,	/* Telesur (TELESUR.GSM) 		*/
	/* URY */
	0x74801F,	(char *)"ANCEL"			, 0,	/* ANCEL		*/
	0x74807F, 	(char *)"Movistar"		, 0,	/* Abiatar S.A.		*/
	0x74810F, 	(char *)"Claro UY"		, 0,	/* CTI Movil Uruguay S.A.		*/

	/*Suriname		   */		   
	0x79502F, 	(char *)"TM Cell"		, 0,	/* Altyn Asyr MC		*/
		
	/* URY */
	0x90105F, 	(char *)"Thuraya" 		, 0,	/* Thuraya Satellite Telecommunications Co 	*/
	0x90112F, 	(char *)"MCP"			, 0,	/* Maritime Communications Partner AS 	*/
};

/*add by qk*/
int match_oprer(lc_char* oper,lc_char *mccmnc)
{
    int nErr = FALSE;
     /*change plmn for hex to string*/
    lc_uint32 plmn_hex = 0;
	lc_uint32 devide_num = 65536;//16^4 ;
	lc_uint32 i = 0;
	lc_uint32 tmp = 0;
    lc_char  plmn_str[6] = {0};
    const PLMN_LIST* p_plmnlist = NULL;
    
    if(NULL == oper || NULL == mccmnc )
    {
        return nErr;
    }
	p_plmnlist = sPLMNList;
    while(0 != p_plmnlist->nCode)
    {
        plmn_hex = p_plmnlist->nCode;
        i = 0;
        devide_num = 1048576;
        memset(&plmn_str,0,sizeof(plmn_str));

        while(i<5 && devide_num!=15)
        {
	        tmp = plmn_hex/devide_num;
            plmn_str[i] = tmp + '0';
            plmn_hex = plmn_hex%devide_num;
            devide_num /= 16;
            i++;
        }
        plmn_str[i]='\0';
        if(0 == strncmp(plmn_str, mccmnc,strlen(plmn_str)))    
        {
            strcpy(oper,p_plmnlist->nVar);
            nErr = TRUE;
            break;
        }
        p_plmnlist++;
    }
    return nErr;
}



#ifdef WLAN_SUPPORT
#ifdef CONFIG_WIFI_SIMPLE_CONFIG//WPS
static void convert_bin_to_str(unsigned char *bin, int len, char *out)
{
	int i;
	char tmpbuf[10];

	out[0] = '\0';

	for (i=0; i<len; i++) {
		sprintf(tmpbuf, "%02x", bin[i]);
		strcat(out, tmpbuf);
	}
}


static int fnget_wpsKey(int eid, request * wp, int argc, char **argv, char *buffer) {
	unsigned char key, vChar, type;
	int mib_id;
	MIB_CE_MBSSIB_T Entry;

	wlan_getEntry(&Entry, 0);

	vChar = Entry.wsc_enc;
	buffer[0]='\0';
	if (vChar == WSC_ENCRYPT_WEP) {
		unsigned char tmp[100];
		vChar = Entry.wep;
		type = Entry.wepKeyType;
		key = Entry.wepDefaultKey; //default key
		if (vChar == 1) {
			if (key == 0)
				strcpy(tmp, Entry.wep64Key1);
			else if (key == 1)
				strcpy(tmp, Entry.wep64Key2);
			else if (key == 2)
				strcpy(tmp, Entry.wep64Key3);
			else
				strcpy(tmp, Entry.wep64Key4);
			
			if(type == KEY_ASCII){
				memcpy(buffer, tmp, 5);
				buffer[5] = '\0';
			}else{
				convert_bin_to_str(tmp, 5, buffer);
				buffer[10] = '\0';
			}
		}
		else {
			if (key == 0)
				strcpy(tmp, Entry.wep128Key1);
			else if (key == 1)
				strcpy(tmp, Entry.wep128Key2);
			else if (key == 2)
				strcpy(tmp, Entry.wep128Key3);
			else
				strcpy(tmp, Entry.wep128Key4);
			
			if(type == KEY_ASCII){
				memcpy(buffer, tmp, 13);
				buffer[13] = '\0';
			}else{
				convert_bin_to_str(tmp, 13, buffer);
				buffer[26] = '\0';
			}
		}
	}
	else {
		if (vChar ==0 || vChar == WSC_ENCRYPT_NONE)
			strcpy(buffer, "N/A");
		else
			strcpy(buffer, Entry.wscPsk);
	}
   	return boaWrite(wp, buffer);
}
#endif
#endif

#ifdef CONFIG_RTL8672_SAR
static int fnget_urlWanadsl(int eid, request * wp, int argc, char **argv, char *buffer)
{
	if (strstr(wp->pathname, "web/admin/"))
		return boaWrite(wp, "/admin/wanadsl.asp");
	else
		return boaWrite(wp, "/wanadsl.asp");
	return 0;
}
#endif

static int fnget_rssi(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_rssi_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "");
	}
}
static int fnget_imei(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_imei_nv",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "");
	}
}
static int fnget_imsi(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_imsi_nv",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "");
	}
}

static int fnget_lte_cellid(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_cellid_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "");
	}
}
static int fnget_lte_pci(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_pci_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "");
	}
}

static int fnget_rsrp(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_rsrp_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "err");
	}
}
static int fnget_rsrq(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_rsrq_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "err");
	}
}
static int fnget_cinr0(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_cinr0_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "err");
	}
}
static int fnget_cinr1(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_cinr1_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "err");
	}
}
static int fnget_sinr0(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_sinr0_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "err");
	}
}
static int fnget_sinr1(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_sinr1_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "err");
	}
}
static int fnget_rsrp0(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_rsrp0_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "err");
	}
}
static int fnget_rsrp1(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_rsrp1_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "err");
	}
}

static int fnget_lte_tx_power(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_txpusch_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "err");
	}
}

static int fnget_lte_bandwidth(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_bndwidth_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "err");
	}
}
static int fnget_lte_dl(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_dlfqcy_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "err");
	}
}

static int fnget_lte_ul(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_ulfqcy_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "err");
	}
}

static int fnget_operatename(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	lc_char plmntmp_oper[50] = {0};
	lc_char plmntmp_mccmnc[50] = {0};

	memset(tmpBuf,0,sizeof(tmpBuf));
	memset(plmntmp_mccmnc,0,sizeof(plmntmp_mccmnc));
	memset(plmntmp_oper,0,sizeof(plmntmp_oper));

	if(NV_OK == nvmram_readnv("wm_wcdma_plmn",plmntmp_mccmnc,sizeof(plmntmp_mccmnc)))
	{
		if (TRUE==match_oprer(plmntmp_oper,plmntmp_mccmnc))
		{
			strcat(tmpBuf,plmntmp_oper);
		 }
		else
		{
			if (strcmp(plmntmp_mccmnc,"0"))  // plmn=0 is error
			{
			    strcat(tmpBuf,plmntmp_mccmnc);
			}
		}
		nvmram_writenv("wm_wcdma_operatorname_tmp",tmpBuf,strlen(tmpBuf)+1);	
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "");
	}
}
static int fnget_networktype(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_service_type_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "");
	}
}

static int fnget_roamingstatus(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_roaming_status_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "");
	}
}

static int fnget_modemstatus(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_modem_status_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "");
	}
}

static int fnget_webapnlist(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("web_apn_list",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "");
	}
}

static int fnget_hideinternalversion(int eid, request * wp, int argc, char **argv, char *buffer)
{
	FILE* fp = fopen("/etc/version_info.txt", "r");
	char  buff[256];
	char *ptr = NULL;
	char tmpBuf[100]={0};
	
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(fp!=NULL)
	{
		while(fgets(buff,sizeof(buff),fp))
		{
			if(strstr(buff,"internal_version:")!=NULL)
			{
				ptr=buff+strlen("internal_version:");			
				if(ptr)
				{
					sprintf(tmpBuf,"%s",ptr);
				}				
				fclose(fp);
				return boaWrite(wp, tmpBuf);
			}	
		}
		fclose(fp);
	}
	return boaWrite(wp, "");
}
static int fnget_hidewebuiversion(int eid, request * wp, int argc, char **argv, char *buffer)
{
	FILE* fp = fopen("/etc/version_info.txt", "r");
	char  buff[256];
	char *ptr = NULL;
	char tmpBuf[100]={0};
	char  tmp[256];
	char  *t = NULL;
	int i = 0;
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(fp!=NULL)
	{
		while(fgets(buff,sizeof(buff),fp))
		{        
		    if(strstr(buff,"webui_version: ")!=NULL)
			{
				ptr=buff+strlen("webui_version: ");

				if(ptr)
				{                    
				    sprintf(tmp,"%s",ptr);
                    strncpy(tmpBuf,tmp,strlen(tmp));
				}
				fclose(fp);
				return boaWrite(wp, tmpBuf);
			}	
		}
		fclose(fp);
	}
	/*if(fp!=NULL)
	{
		while(fgets(buff,sizeof(buff),fp))
		{         
			if(strstr(buff,"internal_version:")!=NULL)
			{
				ptr=buff+strlen("internal_version:");
				if(ptr)
				{
					while((t=strsep(&ptr,"_")) && i <1)
					{
						if(*t == 0)
		                    break;
						i++;
					}

					if(i==1)
					{
						sprintf(tmpBuf,"V1.%s",t);
					}	
				}
				fclose(fp);
				return boaWrite(wp, tmpBuf);
			}
        
		}
		fclose(fp);
	}*/
	return boaWrite(wp, "");
}
static int fnget_hideexternalversion(int eid, request * wp, int argc, char **argv, char *buffer)
{
	FILE* fp = fopen("/etc/version_info.txt", "r");
	char  buff[256];
	char *ptr = NULL;
	char tmpBuf[100]={0};

	memset(tmpBuf,0,sizeof(tmpBuf));
	if(fp!=NULL)
	{
		while(fgets(buff,sizeof(buff),fp))
		{
			if(strstr(buff,"external_version:")!=NULL)
			{
				ptr=buff+strlen("external_version:");
				if(ptr)
				{
					sprintf(tmpBuf,"%s",ptr);
				}
				fclose(fp);
				return boaWrite(wp, tmpBuf);
			}	
		}
		fclose(fp);
	}
	return boaWrite(wp, "");
}

static int fnget_hidehardwareversion(int eid, request * wp, int argc, char **argv, char *buffer)
{
	FILE* fp = fopen("/etc/version_info.txt", "r");
	char  buff[256];
	char *ptr = NULL;
	char tmpBuf[100]={0};

	memset(tmpBuf,0,sizeof(tmpBuf));
	if(fp!=NULL)
	{
		while(fgets(buff,sizeof(buff),fp))
		{
			if(strstr(buff,"hardware_version:")!=NULL)
			{
				ptr=buff+strlen("hardware_version:");
				if(ptr)
				{
					sprintf(tmpBuf,"%s",ptr);
				}
				fclose(fp);
				return boaWrite(wp, tmpBuf);
			}	
		}
		fclose(fp);
	}
	return boaWrite(wp, "");
}

static int fnget_hideproductvariant(int eid, request * wp, int argc, char **argv, char *buffer)
{
	FILE* fp = fopen("/etc/version_info.txt", "r");
	char  buff[256];
	char *ptr = NULL;
	char tmpBuf[100]={0};

	memset(tmpBuf,0,sizeof(tmpBuf));
	if(fp!=NULL)
	{
		while(fgets(buff,sizeof(buff),fp))
		{
			if(strstr(buff,"product_variant:")!=NULL)
			{
				ptr=buff+strlen("product_variant:");
				if(ptr)
				{
					sprintf(tmpBuf,"%s",ptr);
				}
				fclose(fp);
				return boaWrite(wp, tmpBuf);
			}	
		}
		fclose(fp);
	}
	return boaWrite(wp, "");
}

static int fnget_hideskuversion(int eid, request * wp, int argc, char **argv, char *buffer)
{
	FILE* fp = fopen("/etc/version_info.txt", "r");
	char  buff[256];
	char *ptr = NULL;
	char tmpBuf[100]={0};

	memset(tmpBuf,0,sizeof(tmpBuf));
	if(fp!=NULL)
	{
		while(fgets(buff,sizeof(buff),fp))
		{
			if(strstr(buff,"sku:")!=NULL)
			{
				ptr=buff+strlen("sku:");
				if(ptr)
				{
					sprintf(tmpBuf,"%s",ptr);
				}
				fclose(fp);
				return boaWrite(wp, tmpBuf);
			}	
		}
		fclose(fp);
	}
	return boaWrite(wp, "");
}

static int fnget_traffic_data(int eid, request * wp, int argc, char **argv, char *buffer)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_volume_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		return boaWrite(wp, tmpBuf);
	}
	else
	{
		return boaWrite(wp, "err");
	}
}

/*************************************************************************/



web_custome_cmd get_info_custom_list[] = {
	#ifdef WLAN_SUPPORT
	#ifdef CONFIG_WIFI_SIMPLE_CONFIG//WPS
	{ "wps_key", fnget_wpsKey },
	#endif
	#endif
	#ifdef CONFIG_RTL8672_SAR
	{ "url_wanadsl", fnget_urlWanadsl },
	#endif
	{"rssi", fnget_rssi},
	{"imei", fnget_imei},
	{"imsi", fnget_imsi},
	{"rsrp", fnget_rsrp},
	{"rsrq", fnget_rsrq},
    {"cinr0", fnget_cinr0},
	{"cinr1", fnget_cinr1},
	{"sinr0", fnget_sinr0},
	{"sinr1", fnget_sinr1},
    {"rsrp0", fnget_rsrp0},
	{"rsrp1", fnget_rsrp1},
    {"lte_tx_power", fnget_lte_tx_power},
    {"lte_bandwidth", fnget_lte_bandwidth},
    {"lte_dl", fnget_lte_dl},
    {"lte_ul", fnget_lte_ul},
	{"lte_cellid", fnget_lte_cellid},
	{"lte_pci", fnget_lte_pci},
	{"operatename", fnget_operatename},
	{"networktype", fnget_networktype},
	{"modemstatus", fnget_modemstatus},
	{"roamingstatus", fnget_roamingstatus},
	//{"webapnlist", fnget_webapnlist},
	{"hideexternalversion", fnget_hideexternalversion},
	{"hideinternalversion", fnget_hideinternalversion},
	{"hidewebuiversion", fnget_hidewebuiversion},
	{"hidehardwareversion", fnget_hidehardwareversion},
	{"hideproductvariant", fnget_hideproductvariant},
	{"hideskuversion", fnget_hideskuversion},
    {"traffic_data", fnget_traffic_data},
	{ NULL, 0 }
};

int getInfo(int eid, request * wp, int argc, char **argv)
{
	char	*name;
	unsigned char buffer[256+1];
	int idx, ret;

   	if (boaArgs(argc, argv, "%s", &name) < 1) {
   		boaError(wp, 400, "Insufficient args\n");
   		return -1;
   	}

	memset(buffer,0x00,64);
#ifdef CONFIG_DEV_xDSL
 	if ( !strncmp(name, "adsl-drv-", 9) ) {
 		getAdslDrvInfo(&name[9], buffer, 64);
		return boaWrite(wp, "%s", buffer);
	}
#endif
#ifdef CONFIG_USER_XDSL_SLAVE
	if ( !strncmp(name, "adsl-slv-drv-", 13) ) {
		getAdslSlvDrvInfo(&name[13], buffer, 64);
		return boaWrite(wp, "%s", buffer);
	}
#endif /*CONFIG_USER_XDSL_SLAVE*/
	/*+++++add by Jack for VoIP project 20/03/07+++++*/
#ifdef VOIP_SUPPORT
	if(!strncmp(name, "voip_", 5)){
		return asp_voip_getInfo(eid, wp, argc, argv);
	}
#endif /*VOIP_SUPPORT*/
	if(!strcmp(name, "login-user")){
#ifdef USE_LOGINWEB_OF_SERVER
		ret = boaWrite(wp, "%s", g_login_username);
#else
		ret = boaWrite(wp, "%s", wp->user);
#endif
		goto NEXTSTEP;
	}

#ifdef FIELD_TRY_SAFE_MODE
#ifdef CONFIG_DEV_xDSL
	if (!strncmp(name, "safemodenote", 12)) {
		SafeModeData vSmd;
		memset((void *)&vSmd, 0, sizeof(vSmd));
		adsl_drv_get(RLCM_GET_SAFEMODE_CTRL, (void *)&vSmd, SAFEMODE_DATA_SIZE);
		boaWrite(wp, "%s", vSmd.SafeModeNote);
	}
#endif
#endif

 	for (idx=0; get_info_custom_list[idx].cmd != NULL; idx++) {
 		if (!strcmp(name, get_info_custom_list[idx].cmd)) {
 			return get_info_custom_list[idx].handler(eid, wp, argc, argv, buffer);
 		}
 	}

	for (idx=0; get_info_list[idx].cmd != NULL; idx++) {
		if (!strcmp(name, get_info_list[idx].cmd)) {
			if (get_info_list[idx].type == INFO_MIB) {
				if (getMIB2Str(get_info_list[idx].id, buffer)) {
					fprintf(stderr, "failed to get %s\n", name);
					return -1;
				}
			}
			else {
				if (getSYS2Str(get_info_list[idx].id, buffer))
					return -1;
			}
			// Kaohj
			if ((!strncmp(name, "wan-dns", 7))&& !strcmp(buffer, "0.0.0.0"))
				ret = boaWrite(wp, "");
			else{
				#ifdef WLAN_SUPPORT
				if(!strcmp(name, "ssid")){
					translate_control_code(buffer);
				}
				#endif
				ret = boaWrite(wp, "%s", buffer);
			}
			//fprintf(stderr, "%s = %s\n", name, buffer);
			//printf("%s = %s\n", name, buffer);
			break;
		}
	}

NEXTSTEP:
	return ret;
}

int addMenuJavaScript( request * wp,int nums,int maxchildrensize)
{
	boaWrite(wp,"<script >\n");
	int i=0;
	boaWrite(wp,"scores = new Array(%d);\n",nums);
	for(i=0;i<nums;i++ )
		boaWrite(wp,"scores[%d]='Submenu%d';\n",i,i);
	boaWrite(wp,"btns = new Array(%d);\n",nums);
	for(i=0;i<nums;i++ )
		boaWrite(wp,"btns[%d]='Btn%d';\n",i,i);
	boaWrite(wp,"\nfunction initIt()\n"
		"{\n\tdivColl = document.all.tags(\"div\");\n"
		"\tfor (i=0; i<divColl.length; i++)\n "
		"\t{\n\t\twhichEl = divColl[i];\n"
		"\t\tif (whichEl.className == \"Child\")\n"
		"\t\t\twhichEl.style.display = \"none\";\n\t}\n}\n\n");
	boaWrite(wp,"function closeMenu(el)\n"
		"{\n"
		"\tfor(i=0;i<%d;i++)\n"
		"\t{\n\t\tfor(j=0;j<%d;j++)"
		"{\n\t\t\tif(scores[i]!=el)\n"
		"\t\t\t{\n\t\t\t\tid=scores[i]+\"Child\"+j.toString();\n"
		"\t\t\t\tif(document.getElementById(id))\n"
		"\t\t\t\t{\n\t\t\t\t\tdocument.getElementById(id).style.display = \"none\";\n"
		"\t\t\t\t\twhichEl = eval(scores[i] + \"Child\");\n"
		"\t\t\t\t\twhichEl.style.display = \"none\";\n"
#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
		"\t\t\t\t\tdocument.getElementById(btns[i]).src =\"menu-images/menu_folder_closed.gif\";\n"
#endif
		"\t\t\t\t}\n\t\t\t}\n\t\t}\n\t}\n}\n\n",nums, maxchildrensize);

	boaWrite(wp,"function expandMenu(el,imgs, num)\n"
		"{\n\tcloseMenu(el);\n");
	boaWrite(wp,"\tif (num == 0) {\n\t\twhichEl1 = eval(el + \"Child\");\n"
		"\t\tfor(i=0;i<%d;i++)\n",nums);
	boaWrite(wp,"\t\t{\n\t\t\twhichEl = eval(scores[i] + \"Child\");\n"
		"\t\t\tif(whichEl!=whichEl1)\n "
		"\t\t\t{\n\t\t\t\twhichEl.style.display = \"none\";\n"
#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
		"\t\t\t\tdocument.getElementById(btns[i]).src =\"menu-images/menu_folder_closed.gif\";\n"
#endif
		"\t\t\t}\n\t\t}\n");
	boaWrite(wp,"\t\twhichEl1 = eval(el + \"Child\");\n"
		"\t\tif (whichEl1.style.display == \"none\")\n "
		"\t\t{\n"
		"\t\t\twhichEl1.style.display = \"\";\n"
#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
		"\t\t\tdocument.getElementById(imgs).src =\"menu-images/menu_folder_open.gif\";\n"
#endif
		"\t\t}\n\t\telse {\n\t\t\twhichEl1.style.display =\"none\";\n"
#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
		"\t\t\tdocument.getElementById(imgs).src =\"menu-images/menu_folder_closed.gif\";\n"
#endif
		"\t\t}\n\t}\n\telse {\n");
	boaWrite(wp,"\t\tfor(i=0;i<num;i++) {\n"
		"\t\t\tid = el + \"Child\"+i.toString();\n"
		"\t\t\twhichEl1 = document.getElementById(id);\n"
		"\t\t\tif (whichEl1) {\n"
		"\t\t\t\tif (whichEl1.style.display == \"none\")\n"
		"\t\t\t\t{\n"
		"\t\t\t\t\twhichEl1.style.display = \"\";\n"
#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
		"\t\t\t\t\tdocument.getElementById(imgs).src =\"menu-images/menu_folder_open.gif\";\n"
#endif
		"\t\t\t\t}\n\t\t\t\telse {\n\t\t\t\t\twhichEl1.style.display =\"none\";\n"
#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
		"\t\t\t\t\tdocument.getElementById(imgs).src =\"menu-images/menu_folder_closed.gif\";\n"
#endif
		"\t\t\t\t}\n\t\t\t}\n\t\t}\n\t}\n}\n</script>\n");

	boaWrite(wp,"<style type=\"text/css\">\n"
		"\n.link {\n"
/* add by yq_zhou 09.2.02 add sagem logo for 11n*/
#ifdef CONFIG_11N_SAGEM_WEB
		"\tfont-family: arial, Helvetica, sans-serif, bold;\n\tfont-size:10pt;\n\twhite-space:nowrap;\n\tcolor: #000000;\n\ttext-decoration: none;\n}\n"
#else
		"\tfont-family: arial, Helvetica, sans-serif, bold;\n\tfont-size:10pt;\n\twhite-space:nowrap;\n\tcolor: #FFFFFF;\n\ttext-decoration: none;\n}\n"
#endif
		"a:link {text-decoration: none;}"
		"\na:visited {text-decoration: none;}"
		"\na:hover {text-decoration: none;}"
		"\na:active {text-decoration: none;}\n"
		"</style>");
}


//modify adsl-stats.asp for adsl and vdsl2
#define DSL_STATS_FMT_UP \
	"<tr>\n" \
	"	<th align=left bgcolor=#c0c0c0><font size=2>%s</th>\n" \
	"	<td bgcolor=#f0f0f0>%s</td>\n" \
	"</tr>\n"
#define DSL_STATS_FMT_DSUS \
	"<tr bgcolor=#f0f0f0>\n" \
	"	<th align=left bgcolor=#c0c0c0><font size=2>%s</th>\n" \
	"	<td>%s</td><td>%s</td>\n" \
	"</tr>\n"
#define DSL_STATS_FMT_DOWN \
	"<tr bgcolor=#f0f0f0>\n" \
	"	<th align=left bgcolor=#c0c0c0><font size=2>%s</th>\n" \
	"	<td colspan=2>%s</td>\n" \
	"</tr>\n"
//end modify adsl-stats.asp for adsl and vdsl2

int check_display(int eid, request * wp, int argc, char **argv)
{
	char *name;

   	if (boaArgs(argc, argv, "%s", &name) < 1) {
   		boaError(wp, 400, "Insufficient args\n");
   		return -1;
   	}

	if(!strcmp(name,"vid_mark"))
	{
#ifdef CONFIG_LUNA
		boaWrite(wp,"none");
#else
		boaWrite(wp,"block");
#endif
		return 0;
	}
	else if(!strcmp(name,"wan_interface"))
	{
#ifdef CONFIG_LUNA
		boaWrite(wp,"none");
#else
		boaWrite(wp,"block");
#endif
		return 0;
	}
	else if(!strcmp(name,"qos_direction"))
	{
#ifdef CONFIG_LUNA
		boaWrite(wp,"block");
#else
		boaWrite(wp,"none");
#endif
		return 0;
	}
	else if(!strcmp(name,"vlanID"))
	{
#if defined(QOS_TRAFFIC_SHAPING_BY_VLANID)&&defined(CONFIG_RTK_RG_INIT)
		boaWrite(wp,"block");
#else
		boaWrite(wp,"none");
#endif
		return 0;
	}
	else if(!strcmp(name,"ssid"))
	{
#if defined(QOS_TRAFFIC_SHAPING_BY_SSID)&&defined(CONFIG_RTK_RG_INIT)
		boaWrite(wp,"block");
#else
		boaWrite(wp,"none");
#endif
		return 0;
	}
	return -1;
}


// Kaohj
int checkWrite(int eid, request * wp, int argc, char **argv)
{
	char *name;
	unsigned char vChar;
	unsigned short vUShort;
	unsigned int vUInt;
#ifdef CONFIG_USER_VLAN_ON_LAN
	MIB_CE_SW_PORT_T sw_entry;
#endif

   	if (boaArgs(argc, argv, "%s", &name) < 1) {
   		boaError(wp, 400, "Insufficient args\n");
   		return -1;
   	}
	if ( !strcmp(name, "devType") ) {
		if ( !mib_get( MIB_DEVICE_TYPE, (void *)&vChar) )
			return -1;
#ifdef EMBED
		if (0 == vChar)
			boaWrite(wp, "disableTextField(document.adsl.adslConnectionMode);");
#endif
		return 0;
	}

	// Added by davian kuo
#ifdef CONFIG_USER_BOA_WITH_MULTILANG
	if (!strcmp(name, "selinit")) {
		int i = 0;
		char mStr[MAX_LANGSET_LEN] = {0};

		if (!mib_get (MIB_MULTI_LINGUAL, (void *)mStr)) {
			fprintf (stderr, "mib get multi-lingual setting failed!\n");
			return -1;
		}

		for (i = 0; i < LANG_MAX; i++)
			boaWrite(wp, "<option %s value=\"%d\">%s</option>\n",
				(!(strcmp(mStr, lang_set[i].langType)))?"selected":"", i, lang_set[i].langStr);
		return 0;
	}
#endif

#ifdef USE_LOGINWEB_OF_SERVER
	if (!strcmp(name, "loginSelinit")) {
#ifdef CONFIG_USER_BOA_WITH_MULTILANG
		int i = 0;
		char mStr[MAX_LANGSET_LEN] = {0};

		if (!mib_get (MIB_MULTI_LINGUAL, (void *)mStr)) {
			fprintf (stderr, "mib get multi-lingual setting (login) failed!\n");
			return -1;
		}

		boaWrite(wp, "<td><div align=right><font color=#0000FF size=2>%s:</font></div></td>\n", multilang(LANG_LANGUAGE_SELECT));
		boaWrite(wp, "<td>&nbsp;&nbsp;</td>\n");
		boaWrite(wp, "<td><font size=2><select size=\"1\" name=\"loginSelinit\" onChange=\"mlhandle();\">\n");
		for (i = 0; i < LANG_MAX; i++)
			boaWrite(wp, "<option %s value=\"%d\">%s</option>\n",
				(!(strcmp(mStr, lang_set[i].langType)))?"selected":"", i, lang_set[i].langStr);
		boaWrite(wp, "</select></font></td>\n");

		return 0;
#endif
	}
#endif

#ifdef CONFIG_USER_ROUTED_ROUTED
	else if ( !strcmp(name, "rip-on-0") ) {
		if ( !mib_get( MIB_RIP_ENABLE, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "rip-on-1") ) {
		if ( !mib_get( MIB_RIP_ENABLE, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#endif
   // add get apn by qk	
	if( !strcmp(name, "webapnlist") ) {
		MIB_WAN_LTE_T Entry_LTE;
		if(!mib_chain_get( MIB_WAN_LTE_TBL, 0, (void*)&Entry_LTE))
    	{
       	 	return -1;
    	}
		boaWrite(wp, "%s", Entry_LTE.apn_list);
		return 0;
	}
	// add get connect mode  by qk	
	if( !strcmp(name, "webconnectmode") ) {
		MIB_WAN_LTE_T Entry_LTE;
		if(!mib_chain_get( MIB_WAN_LTE_TBL, 0, (void*)&Entry_LTE))
    	{
			WM_LOG_INFO("get conn type error:%d", Entry_LTE.conn_type );
       	 	return -1;
    	}

		WM_LOG_INFO("get conn type success:%d", Entry_LTE.conn_type );

		boaWrite(wp, "%d", Entry_LTE.conn_type);
		return 0;
	}


#ifdef CONFIG_DEV_xDSL
	//modify adsl-set.asp for adsl and vdsl2
	if (!strcmp (name, "adsl_set_title"))
	{
		#ifdef CONFIG_VDSL
		boaWrite (wp, "%s", multilang(LANG_DSL_SETTINGS) );
		#else
		boaWrite (wp, "%s", multilang(LANG_ADSL_SETTINGS) );
		#endif /*CONFIG_VDSL*/
		return 0;
	}
	if (!strcmp (name, "xdsl_type"))
	{
		#ifdef CONFIG_VDSL
		boaWrite (wp, "%s", "DSL" );
		#else
		boaWrite (wp, "%s", "ADSL" );
		#endif /*CONFIG_VDSL*/
		return 0;
	}


	if( !strncmp (name, "adsl_", 5) || !strncmp (name, "adsl_slv_", 9) )
	{
		int ofs;
		XDSL_OP *d;
#ifdef CONFIG_USER_XDSL_SLAVE
		if(!strncmp (name, "adsl_slv_", 9))
		{
			ofs=9;
			d=xdsl_get_op(1);
		}else
#endif /*CONFIG_USER_XDSL_SLAVE*/
		{
			ofs=5;
			d=xdsl_get_op(0);
		}


		//modify adsl-diag.asp for adsl and vdsl2
		if (!strcmp (&name[ofs], "diag_title"))
		{
#ifdef CONFIG_USER_XDSL_SLAVE
			if(d->id)
			{
				#ifdef CONFIG_VDSL
				boaWrite (wp, "%s", multilang(LANG_DSL_SLAVE_TONE_DIAGNOSTICS) );
				#else
				boaWrite (wp, "%s", multilang(LANG_ADSL_SLAVE_TONE_DIAGNOSTICS) );
				#endif /*CONFIG_VDSL*/
			}else
#endif /*CONFIG_USER_XDSL_SLAVE*/
			{
				#ifdef CONFIG_VDSL
				boaWrite (wp, "%s", multilang(LANG_DSL_TONE_DIAGNOSTICS) );
				#else
				boaWrite (wp, "%s", multilang(LANG_ADSL_TONE_DIAGNOSTICS) );
				#endif /*CONFIG_VDSL*/
			}
			return 0;
		}
		if (!strcmp (&name[ofs], "diag_cmt"))
		{
#ifdef CONFIG_USER_XDSL_SLAVE
			if(d->id)
			{
				#ifdef CONFIG_VDSL
				boaWrite (wp, "%s", multilang(LANG_DSL_SLAVE_TONE_DIAGNOSTICS_ONLY_ADSL2_ADSL2_VDSL2_SUPPORT_THIS_FUNCTION) );
				#else
				boaWrite (wp, "%s", multilang(LANG_ADSL_SLAVE_TONE_DIAGNOSTICS_ONLY_ADSL2_2_SUPPORT_THIS_FUNCTION) );
				#endif /*CONFIG_VDSL*/
			}else
#endif /*CONFIG_USER_XDSL_SLAVE*/
			{
				#ifdef CONFIG_VDSL
				boaWrite (wp, "%s", multilang(LANG_DSL_TONE_DIAGNOSTICS_ONLY_ADSL2_ADSL2_VDSL2_SUPPORT_THIS_FUNCTION) );
				#else
				boaWrite (wp, "%s", multilang(LANG_ADSL_TONE_DIAGNOSTICS_ONLY_ADSL2_2_SUPPORT_THIS_FUNCTION) );
				#endif /*CONFIG_VDSL*/
			}
			return 0;
		}
		//modify adsl-stats.asp for adsl and vdsl2
		if (!strcmp (&name[ofs], "title"))
		{
#ifdef CONFIG_USER_XDSL_SLAVE
			if(d->id)
			{
				#ifdef CONFIG_VDSL
				boaWrite (wp, "%s", multilang(LANG_DSL_SLAVE_STATISTICS) );
				#else
				boaWrite (wp, "%s", multilang(LANG_ADSL_SLAVE_STATISTICS) );
				#endif /*CONFIG_VDSL*/
			}else
#endif /*CONFIG_USER_XDSL_SLAVE*/
			{
				#ifdef CONFIG_VDSL
				boaWrite (wp, "%s", multilang(LANG_DSL_STATISTICS) );
				#else
				boaWrite (wp, "%s", multilang(LANG_ADSL_STATISTICS) );
				#endif /*CONFIG_VDSL*/
			}
			return 0;
		}
		if (!strcmp (&name[ofs], "tpstc"))
		{
			#ifdef CONFIG_VDSL
			unsigned char buffer[64];
			d->xdsl_get_info(DSL_GET_TPS, buffer, 64);
			boaWrite (wp, DSL_STATS_FMT_UP, "TPS-TC", buffer );
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		if (!strcmp (&name[ofs], "trellis"))
		{
			#ifndef CONFIG_VDSL
			unsigned char buffer[64];
			d->xdsl_get_info(ADSL_GET_TRELLIS, buffer, 64);
			boaWrite (wp, DSL_STATS_FMT_UP, "Trellis Coding", buffer );
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		if (!strcmp (&name[ofs], "trellis_dsus"))
		{
			#ifdef CONFIG_VDSL
			unsigned char buf_ds[64],buf_us[64];
			d->xdsl_get_info(DSL_GET_TRELLIS_DS, buf_ds, 64);
			d->xdsl_get_info(DSL_GET_TRELLIS_US, buf_us, 64);
			boaWrite (wp, DSL_STATS_FMT_DSUS, "Trellis", buf_ds, buf_us );
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		if (!strcmp (&name[ofs], "ginp_dsus"))
		{
			#ifdef CONFIG_VDSL
			unsigned char buf_ds[64],buf_us[64];
			d->xdsl_get_info(DSL_GET_PHYR_DS, buf_ds, 64);
			d->xdsl_get_info(DSL_GET_PHYR_US, buf_us, 64);
			boaWrite (wp, DSL_STATS_FMT_DSUS, "G.INP", buf_ds, buf_us );
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		if (!strcmp (&name[ofs], "k_dsus"))
		{
			#ifndef CONFIG_VDSL
			unsigned char buf_ds[64],buf_us[64];
			d->xdsl_get_info(ADSL_GET_K_DS, buf_ds, 64);
			d->xdsl_get_info(ADSL_GET_K_US, buf_us, 64);
			boaWrite (wp, DSL_STATS_FMT_DSUS, "K (number of bytes in DMT frame)", buf_ds, buf_us );
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		if (!strcmp (&name[ofs], "n_dsus"))
		{
			#ifdef CONFIG_VDSL
			unsigned char buf_ds[64],buf_us[64];
			d->xdsl_get_info(DSL_GET_N_DS, buf_ds, 64);
			d->xdsl_get_info(DSL_GET_N_US, buf_us, 64);
			boaWrite (wp, DSL_STATS_FMT_DSUS, "N (RS codeword size)", buf_ds, buf_us );
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		if (!strcmp (&name[ofs], "l_dsus"))
		{
			#ifdef CONFIG_VDSL
			unsigned char buf_ds[64],buf_us[64];
			d->xdsl_get_info(DSL_GET_L_DS, buf_ds, 64);
			d->xdsl_get_info(DSL_GET_L_US, buf_us, 64);
			boaWrite (wp, DSL_STATS_FMT_DSUS, "L (number of bits in DMT frame)", buf_ds, buf_us );
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		if (!strcmp (&name[ofs], "inp_dsus"))
		{
			#ifdef CONFIG_VDSL
			unsigned char buf_ds[64],buf_us[64];
			d->xdsl_get_info(DSL_GET_INP_DS, buf_ds, 64);
			d->xdsl_get_info(DSL_GET_INP_US, buf_us, 64);
			boaWrite (wp, DSL_STATS_FMT_DSUS, "INP (DMT frame)", buf_ds, buf_us );
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		if (!strcmp (&name[ofs], "fec_name"))
		{
			#ifdef CONFIG_VDSL
			boaWrite (wp, "FEC errors");
			#else
			boaWrite (wp, "FEC");
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		if (!strcmp (&name[ofs], "ohframe_dsus"))
		{
			#ifdef CONFIG_VDSL
			unsigned char buf_ds[64],buf_us[64];
			d->xdsl_get_info(ADSL_GET_RX_FRAMES, buf_ds, 64);
			d->xdsl_get_info(ADSL_GET_TX_FRAMES, buf_us, 64);
			boaWrite (wp, DSL_STATS_FMT_DSUS, "OH Frame", buf_ds, buf_us );
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		if (!strcmp (&name[ofs], "crc_name"))
		{
			#ifdef CONFIG_VDSL
			boaWrite (wp, "OH Frame errors");
			#else
			boaWrite (wp, "CRC");
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		if (!strcmp (&name[ofs], "llr_dsus"))
		{
			#ifdef CONFIG_VDSL
			unsigned char buf_ds[64],buf_us[64];
			d->xdsl_get_info(ADSL_GET_LAST_LINK_DS, buf_ds, 64);
			d->xdsl_get_info(ADSL_GET_LAST_LINK_US, buf_us, 64);
			boaWrite (wp, DSL_STATS_FMT_DSUS, "Last Link Rate", buf_ds, buf_us );
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		if (!strcmp (&name[ofs], "llr"))
		{
			#ifndef CONFIG_VDSL
			unsigned char buffer[64];
			d->xdsl_get_info(ADSL_GET_LAST_LINK_DS, buffer, 64);
			boaWrite (wp, DSL_STATS_FMT_DOWN, "Last Link DS Rate", buffer );
			d->xdsl_get_info(ADSL_GET_LAST_LINK_US, buffer, 64);
			boaWrite (wp, DSL_STATS_FMT_DOWN, "Last Link US Rate", buffer );
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		if (!strcmp (&name[ofs], "txrx_frame"))
		{
			#ifndef CONFIG_VDSL
			unsigned char buffer[64];
			d->xdsl_get_info(ADSL_GET_TX_FRAMES, buffer, 64);
			boaWrite (wp, DSL_STATS_FMT_DOWN, "TX frames", buffer );
			d->xdsl_get_info(ADSL_GET_RX_FRAMES, buffer, 64);
			boaWrite (wp, DSL_STATS_FMT_DOWN, "RX frames", buffer );
			#endif /*CONFIG_VDSL*/
			return 0;
		}
		//end modify adsl-stats.asp for adsl and vdsl2
	}
#endif // of CONFIG_DEV_xDSL

	if ( !strcmp(name, "dhcpMode") ) {
		boaWrite(wp, "</td><td><input type=\"radio\" name=dhcpdenable value=0 onClick=\"disabledhcpd()\">%s&nbsp;&nbsp;</td>\n", multilang(LANG_NONE));
		boaWrite(wp, "<td><input type=\"radio\"name=dhcpdenable value=1 onClick=\"enabledhcprelay()\">DHCP %s&nbsp;&nbsp;</td>\n", multilang(LANG_RELAY));
		boaWrite(wp, "<td><input type=\"radio\"name=dhcpdenable value=2 onClick=\"enabledhcpd()\">DHCP %s&nbsp;&nbsp;\n", multilang(LANG_SERVER));
		return 0;
	}

	if ( !strcmp(name, "dhcpV6Mode") ) {
		boaWrite(wp, "</td><td><input type=\"radio\" name=dhcpdenable value=0 onClick=\"disabledhcpd()\">%s&nbsp;</td>\n", multilang(LANG_NONE));
		boaWrite(wp, "<td><input type=\"radio\"name=dhcpdenable value=1 onClick=\"enabledhcprelay()\">DHCP%s&nbsp;</td>\n", multilang(LANG_RELAY));
		boaWrite(wp, "<td><input type=\"radio\"name=dhcpdenable value=2 onClick=\"enabledhcpd()\">DHCP%s(%s)&nbsp;</td>\n", multilang(LANG_SERVER), multilang(LANG_MANUAL));
		boaWrite(wp, "<td><input type=\"radio\"name=dhcpdenable value=3 onClick=\"autodhcpd()\">DHCP%s(%s)&nbsp;\n", multilang(LANG_SERVER), multilang(LANG_AUTO));
		return 0;
	}

#ifdef ADDRESS_MAPPING
#ifndef MULTI_ADDRESS_MAPPING
	if ( !strcmp(name, "addressMapType") ) {
 		if ( !mib_get( MIB_ADDRESS_MAP_TYPE, (void *)&vChar) )
			return -1;

		boaWrite(wp, "<option value=0>%s</option>\n", multilang(LANG_NONE));
		boaWrite(wp, "<option value=1>%s</option>\n", multilang(LANG_ONE_TO_ONE));
		boaWrite(wp, "<option value=2>%s</option>\n", multilang(LANG_MANY_TO_ONE));
		boaWrite(wp, "<option value=3>%s</option>\n", multilang(LANG_MANY_TO_MANY_OVERLOAD));

		// Mason Yu on True
		boaWrite(wp, "<option value=4>%s</option>\n", multilang(LANG_ONE_TO_MANY));
		return 0;
	}
#endif	// end of !MULTI_ADDRESS_MAPPING
#endif

#ifdef WLAN_SUPPORT
#ifdef WLAN_ACL
	if (!strcmp(name, "wlanAcNum")) {
		MIB_CE_WLAN_AC_T entry;
		int i;
		vUInt = mib_chain_total(MIB_WLAN_AC_TBL);
		for (i=0; i<vUInt; i++) {
			if (!mib_chain_get(MIB_WLAN_AC_TBL, i, (void *)&entry)) {
				i = vUInt;
				break;
			}
			if(entry.wlanIdx == wlan_idx)
				break;
		}
		if (i == vUInt)
			boaWrite(wp, "disableDelButton();");
		return 0;
	}
#endif
#ifdef WLAN_WDS
	if (!strcmp(name, "wlanWDSNum")) {
		vUInt = mib_chain_total(MIB_WDS_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelButton();");
		return 0;
	}
	if ( !strcmp(name, "wdsEncrypt")) {
		int i;
		char* encryp[5] = {multilang(LANG_NONE), "WEP 64bits", "WEP 128bits", "WPA (TKIP)", "WPA2 (AES)"};
		if ( !mib_get( MIB_WLAN_WDS_ENCRYPT, (void *)&vChar) )
			return -1;
	       for(i=0;i<5;i++){
		   	if(i == WDS_ENCRYPT_TKIP){//skip tkip....by ap team's web
				continue;
		   	}
#ifdef WLAN_QTN
#ifdef WLAN1_QTN
			if(wlan_idx==1)
#else
			if(wlan_idx==0)
#endif
			if((i!=WDS_ENCRYPT_DISABLED) && (i!=WDS_ENCRYPT_AES))
				continue;
#endif
		   	boaWrite(wp, "<option %s value=\"%d\">%s</option>\n", (i==vChar)?"selected":"", i, encryp[i]);
	       }
		return 0;
	}
	if ( !strcmp(name, "wdsWepFormat")) {
		int i;
		unsigned char tmp;
		char* format[4] = {"ASCII (5 characters)", "Hex (10 characters)", "ASCII (13 characters)", "Hex (26 characters)"};
		int skip = 0;
		if ( !mib_get( MIB_WLAN_WDS_ENCRYPT, (void *)&tmp) )
			return -1;
		if(tmp == WDS_ENCRYPT_WEP128){
			skip = 2;
		}
		if ( !mib_get( MIB_WLAN_WDS_WEP_FORMAT, (void *)&vChar) )
			return -1;
		for(i=0;i<2;i++){
		   	boaWrite(wp, "<option %s value=\"%d\">%s</option>\n", (i==vChar)?"selected":"", i, format[i + skip]);
	       }
		return 0;
	}
	if ( !strcmp(name, "wdsPskFormat")) {
		int i;
		char* format[2] = {"Passphrase", "Hex (64 characters)"};
		if ( !mib_get( MIB_WLAN_WDS_PSK_FORMAT, (void *)&vChar) )
			return -1;
		for(i=0;i<2;i++){
#ifdef WLAN_QTN
#ifdef WLAN1_QTN
			if(wlan_idx==1)
#else
			if(wlan_idx==0)
#endif
			if(i!=KEY_HEX)
				continue;
#endif
		   	boaWrite(wp, "<option %s value=\"%d\">%s</option>\n", (i==vChar)?"selected":"", i, format[i]);
	       }
		return 0;
	}
#endif
	if ( !strcmp(name, "wlmode") ) {
		MIB_CE_MBSSIB_T Entry;
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		vChar = Entry.wlanMode;
		if (vChar == AP_MODE) {
			boaWrite(wp, "<option selected value=\"0\">AP</option>\n" );
#ifdef WLAN_CLIENT
			boaWrite(wp, "<option value=\"1\">Client</option>\n" );
#endif
#ifdef WLAN_WDS
			boaWrite(wp, "<option value=\"3\">AP+WDS</option>\n" );
#endif
		}
#ifdef WLAN_CLIENT
		if (vChar == CLIENT_MODE) {
			boaWrite(wp, "<option value=\"0\">AP</option>\n" );
			boaWrite(wp, "<option selected value=\"1\">Client</option>\n" );
#ifdef WLAN_WDS

			boaWrite(wp, "<option value=\"3\">AP+WDS</option>\n" );
#endif
		}
#endif
#ifdef WLAN_WDS
		if (vChar == AP_WDS_MODE) {
			boaWrite(wp, "<option value=\"0\">AP</option>\n" );
#ifdef WLAN_CLIENT
			boaWrite(wp, "<option value=\"1\">Client</option>\n" );
#endif
			boaWrite(wp, "<option selected value=\"3\">AP+WDS</option>\n" );
		}
#endif
		return 0;
	}
#ifdef WLAN_WDS
	if ( !strcmp(name, "wlanWdsEnabled") ) {
		if ( !mib_get( MIB_WLAN_WDS_ENABLED, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}

#endif
	if ( !strcmp(name, "mband") ) {
		MIB_CE_MBSSIB_T Entry;
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		boaWrite(wp, "%d", Entry.wlanBand);
		return 0;
	}
	if ( !strcmp(name, "band") ) {
		MIB_CE_MBSSIB_T Entry;
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		boaWrite(wp, "%d", Entry.wlanBand-1);
		return 0;
	}
	if ( !strcmp(name, "ctrl_disabled") ) {
#ifdef CONFIG_USER_CUSTOM_UKB
		boaWrite(wp, "%s", "disableTextField(document.wlanSetup.ssid);");
#else
		boaWrite(wp, "%s", "");
#endif
		return 0;
	}
	if ( !strcmp(name, "wlband") ) {
#ifdef WIFI_TEST
		boaWrite(wp, "<option value=3>WiFi-G</option>\n" );
		boaWrite(wp, "<option value=4>WiFi-BG</option>\n" );
#endif

#if defined(CONFIG_RTL_92D_SUPPORT) || defined(WLAN_DUALBAND_CONCURRENT)
		if ( !mib_get(MIB_WLAN_PHY_BAND_SELECT, (void *)&vChar) )
			return -1;

#if defined (CONFIG_RTL_92D_SUPPORT)
		unsigned char wlanBand2G5GSelect;
		if ( !mib_get(MIB_WLAN_BAND2G5G_SELECT, (void *)&wlanBand2G5GSelect) )
				return -1;
		if((vChar == PHYBAND_5G) || (wlanBand2G5GSelect == BANDMODESINGLE))
#else
		if(vChar == PHYBAND_5G)
#endif
		{
#ifdef WLAN_QTN
			boaWrite(wp, "<option value=3>5 GHz (A)</option>\n" );
			boaWrite(wp, "<option value=11>5 GHz (A+N)</option>\n" );
			boaWrite(wp, "<option value=63>5 GHz (AC)</option>\n" );
#else
			boaWrite(wp, "<option value=3>5 GHz (A)</option>\n" );
			boaWrite(wp, "<option value=7>5 GHz (N)</option>\n" );
			boaWrite(wp, "<option value=11>5 GHz (A+N)</option>\n" );
#if defined (WLAN0_5G_11AC_SUPPORT) || defined(WLAN1_5G_11AC_SUPPORT)
			boaWrite(wp, "<option value=63>5 GHz (AC)</option>\n" );
			boaWrite(wp, "<option value=71>5 GHz (N+AC)</option>\n" );
			boaWrite(wp, "<option value=75>5 GHz (A+N+AC)</option>\n" );
#endif
#endif
		}
#endif

#if (defined (WLAN0_5G_SUPPORT) || defined(WLAN1_5G_SUPPORT)) && !defined(WLAN_DUALBAND_CONCURRENT)
		boaWrite(wp, "<option value=3>5 GHz (A)</option>\n" );
		boaWrite(wp, "<option value=7>5 GHz (N)</option>\n" );
		boaWrite(wp, "<option value=11>5 GHz (A+N)</option>\n" );
#if defined (WLAN0_5G_11AC_SUPPORT) || defined(WLAN1_5G_11AC_SUPPORT)
		boaWrite(wp, "<option value=63>5 GHz (AC)</option>\n" );
		boaWrite(wp, "<option value=71>5 GHz (N+AC)</option>\n" );		
		boaWrite(wp, "<option value=75>5 GHz (A+N+AC)</option>\n" );
#endif
#endif

#if defined (CONFIG_RTL_92D_SUPPORT)
		if((vChar == PHYBAND_2G) || (wlanBand2G5GSelect == BANDMODESINGLE))
#elif defined(WLAN_DUALBAND_CONCURRENT)
		if(vChar == PHYBAND_2G)
#endif
		{
			boaWrite(wp, "<option value=0>2.4 GHz (B)</option>\n");
			boaWrite(wp, "<option value=1>2.4 GHz (G)</option>\n");
			boaWrite(wp, "<option value=2>2.4 GHz (B+G)</option>\n");
			boaWrite(wp, "<option value=7>2.4 GHz (N)</option>\n" );
			boaWrite(wp, "<option value=9>2.4 GHz (G+N)</option>\n" );
			boaWrite(wp, "<option value=10>2.4 GHz (B+G+N)</option>\n" );
		}
		return 0;
	}
	if ( !strcmp(name, "wlchanwid") ) {
		boaWrite(wp, "<option value=\"0\">20MHZ</option>\n" );
		boaWrite(wp, "<option value=\"1\">40MHZ</option>\n" );
#if defined (WLAN0_5G_11AC_SUPPORT) || defined(WLAN1_5G_11AC_SUPPORT)
		mib_get( MIB_WLAN_PHY_BAND_SELECT, (void *)&vChar);
		if(vChar == PHYBAND_5G) {
			boaWrite(wp, "<option value=\"2\">80MHZ</option>\n" );
		}
#endif
#if defined (WLAN_11N_COEXIST)
#if defined (WLAN0_5G_SUPPORT) || defined(WLAN1_5G_SUPPORT)
		mib_get( MIB_WLAN_PHY_BAND_SELECT, (void *)&vChar);
		if(vChar == PHYBAND_2G)
#endif
			boaWrite(wp, "<option value=\"3\">20/40MHZ</option>\n" );
#endif
		return 0;
	}
	if ( !strcmp(name, "wlctlband") ) {
#if defined (WLAN0_5G_SUPPORT) || defined(WLAN1_5G_SUPPORT)
		mib_get( MIB_WLAN_PHY_BAND_SELECT, (void *)&vChar);
		if(vChar == PHYBAND_5G) {
			boaWrite(wp, "<option value=\"0\">%s</option>\n", multilang(LANG_AUTO) );
			boaWrite(wp, "<option value=\"1\">%s</option>\n", multilang(LANG_AUTO) );
		}
		else{
			boaWrite(wp, "<option value=\"0\">%s</option>\n", multilang(LANG_UPPER) );
			boaWrite(wp, "<option value=\"1\">%s</option>\n", multilang(LANG_LOWER) );
		}

#else
		boaWrite(wp, "<option value=\"0\">%s</option>\n", multilang(LANG_UPPER) );
		boaWrite(wp, "<option value=\"1\">%s</option>\n", multilang(LANG_LOWER) );
#endif
		return 0;
	}
	// Added by Mason Yu for TxPower
	if ( !strcmp(name, "txpower") ) {
		boaWrite(wp, "<option value=\"0\">100%%</option>\n" );
		boaWrite(wp, "<option value=\"1\">70%%</option>\n" );
		boaWrite(wp, "<option value=\"2\">50%%</option>\n" );
		boaWrite(wp, "<option value=\"3\">35%%</option>\n" );
		boaWrite(wp, "<option value=\"4\">15%%</option>\n" );
		return 0;
	}

	if (!strcmp(name, "wifiSecurity")) {
		unsigned char mode = 0;
		MIB_CE_MBSSIB_T Entry;
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		mode = Entry.wlanMode;
		boaWrite(wp, "<option value=%d>%s</option>\n", WIFI_SEC_NONE, multilang(LANG_NONE));
#ifdef WLAN_QTN //QTN do not support WEP
#ifdef WLAN1_QTN
		if(wlan_idx==0)
#elif defined(WLAN0_QTN)
		if(wlan_idx==1)
#endif
#endif
		boaWrite(wp, "<option value=%d>WEP</option>\n", WIFI_SEC_WEP);
#ifndef NEW_WIFI_SEC
		boaWrite(wp, "<option value=%d>WPA</option>\n", WIFI_SEC_WPA);
#endif
		boaWrite(wp, "<option value=%d>WPA2</option>\n", WIFI_SEC_WPA2);
		if (mode != CLIENT_MODE)
		boaWrite(wp, "<option value=%d>%s</option>\n", WIFI_SEC_WPA2_MIXED, multilang(LANG_WPA2_MIXED));
#ifdef CONFIG_RTL_WAPI_SUPPORT
		boaWrite(wp, "<option value=%d>WAPI</option>\n", WIFI_SEC_WAPI);
#endif
		return 0;
	}
#ifdef WLAN_CLIENT
	if (!strcmp(name, "wifiClientSecurity")) {
		boaWrite(wp, "<option value=%d>%s</option>\n", WIFI_SEC_NONE, multilang(LANG_NONE));
		boaWrite(wp, "<option value=%d>WEP</option>\n", WIFI_SEC_WEP);
		boaWrite(wp, "<option value=%d>WPA</option>\n", WIFI_SEC_WPA);
		boaWrite(wp, "<option value=%d>WPA2</option>\n", WIFI_SEC_WPA2);
		boaWrite(wp, "<option value=%d>%s</option>\n", WIFI_SEC_WPA2_MIXED, multilang(LANG_WPA2_MIXED));
#ifdef CONFIG_RTL_WAPI_SUPPORT
		boaWrite(wp, "<option value=%d>WAPI</option>\n", WIFI_SEC_WAPI);
#endif
		return 0;
	}
#endif
	#ifdef WLAN_UNIVERSAL_REPEATER
	if ( !strcmp(name, "repeaterEnabled") ) {
		mib_get( MIB_REPEATER_ENABLED1, (void *)&vChar);
		if (vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	#endif
	if( !strcmp(name, "wlan_idx") ) {
		boaWrite(wp, "%d", wlan_idx);
		return 0;
	}
	if( !strcmp(name, "2G_ssid") ) {
		char ssid[200];
		int i, orig_wlan_idx = wlan_idx;
		MIB_CE_MBSSIB_T Entry;
#if defined(CONFIG_RTL_92D_SUPPORT) || defined(WLAN_DUALBAND_CONCURRENT)
		for(i=0; i<NUM_WLAN_INTERFACE; i++) {
			wlan_idx = i;
			mib_get( MIB_WLAN_PHY_BAND_SELECT, (void *)&vChar);
			if(vChar == PHYBAND_2G) {
				if(!wlan_getEntry(&Entry, 0))
					return -1;
				strcpy(ssid, Entry.ssid);
				translate_web_code(ssid);
				boaWrite(wp, "%s", ssid);
				break;
			}
		}
#else
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		strcpy(ssid, Entry.ssid);
		translate_web_code(ssid);
		boaWrite(wp, "%s", ssid);
#endif
		wlan_idx = orig_wlan_idx;
		return 0;
	}
	if( !strcmp(name, "5G_ssid") ) {
		char ssid[200];
		int i, orig_wlan_idx = wlan_idx;
		MIB_CE_MBSSIB_T Entry;
#if defined (WLAN_QTN)
#ifdef WLAN1_QTN
		wlan_idx = 1;
#else WLAN0_QTN
		wlan_idx = 0;
#endif			
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		strcpy(ssid, Entry.ssid);
		translate_web_code(ssid);
		boaWrite(wp, "%s", ssid);
#elif defined(CONFIG_RTL_92D_SUPPORT) || defined(WLAN_DUALBAND_CONCURRENT)
		for(i=0; i<NUM_WLAN_INTERFACE; i++) {
			wlan_idx = i;
			mib_get( MIB_WLAN_PHY_BAND_SELECT, (void *)&vChar);
			if(vChar == PHYBAND_5G) {
				if(!wlan_getEntry(&Entry, 0))
					return -1;
				strcpy(ssid, Entry.ssid);
				translate_web_code(ssid);
				boaWrite(wp, "%s", ssid);
				break;
			}
		}
#else
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		strcpy(ssid, Entry.ssid);
		boaWrite(wp, "%s", ssid);
#endif
		wlan_idx = orig_wlan_idx;
		return 0;
	}
	if (!strcmp(name, "dfs_enable")) {
#if defined(CONFIG_RTL_DFS_SUPPORT)
		boaWrite(wp, "1");
#else
		boaWrite(wp, "0");
#endif
		return 0;
	}
	if( !strcmp(name, "Band2G5GSupport") ) {
#if defined(CONFIG_RTL_92D_SUPPORT) || defined(WLAN0_5G_SUPPORT) || defined(WLAN1_5G_SUPPORT)
		mib_get( MIB_WLAN_PHY_BAND_SELECT, (void *)&vChar);
		boaWrite(wp, "%d", vChar);
#else
		vChar = PHYBAND_2G;
		boaWrite(wp, "%d", vChar);
#endif //CONFIG_RTL_92D_SUPPORT
		return 0;
	}
#if defined(CONFIG_RTL_92D_SUPPORT)
	if( !strcmp(name, "wlanBand2G5GSelect") ) {
		mib_get( MIB_WLAN_BAND2G5G_SELECT, (void *)&vChar);
		boaWrite(wp, "%d", vChar);
		return 0;
	}
	if( !strcmp(name, "onoff_dmdphy_comment_start") ) {
#ifdef CONFIG_RTL_92D_DMDP
		boaWrite(wp, "");
#else //CONFIG_RTL_92D_DMDP
		boaWrite(wp, "<!--");
#endif //CONFIG_RTL_92D_DMDP
		return 0;
	}

	if( !strcmp(name, "onoff_dmdphy_comment_end") ) {
#ifdef CONFIG_RTL_92D_DMDP
		boaWrite(wp, "");
#else //CONFIG_RTL_92D_DMDP
		boaWrite(wp, "-->");
#endif //CONFIG_RTL_92D_DMDP
		return 0;
	}
#elif defined(WLAN_DUALBAND_CONCURRENT)
	if( !strcmp(name, "wlanBand2G5GSelect") ) {
		boaWrite(wp, "%d", BANDMODEBOTH);
		return 0;
	}
#elif defined(WLAN0_5G_SUPPORT) || defined(WLAN1_5G_SUPPORT)
	if( !strcmp(name, "wlanBand2G5GSelect") ) {
		boaWrite(wp, "%d", BANDMODESINGLE);
		return 0;
	}
#else
	if( !strcmp(name, "wlanBand2G5GSelect") ) {
		boaWrite(wp, "");
		return 0;
	}
#endif //CONFIG_RTL_92D_SUPPORT
	if ( !strcmp(name, "wlan_mssid_num")) {
		boaWrite(wp, "%d", WLAN_MBSSID_NUM);
		return 0;
	}
	if(!strcmp(name,"new_wifi_security")) //wifi security requirements after 2014.01.01
	{
#ifdef NEW_WIFI_SEC
		boaWrite(wp, "1");
#else
		boaWrite(wp, "0");
#endif
		return 0;
	}
	if( !strcmp(name, "is_regdomain_demo") ) {
		mib_get( MIB_WIFI_REGDOMAIN_DEMO, (void *)&vChar);
		boaWrite(wp, "%d", vChar);
		return 0;
	}
	if ( !strcmp(name, "regdomain_list") ) {
		boaWrite(wp, "<option value=\"1\">FCC(1)</option>\n" );
		boaWrite(wp, "<option value=\"2\">IC(2)</option>\n" );
		boaWrite(wp, "<option value=\"3\">ETSI(3)</option>\n" );
		boaWrite(wp, "<option value=\"4\">SPAIN(4)</option>\n" );
		boaWrite(wp, "<option value=\"5\">FRANCE(5)</option>\n" );
		boaWrite(wp, "<option value=\"6\">MKK(6)</option>\n" );
		boaWrite(wp, "<option value=\"7\">ISREAL(7)</option>\n" );
		boaWrite(wp, "<option value=\"8\">MKK1(8)</option>\n" );
		boaWrite(wp, "<option value=\"9\">MKK2(9)</option>\n" );
		boaWrite(wp, "<option value=\"10\">MKK3(10)</option>\n" );
		boaWrite(wp, "<option value=\"11\">NCC(11)</option>\n" );
		boaWrite(wp, "<option value=\"12\">RUSSIAN(12)</option>\n" );
		boaWrite(wp, "<option value=\"13\">CN(13)</option>\n" );
		boaWrite(wp, "<option value=\"14\">GLOBAL(14)</option>\n" );
		boaWrite(wp, "<option value=\"15\">WORLD-WIDE(15)</option>\n" );

		return 0;
	}
	if(!strcmp(name, "11w_support")){
#if 0        /*disable WLAN_11W if there are some issue with it*/
		boaWrite(wp, "1");
#else
		boaWrite(wp, "0");
#endif
		return 0;
	}
#ifdef WLAN_11R
	if(!strcmp(name, "11r_ftkh_num")){
		boaWrite(wp, "%d", MAX_VWLAN_FTKH_NUM);
		return 0;
	}
#endif
#ifdef WLAN_QTN
	if(!strcmp(name, "wlan_qtn_hidden_function")){
#ifdef WLAN1_QTN
		if(wlan_idx==1)
#else
		if(wlan_idx==0)
#endif
			boaWrite(wp, "style=\"display: none\"");
		return 0;
	}
#endif
	if(!strcmp(name, "is_wlan_qtn")){
#ifdef WLAN_QTN
#ifdef WLAN1_QTN
		if(wlan_idx==1)
#else
		if(wlan_idx==0)
#endif
			boaWrite(wp, "1");
		else
#endif
		boaWrite(wp, "0");
		return 0;
	}
	if(!strcmp(name, "ch_list_20")){
#ifdef WLAN_QTN
#ifdef WLAN1_QTN
		if(wlan_idx==1)
		{
			int mib_id = MIB_HW_WLAN1_REG_DOMAIN;
#else
		if(wlan_idx==0)
		{
			int mib_id = MIB_HW_REG_DOMAIN;
#endif
		
			char regdomain;
			char ch_list[1025];
			mib_get(mib_id, &regdomain);
			if(regdomain==0)
				boaWrite(wp, "0");
			else{
				rt_qcsapi_get_channel_list(rt_get_qtn_ifname(getWlanIfName()), regdomain, 20, ch_list);
				boaWrite(wp, "\"%s\"", ch_list);
			}
		}
		else
#endif
		boaWrite(wp, "0");

		return 0;
	}
	if(!strcmp(name, "ch_list_40")){
#ifdef WLAN_QTN
#ifdef WLAN1_QTN
		if(wlan_idx==1)
		{
			int mib_id = MIB_HW_WLAN1_REG_DOMAIN;
#else
		if(wlan_idx==0)
		{
			int mib_id = MIB_HW_REG_DOMAIN;
#endif
			char regdomain;
			char ch_list[1025];
			mib_get(mib_id, &regdomain);
			if(regdomain==0)
				boaWrite(wp, "0");
			else{
				rt_qcsapi_get_channel_list(rt_get_qtn_ifname(getWlanIfName()), regdomain, 40, ch_list);
				boaWrite(wp, "\"%s\"", ch_list);
			}
		}
		else
#endif
		boaWrite(wp, "0");

		return 0;
	}
	if(!strcmp(name, "ch_list_80")){
#ifdef WLAN_QTN
#ifdef WLAN1_QTN
		if(wlan_idx==1)
		{
			int mib_id = MIB_HW_WLAN1_REG_DOMAIN;
#else
		if(wlan_idx==0)
		{
			int mib_id = MIB_HW_REG_DOMAIN;
#endif
			char regdomain;
			char ch_list[1025];
			mib_get(mib_id, &regdomain);
			if(regdomain==0)
				boaWrite(wp, "0");
			else{
				rt_qcsapi_get_channel_list(rt_get_qtn_ifname(getWlanIfName()), regdomain, 80, ch_list);
				boaWrite(wp, "\"%s\"", ch_list);
			}
		}
		else
#endif
		boaWrite(wp, "0");

		return 0;
	}
	if(!strcmp(name,"wlan_support_11ncoexist"))
	{
#ifdef	WLAN_11N_COEXIST
		boaWrite(wp, "1");
#else
		boaWrite(wp, "0");
#endif
	}
#endif // of WLAN_SUPPORT
	if(!strcmp(name,"wlan_support_8812e")) //8812
	{
#if defined(CONFIG_WLAN_HAL_8814AE) ||defined(CONFIG_RTL_8812_SUPPORT) || (defined(WLAN_DUALBAND_CONCURRENT) &&!defined(WLAN_QTN))
		boaWrite(wp, "1");
#else
		boaWrite(wp, "0");
#endif
		return 0;
	}
	if ( !strcmp(name, "wlan_fon")) {
#ifdef CONFIG_USER_FON
			boaWrite(wp, "1");
#else
			boaWrite(wp, "0");
#endif
		return 0;
	}
#ifdef CONFIG_USER_FON
	if ( !strcmp(name, "wlan_fon_onoffline")) {
		mib_get( MIB_FON_ONOFF, (void *)&vChar);
		boaWrite(wp, "%d", vChar);
		return 0;
	}
#endif
	// Added by Mason Yu for 2 level web page
	if ( !strcmp(name, "userMode") ) {
		#ifdef ACCOUNT_CONFIG
		MIB_CE_ACCOUNT_CONFIG_T Entry;
		int totalEntry, i;
		#else
		char suStr[100], usStr[100];
		#endif
#ifdef ACCOUNT_CONFIG
		#ifdef USE_LOGINWEB_OF_SERVER
		if (!strcmp(g_login_username, suName))
		#else
		if (!strcmp(wp->user, suName))
		#endif
		{
			boaWrite(wp, "<option selected value=\"0\">%s</option>\n", suName);
			boaWrite(wp, "<option value=\"1\">%s</option>\n", usName);
		}
		#ifdef USE_LOGINWEB_OF_SERVER
		else if (!strcmp(g_login_username, usName))
		#else
		else if (!strcmp(wp->user, usName))
		#endif
		{
			boaWrite(wp, "<option value=\"0\">%s</option>\n", suName);
			boaWrite(wp, "<option selected value=\"1\">%s</option>\n", usName);
		}
		totalEntry = mib_chain_total(MIB_ACCOUNT_CONFIG_TBL);
		for (i=0; i<totalEntry; i++) {
			if (!mib_chain_get(MIB_ACCOUNT_CONFIG_TBL, i, (void *)&Entry))
				continue;
			#ifdef USE_LOGINWEB_OF_SERVER
			if (!strcmp(g_login_username, Entry.userName))
			#else
			if (strcmp(wp->user, Entry.userName) == 0)
			#endif
				boaWrite(wp, "<option selected value=\"%d\">%s</option>\n", i+2, Entry.userName);
			else
				boaWrite(wp, "<option value=\"%d\">%s</option>\n", i+2, Entry.userName);
		}
#else
		#ifdef USE_LOGINWEB_OF_SERVER
		if (!strcmp(g_login_username, suName))
		#else
		if(!strcmp(wp->user,suName))
		#endif
			{
			sprintf(suStr, "<option selected value=\"0\">%s</option>\n", suName);
			sprintf(usStr, "<option value=\"1\">%s</option>\n", usName);
			}
		else
			sprintf(usStr, "<option selected value=\"1\">%s</option>\n", usName);

		boaWrite(wp, suStr );
		boaWrite(wp, usStr );
#endif
		return 0;
	}
	if ( !strcmp(name, "lan-dhcp-st") ) {
		#ifdef CONFIG_USER_DHCP_SERVER
		if ( !mib_get( MIB_DHCP_MODE, (void *)&vChar) )
			return -1;

		if (DHCP_LAN_SERVER == vChar)
			boaWrite(wp, (char *)multilang(LANG_ENABLED));
		else
		#endif
			boaWrite(wp, (char *)multilang(LANG_DISABLED));
		return 0;
	}
	else if ( !strcmp(name, "br-stp-0") ) {
		if ( !mib_get( MIB_BRCTL_STP, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "br-stp-1") ) {
		if ( !mib_get( MIB_BRCTL_STP, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#ifdef CONFIG_USER_IGMPPROXY
	else if ( !strcmp(name, "igmpProxy0") ) {
		if ( !mib_get( MIB_IGMP_PROXY, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		if (ifWanNum("rt") ==0)
			boaWrite(wp, " disabled");
		return 0;
	}
	else if ( !strcmp(name, "igmpProxy1") ) {
		if ( !mib_get( MIB_IGMP_PROXY, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		if (ifWanNum("rt") ==0)
			boaWrite(wp, " disabled");
		return 0;
	}
	else if ( !strcmp(name, "igmpProxy0d") ) {
		if ( !mib_get( MIB_IGMP_PROXY, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "disabled");
		return 0;
	}
#endif
#if defined(CONFIG_USER_PPTP_CLIENT_PPTP) || defined(CONFIG_USER_PPTPD_PPTPD)
	else if (!strcmp(name, "pptpenable0")) {
		if ( !mib_get( MIB_PPTP_ENABLE, (void *)&vUInt) )
			return -1;
		//printf("pptp %s\n", vUInt?"enable":"disable");
		if (0 == vUInt)
			boaWrite(wp, "checked");
		return 0;
	}
	else if (!strcmp(name, "pptpenable1")) {
		if ( !mib_get( MIB_PPTP_ENABLE, (void *)&vUInt) )
			return -1;
		//printf("pptp %s\n", vUInt?"enable":"disable");
		if (1 == vUInt)
			boaWrite(wp, "checked");
		return 0;
	}
#endif //end of CONFIG_USER_PPTP_CLIENT_PPTP
#if defined(CONFIG_USER_L2TPD_L2TPD) || defined(CONFIG_USER_L2TPD_LNS)
	else if (!strcmp(name, "l2tpenable0")) {
		if (!mib_get( MIB_L2TP_ENABLE, (void *)&vUInt))
			return -1;
		if (0 == vUInt)
			boaWrite(wp, "checked");
		return 0;
	}
	else if (!strcmp(name, "l2tpenable1")) {
		if ( !mib_get( MIB_L2TP_ENABLE, (void *)&vUInt) )
			return -1;
		if (1 == vUInt)
			boaWrite(wp, "checked");
		return 0;
	}
#endif //endof CONFIG_USER_L2TPD_L2TPD
#ifdef CONFIG_NET_IPIP
	else if (!strcmp(name, "ipipenable0")) {
		if (!mib_get( MIB_IPIP_ENABLE, (void *)&vUInt))
			return -1;
		if (0 == vUInt)
			boaWrite(wp, "checked");
		return 0;
	}
	else if (!strcmp(name, "ipipenable1")) {
		if ( !mib_get( MIB_IPIP_ENABLE, (void *)&vUInt) )
			return -1;
		if (1 == vUInt)
			boaWrite(wp, "checked");
		return 0;
	}
#endif//endof CONFIG_NET_IPIP
#ifdef CONFIG_USER_MINIUPNPD
	else if ( !strcmp(name, "upnp0") ) {
		if ( !mib_get( MIB_UPNP_DAEMON, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		if (ifWanNum("rt") ==0)
			boaWrite(wp, " disabled");
		return 0;
	}
	else if ( !strcmp(name, "upnp1") ) {
		if ( !mib_get( MIB_UPNP_DAEMON, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		if (ifWanNum("rt") ==0)
			boaWrite(wp, " disabled");
		return 0;
	}
	else if ( !strcmp(name, "upnp0d") ) {
		//if ( !mib_get( MIB_UPNP_DAEMON, (void *)&vChar) )
		//	return -1;
		if (ifWanNum("rt") ==0)
			boaWrite(wp, "disabled");
		return 0;
	}
#ifdef CONFIG_TR_064
	else if(!strcmp(name, "tr064_switch"))
	{
		int sw = TR064_STATUS;

		boaWrite(wp, "<tr>\n");
		boaWrite(wp, "\t\t<td><b>%s:</b></td>\n", multilang(LANG_TR064));
		boaWrite(wp, "\t\t<td>\n");
		boaWrite(wp, "\t\t\t<input type=radio value=0 name=tr_064_sw %s>%s&nbsp;&nbsp;\n", sw? "":"checked", multilang(LANG_DISABLE));
		boaWrite(wp, "\t\t\t<input type=radio value=1 name=tr_064_sw %s>%s\n", sw? "checked": "", multilang(LANG_ENABLE));
		boaWrite(wp, "\t\t</td>\n\t</tr>\n");
	}
#endif
#endif
#ifdef TIME_ZONE
	else if ( !strcmp(name, "sntp0d") ) {		
		if (ifWanNum("rt") ==0)
			boaWrite(wp, "disabled");
		return 0;
	}
#endif
	else if(!strcmp(name, "config_rtk_rg"))
	{
#ifdef CONFIG_RTK_RG_INIT
		boaWrite(wp, "\"yes\"");
#else
		boaWrite(wp, "\"no\"");
#endif
	}
#ifdef CONFIG_RTK_RG_INIT
	else if(!strcmp(name, "rg_hidden_function"))
	{
		boaWrite(wp, "style=\"display: none\"");
	}
	else if(!strcmp(name, "show_vlan_id"))
	{
		int elan_vid;

		mib_get(MIB_LAN_VLAN_ID1, (void *)&elan_vid);

		boaWrite(wp, "				if(itlk.cmode == 0)\n");
		boaWrite(wp, "					vid.value = %d\n", elan_vid);
		boaWrite(wp, "				else\n");
		boaWrite(wp, "					vid.value = 8\n");
	}
#else
	else if(!strcmp(name, "rg_only_function"))
	{
		boaWrite(wp, "style=\"display: none\"");
	}
#endif
// Mason Yu. MLD Proxy
#ifdef CONFIG_IPV6
	else if ( !strcmp(name, "ipv6enabledisable0") ) {
		if ( !mib_get( MIB_V6_IPV6_ENABLE, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "ipv6enabledisable1") ) {
		if ( !mib_get( MIB_V6_IPV6_ENABLE, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#ifdef CONFIG_USER_ECMH
	else if ( !strcmp(name, "mldproxy0") ) {
		if ( !mib_get( MIB_MLD_PROXY_DAEMON, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		if (ifWanNum("rtv6") ==0)
			boaWrite(wp, " disabled");
		return 0;
	}
	else if ( !strcmp(name, "mldproxy1") ) {
		if ( !mib_get( MIB_MLD_PROXY_DAEMON, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		if (ifWanNum("rtv6") ==0)
			boaWrite(wp, " disabled");
		return 0;
	}
	else if ( !strcmp(name, "mldproxy0d") ) {
		//if ( !mib_get( MIB_MLD_PROXY_DAEMON, (void *)&vChar) )
		//	return -1;
		if (ifWanNum("rtv6") ==0)
			boaWrite(wp, "disabled");
		return 0;
	}
#endif

#if defined(CONFIG_USER_DHCPV6_ISC_DHCP411)
	else if ( !strcmp(name, "prefix_delegation_info") ) {
		struct in6_addr ip6Prefix;
		unsigned char value[48], len;

		len = cmd_get_PD_prefix_len();
		if (0 == len) {
			boaWrite(wp, "");
		}
		else {
			cmd_get_PD_prefix_ip((void *)&ip6Prefix);
			inet_ntop(PF_INET6, &ip6Prefix, value, sizeof(value));
			boaWrite(wp, "%s/%d", value, len);
		}
		return 0;
	}
#endif
#endif	// #ifdef CONFIG_IPV6

#ifdef NAT_CONN_LIMIT
	else if (!strcmp(name, "connlimit")) {
		if (!mib_get(MIB_NAT_CONN_LIMIT, (void *)&vChar))
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
	}
#endif
#ifdef TCP_UDP_CONN_LIMIT
	else if ( !strcmp(name, "connLimit-cap0") ) {
   		if ( !mib_get( MIB_CONNLIMIT_ENABLE, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "connLimit-cap1") ) {
   		if ( !mib_get( MIB_CONNLIMIT_ENABLE, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}

#endif
#ifdef IP_ACL
	else if ( !strcmp(name, "acl-cap0") ) {
   		if ( !mib_get( MIB_ACL_CAPABILITY, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "acl-cap1") ) {
   		if ( !mib_get( MIB_ACL_CAPABILITY, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#endif
#ifdef CONFIG_USER_SNMPD_SNMPD_V2CTRAP
	else if ( !strcmp(name, "snmpd-on") ) {
   		if ( !mib_get( MIB_SNMPD_ENABLE, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "snmpd-off") ) {
   		if ( !mib_get( MIB_SNMPD_ENABLE, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#endif
#ifdef URL_BLOCKING_SUPPORT
	else if ( !strcmp(name, "url-cap0") ) {
   		if ( !mib_get( MIB_URL_CAPABILITY, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "url-cap1") ) {
   		if ( !mib_get( MIB_URL_CAPABILITY, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#endif
//alex_huang
#ifdef URL_ALLOWING_SUPPORT
       else if( !strcmp(name ,"url-cap2") ) {
	   	if( !mib_get (MIB_URL_CAPABILITY,(void*)&vChar) )
			return -1;
		if(2 == vChar)
			{
			    boaWrite(wp, "checked");
			}
		return 0;

       	}
#endif


#ifdef DOMAIN_BLOCKING_SUPPORT
	else if ( !strcmp(name, "domainblk-cap0") ) {
   		if ( !mib_get( MIB_DOMAINBLK_CAPABILITY, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "domainblk-cap1") ) {
   		if ( !mib_get( MIB_DOMAINBLK_CAPABILITY, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#endif
	else if ( !strcmp(name, "dns0") ) {
		if ( !mib_get( MIB_ADSL_WAN_DNS_MODE, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "dns1") ) {
		if ( !mib_get( MIB_ADSL_WAN_DNS_MODE, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#ifdef PORT_FORWARD_GENERAL
	else if ( !strcmp(name, "portFw-cap0") ) {
   		if ( !mib_get( MIB_PORT_FW_ENABLE, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "portFw-cap1") ) {
   		if ( !mib_get( MIB_PORT_FW_ENABLE, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "portFwNum")) {
		vUInt = mib_chain_total(MIB_PORT_FW_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelButton();");
		return 0;
	}
#endif
#ifdef NATIP_FORWARDING
	else if ( !strcmp(name, "ipFwEn")) {
		if ( !mib_get( MIB_IP_FW_ENABLE, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	if ( !strcmp(name, "ipFwNum")) {
		vUInt = mib_chain_total(MIB_IP_FW_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelButton();");
		return 0;
	}
#endif
#ifdef CONFIG_IPV6
#ifdef CONFIG_USER_RADVD
	else if ( !strcmp(name, "radvd_SendAdvert0")) {
		if ( !mib_get( MIB_V6_SENDADVERT, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "radvd_SendAdvert1")) {
		if ( !mib_get( MIB_V6_SENDADVERT, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "radvd_ManagedFlag0")) {
		if ( !mib_get( MIB_V6_MANAGEDFLAG, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "radvd_ManagedFlag1")) {
		if ( !mib_get( MIB_V6_MANAGEDFLAG, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "radvd_OtherConfigFlag0")) {
		if ( !mib_get( MIB_V6_OTHERCONFIGFLAG, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "radvd_OtherConfigFlag1")) {
		if ( !mib_get( MIB_V6_OTHERCONFIGFLAG, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "radvd_EnableULAFlag0")) {
		if ( !mib_get( MIB_V6_ULAPREFIX_ENABLE, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "radvd_EnableULAFlag1")) {
		if ( !mib_get( MIB_V6_ULAPREFIX_ENABLE, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "radvd_OnLink0")) {
		if ( !mib_get( MIB_V6_ONLINK, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "radvd_OnLink1")) {
		if ( !mib_get( MIB_V6_ONLINK, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "radvd_Autonomous0")) {
		if ( !mib_get( MIB_V6_AUTONOMOUS, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "radvd_Autonomous1")) {
		if ( !mib_get( MIB_V6_AUTONOMOUS, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#endif // of CONFIG_USER_RADVD
#endif
#ifdef IP_PORT_FILTER
	else if ( !strcmp(name, "ipf_out_act0")) {
		if ( !mib_get( MIB_IPF_OUT_ACTION, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "ipf_out_act1")) {
		if ( !mib_get( MIB_IPF_OUT_ACTION, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "ipf_in_act0")) {
		if ( !mib_get( MIB_IPF_IN_ACTION, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "ipf_in_act1")) {
		if ( !mib_get( MIB_IPF_IN_ACTION, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#endif
#ifdef CONFIG_IPV6
	else if ( !strcmp(name, "v6_ipf_out_act0")) {
		if ( !mib_get( MIB_V6_IPF_OUT_ACTION, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "v6_ipf_out_act1")) {
		if ( !mib_get( MIB_V6_IPF_OUT_ACTION, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "v6_ipf_in_act0")) {
		if ( !mib_get( MIB_V6_IPF_IN_ACTION, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "v6_ipf_in_act1")) {
		if ( !mib_get( MIB_V6_IPF_IN_ACTION, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#endif
	else if ( !strcmp(name, "macf_out_act0")) {
		if ( !mib_get( MIB_MACF_OUT_ACTION, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "macf_out_act1")) {
		if ( !mib_get( MIB_MACF_OUT_ACTION, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "macf_in_act0")) {
		if ( !mib_get( MIB_MACF_IN_ACTION, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "macf_in_act1")) {
		if ( !mib_get( MIB_MACF_IN_ACTION, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#ifdef CONFIG_USER_DDNS
	else if ( !strcmp(name, "ddns_act")) {
		if ( !mib_get( MIB_DDNS_ENABLE, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "ddns_enable")) {
		if ( !mib_get( MIB_DDNS_ENABLE, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "0");
		else
			boaWrite(wp, "1");
		return 0;
	}
#endif
#ifdef DMZ
	else if ( !strcmp(name, "dmz-cap0") ) {
   		if ( !mib_get( MIB_DMZ_ENABLE, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "dmz-cap1") ) {
   		if ( !mib_get( MIB_DMZ_ENABLE, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#endif
#ifdef CONFIG_USER_VLAN_ON_LAN
	else if ( !strcmp(name, "lan1-vid-cap0") ) {
		if ( !mib_chain_get(MIB_SW_PORT_TBL, 0, &sw_entry))
			return -1;
		if (0 == sw_entry.vlan_on_lan_enabled)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "lan1-vid-cap1") ) {
		if ( !mib_chain_get(MIB_SW_PORT_TBL, 0, &sw_entry))
			return -1;
		if (1 == sw_entry.vlan_on_lan_enabled)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "lan2-vid-cap0") ) {
		if ( !mib_chain_get(MIB_SW_PORT_TBL, 1, &sw_entry))
			return -1;
		if (0 == sw_entry.vlan_on_lan_enabled)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "lan2-vid-cap1") ) {
		if ( !mib_chain_get(MIB_SW_PORT_TBL, 1, &sw_entry))
			return -1;
		if (1 == sw_entry.vlan_on_lan_enabled)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "lan3-vid-cap0") ) {
		if ( !mib_chain_get(MIB_SW_PORT_TBL, 2, &sw_entry))
			return -1;
		if (0 == sw_entry.vlan_on_lan_enabled)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "lan3-vid-cap1") ) {
		if ( !mib_chain_get(MIB_SW_PORT_TBL, 2, &sw_entry))
			return -1;
		if (1 == sw_entry.vlan_on_lan_enabled)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "lan4-vid-cap0") ) {
		if ( !mib_chain_get(MIB_SW_PORT_TBL, 3, &sw_entry))
			return -1;
		if (0 == sw_entry.vlan_on_lan_enabled)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "lan4-vid-cap1") ) {
		if ( !mib_chain_get(MIB_SW_PORT_TBL, 3, &sw_entry))
			return -1;
		if (1 == sw_entry.vlan_on_lan_enabled)
			boaWrite(wp, "checked");
		return 0;
	}
#endif
#ifdef IP_PORT_FILTER
	else if ( !strcmp(name, "ipFilterNum")) {
		vUInt = mib_chain_total(MIB_IP_PORT_FILTER_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelButton();");
		return 0;
	}
#endif
#ifdef CONFIG_IPV6
	else if ( !strcmp(name, "ipFilterNumV6")) {
		vUInt = mib_chain_total(MIB_V6_IP_PORT_FILTER_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelButton();");
		return 0;
	}
#endif
#ifdef TCP_UDP_CONN_LIMIT
	else if ( !strcmp(name, "connLimitNum")) {
		vUInt = mib_chain_total(MIB_TCP_UDP_CONN_LIMIT_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelButton();");
		return 0;
	}
#endif
#ifdef MULTI_ADDRESS_MAPPING
	else if ( !strcmp(name, "AddresMapNum")) {
		vUInt = mib_chain_total(MULTI_ADDRESS_MAPPING_LIMIT_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelButton();");
		return 0;
	}
#endif  // end of MULTI_ADDRESS_MAPPING
#ifdef URL_BLOCKING_SUPPORT
	else if ( !strcmp(name, "keywdNum")) {
		vUInt = mib_chain_total(MIB_KEYWD_FILTER_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelKeywdButton();");
		return 0;
	}
	else if ( !strcmp(name, "FQDNNum")) {
		vUInt = mib_chain_total(MIB_URL_FQDN_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelFQDNButton();");
		return 0;
	}
#endif
#ifdef DOMAIN_BLOCKING_SUPPORT
	else if ( !strcmp(name, "domainNum")) {
		vUInt = mib_chain_total(MIB_DOMAIN_BLOCKING_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelButton();");
		return 0;
	}
#endif
	else if ( !strcmp(name, "ripNum")) {
		vUInt = mib_chain_total(MIB_RIP_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelButton();");
		return 0;
	}
#ifdef IP_ACL
	else if ( !strcmp(name, "aclNum")) {
		vUInt = mib_chain_total(MIB_ACL_IP_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelButton();");
		return 0;
	}
#endif
#ifdef MAC_FILTER
	else if ( !strcmp(name, "macFilterNum")) {
		vUInt = mib_chain_total(MIB_MAC_FILTER_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelButton();");
		return 0;
	}
#endif
#ifdef PARENTAL_CTRL
	else if ( !strcmp(name, "parentCtrlNum")) {
		vUInt = mib_chain_total(MIB_PARENTAL_CTRL_TBL);
		if (0 == vUInt)
			boaWrite(wp, "disableDelButton();");
		return 0;
	}
	else if ( !strcmp(name, "parental-ctrl-on-0") ) {
		if ( !mib_get( MIB_PARENTAL_CTRL_ENABLE, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "parental-ctrl-on-1") ) {
		if ( !mib_get( MIB_PARENTAL_CTRL_ENABLE, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#endif
	else if ( !strcmp(name, "vcMax")) {
		vUInt = mib_chain_total(MIB_ATM_VC_TBL);
		if (vUInt >= 16) {
			boaWrite(wp, "alert(\"Max number of ATM VC Settings is 16!\");");
			boaWrite(wp, "return false;");
		}
		return 0;
	}
	else if ( !strcmp(name, "vcCount")) {
		vUInt = mib_chain_total(MIB_ATM_VC_TBL);
		if (vUInt == 0) {
			boaWrite(wp, "disableButton(document.adsl.delvc);");
			// Commented by Mason Yu. The "refresh" button is be disabled on wanadsl.asp
			//boaWrite(wp, "disableButton(document.adsl.refresh);");
		}
		return 0;
	}
#ifdef CONFIG_USER_PPPOE_PROXY
  else if(!strcmp(name,"pppoeProxy"))
  	{
  	boaWrite(wp,"<tr><td><font size=2><b>PPPoE Proxy:</b></td>"
         "<td><b><input type=\"radio\" value=1 name=\"pppEnable\" >Enable&nbsp;&nbsp;"
	"<input type=\"radio\" value=0 name=\"pppEnable\" checked>Disable</b></td></tr>");
  	}
  else if(!strcmp(name,"pppSettingsDisable"))
  	{
  	  boaWrite(wp,"{document.adsl.pppEnable[0].disabled=true;\n"
	  	"document.adsl.pppEnable[1].disabled=true;}");
  	}
    else if(!strcmp(name,"pppSettingsEnable"))
  	{
  	  boaWrite(wp,"{document.adsl.pppEnable[0].disabled=false;\n"
	  	"document.adsl.pppEnable[1].disabled=false;}else{document.adsl.pppEnable[0].disabled=true;\n"
	  	"document.adsl.pppEnable[1].disabled=true;}"
	  	"document.adsl.pppEnable[0].checked=false;"
	  	"document.adsl.pppEnable[1].checked=true;");
  	}

 #endif
  #ifdef CONFIG_USER_PPPOE_PROXY
     else if(!strcmp(name,"pppoeProxyEnable"))
     	{
	boaWrite(wp,"  if(mode==\"PPPoE\")"
		"{if(pppoeProxyEnable)"
		"{ document.adsl.pppEnable[0].checked=true;\n"
                  "document.adsl.pppEnable[1].checked=false;}\n"
		"else {document.adsl.pppEnable[0].checked=false;"
		 " document.adsl.pppEnable[1].checked=true;}  "
		" document.adsl.pppEnable[0].disabled=false;"
			  " document.adsl.pppEnable[1].disabled=false;");
	boaWrite(wp," }else"
		"{"
		"	  document.adsl.pppEnable[0].checked=false;"
		"	   document.adsl.pppEnable[1].checked=true;"
		"	   document.adsl.pppEnable[0].disabled=true;"
		"	   document.adsl.pppEnable[1].disabled=true;}"
		);
     	}
  #else
     else if(!strcmp(name,"pppoeProxyEnable"))
     	{

     	}
  #endif

#ifdef WLAN_SUPPORT
	else if ( !strcmp(name, "wl_txRate")) {
		struct _misc_data_ misc_data;
		MIB_CE_MBSSIB_T Entry;
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		boaWrite(wp, "band=%d\n", Entry.wlanBand);
		boaWrite(wp, "txrate=%u\n",Entry.fixedTxRate);
		boaWrite(wp, "auto=%d\n",Entry.rateAdaptiveEnabled);
		mib_get( MIB_WLAN_CHANNEL_WIDTH, (void *)&vChar);
		boaWrite(wp, "chanwid=%d\n",vChar);

		//cathy, get rf number
		memset(&misc_data, 0, sizeof(struct _misc_data_));
		getMiscData(getWlanIfName(), &misc_data);
		boaWrite(wp, "rf_num=%u\n", misc_data.mimo_tr_used);
		return 0;
	}
	else if ( !strcmp(name, "wl_chno")) {
//xl_yue:
		mib_get( MIB_HW_REG_DOMAIN, (void *)&vChar);
		boaWrite(wp, "regDomain=%d\n",vChar);
#ifdef WLAN1_QTN
		mib_get( MIB_HW_WLAN1_REG_DOMAIN, (void *)&vChar);
		boaWrite(wp, "regDomain_qtn=%d\n",vChar);
#elif defined(WLAN0_QTN)
		mib_get( MIB_HW_REG_DOMAIN, (void *)&vChar);
		boaWrite(wp, "regDomain_qtn=%d\n",vChar);
#endif
		mib_get( MIB_WLAN_AUTO_CHAN_ENABLED,(void *)&vChar);
		if(vChar)
			boaWrite(wp, "defaultChan=0\n");
		else
		{
			mib_get( MIB_WLAN_CHAN_NUM ,(void *)&vChar);
			boaWrite(wp, "defaultChan=%d\n",vChar);
		}
		return 0;
	}
	else if ( !strcmp(name, "rf_used")) {
		struct _misc_data_ misc_data;
		memset(&misc_data, 0, sizeof(struct _misc_data_));
		getMiscData(getWlanIfName(), &misc_data);
		boaWrite(wp, "%u\n", misc_data.mimo_tr_used);
		return 0;
	}
#endif

	//for web log
	else if ( !strcmp(name, "log-cap0") ) {
   		if ( !mib_get( MIB_SYSLOG, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "log-cap1") ) {
   		if ( !mib_get( MIB_SYSLOG, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
#ifdef CONFIG_USER_SAMBA
	else if (!strcmp(name, "samba-cap0")) {
		if (!mib_get(MIB_SAMBA_ENABLE, &vChar))
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	} else if (!strcmp(name, "samba-cap1")) {
		if (!mib_get(MIB_SAMBA_ENABLE, &vChar))
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if (!strcmp(name, "nmbd-cap")) {
#ifndef CONFIG_USER_NMBD
		boaWrite(wp, "style=\"display: none\"");
#endif
		return 0;
	}
#endif
	else if (!strcmp(name, "anxb-cap")) {
		mib_get(MIB_ADSL_MODE, (void *)&vUShort);
		if(vUShort&ADSL_MODE_ANXB)
			boaWrite(wp, "style=\"display: none\"");
		return 0;
	}
	else if (!strcmp(name, "ginp-cap")) {
#ifndef ENABLE_ADSL_MODE_GINP
		boaWrite(wp, "style=\"display: none\"");
#endif
		return 0;
	}
	else if (!strcmp(name, "vdsl-cap")) {
#ifndef CONFIG_VDSL
		boaWrite(wp, "style=\"display: none\"");
#endif
		return 0;
	}
	else if (!strcmp(name, "IPQoS")) {
#if !defined(CONFIG_USER_IP_QOS) || defined(BRIDGE_ONLY_ON_WEB)
		boaWrite(wp, "style=\"display: none\"");
#endif
		return 0;
	}
	else if (!strcmp(name, "bridge-only")) {
#ifdef CONFIG_SFU
		boaWrite(wp, "style=\"display: none\"");
#endif
		return 0;
	}

	if (!strcmp(name, "syslog-log") || !strcmp(name, "syslog-display")) {
		char *SYSLOGLEVEL[] = {"Emergency", "Alert", "Critical", "Error", "Warning", "Notice", "Infomational", "Debugging"};
		int i;
		if (!strcmp(name, "syslog-log")) {
			if (!mib_get(MIB_SYSLOG_LOG_LEVEL, (void *)&vChar))
				return -1;
		}
		else if (!strcmp(name, "syslog-display")) {
			if (!mib_get(MIB_SYSLOG_DISPLAY_LEVEL, (void *)&vChar))
				return -1;
		}
		for (i=0; i<8; i++) {
			if (i == vChar)
				boaWrite(wp,"<option selected value=\"%d\">%s</option>", i, SYSLOGLEVEL[i]);
			else
				boaWrite(wp,"<option value=\"%d\">%s</option>", i, SYSLOGLEVEL[i]);
		}
		return 0;
	}
#ifdef CONFIG_USER_RTK_SYSLOG_REMOTE
	if (!strcmp(name, "syslog-mode")) {
		char *SYSLOGMODE[] = { "", "Local", "Remote", "Both" };
		int i;
		if (!mib_get(MIB_SYSLOG_MODE, &vChar))
			return -1;
		for (i = 1; i <= 3; i++) {
			if (i == vChar)
				boaWrite(wp, "<option selected value=\"%d\">%s</option>", i, SYSLOGMODE[i]);
			else
				boaWrite(wp, "<option value=\"%d\">%s</option>", i, SYSLOGMODE[i]);
		}
	}
#endif

#ifdef _CWMP_MIB_
	else if ( !strcmp(name, "tr069-interval") ) {
   		if ( !mib_get( CWMP_INFORM_ENABLE, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "disabled");
		return 0;
	}
	else if ( !strcmp(name, "tr069-inform-0") ) {
   		if ( !mib_get( CWMP_INFORM_ENABLE, (void *)&vChar) )
			return -1;
		if (0 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "tr069-inform-1") ) {
   		if ( !mib_get( CWMP_INFORM_ENABLE, (void *)&vChar) )
			return -1;
		if (1 == vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "tr069-dbgmsg-0") ) {
   		if ( !mib_get( CWMP_FLAG, (void *)&vChar) )
			return -1;
		if ( (vChar & CWMP_FLAG_DEBUG_MSG)==0 )
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "tr069-dbgmsg-1") ) {
   		if ( !mib_get( CWMP_FLAG, (void *)&vChar) )
			return -1;
		if ( (vChar & CWMP_FLAG_DEBUG_MSG)!=0 )
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "tr069-sendgetrpc-0") ) {
   		if ( !mib_get( CWMP_FLAG, (void *)&vChar) )
			return -1;
		if ( (vChar & CWMP_FLAG_SENDGETRPC)==0 )
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "tr069-sendgetrpc-1") ) {
   		if ( !mib_get( CWMP_FLAG, (void *)&vChar) )
			return -1;
		if ( (vChar & CWMP_FLAG_SENDGETRPC)!=0 )
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "tr069-skipmreboot-0") ) {
   		if ( !mib_get( CWMP_FLAG, (void *)&vChar) )
			return -1;
		if ( (vChar & CWMP_FLAG_SKIPMREBOOT)==0 )
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "tr069-skipmreboot-1") ) {
   		if ( !mib_get( CWMP_FLAG, (void *)&vChar) )
			return -1;
		if ( (vChar & CWMP_FLAG_SKIPMREBOOT)!=0 )
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "tr069-delay-0") ) {
   		if ( !mib_get( CWMP_FLAG, (void *)&vChar) )
			return -1;
		if ( (vChar & CWMP_FLAG_DELAY)==0 )
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "tr069-delay-1") ) {
   		if ( !mib_get( CWMP_FLAG, (void *)&vChar) )
			return -1;
		if ( (vChar & CWMP_FLAG_DELAY)!=0 )
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "tr069-autoexec-0") ) {
   		if ( !mib_get( CWMP_FLAG, (void *)&vChar) )
			return -1;
		if ( (vChar & CWMP_FLAG_AUTORUN)==0 )
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "tr069-autoexec-1") ) {
   		if ( !mib_get( CWMP_FLAG, (void *)&vChar) )
			return -1;
		if ( (vChar & CWMP_FLAG_AUTORUN)!=0 )
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "tr069-enable-cwmp-1") ) {
   		if ( !mib_get( CWMP_FLAG2, (void *)&vChar) )
			return -1;
		if ( (vChar & CWMP_FLAG2_CWMP_DISABLE)==0 )
			boaWrite(wp, "checked");
		return 0;
	}
	else if ( !strcmp(name, "tr069-enable-cwmp-0") ) {
   		if ( !mib_get( CWMP_FLAG2, (void *)&vChar) )
			return -1;
		if ( (vChar & CWMP_FLAG2_CWMP_DISABLE)!=0 )
			boaWrite(wp, "checked");
		return 0;
	}

#endif
#ifdef WLAN_SUPPORT
#ifdef CONFIG_WIFI_SIMPLE_CONFIG // WPS
	else if (!strcmp(name, "wpsVer") ) {
		#ifdef WPS20
			boaWrite(wp, "wps20 = 1;\n");
		#else
			boaWrite(wp, "wps20 = 0;\n");
		#endif
		return 0;
	}
	else if (!strcmp(name, "wpsVerConfig") ) {
		#ifdef WPS_VERSION_CONFIGURABLE
			boaWrite(wp, "wps_version_configurable = 1;\n");
		#else
			boaWrite(wp, "wps_version_configurable = 0;\n");
		#endif
		return 0;
	}
	else if (!strcmp(name, "wpsUseVersion")) {
		boaWrite(wp, "<option value=0>V1</option>\n");
		boaWrite(wp, "<option value=1>V2</option>\n");
		return 0;
	}
	else if (!strcmp(name, "wscConfig-0") ) {
		MIB_CE_MBSSIB_T Entry;
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		if (!Entry.wsc_configured) boaWrite(wp, "checked");
		return 0;
	}
	else if (!strcmp(name, "wscConfig-1")) {
		MIB_CE_MBSSIB_T Entry;
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		if (Entry.wsc_configured)
			boaWrite(wp, "checked");
		return 0;
	}
	else if (!strcmp(name, "wscConfig-A")) {
		MIB_CE_MBSSIB_T Entry;
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		if (Entry.wsc_configured)
			boaWrite(wp, "isConfig=1;");
		else
			boaWrite(wp, "isConfig=0;");
		return 0;
	}
	else if (!strcmp(name,"wscConfig")){
		MIB_CE_MBSSIB_T Entry;
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		if (Entry.wsc_configured)
			boaWrite(wp, "enableButton(form.elements['resetUnConfiguredBtn']);");
		else
			boaWrite(wp, "disableButton(form.elements['resetUnConfiguredBtn']);");
		return 0;
	}
	else if (!strcmp(name, "wlanMode"))  {
		MIB_CE_MBSSIB_T Entry;
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		if (Entry.wlanMode == CLIENT_MODE)
			boaWrite(wp, "isClient=1;");
		else
			boaWrite(wp, "isClient=0;");
		return 0;

	}
	else if (!strcmp(name, "wscDisable"))  {

		MIB_CE_MBSSIB_T Entry;
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		if (Entry.wsc_disabled)
			boaWrite(wp, "checked");
		return 0;
	}
	else if (!strcmp(name, "wps_auth"))  {

		MIB_CE_MBSSIB_T Entry;
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		switch(Entry.wsc_auth) {
			case WSC_AUTH_OPEN: boaWrite(wp, "Open"); break;
			case WSC_AUTH_WPAPSK: boaWrite(wp, "WPA PSK"); break;
			case WSC_AUTH_SHARED: boaWrite(wp, "WEP Shared"); break;
			case WSC_AUTH_WPA: boaWrite(wp, "WPA Enterprise"); break;

			case WSC_AUTH_WPA2: boaWrite(wp, "WPA2 Enterprise"); break;
			case WSC_AUTH_WPA2PSK: boaWrite(wp, "WPA2 PSK"); break;
			case WSC_AUTH_WPA2PSKMIXED: boaWrite(wp, "WPA2-Mixed PSK"); break;
			default:
				break;
		}
		return 0;
	}
	else if (!strcmp(name, "wps_enc"))  {
		MIB_CE_MBSSIB_T Entry;
		if(!wlan_getEntry(&Entry, 0))
			return -1;
		vChar = Entry.encrypt;
		if ((WIFI_SECURITY_T)vChar == WIFI_SEC_WPA2_MIXED) {
			vUInt = 0;
			vUInt |= Entry.unicastCipher;
			vUInt |= Entry.wpa2UnicastCipher;
			switch (vUInt) {
				case WPA_CIPHER_TKIP: boaWrite(wp, "TKIP"); break;
				case WPA_CIPHER_AES: boaWrite(wp, "AES"); break;
				case WPA_CIPHER_MIXED: boaWrite(wp, "TKIP+AES"); break;
				default:
					break;
			}
		}
		else {
			switch(Entry.wsc_enc) {
				case 0:
				case WSC_ENCRYPT_NONE: boaWrite(wp, "None"); break;
				case WSC_ENCRYPT_WEP: boaWrite(wp, "WEP"); break;
				case WSC_ENCRYPT_TKIP: boaWrite(wp, "TKIP"); break;
				case WSC_ENCRYPT_AES: boaWrite(wp, "AES"); break;
				case WSC_ENCRYPT_TKIPAES: boaWrite(wp, "TKIP+AES"); break;
				default:
					break;
			}
		}
		return 0;
	}
	else if(!strcmp(name, "blockingvap"))
		{
			#ifndef CONFIG_RTK_RG_INIT
			boaWrite(wp, "<tr><td><b>%s%s", multilang(LANG_BLOCKING_BETWEEN_VAP),"</b></td>");
			boaWrite(wp, "<td><input type=\"radio\" name=\"mbssid_block\" value=\"disable\" >%s", multilang(LANG_DISABLE));
			boaWrite(wp, "<input type=\"radio\" name=\"mbssid_block\" value=\"enable\" >%s%s",multilang(LANG_ENABLE),"</td></tr>");
			#endif
		}
#endif
#endif
	// Jenny, for RFC1577
	if ( !strcmp(name, "adslcmode") ) {
#ifdef CONFIG_IPV6
	char vChar=-1;

	mib_get(MIB_V6_IPV6_ENABLE, (void *)&vChar);
#endif
#ifdef CONFIG_ATM_CLIP
		boaWrite(wp, "<option value=\"5\">1577 Routed</option>" );
#endif

#ifdef CONFIG_IPV6
	if (vChar == 1){
#if defined(CONFIG_IPV6) && defined(DUAL_STACK_LITE)
		boaWrite(wp, "<option value=\"6\">DS-Lite</option>" );
#endif
#if defined(CONFIG_IPV6) && defined(CONFIG_IPV6_SIT_6RD)
		boaWrite(wp, "<option value=\"8\">6rd</option>" );
#endif
	}
#endif
		return 0;
	}
	else if ( !strcmp(name, "ethcmode") ) {
#ifdef CONFIG_IPV6
	char vChar=-1;

	mib_get(MIB_V6_IPV6_ENABLE, (void *)&vChar);
	if (vChar == 1){
#if defined(CONFIG_IPV6) && defined(DUAL_STACK_LITE)
		boaWrite(wp, "<option value=\"6\">DS-Lite</option>" );
#endif
#if defined(CONFIG_IPV6) && defined(CONFIG_IPV6_SIT_6RD)
		boaWrite(wp, "<option value=\"8\">6rd</option>" );
#endif
	}
#endif
		return 0;
	}
//add by ramen for create new menu
       else if(!strcmp(name,"CreateMenu")){

	   createMenu(0,wp,0,0);
	   boaWrite(wp,"</body>\n</html>\n");
	   	return 0;
       	}if(!strcmp(name,"CreateMenu_user"))

       		{
       		createMenu_user(0,wp,0,0);
			return 0;
       		}

	else if(!strcmp(name,"naptEnable"))
	{
		boaWrite(wp,"\tif ((document.adsl.adslConnectionMode.selectedIndex == 1) ||\n"
			"\t\t(document.adsl.adslConnectionMode.selectedIndex == 2))\n"
			"\t\tdocument.adsl.naptEnabled.checked = true;\n"
			"\telse\n"
			"\t\tdocument.adsl.naptEnabled.checked = false;\n");
		return 0;
	}
	else if(!strcmp(name,"TrafficShapingByVid"))
	{
#if defined(QOS_TRAFFIC_SHAPING_BY_VLANID)&&defined(CONFIG_RTK_RG_INIT)
		boaWrite(wp,"1");
#else
		boaWrite(wp,"0");
#endif

	}
	else if(!strcmp(name,"TrafficShapingBySsid"))
	{
#if defined(QOS_TRAFFIC_SHAPING_BY_SSID)&&defined(CONFIG_RTK_RG_INIT)
		boaWrite(wp,"1");
#else
		boaWrite(wp,"0");
#endif

	}
	else if(!strcmp(name,"IPv6Show"))
	{
#ifdef CONFIG_IPV6
		if ( mib_get( MIB_V6_IPV6_ENABLE, (void *)&vChar) )
		{
			if (0 == vChar)
				boaWrite(wp,"0");
			else
				boaWrite(wp,"1");
		}
#else
		boaWrite(wp,"0");
#endif
		return 0;
	}
	else if(!strcmp(name,"LUNAShow"))
	{
#ifdef CONFIG_LUNA
		boaWrite(wp,"1");
#else
		boaWrite(wp,"0");
#endif
		return 0;
	}
	else if(!strcmp(name,"DSLiteShow"))
	{
#ifdef DUAL_STACK_LITE
		boaWrite(wp,"1");
#else
		boaWrite(wp,"0");
#endif
		return 0;
	}
	else if(!strcmp(name,"6rdShow"))
	{
#if defined(CONFIG_IPV6) && defined(CONFIG_IPV6_SIT_6RD)
		boaWrite(wp,"1");
#else
		boaWrite(wp,"0");
#endif
		return 0;
	}
#ifdef CONFIG_IP_NF_ALG_ONOFF
	else if(!strcmp(name,"GetAlgType"))
		{
		GetAlgTypes(wp);
		return 0;
		}
	else if(!strcmp(name,"AlgTypeStatus"))
		{
		CreatejsAlgTypeStatus( wp);
	 	return 0;
		}
#endif
#ifdef DNS_BIND_PVC_SUPPORT
	else if(!strcmp(name,"DnsBindPvc"))
		{
		unsigned char dnsBindPvcEnable=0;
		mib_get(MIB_DNS_BIND_PVC_ENABLE,(void*)&dnsBindPvcEnable);
		//printf("dns bind pvc = %d\n",dnsBindPvcEnable);
		boaWrite(wp,"<font size=2>IPv4 %s:<input type=\"checkbox\" name=\"enableDnsBind\" value=\"on\" %s onClick=\"DnsBindPvcClicked();\"></font>",
		multilang(LANG_WAN_INTERFACE_BINDING), (dnsBindPvcEnable) ? "checked" : "");
		return 0;
		}
#endif
#ifdef CONFIG_IPV6
#ifdef DNSV6_BIND_PVC_SUPPORT
	else if(!strcmp(name,"Dnsv6BindPvc"))
		{
		unsigned char dnsv6BindPvcEnable=0;
		mib_get(MIB_DNSV6_BIND_PVC_ENABLE,(void*)&dnsv6BindPvcEnable);
		//printf("dnsv6 bind pvc = %d\n",dnsv6BindPvcEnable);
		boaWrite(wp,"<font size=2>IPv6 %s:<input type=\"checkbox\" name=\"enableDnsv6Bind\" value=\"on\" %s onClick=\"Dnsv6BindPvcClicked();\"></font>",
		multilang(LANG_WAN_INTERFACE_BINDING), (dnsv6BindPvcEnable) ? "checked" : "");
		return 0;
		}
#endif
#endif
	else  if(!strcmp(name,"WanPvcRouter"))
		{
#ifdef DNS_BIND_PVC_SUPPORT
				MIB_CE_ATM_VC_T Entry;
				int entryNum;
				int mibcnt;
				char interfacename[MAX_NAME_LEN];
				entryNum = mib_chain_total(MIB_ATM_VC_TBL);
				unsigned char forSelect=0;
		            for(mibcnt=0;mibcnt<entryNum;mibcnt++)
		            {
		            if (!mib_chain_get(MIB_ATM_VC_TBL, mibcnt, (void *)&Entry))
						{
		  					boaError(wp, 400, "Get chain record error!\n");
							return -1;
						}
			      if(Entry.cmode!=CHANNEL_MODE_BRIDGE)// CHANNEL_MODE_BRIDGE CHANNEL_MODE_IPOE CHANNEL_MODE_PPPOE CHANNEL_MODE_PPPOA	CHANNEL_MODE_RT1483	CHANNEL_MODE_RT1577
			      	{
			      	boaWrite(wp,"0");
				return 0;
		                  }

		            }
		           boaWrite(wp,"1");
#else
	 		 boaWrite(wp,"0");
#endif
			return 0;

		}

#ifdef DNS_BIND_PVC_SUPPORT
	else if(!strcmp(name,"dnsBindPvcInit"))
			{
				unsigned int dnspvc1,dnspvc2,dnspvc3;
				if(!mib_get(MIB_DNS_BIND_PVC1,(void*)&dnspvc1))
					{
					boaError(wp, 400, "Get MIB_DNS_BIND_PVC1 record error!\n");
							return -1;
					}
				if(!mib_get(MIB_DNS_BIND_PVC2,(void*)&dnspvc2))
					{
					boaError(wp, 400, "Get MIB_DNS_BIND_PVC2 record error!\n");
							return -1;
					}
				if(!mib_get(MIB_DNS_BIND_PVC3,(void*)&dnspvc3))
					{
					boaError(wp, 400, "Get MIB_DNS_BIND_PVC3 record error!\n");
							return -1;
					}

				    boaWrite(wp,"DnsBindSelectdInit('wanlist1',%d);\n",dnspvc1);
				    boaWrite(wp,"DnsBindSelectdInit('wanlist2',%d);\n",dnspvc2);
				    boaWrite(wp,"DnsBindSelectdInit('wanlist3',%d);\n",dnspvc3);
				boaWrite(wp,"DnsBindPvcClicked();");
				return 0;
			}
#endif

#ifdef CONFIG_IPV6
#ifdef DNSV6_BIND_PVC_SUPPORT
	else if(!strcmp(name,"dnsv6BindPvcInit"))
			{
				unsigned int dnspvc1,dnspvc2,dnspvc3;
				if(!mib_get(MIB_DNSV6_BIND_PVC1,(void*)&dnspvc1))
					{
					boaError(wp, 400, "Get MIB_DNSV6_BIND_PVC1 record error!\n");
							return -1;
					}
				if(!mib_get(MIB_DNSV6_BIND_PVC2,(void*)&dnspvc2))
					{
					boaError(wp, 400, "Get MIB_DNSV6_BIND_PVC2 record error!\n");
							return -1;
					}
				if(!mib_get(MIB_DNSV6_BIND_PVC3,(void*)&dnspvc3))
					{
					boaError(wp, 400, "Get MIB_DNSV6_BIND_PVC3 record error!\n");
							return -1;
					}

				    boaWrite(wp,"DnsBindSelectdInit('v6wanlist1',%d);\n",dnspvc1);
				    boaWrite(wp,"DnsBindSelectdInit('v6wanlist2',%d);\n",dnspvc2);
				    boaWrite(wp,"DnsBindSelectdInit('v6wanlist3',%d);\n",dnspvc3);
				boaWrite(wp,"Dnsv6BindPvcClicked();");
				return 0;
			}
#endif
#endif
	else if(!strcmp(name, "dgw")){
#ifdef DEFAULT_GATEWAY_V1
		boaWrite(wp, "\tif (droute == 1)\n");
		boaWrite(wp, "\t\tdocument.adsl.droute[1].checked = true;\n");
		boaWrite(wp, "\telse\n");
		boaWrite(wp, "\t\tdocument.adsl.droute[0].checked = true;\n");
#else
		GetDefaultGateway(eid, wp, argc, argv);
		boaWrite(wp, "\tautoDGWclicked();\n");
#endif
	}
/* add by yq_zhou 09.2.02 add sagem logo for 11n*/
	else if(!strncmp(name, "title", 5))	{
#ifdef CONFIG_SFU
		boaWrite(wp,	"<img src=\"graphics/topbar_sfu.gif\" width=900 height=60 border=0>");
#elif CONFIG_VDSL
		boaWrite(wp,	"<img src=\"graphics/topbar_vdsl.gif\" width=900 height=60 border=0>");
#elif CONFIG_11N_SAGEM_WEB
		boaWrite(wp,	"<img src=\"graphics/sagemlogo1.gif\" width=1350 height=60 border=0>");
#else
		boaWrite(wp,	"<img src=\"graphics/topbar.gif\" width=900 height=60 border=0>");
#endif
	}
	else if(!strncmp(name, "logobelow", 9))	{
#ifdef CONFIG_11N_SAGEM_WEB
		boaWrite(wp,	"<img src=\"graphics/sagemlogo2.gif\" width=180 height=60 border=0>");
#endif
	}
#ifdef CONFIG_ETHWAN
	else if(!strncmp(name, "ethwanSelection", 15)){
		MIB_CE_ATM_VC_T Entry;

		memset((void *)&Entry, 0, sizeof(Entry));
		if (getWanEntrybyMedia(&Entry, MEDIA_ETH)>=0)
			boaWrite(wp, "document.ethwan.adslConnectionMode.value = \"%d\";\n", Entry.cmode);
	}
#endif
#ifdef CONFIG_INIT_SCRIPTS
	else if(!strncmp(name, "getStartScriptContent", 21))
	{
		FILE *sct_fp;
		char line[128];

		if(sct_fp = fopen("/var/config/start_script", "r"))
		{
			while(fgets(line, 127, sct_fp))
				boaWrite(wp, "%s<br>", line);
		}
		else
			boaWrite(wp, "Open script file failed!\n");
	}
	else if(!strncmp(name, "getEndScriptContent", 21))
	{
		FILE *sct_fp;
		char line[128];

		if(sct_fp = fopen("/var/config/end_script", "r"))
		{
			while(fgets(line, 127, sct_fp))
				boaWrite(wp, "%s<br>", line);
		}
		else
			boaWrite(wp, "Open script file failed!\n");
	}
#endif
#ifndef CONFIG_DEV_xDSL
	else if(!strcmp(name, "wan_mode_atm"))
	{
		boaWrite(wp, "style=\"display: none\"");
	}
#endif
#ifndef CONFIG_ETHWAN
	else if(!strcmp(name, "wan_mode_ethernet"))
	{
		boaWrite(wp, "style=\"display: none\"");
	}
#endif
#ifndef CONFIG_PTMWAN
	else if(!strcmp(name, "wan_mode_ptm"))
	{
		boaWrite(wp, "style=\"display: none\"");
	}
#endif
#ifndef CONFIG_PTM_BONDING
	else if(!strcmp(name, "wan_mode_bonding"))
	{
		boaWrite(wp, "style=\"display: none\"");
	}
#endif
#ifndef WLAN_WISP
	else if(!strcmp(name, "wan_mode_wireless"))
	{
		boaWrite(wp, "style=\"display: none\"");
	}
#endif
#if defined(CONFIG_RTK_RG_INIT) && defined (CONFIG_IPV6)
	else if (!strcmp(name, "lan_ipv6_mode_auto")) {
		if (!mib_get(MIB_LAN_IPV6_MODE1, (void *)&vChar))
			return -1;
		if (!vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if (!strcmp(name, "lan_ipv6_mode_manual")) {
		if (!mib_get(MIB_LAN_IPV6_MODE1, (void *)&vChar))
			return -1;
		if (vChar)
			boaWrite(wp, "checked");
		return 0;
	}
	else if (!strcmp(name, "lan_ipverion_v4only")) {
		if (!mib_get(MIB_LAN_IP_VERSION1, (void *)&vChar))
			return -1;
		if (vChar==0)
			boaWrite(wp, "selected");
		return 0;
	}
	else if (!strcmp(name, "lan_ipverion_v4v6")) {
		if (!mib_get(MIB_LAN_IP_VERSION1, (void *)&vChar))
			return -1;
		if (vChar==1)
			boaWrite(wp, "selected");
		return 0;
	}
#endif
	else
		return -1;

	return 0;
}

#ifdef WLAN_MBSSID
int getVirtualIndex(int eid, request *wp, int argc, char **argv)
{
	int ret, vwlan_idx;
	char s_ifname[16];
	MIB_CE_MBSSIB_T Entry;
	int i=0;

	snprintf(s_ifname, 16, "%s", (char *)getWlanIfName());
	vwlan_idx = atoi(argv[--argc]);

	if (vwlan_idx > NUM_VWLAN_INTERFACE) {
		//fprintf(stderr, "###%s:%d wlan_idx=%d vwlan_idx=%d###\n", __FILE__, __LINE__, wlan_idx, vwlan_idx);
		boaWrite(wp, "0");
		return 0;
	}

	if (vwlan_idx > 0)
		snprintf(s_ifname, 16, "%s-vap%d", (char *)getWlanIfName(), vwlan_idx - 1);

	if (!mib_chain_get(MIB_MBSSIB_TBL, vwlan_idx, (void *)&Entry)) {
		printf("Error! Get MIB_MBSSIB_TBL(getVirtualIndex) error.\n");
		return 0;
	}

	if (!strcmp("band", argv[0]))
		boaWrite(wp, "%d", Entry.wlanBand);
	else if (!strcmp("wlanDisabled", argv[0]))
		boaWrite(wp, "%d", Entry.wlanDisabled);
	else if (!strcmp("wlanAccess", argv[0]))
		boaWrite(wp, "%d", Entry.userisolation);
	else if (!strcmp("rateAdaptiveEnabled", argv[0]))
		boaWrite(wp, "%d", Entry.rateAdaptiveEnabled);
	else if (!strcmp("fixTxRate", argv[0]))
		boaWrite(wp, "%u", Entry.fixedTxRate);
	else if (!strcmp("wmmEnabled", argv[0]))
		boaWrite(wp, "%u", Entry.wmmEnabled);

	return ret;
}
#endif

void write_wladvanced(int eid, request * wp, int argc, char **argv)        //add by yq_zhou 1.20
{
#ifdef WPS20
	unsigned char wpsUseVersion;
	mib_get(MIB_WSC_VERSION, (void *) &wpsUseVersion);
#endif

#ifdef WLAN_SUPPORT
#ifdef WLAN_QTN
#ifdef WLAN1_QTN
	if(wlan_idx==1)
#elif defined(WLAN0_QTN)
	if(wlan_idx==0)
#endif
		boaWrite(wp,"<tr style=\"display: none\">");
	else
#endif
	boaWrite(wp,"<tr>");
    boaWrite(wp, "<td width=\"30%%\"><font size=2><b>%s:</b></td>\n"
     "<td width=\"70%%\"><font size=2>\n"
     "<input type=\"radio\" value=\"long\" name=\"preamble\">%s&nbsp;&nbsp;\n"
     "<input type=\"radio\" name=\"preamble\" value=\"short\">%s</td></tr>\n",
		multilang(LANG_PREAMBLE_TYPE), multilang(LANG_LONG_PREAMBLE),
		multilang(LANG_SHORT_PREAMBLE));

#ifdef WPS20
	if (wpsUseVersion) {
			boaWrite(wp,
			 "<tr><td width=\"30%%\"><font size=2><b>%s %s:</b></td>\n"
			 "<td width=\"70%%\"><font size=2>\n"
			 "<input type=\"radio\" name=\"hiddenSSID\" value=\"no\">%s&nbsp;&nbsp;\n"
			 "<input type=\"radio\" name=\"hiddenSSID\" value=\"yes\" onClick=\"alert('If you disable this, WPS will be disabled!')\">%s</td></tr>\n",
			multilang(LANG_BROADCAST), multilang(LANG_SSID), multilang(LANG_ENABLED), multilang(LANG_DISABLED));
	}
	else
#endif
	{
		boaWrite(wp,
	     "<tr><td width=\"30%%\"><font size=2><b>%s %s:</b></td>\n"
	     "<td width=\"70%%\"><font size=2>\n"
	     "<input type=\"radio\" name=\"hiddenSSID\" value=\"no\">%s&nbsp;&nbsp;\n"
	     "<input type=\"radio\" name=\"hiddenSSID\" value=\"yes\">%s</td></tr>\n",
		multilang(LANG_BROADCAST), multilang(LANG_SSID), multilang(LANG_ENABLED), multilang(LANG_DISABLED));
	}
	boaWrite(wp,
     "<tr><td width=\"30%%\"><font size=2><b>%s:</b></td>\n"
     "<td width=\"70%%\"><font size=2>\n"
     "<input type=\"radio\" name=block value=1>%s&nbsp;&nbsp;\n"
     "<input type=\"radio\" name=block value=0>%s</td></tr>\n",
	multilang(LANG_RELAY_BLOCKING), multilang(LANG_ENABLED), multilang(LANG_DISABLED));

#ifdef WLAN_QTN
#ifdef WLAN1_QTN
	if(wlan_idx==1)
#elif defined(WLAN0_QTN)
	if(wlan_idx==0)
#endif
		boaWrite(wp,"<tr style=\"display: none\">");
	else
#endif
	boaWrite(wp,"<tr>");
	boaWrite(wp,"<td width=\"30%%\"><font size=2><b>%s:</b></td>\n"
     "<td width=\"70%%\"><font size=2>\n"
     "<input type=\"radio\" name=\"protection\" value=\"yes\">%s&nbsp;&nbsp;\n"
     "<input type=\"radio\" name=\"protection\" value=\"no\">%s</td></tr>\n",
	multilang(LANG_PROTECTION), multilang(LANG_ENABLED), multilang(LANG_DISABLED));
#ifdef WLAN_QTN
#ifdef WLAN1_QTN
	if(wlan_idx==1)
#elif defined(WLAN0_QTN)
	if(wlan_idx==0)
#endif
		boaWrite(wp,"<tr style=\"display: none\">");
	else
#endif
	boaWrite(wp,"<tr>");
  	boaWrite(wp,"<td width=\"30%%\"><font size=2><b>%s:</b></td>\n"
     "<td width=\"70%%\"><font size=2>\n"
     "<input type=\"radio\" name=\"aggregation\" value=\"enable\">%s&nbsp;&nbsp;\n"
     "<input type=\"radio\" name=\"aggregation\" value=\"disable\">%s</td></tr>\n",
	multilang(LANG_AGGREGATION), multilang(LANG_ENABLED), multilang(LANG_DISABLED));
       boaWrite(wp,
     "<tr id=\"ShortGi\" style=\"display:\">\n"
     "<td width=\"30%%\"><font size=2><b>%s:</b></td>\n"
     "<td width=\"70%%\"><font size=2>\n"
     "<input type=\"radio\" name=\"shortGI0\" value=\"on\">%s&nbsp;&nbsp;\n"
     "<input type=\"radio\" name=\"shortGI0\" value=\"off\">%s</td></tr>\n",
	multilang(LANG_SHORT_GI), multilang(LANG_ENABLED), multilang(LANG_DISABLED));
#endif /*WLAN_SUPPORT*/
}

int getNameServer(int eid, request * wp, int argc, char **argv) {

	FILE *fp;
	char buffer[128], tmpbuf[64];
	int count = 0;
	//fprintf(stderr, "getNameServer %x\n", gResolvFile);
	//boaWrite(wp, "[]", tmpbuf);
	//if ((gResolvFile == NULL) ||
	// for IPv4
	if ( (fp = fopen(RESOLV_BACKUP, "r")) == NULL ) {
		//printf("Unable to open resolver file\n");
		return -1;
	}

	while (fgets(buffer, sizeof(buffer), fp) != NULL) {		
		if (sscanf(buffer, "%s", tmpbuf) != 1) {
			continue;
		}

		if (count == 0)
			boaWrite(wp, "%s", tmpbuf);
		else
			boaWrite(wp, ", %s", tmpbuf);
		count ++;
	}
	fclose(fp);

	return 0;
}

#ifndef RTF_UP
/* Keep this in sync with /usr/src/linux/include/linux/route.h */
#define RTF_UP          0x0001	/* route usable                 */
#define RTF_GATEWAY     0x0002	/* destination is a gateway     */
#define RTF_HOST        0x0004	/* host entry (net otherwise)   */
#define RTF_REINSTATE   0x0008	/* reinstate route after tmout  */
#define RTF_DYNAMIC     0x0010	/* created dyn. (by redirect)   */
#define RTF_MODIFIED    0x0020	/* modified dyn. (by redirect)  */
#define RTF_MTU         0x0040	/* specific MTU for this route  */
#ifndef RTF_MSS
#define RTF_MSS         RTF_MTU	/* Compatibility :-(            */
#endif
#define RTF_WINDOW      0x0080	/* per route window clamping    */
#define RTF_IRTT        0x0100	/* Initial round trip time      */
#define RTF_REJECT      0x0200	/* Reject route                 */
#endif

// Jenny, get default gateway information
int getDefaultGW(int eid, request * wp, int argc, char **argv)
{
	char buff[256];
	int flgs;
	struct in_addr gw, dest, mask, inAddr;
	char ifname[16], dgw[16];
	int found=0;
	FILE *fp;

	if (!(fp=fopen("/proc/net/route", "r"))) {
		printf("Error: cannot open /proc/net/route - continuing...\n");
		return -1;
	}

	fgets(buff, sizeof(buff), fp);
	while (fgets(buff, sizeof(buff), fp) != NULL) {
		//if (sscanf(buff, "%*s%*lx%lx%X%", &g, &flgs) != 2) {
		if (sscanf(buff, "%s%x%x%x%*d%*d%*d%x", ifname, &dest, &gw, &flgs, &mask) != 5) {
			printf("Unsuported kernel route format\n");
			fclose(fp);
			return -1;
		}

		//printf("ifname=%s, dest=%x, gw=%x, flgs=%x, mask=%x\n", ifname, dest.s_addr, gw.s_addr, flgs, mask.s_addr);
		if(flgs & RTF_UP  && strcmp(ifname, "lo")!=0) {
			// default gateway
			if (getInAddr(ifname, IP_ADDR, (void *)&inAddr) == 1) {
				if (inAddr.s_addr == 0x40404040) {
					boaWrite(wp, "");
					continue;
				}
			}
		
			if (dest.s_addr == 0 && mask.s_addr == 0) {
				found++;
				if (found > 1)
					boaWrite(wp, ", ");
				
				if (gw.s_addr != 0) {
					strncpy(dgw,  inet_ntoa(gw), 16);
					boaWrite(wp, "%s", dgw);
				}
				else {
					boaWrite(wp, "%s", ifname);
				}
			}
		}
	}
	fclose(fp);
	return 0;
}

#ifdef CONFIG_IPV6
int getDefaultGW_ipv6(int eid, request * wp, int argc, char **argv)
{
	char buff[256];
	struct in6_addr addr;
	unsigned char len;
	unsigned char devname[10];
	unsigned char value[48];
	FILE *fp;
	int i;

	if (!(fp=fopen("/proc/net/ipv6_route", "r"))) {
		printf("Error: cannot open /proc/net/ipv6_route - continuing...\n");
		return -1;
	}

	fgets(buff, sizeof(buff), fp);
	while (fgets(buff, sizeof(buff), fp) != NULL) {
		if(sscanf( buff,
			"%*32s%02hhx%*32s%*02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%02hhx%*x%*x%*x%*x%s",
			&len,
			&addr.s6_addr[ 0], &addr.s6_addr[ 1], &addr.s6_addr[ 2], &addr.s6_addr[ 3],
			&addr.s6_addr[ 4], &addr.s6_addr[ 5], &addr.s6_addr[ 6], &addr.s6_addr[ 7],
			&addr.s6_addr[ 8], &addr.s6_addr[ 9], &addr.s6_addr[10], &addr.s6_addr[11],
			&addr.s6_addr[12], &addr.s6_addr[13], &addr.s6_addr[14], &addr.s6_addr[15], devname)) {

			//printf("len=%d, devname=%s\n", len, devname);
			//for ( i=0; i<16; i++)
			//	printf("%x ", addr.s6_addr[i]);
			//printf("\n");

			if( len == 0 && (strcmp(devname, "lo") !=0) ) {
				inet_ntop(PF_INET6, &addr, value, sizeof(value));
				boaWrite(wp, "%s", value);
				fclose(fp);
				return 0;
			}
		}
	}
	boaWrite(wp, "");
	fclose(fp);
	return 0;
}
#endif

int multilang_asp(int eid, request * wp, int argc, char **argv)
{
	int key;

   	if (boaArgs(argc, argv, "%d", &key) < 1) {
   		boaError(wp, 400, "Insufficient args\n");
   		return -1;
   	}

	return boaWrite(wp, "%s", multilang(key));
}

int WANConditions(int eid, request * wp, int argc, char **argv)
{
	int wan_bitmap = WAN_MODE_MASK;
	int i, count = 0;

	for(i = 0; i < 5; i++)
	{
		if(1 & (wan_bitmap >> i))
			count++;
	}

	if(count > 1)
		return 0;
	else
		return boaWrite(wp, "style=\"display:none\"");
}
void ShowWanMode(int eid, request * wp, int argc, char **argv)
{
	char *name;
#ifdef WLAN_WISP
	char mode;
	char rptEnabled;
	int orig_idx, i;
	int enable_wisp=0;
#endif

	if (boaArgs(argc, argv, "%s", &name) < 1) {
		boaError(wp, 400, "Insufficient args\n");
		return;
	}

	if(!strcmp(name, "wlan"))
#ifdef WLAN_WISP
	{
		orig_idx = wlan_idx;
		for(i=0;i<NUM_WLAN_INTERFACE;i++){
			wlan_idx = i;
			mib_get(MIB_WLAN_MODE, &mode);
			mib_get(MIB_REPEATER_ENABLED1, &rptEnabled);

			if((mode == AP_MODE || mode == AP_WDS_MODE) && rptEnabled){
				enable_wisp=1;
				break;
			}
		}
		
		wlan_idx = orig_idx;

		if(enable_wisp)
			boaWrite(wp, "");
		else
			boaWrite(wp, "disabled");
	}
#else
	boaWrite(wp, "disabled");				
#endif
}


