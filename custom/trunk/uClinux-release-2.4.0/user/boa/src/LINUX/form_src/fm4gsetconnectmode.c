#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <time.h>
#include <net/route.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <sys/ioctl.h>
#include "include/lc_common.h"
#include "include/nvmanager.h"
#include "include/ipc_msg_id_define.h"
#include "include/lc_ipc.h"
#include "include/lte_sdk.h"
#include "include/wm_log.h"
/*-- Local inlcude files --*/
#include "../webs.h"
#include "webform.h"
#include "mib.h"
#include "utility.h"

typedef struct wm_auto_connect_tag
{
     lc_uint32 u32auto_connect;
} wm_auto_connect_t;

int  ipc_init_wan(int id)
{
	lc_uint32 ret;
	ret = ipc_msg_init(id, NULL, NULL, NULL, TRUE);
    if(IPC_OK != ret)
    {	
        return 0;
    }
	return 1;
}

///////////////////////////////////////////////////////////////////
void form4GConfsetconnectmode(request * wp, char *path, char *query)
{
	char	*strData,*strDatatmp;
	char tmp_web[8] = {0};
	MIB_WAN_LTE_T Entry_LTE;
	size_t len = 32;
	lc_uint32 nm_response,ret;

	strData = boaGetVar(wp, "ipt_connect_mode", "");

	wm_auto_connect_t wm_action_connect;
    wm_action_connect.u32auto_connect = atoi(strData);

	WM_LOG_INFO("get form4GConfsetconnectmode :%d", wm_action_connect.u32auto_connect);	

	ipc_init_wan(CGI_ID);
	ret = send_request_syn(MM_ID,WM_MSG_DUALMODE_SET_PREF_REQ,&wm_action_connect,sizeof(wm_auto_connect_t),20000,&nm_response,&len);

	WM_LOG_INFO("send syn request value:%d", ret );

	ipc_msg_fin();

	strData = boaGetVar(wp, "submit-url", "");
	OK_MSG(strData);
	return;
}

void form4Ggetwanstatus(request * wp, char *path, char *query)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_status_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		OK_MSGWAN(tmpBuf);
	}
	else
	{
		OK_MSGWAN("err");
	}
	return;
}

