#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <time.h>
#include <net/route.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <sys/ioctl.h>
#include "include/lc_common.h"
#include "include/nvmanager.h"
#include "include/ipc_msg_id_define.h"
#include "include/lc_ipc.h"
#include "include/lte_sdk.h"
#include "include/mm_extern.h"
#include "include/wm_log.h"
/*-- Local inlcude files --*/
#include "../webs.h"
#include "webform.h"
#include "mib.h"
#include "utility.h"
int  ipc_init_lte(int id)
{
	lc_uint32 ret;
	ret = ipc_msg_init(id, NULL, NULL, NULL, TRUE);
    if(IPC_OK != ret)
    {	
        return 0;
    }
	return 1;
}

///////////////////////////////////////////////////////////////////
void form4GConfapn(request * wp, char *path, char *query)
{
	char	*strData,*strDatatmp;
	char tmp_web[8] = {0};
	MIB_WAN_LTE_T Entry_LTE;

	lc_uint32 nm_response,ret;
	size_t len=32;

	strData = boaGetVar(wp, "ipt_apn_list", "");
	WM_LOG_INFO("form4GConfapn get:%s\n",strData);

	if(!mib_chain_get( MIB_WAN_LTE_TBL, 0, (void*)&Entry_LTE))
    {
        WM_LOG_INFO("get mib LTE TABLE error\n"); 
    }
	
	if ( strData[0] ) 
		strcpy(Entry_LTE.apn_list, strData);
	else 
		strcpy(Entry_LTE.apn_list, strData);
    
    if(!mib_chain_update(MIB_WAN_LTE_TBL, &Entry_LTE, 0))
    {
       	strData = boaGetVar(wp, "submit-url", "");
		OK_MSG(strData);
		return;
    }
    mib_update(CURRENT_SETTING, CONFIG_MIB_ALL);

	strDatatmp = boaGetVar(wp, "ipt_apn_send_msg", "");
	strcpy(tmp_web,strDatatmp);
	ipc_init_lte(CGI_ID);
	if (!strcmp(tmp_web,"1"))
	{
	    ret = send_request_syn(MM_ID,WM_SET_DEFAULT_APN_REQ,NULL,0,20000,&nm_response,&len);
	}
	ipc_msg_fin();
	#ifdef COMMIT_IMMEDIATELY
	Commit();
	#endif
	strData = boaGetVar(wp, "submit-url", "");
	OK_MSG(strData);
	return;
}

void form4GConnectgetstatus(request * wp, char *path, char *query)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_status_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		OK_MSGWAN(tmpBuf);
	}
	else
	{
		OK_MSGWAN("err");
	}
	return;
}

void form4Ggetapn(request * wp, char *path, char *query)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	MIB_WAN_LTE_T Entry_LTE;
	if(!mib_chain_get( MIB_WAN_LTE_TBL, 0, (void*)&Entry_LTE))
   	{
       	OK_MSGWAN("");
   	}
	else
	{
		OK_MSGWAN(Entry_LTE.apn_list);
	}
	return;
}
void form4Ggetactiveapn(request * wp, char *path, char *query)
{
	char tmpBuf[1024]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_connection_active_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		OK_MSGWAN(tmpBuf);
	}
	else
	{
		OK_MSGWAN("err");
	}
	return;
}


