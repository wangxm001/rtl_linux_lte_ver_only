/*

*  fmmenucreate_user.c is used to create menu

*  added by xl_yue

*/





#include <string.h>

#include <stdlib.h>

#include <unistd.h>

#include <sys/types.h>

#include <sys/wait.h>



#include "../webs.h"

#include "mib.h"

#include "webform.h"

#include "utility.h"

#include "multilang.h"



#ifdef CONFIG_DEFAULT_WEB	// default pages

/*

 *	Second Layer Menu

 */

#if defined(CONFIG_RTL_92D_SUPPORT) || defined(WLAN_DUALBAND_CONCURRENT)
struct RootMenu childmenu_wlan0_user[] = {

	{"Basic Settings", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlbasic.asp&wlan_idx=0",	"Setup wireless basic configuration", 0, 0, MENU_DISPLAY, LANG_BASIC_SETTINGS},

	{"Advanced Settings", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wladvanced.asp&wlan_idx=0",   "Setup wireless advanced configuration", 0, 0, MENU_DISPLAY, LANG_ADVANCED_SETTINGS},

	{"Security", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlwpa.asp&wlan_idx=0", "Setup wireless security", 0, 0, MENU_DISPLAY, LANG_SECURITY},

#ifdef WLAN_11R
	{"Fast Roaming", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlft.asp&wlan_idx=0", "Fast BSS Transition", 0, 0, MENU_DISPLAY, LANG_FAST_ROAMING},
#endif

#ifdef WLAN_ACL

	{"Access Control", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlactrl.asp&wlan_idx=0",	"Setup access control list for wireless clients", 0, 0, MENU_DISPLAY, LANG_ACCESS_CONTROL},

#endif

#ifdef WLAN_WDS

	{"WDS", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlwds.asp&wlan_idx=0", "WDS Settings", 0, 0, MENU_DISPLAY, LANG_WDS},

#endif

#if defined(WLAN_CLIENT) || defined(WLAN_SITESURVEY)

	{"Site Survey", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlsurvey.asp&wlan_idx=0", "Wireless Site Survey", 0, 0, MENU_DISPLAY, LANG_SITE_SURVEY},

#endif

#ifdef CONFIG_WIFI_SIMPLE_CONFIG	// WPS

	{"WPS", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlwps.asp&wlan_idx=0", "Wireless Protected Setup", 0, 0, MENU_DISPLAY, LANG_WPS},

#endif

	{"Status", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlstatus.asp&wlan_idx=0", "Wireless Current Status", 0, 0, MENU_DISPLAY, LANG_STATUS},

	{0, 0, 0, 0, 0, 0, 0, 0}

};

struct RootMenu childmenu_wlan1_user[] = {

	{"Basic Settings", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlbasic.asp&wlan_idx=1",	"Setup wireless basic configuration", 0, 0, MENU_DISPLAY, LANG_BASIC_SETTINGS},

	{"Advanced Settings", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wladvanced.asp&wlan_idx=1",   "Setup wireless advanced configuration", 0, 0, MENU_DISPLAY, LANG_ADVANCED_SETTINGS},

	{"Security", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlwpa.asp&wlan_idx=1", "Setup wireless security", 0, 0, MENU_DISPLAY, LANG_SECURITY},

#ifdef WLAN_11R
	{"Fast Roaming", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlft.asp&wlan_idx=1", "Fast BSS Transition (user)", 0, 0, MENU_DISPLAY, LANG_FAST_ROAMING},
#endif

#ifdef WLAN_ACL

	{"Access Control", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlactrl.asp&wlan_idx=1",	"Setup access control list for wireless clients", 0, 0, MENU_DISPLAY, LANG_ACCESS_CONTROL},

#endif

#ifdef WLAN_WDS

	{"WDS", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlwds.asp&wlan_idx=1", "WDS Settings", 0, 0, MENU_DISPLAY, LANG_WDS},

#endif

#if defined(WLAN_CLIENT) || defined(WLAN_SITESURVEY)

	{"Site Survey", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlsurvey.asp&wlan_idx=1", "Wireless Site Survey", 0, 0, MENU_DISPLAY, LANG_SITE_SURVEY},

#endif

#ifdef CONFIG_WIFI_SIMPLE_CONFIG	// WPS

	{"WPS", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlwps.asp&wlan_idx=1", "Wireless Protected Setup", 0, 0, MENU_DISPLAY, LANG_WPS},

#endif

	{"Status", MENU_URL, "../boaform/admin/formWlanRedirect?redirect-url=/admin/wlstatus.asp&wlan_idx=1", "Wireless Current Status", 0, 0, MENU_DISPLAY, LANG_STATUS},

	{0, 0, 0, 0, 0, 0, 0, 0}

};
#endif //CONFIG_RTL_92D_SUPPORT

/*

 *	First Layer Menu

 */

struct RootMenu childmenu_status_user[] = {
	{"Device", MENU_URL, "status_user.asp", "Device status", 0, 0, MENU_DISPLAY, LANG_DEVICE},
#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
#ifdef CONFIG_IPV6
	{"IPv6", MENU_URL, "status_ipv6.asp", "IPv6 status", 0, 0, MENU_DISPLAY, LANG_IPV6},
#endif
#endif

#ifdef CONFIG_USER_CUSTOM_UKB
#if defined(CONFIG_RTL_8676HWNAT)
  {"LAN Port", MENU_URL, "lan_port_status.asp",   "LAN Port Status", 0, 0, MENU_DISPLAY, LANG_LAN_PORT},
#endif
#endif
  {0, 0, 0, 0, 0, 0, 0, 0}
};

struct RootMenu childmenu_wlan_user[] = {

#if defined(CONFIG_RTL_92D_SUPPORT)
  {"Wireless Band Mode", MENU_URL, "wlbandmode.asp",   "Setup wireless band mode", 0, 0, MENU_DISPLAY, LANG_WIRELESS_BAND_MODE},
  {"wlan0 (5GHz)", MENU_FOLDER, &childmenu_wlan0_user, "", sizeof (childmenu_wlan0_user) / sizeof (struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_WLAN0_5GHZ},
  {"wlan1 (2.4GHz)", MENU_FOLDER, &childmenu_wlan1_user, "", sizeof (childmenu_wlan1_user) / sizeof (struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_WLAN1_2_4GHZ},
#elif defined (WLAN_DUALBAND_CONCURRENT)
#if defined (CONFIG_WLAN0_2G_WLAN1_5G) || defined(WLAN1_QTN)
  {"wlan0 (2.4GHz)", MENU_FOLDER, &childmenu_wlan0_user, "", sizeof (childmenu_wlan0_user) / sizeof (struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_WLAN0_2_4GHZ},
  {"wlan1 (5GHz)", MENU_FOLDER, &childmenu_wlan1_user, "", sizeof (childmenu_wlan1_user) / sizeof (struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_WLAN1_5GHZ},
#else
  {"wlan0 (5GHz)", MENU_FOLDER, &childmenu_wlan0_user, "", sizeof (childmenu_wlan0_user) / sizeof (struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_WLAN0_5GHZ},
  {"wlan1 (2.4GHz)", MENU_FOLDER, &childmenu_wlan1_user, "", sizeof (childmenu_wlan1_user) / sizeof (struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_WLAN1_2_4GHZ},
#endif
#else //CONFIG_RTL_92D_SUPPORT || WLAN_DUALBAND_CONCURRENT || CONFIG_MASTER_WLAN0_ENABLE || CONFIG_SLAVE_WLAN1_ENABLE
#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
	{"Basic Settings", MENU_URL, "wlbasic.asp", "Setup wireless basic configuration", 0, 0, MENU_DISPLAY, LANG_BASIC_SETTINGS},

	{"Advanced Settings", MENU_URL, "wladvanced.asp", "Setup wireless advanced configuration", 0, 0, MENU_DISPLAY, LANG_ADVANCED_SETTINGS},
#endif
	{"Security", MENU_URL, "wlwpa.asp", "Setup wireless security", 0, 0, MENU_DISPLAY, LANG_SECURITY},

#ifdef WLAN_11R
	{"Fast Roaming", MENU_URL, "wlft.asp", "Fast BSS Transition", 0, 0, MENU_DISPLAY, LANG_FAST_ROAMING},
#endif

#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
#ifdef WLAN_ACL

	{"Access Control", MENU_URL, "wlactrl.asp", "Setup access control list for wireless clients", 0, 0, MENU_DISPLAY, LANG_ACCESS_CONTROL},

#endif
#endif

#ifdef WLAN_WDS

	{"WDS", MENU_URL, "wlwds.asp", "WDS Settings", 0, 0, MENU_DISPLAY, LANG_WDS},

#endif

#if defined(WLAN_CLIENT) || defined(WLAN_SITESURVEY)

	{"Site Survey", MENU_URL, "wlsurvey.asp", "Wireless Site Survey", 0, 0, MENU_DISPLAY, LANG_SITE_SURVEY},

#endif

#ifdef CONFIG_WIFI_SIMPLE_CONFIG	// WPS

	{"WPS", MENU_URL, "wlwps.asp", "Wireless Protected Setup", 0, 0, MENU_DISPLAY, LANG_WPS},

#endif
#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
	{"Status", MENU_URL, "wlstatus.asp", "Wireless Current Status", 0, 0, MENU_DISPLAY, LANG_STATUS},
#endif
#endif //CONFIG_RTL_92D_SUPPORT

	{0, 0, 0, 0, 0, 0, 0, 0}

};



struct RootMenu childmenu_wan_user[] = {

#ifdef CONFIG_ETHWAN
#if defined(CONFIG_GPON_FEATURE) || defined(CONFIG_EPON_FEATURE)
	{PON_CONF_STR, MENU_URL, "../boaform/admin/formWanRedirect?redirect-url=/admin/multi_wan_generic.asp&if=pon", "PON WAN Configuration", 0, 0, MENU_DISPLAY, LANG_PON_WAN},
#else
#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
#ifdef CONFIG_RTL_MULTI_ETH_WAN
	{ETHWAN_CONF_STR, MENU_URL, "../boaform/admin/formWanRedirect?redirect-url=/admin/multi_wan_generic.asp&if=eth", "ETH WAN Configuration", 0, 0, MENU_DISPLAY, LANG_ETHERNET_WAN},
#else
	{ETHWAN_CONF_STR, MENU_URL, "waneth.asp", "Ethernet WAN Configuration", 0, 0, MENU_DISPLAY, LANG_ETHERNET_WAN},
#endif
#endif
#endif
#endif
#ifdef CONFIG_PTMWAN
	{PTMWAN_CONF_STR, MENU_URL, "../boaform/admin/formWanRedirect?redirect-url=/admin/multi_wan_generic.asp&if=ptm", "PTM WAN Configuration", 0, 0, MENU_DISPLAY, LANG_PTM_WAN},
#endif /*CONFIG_PTMWAN*/
#ifdef CONFIG_RTL8672_SAR
  {DSLWAN_CONF_STR, MENU_URL, "wanadsl.asp", "ADSL Channel Configuration", 0, 0, MENU_DISPLAY, LANG_ATM_WAN},
  {ATM_SETTINGS_STR, MENU_URL, "wanatm.asp", "Setup ATM", 0, 0, MENU_DISPLAY, LANG_ATM_SETTINGS},
  {ADSL_SETTINGS_STR, MENU_URL, "/admin/adsl-set.asp", "Setup ADSL", 0, 0, MENU_DISPLAY, LANG_DSL_SETTINGS},
  #ifdef CONFIG_DSL_VTUO
  {VTUO_SETTINGS_STR, MENU_URL, "/admin/vtuo-set.asp", "Setup VTU-O DSL", 0, 0, MENU_DISPLAY, LANG_VTUO_SETTINGS},
  #endif /*CONFIG_DSL_VTUO*/
#endif
#ifdef CONFIG_USER_PPPOMODEM
	{"3G Settings", MENU_URL, "wan3gconf.asp", "Setup 3G WAN", 0, 0, MENU_DISPLAY, LANG_3G_SETTINGS},
#endif //CONFIG_USER_PPPOMODEM

#ifdef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
  {"Connection", MENU_URL, "lct_connect.asp", "Setup 4G WAN", 0, 0, MENU_DISPLAY, LANG_4G_SETTINGS},
#endif

#ifdef CONFIG_USER_CUSTOM_UKB
  {"LTE Info", MENU_URL, "lct_lte_info.asp", "Setup 4G APN", 0, 0, MENU_DISPLAY, LANG_LTE_SETTINGS},
#endif
	{0, 0, 0, 0, 0, 0, 0, 0}

};



struct RootMenu childmenu_fw_user[] = {
#ifdef CONFIG_USER_CUSTOM_UKB
  {"IP/Port Filtering", MENU_URL, "fw-ipportfilter.asp",
   "Setup IP/Port filering", 0, 0, MENU_DISPLAY, LANG_IP_PORT_FILTERING},
#endif

#ifdef MAC_FILTER
#ifdef CONFIG_RTK_RG_INIT
	{"MAC Filtering", MENU_URL, "fw-macfilter_rg.asp", "Setup MAC filering", 0, 0, MENU_DISPLAY, LANG_MAC_FILTERING},
#else
	{"MAC Filtering", MENU_URL, "fw-macfilter.asp", "Setup MAC filering", 0, 0, MENU_DISPLAY, LANG_MAC_FILTERING},
#endif
#endif

#ifdef PORT_FORWARD_GENERAL
  {"Port Forwarding", MENU_URL, "fw-portfw.asp", "Setup port-forwarding", 0,
   0, MENU_DISPLAY, LANG_PORT_FORWARDING},
#endif

#ifdef URL_BLOCKING_SUPPORT
  {"URL Blocking", MENU_URL, "url_blocking.asp", "URL Blocking Setting", 0,
   0, MENU_DISPLAY, LANG_URL_BLOCKING},
#endif
#ifdef CONFIG_USER_CUSTOM_UKB
  {"Domain Blocking", MENU_URL, "domainblk.asp", "Domain Blocking Setting", 0,
   0, MENU_DISPLAY, LANG_DOMAIN_BLOCKING},
#endif
#ifdef DMZ
  {"DMZ", MENU_URL, "fw-dmz.asp", "Setup DMZ",0, 0, MENU_DISPLAY, LANG_DMZ},
#endif
	{0, 0, 0, 0, 0, 0, 0, 0}

};



struct RootMenu childmenu_admin_user[] = {

	{"Commit/Reboot", MENU_URL, "reboot.asp", "Commit/reboot the system", 0, 0, MENU_DISPLAY, LANG_COMMIT_REBOOT},
#ifdef CONFIG_USER_BOA_WITH_MULTILANG
#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
	{"Multi-lingual Settings", MENU_URL, "multi_lang.asp", "Multi-language setting", 0, 0, MENU_DISPLAY, LANG_MULTI_LINGUAL_SETTINGS},
#endif
#endif

#ifdef CONFIG_USER_CUSTOM_UKB
#ifdef CONFIG_SAVE_RESTORE
  {"Backup/Restore", MENU_URL, "saveconf.asp",
   "Backup/restore current settings", 0, 0, MENU_DISPLAY, LANG_BACKUP_RESTORE},
#endif
#endif

#ifdef ACCOUNT_LOGIN_CONTROL

	{"Logout", MENU_URL, "/admin/adminlogout.asp", "Logout", 0, 0, MENU_DISPLAY, LANG_LOGOUT},

#endif

#ifdef CONFIG_USER_CUSTOM_UKB
#ifndef SEND_LOG
  {"System Log", MENU_URL, "syslog.asp", "Show system log", 0, 0, MENU_DISPLAY, LANG_SYSTEM_LOG},
#else
  {"System Log", MENU_URL, "syslog_server.asp", "Show system log", 0, 0, MENU_DISPLAY, LANG_SYSTEM_LOG},
#endif
#endif

#ifndef CONFIG_USER_LTE_TRICHEER
	{"Password", MENU_URL, "/admin/user-password.asp", "Setup access password", 0, 0, MENU_DISPLAY, LANG_PASSWORD},
#endif

#ifdef CONFIG_USER_CUSTOM_UKB
	{"Password", MENU_URL, "/admin/user-password.asp", "Setup access password", 0, 0, MENU_DISPLAY, LANG_PASSWORD},

#ifdef WEB_UPGRADE
#ifdef UPGRADE_V1
  {"Firmware Upgrade", MENU_URL, "upgrade.asp", "Firmware Upgrade", 0, 0, MENU_DISPLAY, LANG_FIRMWARE_UPGRADE},
#endif // of UPGRADE_V1
#endif // of WEB_UPGRADE
#endif

#ifdef IP_ACL

	{"ACL Config", MENU_URL, "acl.asp", "ACL Setting", 0, 0, MENU_DISPLAY, LANG_ACL_CONFIG},

#endif

#ifdef CONFIG_USER_CUSTOM_UKB
#ifdef TIME_ZONE
  {"Time Zone", MENU_URL, "tz.asp", "Time Zone Configuration", 0, 0, MENU_DISPLAY, LANG_TIME_ZONE},
#endif
#endif

//added by xl_yue

#ifdef USE_LOGINWEB_OF_SERVER

	{"Logout", MENU_URL, "/admin/logout.asp", "Logout", 0, 0, MENU_DISPLAY, LANG_LOGOUT},

#endif

	{0, 0, 0, 0, 0, 0, 0, 0}

};

#ifdef CONFIG_USER_CUSTOM_UKB

struct RootMenu childmenu_dns_user[] = {
  //{"DNS Server", MENU_URL, "dns.asp", "DNS Server Configuration", 0, 0, MENU_DISPLAY, LANG_DNS_SERVER},
#ifdef CONFIG_USER_DDNS
  {"Dynamic DNS", MENU_URL, "ddns.asp", "DDNS Configuration", 0, 0, MENU_DISPLAY, LANG_DYNAMIC_DNS},
#endif
  {0, 0, 0, 0, 0, 0, 0, 0}
};

struct RootMenu childmenu_service_user[] = {
#ifdef CONFIG_USER_DHCP_SERVER
  //{"DHCP Mode", MENU_URL, "dhcpmode.asp", "DHCP Mode Configuration", 0, 0, MENU_DISPLAY},
#ifdef IMAGENIO_IPTV_SUPPORT
  {"DHCP", MENU_URL, "dhcpd_sc.asp", "DHCP Configuration", 0, 0, MENU_DISPLAY, LANG_DHCP},
#else
  {"DHCP", MENU_URL, "dhcpd.asp", "DHCP Configuration", 0, 0, MENU_DISPLAY, LANG_DHCP},
#endif
#endif
#ifdef CONFIG_USER_VLAN_ON_LAN
  {"VLAN on LAN", MENU_URL, "vlan_on_lan.asp", "VLAN on LAN Configuration", 0, 0, MENU_DISPLAY, LANG_VLAN_ON_LAN},
#endif
#ifndef CONFIG_SFU
#ifdef CONFIG_USER_DDNS
  {"DNS", MENU_FOLDER, &childmenu_dns_user, "",
   sizeof (childmenu_dns_user) / sizeof (struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_DNS},
#endif
  {"Firewall", MENU_FOLDER, &childmenu_fw_user, "",
   sizeof (childmenu_fw_user) / sizeof (struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_FIREWALL},
#endif
#if defined(CONFIG_USER_IGMPPROXY)&&!defined(CONFIG_IGMPPROXY_MULTIWAN)
  {"IGMP Proxy", MENU_URL, "igmproxy.asp", "IGMP Proxy Configuration", 0, 0, MENU_DISPLAY, LANG_IGMP_PROXY},
#endif
#if defined(CONFIG_USER_UPNPD)||defined(CONFIG_USER_MINIUPNPD)
  {"UPnP", MENU_URL, "upnp.asp", "UPnP Configuration", 0, 0, MENU_DISPLAY, LANG_UPNP},
#endif
#ifdef CONFIG_USER_ROUTED_ROUTED
  {"RIP", MENU_URL, "rip.asp", "RIP Configuration", 0, 0, MENU_DISPLAY, LANG_RIP},
#endif
#ifdef WEB_REDIRECT_BY_MAC
  {"Landing Page", MENU_URL, "landing.asp", "Landing Page Configuration", 0, 0, MENU_DISPLAY, LANG_LANDING_PAGE},
#endif
#if defined(CONFIG_USER_MINIDLNA)
	{"DMS", MENU_URL, "dms.asp", "DMS Configuration", 0, 0, MENU_DISPLAY, LANG_DMS},
#endif
#ifdef CONFIG_USER_SAMBA
  {"Samba", MENU_URL, "samba.asp", "Samba Configuration", 0, 0, MENU_DISPLAY, LANG_SAMBA},
#endif
  {0, 0, 0, 0, 0, 0, 0, 0}
};

#ifdef VOIP_SUPPORT
struct RootMenu childmenu_VoIP_user[] = {
  {"Port1", MENU_URL, "voip_general.asp?port=0", "Setup VoIP Port 1", 0, 0, MENU_DISPLAY, LANG_PORT1},
#if CONFIG_RTK_VOIP_CON_CH_NUM > 1
  {"Port2", MENU_URL, "voip_general.asp?port=1", "Setup VoIP Port 2", 0, 0, MENU_DISPLAY, LANG_PORT2},
#endif
#ifdef CONFIG_RTK_VOIP_DRIVERS_FXO
  {"FXO", MENU_URL, "voip_general.asp?port=2", "Configure FXO connection", 0, 0, MENU_DISPLAY, LANG_FXO},
#endif
  {"Tone", MENU_URL, "voip_tone.asp", "Configure SLIC tones", 0, 0, MENU_DISPLAY, LANG_TONE},
  {"Ring", MENU_URL, "voip_ring.asp", "Configure Ring Types of Phone", 0, 0, MENU_DISPLAY, LANG_RING},
  {"Other", MENU_URL, "voip_other.asp", "Other Services", 0, 0, MENU_DISPLAY, LANG_OTHERS},
//  {"Config", MENU_URL, "voip_config.asp", "Configuration Upload/Download", 0, 0, MENU_DISPLAY, LANG_CONFIG},
 {"Network",MENU_URL,"voip_network.asp","Network",0,0,MENU_DISPLAY, LANG_NETWORK},

  {0, 0, 0, 0, 0, 0, 0, 0}
};
#endif

struct RootMenu childmenu_statis_user[] = {
#ifdef CONFIG_SFU
{"Statistics", MENU_URL, "stats.asp", "Display packet statistics", 0, 0, MENU_DISPLAY, LANG_STATISTICS},
#else
  {"Interface", MENU_URL, "stats.asp", "Display packet statistics", 0, 0, MENU_DISPLAY, LANG_INTERFACE},
#endif
  {ADSL_STR, MENU_URL, "/admin/adsl-stats.asp",
#ifdef CONFIG_VDSL
  	"Display DSL statistics",
#else
  	"Display ADSL statistics",
#endif /*CONFIG_VDSL*/
  	0, 0, MENU_HIDDEN, LANG_DSL},

#ifdef CONFIG_DSL_VTUO
  {VTUO_STATUS_STR, MENU_URL, "/admin/vtuo-stats.asp", "Display VTU-O DSL statistics", 0, 0, MENU_HIDDEN, LANG_VTUO_DSL},
#endif /*CONFIG_DSL_VTUO*/

#ifdef CONFIG_USER_XDSL_SLAVE
  {ADSL_SLV_STR, MENU_URL, "/admin/adsl-slv-stats.asp",
#ifdef CONFIG_VDSL
  	"Display DSL Slave statistics",
#else
  	"Display ADSL Slave statistics",
#endif /*CONFIG_VDSL*/
  	0, 0, MENU_HIDDEN, LANG_DSL_SLAVE},
#endif /*CONFIG_USER_XDSL_SLAVE*/

#if defined(CONFIG_GPON_FEATURE) || defined(CONFIG_EPON_FEATURE)
  {PON_STR, MENU_URL, "/admin/pon-stats.asp", "Display PON Statistics", 0, 0, MENU_DISPLAY, LANG_PON},
#endif /*CONFIG_DSL_VTUO*/

  {0, 0, 0, 0, 0, 0, 0, 0}
};

#endif

/*

 *	Root Menu

 */

struct RootMenu rootmenu_user[] = {

	{"Status", MENU_FOLDER, &childmenu_status_user, "", sizeof(childmenu_status_user) / sizeof(struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_STATUS},

#ifdef CONFIG_USER_CUSTOM_UKB
	{"LAN", MENU_URL, "tcpiplan.asp", "Setup LAN Interface", 0, 0, MENU_DISPLAY, LANG_LAN},
#endif

#ifdef WLAN_SUPPORT

	{"WLAN", MENU_FOLDER, &childmenu_wlan_user, "", sizeof(childmenu_wlan_user) / sizeof(struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_WIRELESS},

#endif

	{"WAN", MENU_FOLDER, &childmenu_wan_user, "", sizeof(childmenu_wan_user) / sizeof(struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_WAN},

#ifdef CONFIG_USER_CUSTOM_UKB
  {"Services", MENU_FOLDER, &childmenu_service_user, "",
   sizeof (childmenu_service_user) / sizeof (struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_SERVICES},
   
#ifdef VOIP_SUPPORT
  {"VoIP", MENU_FOLDER, &childmenu_VoIP_user, "",
   sizeof (childmenu_VoIP_user) / sizeof (struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_VOIP},
#endif

#endif

#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
#ifndef CONFIG_USER_CUSTOM_UKB
	{"Firewall", MENU_FOLDER, &childmenu_fw_user, "", sizeof(childmenu_fw_user) / sizeof(struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_FIREWALL},
#endif
	{"Admin", MENU_FOLDER, &childmenu_admin_user, "", sizeof(childmenu_admin_user) / sizeof(struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_ADMIN},
#endif

#ifdef CONFIG_USER_CUSTOM_UKB
{"Statistics", MENU_FOLDER, &childmenu_statis_user, "",
   sizeof (childmenu_statis_user) / sizeof (struct RootMenu) - 1, 0, MENU_DISPLAY, LANG_STATISTICS},
#endif
   
	{0, 0, 0, 0, 0, 0, 0, 0}

};

#endif				// of CONFIG_DEFAULT_WEB


int createMenu_user(int eid, request * wp, int argc, char ** argv)
{
	int i = 0, totalIdNums = 0, maxchildrensize = 0;

	int IdIndex = 0;

	unsigned char isRootMenuEnd = 0;

 int nLanguageAR = 0;
  
#ifdef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
	char mStr[MAX_LANGSET_LEN] = {0};
	
	if (!mib_get (MIB_MULTI_LINGUAL, (void *)mStr)) 
	{
		fprintf (stderr, "mib get multi-lingual setting (boa) failed!\n");
	}
	if(!strcmp(mStr, "ar"))
	{
		nLanguageAR = 1;
	}
#endif

#ifdef CONFIG_RTL_92D_SUPPORT

	wlanMenuUpdate(rootmenu);

	wlanMenuUpdate(rootmenu_user);

#endif //CONFIG_RTL_92D_SUPPORT


	//calc the id nums and the max children size

	totalIdNums = calcFolderNum(rootmenu_user, &maxchildrensize);

	//product the js code

	addMenuJavaScript(wp, totalIdNums, maxchildrensize);

	//create the header
/* add by yq_zhou 09.2.02 add sagem logo for 11n*/
#ifdef CONFIG_11N_SAGEM_WEB
  boaWrite (wp, "<body  onload=\"initIt()\" bgcolor=\"#FFFFFF\" >\n");
#else
#ifdef CONFIG_USER_CUSTOM_UKB
  boaWrite (wp, "<body  onload=\"initIt()\" bgcolor=\"#0dabe1\" >\n");
#elif defined(CONFIG_USER_BOA_WITH_MULTILANG_AR_AR)
	boaWrite (wp, "<body  onload=\"initIt()\" bgcolor=\"#4a5661\" >\n");
#else
  boaWrite (wp, "<body  onload=\"initIt()\" bgcolor=\"#000000\" >\n");
#endif
#endif

	boaWrite(wp, "<table width=100%% border=0 cellpadding=0 cellspacing=0>\n<tr><td  width=100%% align=%s>\n", nLanguageAR ? "right" : "left");
#ifndef CONFIG_USER_BOA_WITH_MULTILANG_AR_AR
	boaWrite(wp, "<table border=0 cellpadding=0 cellspacing=0>\n" "<tr><td width=18 height=18><img src=menu-images/menu_root.gif width=18 height=18></td>\n" "<td  height=18 colspan=4 class=link><font size=3>%s:</font></td></tr>\n</table>\n", multilang(LANG_SITE_CONTENTS));
#endif


	if (rootmenu_user[1].u.addr)

		addMenu(wp, &rootmenu_user[0], 0, &IdIndex, 0);

	else

		addMenu(wp, &rootmenu_user[0], 0, &IdIndex, 1);



	boaWrite(wp, "</td></tr>\n</table>\n");


	return 0;
}
