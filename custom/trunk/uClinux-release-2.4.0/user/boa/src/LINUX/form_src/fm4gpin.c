/***************************************************************************************
 *
 *
 *       Filename:  fm4gpin.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  09/21/2016 07:50:43 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Eagle-Team
 *      Copyright:  Copyright (c) 2016, Xi'an LongQuest Technology LTD,all rights reserved
 *
 ***************************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <time.h>
#include <net/route.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <sys/ioctl.h>
#include "include/lc_common.h"
#include "include/nvmanager.h"
#include "include/mm_extern.h"
#include "include/ipc_msg_id_define.h"
#include "include/lc_ipc.h"
#include "include/lte_sdk.h"
#include "include/wm_log.h"
/*-- Local inlcude files --*/
#include "../webs.h"
#include "webform.h"
#include "mib.h"
#include "utility.h"

typedef enum pin_operation_t
{
	PIN_SET_LOCK = 1,
    PIN_SET_UNLOCK,
    PIN_SET_MODIFY,
    PIN_SET_DISABLE,
    PIN_SET_PUK,
	PIN_SET_UNKNOWN,
}pin_operation_t;

typedef struct {
	int pinset;
	char oldpin[16];
	char newpin[16];
	char puk[16];
}setpin_args;
int  ipc_init_lte_pin(int id)
{
	lc_uint32 ret;
	ret = ipc_msg_init(id, NULL, NULL, NULL, TRUE);
    if(IPC_OK != ret)
    {	
        return 0;
    }
	return 1;
}


///////////////////////////////////////////////////////////////////
void form4GsubmitInternetPin(request * wp, char *path, char *query)
{
	char	*strData,*strDatatmp;
	char tmp_web[8] = {0};
	setpin_args pin_args;

	ipc_init_lte_pin(CGI_ID);
	
	strData = boaGetVar(wp, "oldpin", "");
	strncpy(pin_args.oldpin, strData, sizeof(pin_args.oldpin));

	strData = boaGetVar(wp, "newpin", "");
	strncpy(pin_args.newpin, strData, sizeof(pin_args.oldpin));

	strData = boaGetVar(wp, "puk", "");
	strncpy(pin_args.puk, strData, sizeof(pin_args.oldpin));

	strData = boaGetVar(wp, "pinset", "");
	pin_args.pinset = atoi(strData);
	switch(pin_args.pinset)
	{
		case PIN_SET_LOCK:
		{
			enable_pin_req_t  wan_pin_oper;
			int  wan_pin_response;
			size_t len=32;
			lc_uint32 ret;			
 
			strcpy(wan_pin_oper.pin, pin_args.oldpin);
			ret = send_request_syn(MM_ID,WM_MSG_PIN_ENABLE_REQ,&wan_pin_oper,sizeof(wan_pin_oper),5000,&wan_pin_response,&len);
			break;
		}
		case PIN_SET_UNLOCK:
		{
			unlock_pin_req_t  wan_pin_oper;
			int  wan_pin_response;
			size_t len=32;
			lc_uint32 ret;
				
			strcpy(wan_pin_oper.pin , pin_args.oldpin);
			ret = send_request_syn(MM_ID,WM_MSG_PIN_UNLOCK_REQ,&wan_pin_oper,sizeof(wan_pin_oper),5000,&wan_pin_response,&len);
			break;
		}
		case PIN_SET_MODIFY:
		{
			change_pin_req_t  wan_pin_oper;
			int  wan_pin_response;
			size_t len=32;
			lc_uint32 ret;

			strcpy(wan_pin_oper.old_pin , pin_args.oldpin);
			strcpy(wan_pin_oper.new_pin , pin_args.newpin);
			ret = send_request_syn(MM_ID,WM_MSG_PIN_MODIFY_REQ,&wan_pin_oper,sizeof(wan_pin_oper),5000,&wan_pin_response,&len);
			break;
		}
		case PIN_SET_DISABLE:
		{					
			disable_pin_req_t  wan_pin_oper;
			int  wan_pin_response;
			size_t len=32;
			lc_uint32 ret;

			strcpy(wan_pin_oper.pin , pin_args.oldpin);
			ret = send_request_syn(MM_ID,WM_MSG_PIN_DISABLE_REQ,&wan_pin_oper,sizeof(wan_pin_oper),5000,&wan_pin_response,&len);
			break;
		}
		case PIN_SET_PUK:
		{
			unlock_puk_req_t  wan_pin_oper;
			int  wan_pin_response;
			size_t len=32;
			lc_uint32 ret;

			strcpy(wan_pin_oper.new_pin , pin_args.newpin);
			strcpy(wan_pin_oper.puk , pin_args.puk);
			ret = send_request_syn(MM_ID,WM_MSG_PUK_UNLOCK_REQ,&wan_pin_oper,sizeof(wan_pin_oper),5000,&wan_pin_response,&len);
			break;
		}
		default:
			break;
	}
	ipc_msg_fin();
	#ifdef COMMIT_IMMEDIATELY
	Commit();
	#endif
	strData = boaGetVar(wp, "submit-url", "");
	OK_MSG(strData);
	return;
}
void form4GConnectget(request * wp, char *path, char *query)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_status_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		OK_MSGWAN(tmpBuf);
	}
	else
	{
		OK_MSGWAN("err");
	}
	return;
}

void form4GInternetPin(request * wp, char *path, char *query)
{
	char tmpBuf[100]={0};
	char format_output[100];
	memset(tmpBuf,0,sizeof(tmpBuf));
	memset(format_output,0,sizeof(format_output));
	if(NV_OK == nvmram_readnv("wm_wcdma_remain_pin",tmpBuf,sizeof(tmpBuf)))
	{
		strcat(format_output,tmpBuf);
		strcat(format_output,",");
		memset(tmpBuf,0,sizeof(tmpBuf));
	}
	else
	{
		strcat(format_output,"err");
		strcat(format_output,",");
	}
	if(NV_OK == nvmram_readnv("wm_wcdma_remain_puk",tmpBuf,sizeof(tmpBuf)))
	{
			strcat(format_output,tmpBuf);
			strcat(format_output,",");
	}
	else
	{
		strcat(format_output,"err");
		strcat(format_output,",");
	}
	OK_MSGWAN(format_output);
	return;
}
void form4Ghideversion(request * wp, char *path, char *query)
{
	char tmpBuf[100]={0};
	char format_output[100];
	memset(tmpBuf,0,sizeof(tmpBuf));
	memset(format_output,0,sizeof(format_output));
	WM_LOG_INFO("form4Ghideversion");
	if(NV_OK == nvmram_readnv("wm_wcdma_modem_version_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		WM_LOG_INFO("form4Ghideversion success:%s",tmpBuf);
		strcat(format_output,tmpBuf);		
	}
	else
	{
		WM_LOG_INFO("form4Ghideversion error");
		strcat(format_output,"");
	}
	OK_MSGWAN(format_output);
	return;
}
void form4GLOGDebug(request * wp, char *path, char *query)
{
	WM_LOG_INFO("form4GLOGDebug start");
	
    system("/bin/ifconfig 4g1 192.168.16.2 up && iptables -t nat -I POSTROUTING -o 4g1 -j MASQUERADE");
    system ("/bin/echo 1 > /proc/sys/net/ipv4/ip_forward");
	
	va_cmd(IPTABLES, 6, 1, (char*)FW_DEL, (char *)FW_INPUT, "-d", "192.168.15.1" , "-j", (char *)FW_DROP);
	
	WM_LOG_INFO("form4GLOGDebug end");
	
	OK_MSGWAN("OK");
	return;
}




