#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <time.h>
#include <net/route.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <sys/ioctl.h>
#include "include/lc_common.h"
#include "include/nvmanager.h"
#include "include/ipc_msg_id_define.h"
#include "include/lc_ipc.h"
#include "include/lte_sdk.h"
/*-- Local inlcude files --*/
#include "../webs.h"
#include "webform.h"
#include "mib.h"
#include "utility.h"

int  ipc_init(int id)
{
	lc_uint32 ret;
	ret = ipc_msg_init(id, NULL, NULL, NULL, TRUE);
    if(IPC_OK != ret)
    {	
        return 0;
    }
	return 1;
}

///////////////////////////////////////////////////////////////////
void form4GConf(request * wp, char *path, char *query)
{
	char	*strData;
	char tmpBuf[100];
	lc_uint32 ret;
	strData = boaGetVar(wp, "connectstatus", "");
	ipc_init(CGI_ID);
	if(!strcmp("1", strData))
	{
		//CGI_MSG("%s","call WM_MSG_CONNECT_REQ send  start.\n");
		ret = send_request(MM_ID,WM_MSG_CONNECT_REQ,NULL,0);
	//	CGI_MSG("%s","call WM_MSG_CONNECT_REQ send notify success.\n");
	}
	else
	{
	//	CGI_MSG("%s","call WM_MSG_DISCONNECT_REQ send start.\n");
		ret = send_request(MM_ID,WM_MSG_DISCONNECT_REQ,NULL,0);
	//	CGI_MSG("%s","call WM_MSG_DISCONNECT_REQ send notify success.\n");
	}
	ipc_msg_fin();
	strData = boaGetVar(wp, "submit-url", "");
	if(ret != TRUE )
	{
       OK_MSGWAN("-1");
        
    }
   	OK_MSGWAN("0");
	return;
}

void form4GConfget(request * wp, char *path, char *query)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_status_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		OK_MSGWAN(tmpBuf);
	}
	else
	{
		OK_MSGWAN("err");
	}
	return;
}
void form4GTrafficdata(request * wp, char *path, char *query)
{
	char tmpBuf[2048]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_volume_value_tmp_bak",tmpBuf,sizeof(tmpBuf)))
	{
		OK_MSGWAN(tmpBuf);
	}
	else
	{
		OK_MSGWAN("err");
	}
	return;
}

void form4GResetdata(request * wp, char *path, char *query)
{
	char tmpBuf[2048]={0};
	lc_uint32 nm_response,ret;
	size_t len=32;
	memset(tmpBuf,0,sizeof(tmpBuf));
	if(NV_OK == nvmram_readnv("wm_wcdma_volume_value_tmp",tmpBuf,sizeof(tmpBuf)))
	{
		ipc_init(CGI_ID);
		ret = send_request_syn(MM_ID,WM_MSG_CLEAR_PEAK_REQ,NULL,0,20000,&nm_response,&len);
		ipc_msg_fin();
		OK_MSGWAN("0");
	}
	else
	{
		OK_MSGWAN("err");
	}
	return;
}

void form4GgetRsetperiod(request * wp, char *path, char *query)
{
	char tmpBuf[100]={0};
	memset(tmpBuf,0,sizeof(tmpBuf));
	MIB_WAN_LTE_T Entry_LTE;
	if(!mib_chain_get( MIB_WAN_LTE_TBL, 0, (void*)&Entry_LTE))
   	{
       	OK_MSGWAN("err");
   	}
	else
	{
		sprintf(tmpBuf,"%d",Entry_LTE.peak_time);
		OK_MSGWAN(tmpBuf);
	}
	return;
}

void form4Gsetperiod(request * wp, char *path, char *query)
{
	char *p_tmp_webvar;

	MIB_WAN_LTE_T Entry_LTE;
	p_tmp_webvar = boaGetVar(wp,"max_data_rate_period","5");
	
  

	if(!mib_chain_get( MIB_WAN_LTE_TBL, 0, (void*)&Entry_LTE))
   	{
       	  WM_LOG_ERROR("get  mib err\n");
   	}
	else
	{
		Entry_LTE.peak_time = atoi(p_tmp_webvar);
	}
	 
    if(!mib_chain_update(MIB_WAN_LTE_TBL, &Entry_LTE, 0))
    {
        WM_LOG_ERROR("set mib LTE TABLE max_data_rate_period error\n");
        
    }
	  mib_update(CURRENT_SETTING, CONFIG_MIB_ALL);
	return;
}

