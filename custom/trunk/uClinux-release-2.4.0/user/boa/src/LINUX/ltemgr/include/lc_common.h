#ifndef __LC_COMMON_H
#define __LC_COMMON_H

#include "ipc_msg_id_define.h"
#include "track_id_define.h"
#include "lte_sdk.h"


/*system work mode*/
typedef enum
{
    PERFORMANCE_PREFER,
    POWERSAVE_PREFER
}sys_work_mode_t;

/*systerm state define*/
typedef enum
{
    SYS_INIT,
    SYS_ACTIVE,
    SYS_FACTORY,
    SYS_UPGRADE,
    SYS_REBOOT,
    SYS_POWEROFF,
    SYS_TEMPERATURE_CRITICAL,
    SYS_DEACTIVE,
    SYS_SLEEP,
    SYS_BATTERY_CRITICAL,
	SYS_FORCE_POWER_OFF, 	/*pacino.cao 2012-0504 support force power off and recovery state*/
	SYS_CONFIG_UPDATE,
    SYS_MODEM,
    SYS_MAX
}sys_state_t;

/*app module ID*/
typedef enum
{
    WM_ID,
    MM_ID,
    CGI_ID,
    // 可能存在的客户端共用
    // 这两个ID在IPC初始化时只能初始化为多进程形式
    CLI1_ID,
    CLI2_ID,
    EM_ID,
    MAX_ID
}module_id_t;

/*模块属性*/
typedef enum
{
    NORMAL=1,
    IMPORTANT=2,
    SUPER=4,
    REGISTER=8,  /*标志是否向SC注册过*/
    POLL_SLEEP=16
    
}module_attr_t;

/*watchdog属性*/
typedef enum
{
LCT_NO_ACTION,//不动作
LCT_SYSTEM_REBOOT,//系统重启
LCT_SELF_RESTART//模块自己重启
} watchdog_attr_t;
#define MAX_MODULE_NAME           10  //module name length
#define  MAX_MODULE_ENTYR         40  //module entry length
#define  MAX_MODULE_PARA          40  //module para length

/*app task info struct*/
typedef struct
{
    lc_uint32       module_id;/*模块Id*/
    lc_uint8        moudle_name[MAX_MODULE_NAME];/*模块名*/
    lc_uint8        module_entry[MAX_MODULE_ENTYR];/*模块入口*/
    lc_uint8        module_para[MAX_MODULE_PARA];/*模块参数*/
    lc_uint32       module_attr; /*模块属性,以掩码方式管理，同时可以标示该模块
                                  *是否向SC注册过*/
    lc_uint32       st_change_time;/*状态切换需要的时间*/
    lc_int32        watchdog_init_time;//初始超时时间
    lc_int32        watchdog_current_time;//当前超时时间
    lc_uint32       watchdog_attr;//异常恢复策略
}app_task_info_t;


#endif
