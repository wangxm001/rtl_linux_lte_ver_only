#ifndef __COS_STRING_H
#define __COS_STRING_H

#include <ctype.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <syslog.h>

#include <stdlib.h>
#include <string.h>

#include "lc_common.h"



#define lc_toupper(c) (toupper_ascii_fast_table[(c&0xff)] & 0xff)
#define lc_tolower(c) (tolower_ascii_fast_table[(c&0xff)] & 0xff)
//#define isupper(c) ((c&0xff) != tolower(c&0xff))
//#define islower(c) ((c&0xff) != toupper(c&0xff))

lc_char * lc_strcat (lc_char *destination, const lc_char *append  );

lc_char * lc_strchr (const lc_char * s, lc_char c );
lc_int32 lc_strcmp
    (
    const lc_char * s1, /* string to compare */
    const lc_char * s2  /* string to compare s1 to */
    );
lc_int32 lc_strcoll
    (
    const lc_char * s1, /* string 1 */
    const lc_char * s2  /* string 2 */
    );

lc_char * lc_strcpy
    (
    lc_char *       s1, /* string to copy to */
    const lc_char * s2  /* string to copy from */
    );

size_t lc_strcspn
    (
    const lc_char * s1, /* string to search */
    const lc_char * s2  /* set of characters to look for in s1 */
    );

lc_char * lc_strerror
    (
    lc_int32 errcode /* error code */
    );

size_t lc_strlen
    (
    const lc_char * s /* string */
    );

lc_char * lc_strncat
    (
    lc_char *       dst, /* string to append to */
    const lc_char * src, /* string to append */
    size_t       n    /* max no. of characters to append */
    );

lc_int32 lc_strncmp
    (
    const lc_char * s1, /* string to compare */
    const lc_char * s2, /* string to compare s1 to */
    size_t       n   /* max no. of characters to compare */
    );

lc_char * lc_strncpy
    (
    lc_char *       s1, /* string to copy to */
    const lc_char * s2, /* string to copy from */
    size_t       n   /* max no. of characters to copy */
    );

lc_char * lc_strstr
    (
    const lc_char * s,   /* string to search */
    const lc_char * find /* substring to look for */
    );

lc_char * lc_strtok
    (
    lc_char *       string,   /* string */
    const lc_char * separator /* separator indicator */
    );


void lc_str_tolower(lc_char *str);
void lc_str_toupper (lc_char *str);
lc_int32 lc_strcasecmp (const lc_char *s1, const lc_char *s2);
lc_int32 lc_strncasecmp (const lc_char *s1, const lc_char *s2, size_t len);
lc_int32 lc_strwicmp(const lc_char *psz1, const lc_char *psz2);
int lc_get_int_arg(char *src, char sep, int *argc, int *argv);

#endif

