/***************************************************************************************
 *
 *
 *    File name:  liblog.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  May 25 2010
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  lindong
 *      Copyright:  Copyright (c) 2009, Xi'an LongQuest Technology LTD,all rights reserved
 *
 ***************************************************************************************/

#ifndef LC_LIBLOG_H
#define LC_LIBLOG_H
/*************************************************************************************
*
*                                       Extern File
*
***************************************************************************************/

#include <syslog.h>
#include "lc_common.h"

/*************************************************************************************
*
*                                       End extern File
*
***************************************************************************************/


/*************************************************************************************
*
*                                       Data structure define
*
***************************************************************************************/
typedef enum
{
    LC_LOG_IDENT_SC = 0,      //system control
    LC_LOG_IDENT_DM,          //device manager
    LC_LOG_IDENT_AT,          //AT
    LC_LOG_IDENT_WM,          //wan manager
    LC_LOG_IDENT_NM,          //network manager
    LC_LOG_IDENT_RM,          //recorder manager
    LC_LOG_IDENT_UI,          //led lcd
    LC_LOG_IDENT_UP,          //upgrade
    LC_LOG_IDENT_NS,          //net storage
    LC_LOG_IDENT_DIAG,        //diagnosis
    LC_LOG_IDENT_CGI,         //cgi
    LC_LOG_IDENT_WATCHDOG,    //watch dog
	LC_LOG_IDENT_MM,
    LC_LOG_IDENT_WLAN,        //WLAN
    LC_LOG_IDENT_SMS_PB,      //SMS and PB
    LC_LOG_IDENT_CM,          //CALL MANAGER
    LC_LOG_IDENT_INVALID,     //invalid and max ID

    LC_LOG_IDENT_CONFIG_FILE_START = 1000,  //config file logid.cfg ID start from 1000
}log_ident_t;

typedef enum
{
    LC_LOG_SUCCESS = 0,   //success
    LC_LOG_FAILED,        //failed
}log_error_t;


typedef enum
{
    LC_LOG_DEBUG = 0,   //debug level
    LC_LOG_INFO,        //infomation level 
    LC_LOG_ERROR,       //error level
}log_level_t;


/*************************************************************************************
*
*                                       End data structure define
*
***************************************************************************************/

/*************************************************************************************
*
*                                       Function define
*
***************************************************************************************/

/********************************************************************************
*
*    Name:log_init
*    Description:init the log library
*    Parms:modules's ID , it will be enumerated by the logident_t and log config file
*    Return:
*           LC_LOG_SUCCESS:success;
*           LC_LOG_FAILED:fail
*    Notes:
********************************************************************************/
lc_int32 log_init(lc_int32 module_id);

/********************************************************************************
*
*    Name:lc_log_debug
*    Description:out put the log information, 
*    Parms:
*        log_level:LC_LOG_DEBUG,LC_LOG_INFO,LC_LOG_ERROR
*        format:reference the printf function parameter
*    Return:NULL
*             
*    Notes:
********************************************************************************/
void log_print(lc_int32 log_level, const lc_int8 *format, ...);

/*************************************************************************************
*
*                              End function define
*
***************************************************************************************/
#endif
