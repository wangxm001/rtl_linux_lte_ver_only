#ifndef __COS_COMMON_H
#define __COS_COMMON_H

/* Print directly to the console */
#define cCOS_DEBUG(fmt, args...) do { \
	FILE *fp = fopen("/dev/console", "w"); \
	if (fp) { \
		fprintf(fp, fmt, ## args); \
		fclose(fp); \
	} \
} while (0)


#ifdef LOG_DEBUG
#define COS_DEBUG(formats, args...) \
  log_print(0,"[LOG][%s:%d]:"formats"\n", __FILE__, __LINE__, ##args);

#else 
#define COS_DEBUG(formats, args...)  \
  cCOS_DEBUG("[LOG][%s:%d]:"formats"\n", __FILE__, __LINE__, ##args);

#endif


#endif

