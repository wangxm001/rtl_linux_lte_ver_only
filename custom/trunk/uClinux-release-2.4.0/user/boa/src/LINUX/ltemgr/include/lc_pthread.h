/***************************************************************************************
 *
 *
 *       Filename:  lc_pthread.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  05/17/2010 03:29:08 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  daishenglong
 *      Copyright:  Copyright (c) 2010, Xi'an LongQuest Technology LTD,all rights reserved
 *
 ***************************************************************************************/


#ifndef __COS_PTHREAD_H
#define __COS_PTHREAD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <netdb.h>
#include <syslog.h>
#include <pthread.h>
#include <time.h>

#include "lc_common.h"

/********************************************************************
 *
 *         Name:  lc_fork
 *  Description:  create a process
 *        Input:  NULL
 *       Output:  NULL
 *       Return: On success, the PID of the child process is returned 
                 in the parent, and 0 is returned in the  child.On  
                 failure,  -1  is returned in the parent, no child
                 process is created, and errno is set appropri�\ ately.
 *        Notes: 
********************************************************************/

lc_int32 lc_fork();

/********************************************************************************
 *
 *         Name:  _eval
 *  Description:  exec the system process
 *        Parms:  
 *       Return:
 *        Notes:
 ********************************************************************************/

int _eval(char *const argv[], const char *path, int timeout, int *ppid);

/********************************************************************************
 *
 *         Name:  f_read
 *  Description:  open and read buffer from file
 *        Parms:	
                path -- file name
                buffer -- destination buffer
                max -- length
 *       Return:
 *        Notes:
 ********************************************************************************/

int f_read(const char *path, void *buffer, int max);

/********************************************************************************
 *
 *         Name:  f_read_string
 *  Description:  read string from file
 *        Parms: 
                filename --- file name
                buffer --- the destination buffer
                max --- length
 *       Return:
 *        Notes:
 ********************************************************************************/
int f_read_string(const char *path, char *buffer, int max);

/*get process name accroding to pid*/
char *psname(int pid, char *buffer, int maxlen);


/*get pid from process name*/
int lc_pidof(const char *name);

/********************************************************************************
 *
 *         Name:  lc_killall
 *  Description:  kill process
 *        Parms:
 *       Return:
 *        Notes:
 ********************************************************************************/
int lc_killall(const char *name, int sig);

/********************************************************************************
 *
 *         Name:  lc_killall_tk
 *  Description:  kill process by name 
 *        Parms:  name --- process name
 *       Return:
 *        Notes:
 ********************************************************************************/
void lc_killall_tk(const char *name);


/********************************************************************
 *
 *         Name:  lc_pthread_create
 *  Description:  creat a thread
 *        Input:  tidp: thread id.
                  attr: new thread attribute
                  start_rtn: new thread start function
                  arg: start_rtn function input argument
 *       Output:  NULL
 *       Return: 
                 If successful,  return zero; otherwise, an error number
                 shall  be returned to indicate the error.
 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_create(pthread_t *  tidp,
                             const pthread_attr_t *  attr, 
                             void *(*start_rtn)(void *),
                             void *  arg);

/********************************************************************
 *
 *         Name:  lc_pthread_exit
 *  Description:  terminate one thread
 *        Input:  NULL
 *       Output:  value_ptr: thread return value
 *       Return: 
 *        Notes: 
********************************************************************/

void lc_pthread_exit(void *value_ptr);


/********************************************************************
 *
 *         Name:  lc_pthread_exit
 *  Description:  This  function  shall suspend execution of the 
                 calling thread until the target thread terminates, 
                 unless  the  target  thread  has  already  terminated.
 *        Input:  thread:thread id
 *       Output:  value_ptr: thread return value
 *       Return: 
 *        Notes: 
********************************************************************/


lc_int32 lc_pthread_join(pthread_t thread, void **value_ptr);

/********************************************************************
 *
 *         Name:  lc_pthread_exit
 *  Description:  to request that thread be canceled
 *        Input:  thread:thread id
 *       Output: NULL
 *       Return:   If successful,  return zero; otherwise, an error
                   number shall  be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_cancel(pthread_t tid);


/********************************************************************
 *
 *         Name:  lc_pthread_attr_init
 *  Description:  to init a thread attributes object
 *        Input: 
                  attr: thread attribute data 
 *       Output: NULL
 *       Return:   If successful,  return zero; otherwise, an error
                   number shall  be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_attr_init(pthread_attr_t *attr);


/********************************************************************
 *
 *         Name:  lc_pthread_attr_destroy
 *  Description:  destroy a thread attributes object
 *        Input: 
                  attr:  Attributes objects are provided for threads
 *       Output: NULL
 *       Return:   If successful,  return zero; otherwise, an error
                   number shall  be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_attr_destroy(pthread_attr_t *attr);

/********************************************************************
 *
 *         Name:  lc_pthread_attr_getscope
 *  Description:  get the contentionscope attribute in the attr object.
 *        Input: 
                  attr:  Attributes objects are provided for threads
         Output:  contentionscope:The contentionscope attribute may 
                  have the values PTHREAD_SCOPE_SYSTEM, signifying 
                  system scheduling contention scope, or PTHREAD_SCOPE_PROCESS,
                  signifying process  scheduling  contention  scope.
 *      
 *       Return:   If successful,  return zero; otherwise, an error
                   number shall  be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_attr_getscope(const pthread_attr_t *  attr,
                              lc_int32 *  contentionscope);


/********************************************************************
 *
 *         Name:  lc_pthread_attr_setscope
 *  Description:  set the contentionscope attribute in the attr object.
 *        Input: 
                  attr:  Attributes objects are provided for threads
                  contentionscope:The contentionscope attribute may 
                  have the values PTHREAD_SCOPE_SYSTEM, signifying 
                  system scheduling contention scope, or PTHREAD_SCOPE_PROCESS,
                  signifying process  scheduling  contention  scope.
 *       Output: NULL
 *       Return:   If successful,  return zero; otherwise, an error
                   number shall  be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_attr_setscope(pthread_attr_t *attr, lc_int32 contentionscope);

/********************************************************************
 *
 *         Name:  lc_pthread_attr_getdetachstate
 *  Description:  get the detachstate attribute of the thread.
 *        Input: 
                  attr:  Attributes objects are provided for threads
          Output   detachstate: detachstate shall be set  to  either 
          PTHREAD_CREATE_DETACHED  or PTHREAD_CREATE_JOINABLE.

 *       Return:   If successful,  return zero; otherwise, an error
                   number shall  be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_attr_getdetachstate(const pthread_attr_t *attr,
                                        lc_int32 *detachstate);

/********************************************************************
 *
 *         Name:  lc_pthread_attr_setdetachstater
 *  Description:  set the detachstate attribute to the thread.
 *        Input: 
                  attr:  Attributes objects are provided for threads
                  detachstate: the application shall set detachstate
                  to either PTHREAD_CREATE_DETACHED or
                  PTHREAD_CREATE_JOIN ABLE.
 *       Output: NULL
 *       Return:   If successful,  return zero; otherwise, an error
                   number shall  be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_attr_setdetachstate(pthread_attr_t *attr,
                                         lc_int32 detachstate);

/********************************************************************
 *
 *         Name:  lc_pthread_attr_getstacksize
 *  Description:  get the stack size attribute of the thread attributes 
                  object referred to by attr in the buffer pointed to  
                  by stacksize.
                  
 *        Input: 
                  attr:  Attributes objects are provided for threads
          Output   stacksize: the stack size attribute of the thread
 *       Return:   If successful,  return zero; otherwise, an error
                   number shall  be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_attr_getstacksize(pthread_attr_t *attr,
                                       size_t *stacksize);


/********************************************************************
 *
 *         Name:  lc_pthread_attr_setstacksize
 *  Description:  set the stack size attribute to the thread.
 *        Input: 
                  attr:  Attributes objects are provided for threads
                  stacksize: the application shall set detachstate
                  to either PTHREAD_CREATE_DETACHED or
                  PTHREAD_CREATE_JOIN ABLE.
 *       Output: NULL
 *       Return:   If successful,  return zero; otherwise, an error
                   number shall  be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_attr_setstacksize(pthread_attr_t *attr, 
                                      size_t stacksize);

/********************************************************************
 *
 *         Name:  lc_pthread_mutex_init
 *  Description:   initialize the mutex referenced by mutex with 
                   attributes specified by attr.
 *        Input: 
                  mutex:  the pthread mutex object
                  attr: the attribute of the thread mutex
 *       Output: NULL
 *       Return:   If successful,  return zero; otherwise, an error
                   number shall  be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_mutex_init(pthread_mutex_t *  mutex,
             const pthread_mutexattr_t *  attr);

/********************************************************************
 *
 *         Name:  lc_pthread_mutex_destroy
 *  Description:   destroy the pthread mutex
 *        Input: 
                  mutex:  the pthread mutex object
             
 *       Output: NULL
 *       Return:   If successful,  return zero; otherwise, an error
                   number shall  be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_mutex_destroy(pthread_mutex_t *mutex);

/********************************************************************
 *
 *         Name:  lc_pthread_mutex_lock
 *  Description:   the thread will lock the mutex ,the calling thread 
                   shall block until the mutex becomes available.
                   This operation shall  return  with the mutex 
                   object referenced by mutex in the locked state
                   with the calling thread as its owner
 *        Input: 
                  mutex:  the pthread mutex object
             
 *       Output: NULL
 *       Return:   If successful,  return zero; otherwise, an error
                   number shall  be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_mutex_lock(pthread_mutex_t *mutex);


/********************************************************************
 *
 *         Name:  lc_pthread_mutex_trylock
 *  Description:  Try to lock the mutex , the calling thread will return 
                   whether the mutex had been locked.
 *        Input: 
                  mutex:  the pthread mutex object
             
 *       Output: NULL
 *       Return:   If successful,  return zero if a lock on the mutex 
                   object  referenced  by mutex is acquired. Otherwise,
                   an error number is returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_mutex_trylock(pthread_mutex_t *mutex);


/********************************************************************
 *
 *         Name:  lc_pthread_mutex_unlock
 *  Description:  shall release the mutex object referenced by mutex.
 *        Input: 
                  mutex:  the pthread mutex object
             
 *       Output: NULL
 *       Return:   If successful,  hall return zero; otherwise an error 
                   number shall be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_mutex_unlock(pthread_mutex_t *mutex);

#if 0

/********************************************************************
 *
 *         Name:  lc_pthread_mutex_timedlock
 *  Description:  If the mutex cannot be locked without waiting for
                   another thread to unlock the mutex, this wait 
                   shall be terminated when the specified timeout expires..
 *        Input: 
                  mutex:  the pthread mutex object
                  abs_timeout: waiting time
 *       Output: NULL
 *       Return:   If successful, return zero; otherwise an error 
                   number shall be returned to indicate the error. 
 *        Notes: 
********************************************************************/


 lc_int32 lc_pthread_mutex_timedlock(pthread_mutex_t *  mutex,
              const struct timespec *  abs_timeout);

#endif


/********************************************************************
 *
 *         Name:  lc_pthread_cond_init
 *  Description:  to initialize  condition  variable use default 
                   attribute.
 *        Input: 
                  cond:  the pthread condition  variable object
                  attr: the attribute if the condition  variable object
 *       Output: NULL
 *       Return:   If successful, return zero; otherwise an error 
                   number shall be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_cond_init(pthread_cond_t *  cond,
              const pthread_condattr_t *  attr);

/********************************************************************
 *
 *         Name:  lc_pthread_cond_destroy
 *  Description:  to destroy  condition  variable 
 *        Input: 
                  cond:  the pthread condition  variable object
                 
 *       Output: NULL
 *       Return:   If successful, return zero; otherwise an error 
                   number shall be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_cond_destroy(pthread_cond_t *cond);


/********************************************************************
 *
 *         Name:  lc_pthread_cond_broadcast
 *  Description:  unblock all threads currently blocked on the specified
                   condition variable cond 
 *        Input: 
                  cond:  the pthread condition variable object
                 
 *       Output: NULL
 *       Return:   If successful, return zero; otherwise an error 
                   number shall be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_cond_broadcast(pthread_cond_t *cond);


/********************************************************************
 *
 *         Name:  lc_pthread_cond_signal
 *  Description:  unblock at least one of the threads that are blocked
                 on the specified condition variable cond (if any
                  threads are blocked on cond). 
 *        Input: 
                  cond:  the pthread condition variable object
                 
 *       Output: NULL
 *       Return:   If successful, return zero; otherwise an error 
                   number shall be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_cond_signal(pthread_cond_t *cond);

/********************************************************************
 *
 *         Name:  lc_pthread_cond_wait
 *  Description:  block on a condition variable. They shall be called 
                  with mutex locked by the calling thread or undefined
                  behavior results.
		 
 *        Input: 
                  cond:  the pthread condition variable object
                  mutex:the pthread mutex object
                  
 *       Output: NULL
 *       Return:   If successful, return zero; otherwise an error 
                   number shall be returned to indicate the error. 
 *        Notes: 
********************************************************************/
lc_int32 lc_pthread_cond_wait(pthread_cond_t *  cond,
             pthread_mutex_t *  mutex);


/********************************************************************
 *
 *         Name:  lc_pthread_cond_timedwait
 *  Description:  block on a condition variable. They shall be called 
                  with mutex locked by the calling thread or undefined
                  behavior results.
		 
 *        Input: 
                  cond:  the pthread condition variable object
                  mutex:the pthread mutex object
                  abstime:waiting time
 *       Output: NULL
 *       Return:   If successful, return zero; otherwise an error 
                   number shall be returned to indicate the error. 
 *        Notes: 
********************************************************************/

lc_int32 lc_pthread_cond_timedwait(pthread_cond_t *  cond,
			 pthread_mutex_t *  mutex,
			 const struct timespec *  abstime);

/********************************************************************
 *
 *         Name:  lc_pidof
 *  Description:  query a process if exist by name.
		 
 *        Input:  const char *name , the name of process. 
                  
 *       Output: NULL
 *       Return:   If successful, return PID ; otherwise return -1. 
 *        Notes: 
********************************************************************/

int lc_pidof(const char *name);






#define lc_eval(cmd, args...) ({ \
        char *argv[] = { cmd, ## args, NULL }; \
        _eval(argv, ">/dev/console", 0, NULL); \
        })



#define lc_evalpid(cmd, pid, args...) ({ \
        char *argv[] = { cmd, ## args, NULL }; \
        _eval(argv, ">/dev/console" , 0, pid); \
        })






#endif



