/*****************************************************************
 *
 *
 *     Filename:  sdk_timer.h
 *
 *    Description:  This file provides an interface of lc_timer
 *
 *        Version:  1.0
 *        Created:  
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  wangxiaoming
 *      Copyright:  Xi'an LongQuest Technology LTD,all rights reserved
 *
 *******************************************************************/

#ifndef SDK_TIMER_H

#define SDK_TIMER_H

#include "lc_common.h"
#include "lc_list.h"
#include "lc_mem.h"
#include "liblog.h"
#ifdef TIMERSTDIO
#define TIMER_LOG_MSG(log_level, format, args...) \
	printf("[%s:%d]"format, __FILE__, __LINE__, ##args) 
#else
#ifdef TIMERDEBUG
#define TIMER_LOG_MSG(log_level, format, args...) \
	log_print(log_level,"[%s:%d]"format, __FILE__, __LINE__, ##args) 
#else
#define TIMER_LOG_MSG(log_level, format, args...) \
if(log_level == LC_LOG_DEBUG || log_level == LC_LOG_INFO) \
    ; \
else \
	log_print(log_level,"[%s:%d]"format, __FILE__, __LINE__, ##args) 
#endif

#endif



#define TIMER_US            (1000)                              /*1000us*/
#define TIMER_TICK          (50)                                /*50ms*/
#define TIMER_TICK_US       (TIMER_TICK*TIMER_US)               /*50000us*/
#define TIMER_TICK_PATH     (-TIMER_TICK_US/10)                 /*5000us*/
#define TIMER_MININTERVAL   (2*TIMER_TICK)                      /*mini interval of timer(100ms)*/
#define TIMER_MAXINTERVAL   0xFFFFFFFF                          /*max interval of timer(2^32ms)*/

#define TIMER_TRANSFER(a)   (a/TIMER_TICK)      

#define TIMER_MAXNAME       20                                  /*max length of timer's name*/
#define TIMER_REPEAT        1                                   /*repeat timer*/
#define TIMER_NOREPEAT      0                                   /*no-repeat timer*/
#define TIMER_INVALID       0x0                                 /*invalid timer_id*/
#define NODE_DATA(list)     ((timer_node_t *) list->data)


typedef enum
{
    TIME_OK=0,          //success
    TIME_ERRLIST,       //timer_list error
    TIME_ERRARGS,       //args error
    TIME_FAILURE,       //can't find timer
    TIME_ERRTHREAD,     //start timer_thread error
    TIME_ERR=-1,        //operation error
}timer_err_t;


/*timer structure*/
struct timer
{
    lc_char             name[TIMER_MAXNAME];    //timer name
    lc_uint32           interval;               //interval of timer(ms)
    lc_uint32           repeat;                 //repeat timer or not
    void                (*func)(void *args);    //call back function when timer interval expired
    void                *user_data;             //call back function args
};

/*timer node structure*/
typedef struct timer_node
{    
    lc_uint32           index;                 //node memory address
	lc_uint32 			session_id;			   //excusive id for timer
    lc_int32           re_interval;            //interval relative  to preview node
    struct timer        data;                  //the data of node
}timer_node_t;

/*timer statistics sturcture*/
typedef struct timer_state
{
    lc_uint32           rep_count;              //count of repeat timer
    lc_uint32           norep_count;            //count of no-repeat timer
}timer_state_t;

/*the header of timer_list structure*/
typedef struct timer_header
{
    timer_state_t        data;                  //the statistic data of timer_list
    list_head           *header;                //the header of timer_list
}timer_header_t;

typedef struct exclusive_id
{
    lc_uint32           timer_id;
    lc_uint32           session_id;
}exclusive_id_t;


/********************************************************************
 *
 *         Name:  init_timer
 *  Description:  inite timer_list and start timer_thread
 *        Input: 
 *          
 *       Output:  
 *       Return: timer_err_t
 *       Notes: before calling start_timer()/stop_timer()/query_timer,must call init_timer()
 *       one node .
********************************************************************/
timer_err_t init_timer(void);
/********************************************************************
 *
 *         Name:  start_timer
 *  Description:  start one special timer
 *        Input: ptimer: pointer of timer information
 *          
 *       Output: only_id : exclusive id which will be assigned
 *       Return: timer_err_t
 *       Notes: before calling ,must call init_timer() to init
********************************************************************/
timer_err_t start_timer(struct timer *ptimer,exclusive_id_t *only_id);
/********************************************************************
 *
 *         Name:  stop_timer
 *  Description:  stop one special timer by exclusive id
 *        Input: only_id : this timer's exclusive id
 *          
 *       Output: 
 *       Return: timer_err_t
 *       Notes: before calling ,must call init_timer() to init
********************************************************************/
timer_err_t stop_timer(exclusive_id_t only_id);

/********************************************************************
 *
 *         Name:  query_timer
 *  Description:  query timer_list and get the statistics of timer_list
 *        Input: 
 *          
 *       Output: stat:statistics of timer_list
 *       Return: timer_err_t
 *       Notes: before calling ,must call init_timer() to init
 *       
********************************************************************/
timer_err_t query_timer(timer_state_t *stat);

/********************************************************************
 *
 *         Name:  exist_timer
 *  Description:  check existing of special timer
 *        Input: only_id : exclusive id
 *          
 *       Output: 
 *       Return: timer_err_t
 *       Notes: before calling ,must call init_timer() to init
********************************************************************/
timer_err_t exist_timer(exclusive_id_t only_id);


/********************************************************************
 *
 *         Name:  get_timer
 *  Description:  get timer by timer's name
 *        Input:  pname: timer's name           
 *       Output:  ptimer: address of timer which name is pname
 *       Return: timer_err_t
 *       Notes: before calling ,must call init_timer() to init
********************************************************************/
//timer_err_t get_timer(struct timer *ptimer,lc_char *pname);

/********************************************************************
 *
 *         Name:  get_timer_id
 *  Description:  get timer_id by timer's name
 *        Input:  pname: timer's name 
 *       Output:  timer_id: timer id 
 *       Return: timer_err_t
 *       Notes: before calling ,must call init_timer() to init
********************************************************************/
//timer_err_t get_timer_id(,lc_char *pname);

#endif

