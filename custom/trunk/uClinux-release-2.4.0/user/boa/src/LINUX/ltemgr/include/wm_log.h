/******************************************************************************
*
*       Filename:  wm_sm.h
*
*    Description:  log ��ӡ
*
*        Version:  1.0
*        Created:  04/08/2010
*       Revision:  none
*       Compiler:  gcc
*
*         Author:  lindong
*        Company:  Xi'an LongQuest Technology LTD
*      Copyright:  Copyright (c) 2009, Xi'an LongQuest Technology LTD,all rights reserved
*
*****************************************************************************/
#ifndef _WM_LOG_H_
#define _WM_LOG_H_
#include <stdio.h>
#include "liblog.h"
#include "lte_sdk.h"



#define WM_LOG_DEBUG_AT(formats, args...) \
	cprintf("[%s:%d]"formats"\n", __FUNCTION__, __LINE__, ##args);

#define WM_LOG_INFO(formats, args...) \
	cprintf("[%s:%d]"formats"\n", __FUNCTION__, __LINE__, ##args);
#endif
