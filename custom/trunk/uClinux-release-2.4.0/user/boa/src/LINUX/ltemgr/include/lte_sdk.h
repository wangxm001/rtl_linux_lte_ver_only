/******************************************************************************
*
*       Filename:  lte_sdk.h
*
*    Description:  lte modem interface
*
*        Version:  1.0
*        Created:  04/08/2010
*       Revision:  none
*       Compiler:  gcc
*
*         Author:  lindong
*        Company:  Xi'an LongQuest Technology LTD
*      Copyright:  Copyright (c) 2009, Xi'an LongQuest Technology LTD,all rights reserved
*
*****************************************************************************/
#ifndef LTE_SDK_H_
#define LTE_SDK_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

#define OK      0
#define ERROR   -1

#define cprintf(fmt, args...) do { \
    struct stat statbuff = {0}; \
    FILE* logfd = fopen("/tmp/ltelog.1", "a"); \
    if (logfd) { \
        fprintf(logfd, "[%s:%s:%d]"fmt"", __FILE__, __func__, __LINE__,##args); \
        fclose(logfd); \
    } \
    if(stat("/tmp/ltelog.1", &statbuff) == 0 && statbuff.st_size > 1024*256) \
    { \
        rename("/tmp/ltelog.1", "/tmp/ltelog.2"); \
    } \
} while (0)

#define WM_LOG_ERROR(formats, args...) \
    cprintf(formats"\n",##args);

#define WM_LOG_DEBUG(formats, args...) \
    cprintf(formats"\n", ##args);


#define WM_MSG_LTE_SDK_LINK_STATUS_NOTIFY       (0x000AF000 + 1)
#define WM_MSG_LTE_SDK_IFACE_STATUS_NOTIFY       (0x000AF000 + 2)
#define WM_MSG_LTE_SDK_MODEM_REPORT_NOTIFY      (0x000AF000 + 3)

#ifndef  IF_NAMESIZE
#define IF_NAMESIZE 16
#endif

/*
 * IPV6
 */
#define WM_IPV6_ADDR_SIZE_IN_BYTES 16
#define WM_WAN_IP_FAMILY_IPV4   0x01
#define WM_WAN_IP_FAMILY_IPV6   0x02
#define WM_WAN_IP_FAMILY_DUAL_STACK (WM_WAN_IP_FAMILY_IPV4|WM_WAN_IP_FAMILY_IPV6)

#define IP_LEN_MAX 18
#define _PATH_PROCNET_DEV               "/proc/net/dev"
#define _WAN_LTE_DEV               "4g0"
//#define _WAN_LTE_DEV               "nas0_0"



typedef unsigned char       lc_uchar;
typedef unsigned char       lc_uint8;
typedef  char               lc_char;
typedef  char               lc_int8;

typedef unsigned short      lc_uint16;
typedef unsigned long       lc_uint32;
typedef unsigned long long  lc_uint64;
typedef short               lc_int16;
typedef long                lc_int32;
typedef long long           lc_int64;
typedef unsigned long       lc_size_t;

#ifndef FALSE
typedef enum { FALSE = 0, TRUE = 1 } lc_bool;
#else
typedef enum { LC_FALSE = 0, LC_TRUE = 1 } lc_bool;
#endif



typedef enum
{
    WM_DISCONNECTED,  // 处于断开连接状态
    WM_DISCONNECTING,  //处于挡开连接进行中
    WM_CONNECTED,      // 处于连接状态
    WM_CONNECTING,   //处于连接进行中
    WM_LINK_STA_CLEAR,
    WM_LINK_STA_MAX
}wm_link_status_type;
typedef enum
{
    WM_NOT_READY,  // 该interface没有准备好
    WM_READY,       //该interface准备好了
    WM_WARNING,    // 该interface处理即将断开的状态
    WM_WARNING_CLEAR,
    WM_IFACE_STA_MAX
}wm_iface_status;

typedef enum
{
    WM_SIM_READY,
    WM_SIM_NOT_INSERT,
    WM_SIM_PIN_LOCKED,
    WM_SIM_PUK_LOCKED,
    WM_SIM_LOCKED,
    WM_SIM_NOT_READY,
    WM_SIM_BUSY,
    WM_SIM_READY_NETWORK_LOCKED,
    WM_SIM_STA_MAX
}wm_sim_status;

typedef enum
{
    WM_WCDMA_EVDO_PSRAT_NONE = 0,
    WM_WCDMA_EVDO_PSRAT_GSM,
    WM_WCDMA_EVDO_PSRAT_GPRS,
    WM_WCDMA_EVDO_PSRAT_EDGE,
    WM_WCDMA_EVDO_PSRAT_WCDMA,
    WM_WCDMA_EVDO_PSRAT_HSDPA,
    WM_WCDMA_EVDO_PSRAT_HSUPA,
    WM_WCDMA_EVDO_PSRAT_LTE,
    WM_WCDMA_EVDO_PSRAT_UMTS,
    WM_WCDMA_EVDO_PSRAT_HSPAPLUS,
    WM_WCDMA_EVDO_PSRAT_DC_HSPAPLUS,
    WM_WCDMA_EVDO_PSRAT_CDMA,
    WM_WCDMA_EVDO_PSRAT_EVDO_O,
    WM_WCDMA_EVDO_PSRAT_EVDO_A,
    WM_WCDMA_EVDO_PSRAT_EVDO_B,
    WM_TD_SCDMA,
    WM_NET_SERVICE_MAX

}wm_wcdma_evdo_net_service_type;

typedef enum
{
    WM_ROAMING_HOME,//no roaming
    WM_ROAMING_ROAMING,//roaming
    WM_ROAMING_UNKNOWN,
    WM_ROAMING_MAX,
}wm_roaming_state_type;

typedef enum
{
    WM_WCDMA_LED_NONE,
    WM_WCDMA_LED_NETWORK,
    WM_WCDMA_LED_INTERNET,
    WM_WCDMA_LED_SIM,
    WM_WCDMA_LED_MAX,

}wm_led_type_e;


typedef enum
{
    WM_WCDMA_LED_COLOR_NONE,
    WM_WCDMA_LED_COLOR_RED,
    WM_WCDMA_LED_COLOR_GREEN,
    WM_WCDMA_LED_COLOR_YELLOW,
    WM_WCDMA_LED_COLOR_MAX,

}wm_led_color_e;

typedef enum
{
    WM_WCDMA_LED_FLASH_NONE,
    WM_WCDMA_LED_ALWAYS_OFF,
    WM_WCDMA_LED_FLASH_SLOW,
    WM_WCDMA_LED_FLASH_QUICK,
    WM_WCDMA_LED_ALWAYS_FLASH,
    WM_WCDMA_LED_ALWAYS_ON,
    WM_WCDMA_LED_ALWAYS_ON_1,
    WM_WCDMA_LED_ALWAYS_ON_2,
    WM_WCDMA_LED_ALWAYS_ON_3,
    WM_WCDMA_LED_ALWAYS_ON_4,
    WM_WCDMA_LED_ALWAYS_ON_5,
    WM_WCDMA_LED_FLASH_MAX,

}wm_led_flash_e;



typedef enum
{
    WM_IP_DOWN,
    WM_IP_UP,
    WM_IPDOWN_MAX
}wm_ip_updown_type;

typedef enum
{
    WM_ETH,
    WM_WIFI,
    WM_WCDMA,
    WM_MAX ,
}wm_iface_type;

typedef enum
{
    WM_NET_SET_TYPE_AUTO,
    WM_NET_SET_TYPE_MANUAL,
    WM_NET_SET_TYPE_MAX
}wm_net_set_type_tag;

typedef enum
{
    IFACE_PRIO_LEVEL1,
    IFACE_PRIO_LEVEL2,
    IFACE_PRIO_LEVEL3,
    IFACE_PRIO_MAX
}iface_priority_tag;

typedef struct wm_ip_updown_notify_tag
{
    lc_uint32 u32updown;//wm_ip_updown_type
    lc_uint32 u32wan_type;//wm_ip_updown_type
    //~ IPV4
    lc_char wan_ifname[IF_NAMESIZE];
    lc_char wan_ip[IP_LEN_MAX];
    lc_char gateway[IP_LEN_MAX];
    lc_char dns1[IP_LEN_MAX];
    lc_char dns2[IP_LEN_MAX];
    lc_char mask[IP_LEN_MAX];
    //~ IPV6
    lc_int8 wan_ip_family;  /*WM_WAN_IP_FAMILY_IPV4/WM_WAN_IP_FAMILY_IPV6/WM_WAN_IP_FAMILY_DUAL_STACK*/
    /*v6 info*/
    lc_uchar wan_ip6addr[WM_IPV6_ADDR_SIZE_IN_BYTES];
    lc_uchar wan_ip6gwaddr[WM_IPV6_ADDR_SIZE_IN_BYTES];
    lc_uchar wan_ip6dns0[WM_IPV6_ADDR_SIZE_IN_BYTES];
    lc_uchar wan_ip6dns1[WM_IPV6_ADDR_SIZE_IN_BYTES];
    unsigned long   wan_ip6mtu;

}wm_ip_updown_notify_t;


/**
*WM_MSG_LTE_SDK_LINK_STATUS_NOTIFY notify
**/
/*链路状态上报消息内容*/
typedef struct link_status_tag
{
     wm_iface_type                 iface_id;
     wm_iface_status               iface_status;
     wm_link_status_type           link_status;
     wm_ip_updown_notify_t         ip;
}link_status_evt_t;


/**
*WM_MSG_IFACE_STATUS_NOTIF start
**/
typedef int (*active_handler)(void* msg);
/*接口上报消息内容*/
typedef struct iface_status_tag
{
     link_status_evt_t                  link;
     iface_priority_tag                 iface_pri;
     wm_net_set_type_tag                con_type;
     active_handler                     ready_func;
     active_handler                     noready_func;
     active_handler                     connected_func;
     active_handler                     unconnected_func;
     active_handler                     connect_opr;
     active_handler                     unconnect_opr;
}iface_status_evt_t;

struct user_net_device_stats {
    unsigned long long rx_packets;  /* total packets received       */
    unsigned long long tx_packets;  /* total packets transmitted    */
    unsigned long long rx_bytes;    /* total bytes received         */
    unsigned long long tx_bytes;    /* total bytes transmitted      */
    unsigned long rx_errors;    /* bad packets received         */
    unsigned long tx_errors;    /* packet transmit problems     */
    unsigned long rx_dropped;   /* no space in linux buffers    */
    unsigned long tx_dropped;   /* no space available in linux  */
    unsigned long rx_multicast; /* multicast packets received   */
    unsigned long rx_compressed;
    unsigned long tx_compressed;
    unsigned long collisions;

    /* detailed rx_errors: */
    unsigned long rx_length_errors;
    unsigned long rx_over_errors;   /* receiver ring buff overflow  */
    unsigned long rx_crc_errors;    /* recved pkt with crc error    */
    unsigned long rx_frame_errors;  /* recv'd frame alignment error */
    unsigned long rx_fifo_errors;   /* recv'r fifo overrun          */
    unsigned long rx_missed_errors; /* receiver missed packet     */
    /* detailed tx_errors */
    unsigned long tx_aborted_errors;
    unsigned long tx_carrier_errors;
    unsigned long tx_fifo_errors;
    unsigned long tx_heartbeat_errors;
    unsigned long tx_window_errors;
    unsigned long up_time;
    float         rx_max_rate;
    float         tx_max_rate;
    char          rx_max_rate_time[32];
    char          tx_max_rate_time[32];
};



typedef struct sys_status_tag
{
     wm_iface_type                      iface_id;
     wm_iface_status                    iface_sta;
     wm_link_status_type                link_sta;
     wm_sim_status                      sim_sta;
     wm_wcdma_evdo_net_service_type     radio_type;
     struct user_net_device_stats       dev_sta;
     struct user_net_device_stats       dev_peak_sta;
     unsigned char                      pin_chg_en;
     unsigned int                       peak_time;
}sys_status_tag_t;



/*struct of enable pin request*/
typedef struct enable_pin_req_tag
{

    unsigned char pin[16];

}enable_pin_req_t;

/*WM_MSG_PIN_UNLOCK_REQ */
typedef struct unlock_pin_req_tag
{
    unsigned char pin[16];

}unlock_pin_req_t;


typedef struct unlock_network_req_tag
{
    unsigned char password[16];

}unlock_network_req_t;

/*WM_MSG_PIN_UNLOCK_RSP*/
typedef struct unlock_pin_rsp_tag
{
    int err_code;
}unlock_pin_rsp_t;

/*WM_MSG_PIN_MODIFY_REQ*/
typedef struct change_pin_req_tag
{
    unsigned char old_pin[16];
    unsigned char new_pin[16];
}change_pin_req_t;

/*WM_MSG_PIN_MODIFY_RSP*/
typedef struct change_pin_rsp_tag
{
    int err_code;

}change_pin_rsp_t;


/*WM_MSG_PIN_DISABLE_REQ*/
typedef struct disable_pin_req_tag
{

    unsigned char pin[16];

}disable_pin_req_t;

/*WM_MSG_PIN_DISABLE_RSP*/
typedef struct disable_pin_rsp_tag
{
    int err_code;

}disable_pin_rsp_t;

/*WM_MSG_PUK_UNLOCK_REQ*/
typedef struct unlock_puk_req_tag
{
    unsigned char puk[16];
    unsigned char new_pin[16];

}unlock_puk_req_t;

typedef struct unlock_puk_rsp_tag
{
    int err_code;

}unlock_puk_rsp_t;


/*struct of enable pin response*/
typedef struct enable_pin_rsp_tag
{
    int err_code;
}enable_pin_rsp_t;


typedef struct callback_msg_data
{
    int         msg_id;
    const void  *data;
    int         data_len;
    int         result;
} callback_msg_data_s;

typedef lc_bool (*lte_notify_msg_handler)(callback_msg_data_s* msg);

typedef struct lct_config
{
    char         name[32];
    int          log_enable;
    lte_notify_msg_handler  msg_func;
} lct_config_s;

enum
{
    MM_AT_DATA,
    MM_AT_FILE
};
typedef struct mm_at_req_tag
{
    int req_len;
    int type;
    int timeout;
    int match_num;
    char match_string[5][16];
    char data[0];
} mm_at_req_t;

typedef struct mm_at_rsp_tag
{
    int err_code;
    int type;
    int rsp_len;
    char data[0];
} mm_at_rsp_t;



/*internal interface*/

lc_bool send_request
(
    lc_uint32 module_id,
    lc_uint32 msg_id,
    const void *data,
    size_t len
);

lc_bool send_notify
(
    lc_uint32 msg_id,
    const void *data,
    size_t len
);
lc_bool send_request_syn
(
    lc_uint32 module_id,
    lc_uint32 msg_id,
    const void *data,
    size_t len,
    lc_uint32 u32timeout,
    void *rsp_buff,
    size_t *rsp_len
);

typedef enum
{
    LCT_AUTH_NONE            = 0,
    LCT_AUTH_PAP_ONLY        = 1,
    LCT_AUTH_CHAP_ONLY       = 2,
    LCT_AUTH_PAP_CHAP_BOTH   = 3,
    LCT_AUTH_DENY            = 4
}auth_type_tag;

typedef enum
{
    OFF_THIS_FEATURE,     //do not distinguish the 4G APN from 3/2g APN, all net type use the same APN!
    WM_3G_2G,
    WM_4G
}apn_type_tag;

typedef enum
{
    WM_IPV4 = 1,
    WM_IPV6,
    WM_IPV4V6
}apn_family_tag;



typedef struct modem_apn_t
{
     char               imsicode[16];
     char               apn[128];
     char               profilename[128];
     char               username[32];
     char               password[32];
     char               telnum[16];
     auth_type_tag      auth;
     apn_type_tag       apn_type;
     apn_family_tag     ip_mode;
     char dnsmode;
     char dns1[128];
     char dns2[128];
     int  default_flag;  //1:default 0:not default
}modem_apn_tag;

typedef struct get_pref_nettype_rsp_tag
{
    int err_code;
    int pref_nettype;
}get_pref_nettype_rsp_t;

typedef struct set_pref_nettype_req_tag
{
    int pref_nettype;
}set_pref_nettype_req_t;

typedef struct set_pref_nettype_rsp_tag
{
    int err_code;
}set_pref_nettype_rsp_t;

typedef struct set_nettype_rsp_tag
{
    int err_code;
}set_nettype_rsp_t;




char *get_radio_str(wm_wcdma_evdo_net_service_type net_type);
char *get_iface_conn_str(wm_net_set_type_tag if_type);
char *get_sim_str(wm_sim_status sim_type);
char* get_link_str(wm_link_status_type status);
char* get_iface_status_str(wm_iface_status status);
char *get_iface_priority_str(iface_priority_tag pri);
int parse_apn_list(char *apn_list, modem_apn_tag *out);
int auto_match_apn(const char *fname, const char* plmn, modem_apn_tag *out);
char* get_split_str(const char *instr, const char *spstr, char *outstr, int maxlen);





/*external interface*/

int lct_module_init
(
    lct_config_s *config
);

int lct_module_fin
(
    void
);


int lct_connect
(
    void
);
int lct_disconnect
(
    void
);

int lct_peak_clear
(
    void
);

int lct_enable_pin
(
    enable_pin_req_t  *poper
);
int lct_disenable_pin
(
    unlock_pin_req_t *poper
);
int lct_modify_pin
(
    change_pin_req_t  *poper
);
int lct_modify_pin_en
(
    unsigned char  *poper
);
int lct_net_imsi_get_req
(
    char *buff,
    int len
);

int lct_net_imei_get_req
(
    char *buff,
    int len
);
int lct_sim_status_get_req
(
    char *buff,
    int len
);

int lct_ext_at_req
(
    char *atcmd,
    char *at_rst,
    int  rst_len
);

int lct_inface_status_req
(
    iface_status_evt_t *mstatus
);

int lct_net_set_pref_req
(
    set_pref_nettype_req_t *pref
);

void lte_set_led_status
(
    wm_led_type_e type,
    wm_led_color_e color,
    wm_led_flash_e flash
);

int lct_apn_set_req
(
    void
);

int lct_readlist_proc
(
    char *target,
    struct user_net_device_stats *status
);

int lct_net_set_status_test_req
(
    int *pref
);

int get_current_time
(
    struct tm *cur_time
);

#endif
