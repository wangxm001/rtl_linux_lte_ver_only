#ifndef __COS_NET_H
#define __COS_NET_H

#include <ctype.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <syslog.h>

#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/sysinfo.h>
#include <arpa/inet.h>
#include <signal.h>
#include <net/route.h>
#include <linux/if_ether.h>
#include <linux/sockios.h>
#include <linux/types.h>

#include <net/if_arp.h>
#include <net/if.h>

#include <sys/wait.h>

#include "lc_common.h"
#include "lc_file.h"
#include "lc_string.h"


#include <sys/socket.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdarg.h>
#include <dirent.h>
#include <time.h>
#include <sys/mman.h>
#include <asm/unistd.h>
#include <sys/syscall.h>

#define lc_sscanf sscanf

#if 0
typedef __kernel_fd_set        fd_set;

#define FD_SETSIZE        __FD_SETSIZE
#define FD_SET(fd,fdsetp)    __FD_SET(fd,fdsetp)
#define FD_CLR(fd,fdsetp)    __FD_CLR(fd,fdsetp)
#define FD_ISSET(fd,fdsetp)    __FD_ISSET(fd,fdsetp)
#define FD_ZERO(fdsetp)        __FD_ZERO(fdsetp)
#endif


lc_int32 lc_brctl_add_br (const lc_char *br_name);
lc_int32 lc_brctl_add_if (const lc_char *br_name,const lc_char *if_name);
lc_int32 lc_brctl_del_br (const lc_char *br_name);
lc_int32 lc_brctl_del_if (const lc_char *br_name,const lc_char *if_name);
lc_int32 lc_ifconfig(const lc_char *name, lc_int32 flags, const lc_char *addr, const lc_char *netmask);

lc_int32 lc_route_add ( lc_char *name, lc_int32 metric, lc_char *dst, lc_char *gateway, lc_char *genmask);
lc_int32 lc_route_add_default (lc_char * name,lc_char *gateway );
lc_int32 lc_route_del ( lc_char *name, lc_int32 metric, lc_char *dst, lc_char *gateway, lc_char *genmask);
lc_int32 lc_route_del_default (lc_char * name,lc_char *gateway );

/*arp part interface api*/
lc_int32 lc_is_interface_exit(char *ifname);
lc_int32 lc_is_interface_up(char *ifname);
lc_int32 lc_get_ip_by_ifname(char *ifname, struct in_addr *ip);
lc_int32 lc_get_mac_by_ifname(char *ifname, char *mac);
lc_int32 lc_arp_update2(char *ifname);
lc_int32 lc_arp_update(lc_char *p_dest_ip,lc_char *p_sor_ip,lc_char *if_name ,lc_char *p_sor_mac);
lc_int32 lc_arp_check (lc_uint32 yiaddr, lc_uint32 ip, lc_uchar *mac, 
                                        lc_uchar *mac_input,lc_char *interface, lc_int32 timeout);
lc_int32 lc_get_climac_by_ip (lc_char *ip, lc_char *mac);

lc_int32 lc_socket(lc_int32 domain, lc_int32 type, lc_int32 protocol);
lc_int32 lc_bind(lc_int32 sockfd, const struct sockaddr *addr,  socklen_t addrlen);
lc_int32 lc_listen(lc_int32 sockfd, lc_int32 backlog);
lc_int32 lc_accept(lc_int32 sockfd, struct sockaddr *addr, socklen_t *addrlen);
lc_int32 lc_connect(lc_int32 sockfd, const struct sockaddr *addr, socklen_t addrlen);


lc_int16 lc_htons(lc_int16 hostshort);
lc_int32 lc_htonl(lc_int32 hostlong);
lc_int16 lc_ntohs(lc_int16 netshort);
lc_int32 lc_ntohl(lc_int32 netlong);

in_addr_t lc_inet_addr(const lc_char *cp);
lc_int32 lc_inet_aton(const lc_char *cp, struct in_addr *inp);
lc_char *lc_inet_ntoa(struct in_addr in);

#endif

