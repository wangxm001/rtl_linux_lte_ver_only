/******************************************************************************
 *
 *       Filename:  lc_mem.h
 *        
 *    Description:  内存管理模块头文件
 *                  声明了初始化、终结化、内存申请、内存释放以及内存诊断接口。
 *                  内存管理将以128字节为依据，将128字节以上的内存块和不大于128
 *                  字节的内存块分别管理。前者称为大块内存，后者称为小块内存。
 *
 *        Version:  1.0
 *        Created:  05/21/2010 04:21:04 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  徐扬   Xu Yang (Xuy), 
 *        Company:  Xi'an LongQuest Technology LTD
 *      Copyright:  Copyright (c) 2010, Xu Yang, all rigths reserved.
 *
 *****************************************************************************/

#ifndef LC_MEM_H
#define LC_MEM_H

#include <linux/unistd.h>
#include <sys/syscall.h>
#include <unistd.h>

#include "lc_common.h"


// 内存管理模块返回值定义
enum lc_sdk_mem_ret_value
{
    MEM_OK = 0,       // 成功
    MEM_INVALID_MEM,  // 无效内存区域。此时指定内存区域可能不会真正得到释放。
    MEM_DOUBLE_FREE,  // 多次释放同一内存区域
};

// 内存块追踪头部
// 此结构将附加于大块内存和开启了追踪功能的小块内存的头部
typedef struct lc_sdk_mem_trace_head
{
    lc_uint32 thread_id;                 // 线程ID
    lc_uint32 src_file_no;               // 源文件编号
    lc_uint16 code_line;                 // 代码行号
    struct lc_sdk_mem_trace_head *prev;  // 前向指针
    struct lc_sdk_mem_trace_head *next;  // 后向指针
    lc_uint32 size;                      // 内存大小，不包含头部
    lc_uint16 check_flag;                // 校验标志， 55 AA
    lc_uint16 block_type;                // 内存块类型
                                         // 大块内存 0x00， 小块内存 0x01
} mem_tr_head_t;

// 小块内存的管理头部
typedef struct lc_sdk_mem_sblock_head
{
    lc_uint32 size;        // 内存大小，不包含头部
    lc_uint16 check_flag;  // 校验标志， 55 AA
    lc_uint16 block_type;  // 内存块类型
                           // 大块内存 0x00， 小块内存 0x01
} mem_sblock_head_t;

// 小块内存空闲链节点
typedef union lc_sdk_mem_free_block
{
    union lc_sdk_mem_free_block *next;  // 后向指针
    lc_uint8 data[1];                   // 用户内存空间
} mem_free_block_t;


// 回调函数标记宏
#define LC_MEM_CALLBACK

// 内存不足（Out Of Memory）处理函数原型
// 用户可以自己定义oom函数来释放一定数量的内存
// 内存申请函数将在申请失败后调用oom函数，然后再尝试一次申请
typedef LC_MEM_CALLBACK void (*oom_handler)(void);


/******************************************************************************
 *         Name:  mem_init
 *  Description:  内存管理模块初始化。
 *        Input:  
 *                oom:  回调函数指针。用户可以传入自定义的函数来处理内存
 *                      分配失败。可以为NULL。此函数无参数和返回值。
 *       Output:
 *                无
 *       Return:  无  
 *        Notes:  无  
 *****************************************************************************/
void 
mem_init(LC_MEM_CALLBACK void (*oom_handler)(void));

/******************************************************************************
 *         Name:  mem_fin
 *  Description:  内存管理模块终结化。这将释放所有还未释放的内存块以及所有的
 *                空闲块。
 *        Input:  
 *                无 
 *       Output:
 *                无
 *       Return:  无  
 *        Notes:  无
 *****************************************************************************/
void 
mem_fin(void);

/******************************************************************************
 *         Name:  mem_malloc_ex
 *  Description:  内存分配函数。需要传入调用者的信息以进行追踪记录。
 *        Input:  
 *                thread_id: 调用者线程ID
 *                line_num:  源文件代码行号
 *                file_no:   源文件文件编号
 *                n:         需要分配的字节数
 *       Output:
 *                无
 *       Return:  已分配内存的起始地址指针。分配失败时为NULL。
 *        Notes:  无  
 *****************************************************************************/
void* 
mem_malloc_ex(lc_uint16 thread_id, 
              lc_uint16 line_num, 
              lc_uint32 file_no, 
              size_t n);

/******************************************************************************
 *         Name:  mem_malloc_ex
 *  Description:  内存分配函数封装宏
 *        Input:  
 *                n:  需要分配的字节数
 *       Output:
 *                无
 *       Return:  已分配内存的起始地址指针。分配失败时为NULL。
 *        Notes:  无  
 *****************************************************************************/
#define mem_malloc(n) \
mem_malloc_ex(syscall(SYS_gettid), __LINE__, MEM_SRC_FILE_NO, (n))

/******************************************************************************
 *         Name:  mem_malloc_ex
 *  Description:  内存释放函数。需要传入调用者的信息在发生错误时进行记录。
 *        Input:  
 *                thread_id: 调用者线程ID
 *                line_num:  源文件代码行号
 *                file_no:   源文件文件编号
 *                p:         需要释放的指针。可以为NULL。
 *       Output:
 *                无
 *       Return:  
 *                MEM_OK           成功
 *                MEM_INVALID_MEM  无效内存区域。此时指定内存区域可能不会真正
 *                                 得到释放。
 *                MEM_DOUBLE_FREE  多次释放同一内存区域
 *        Notes:  无  
 *****************************************************************************/
lc_int32 
mem_free_ex(lc_uint16 thread_id, 
            lc_uint16 line_num, 
            lc_uint32 file_no, 
            void *p);

/******************************************************************************
 *         Name:  mem_free_ex
 *  Description:  内存释放函数封装宏
 *        Input:  
 *                p:  需要释放的指针。可以为NULL。
 *       Output:
 *                无
 *       Return:  
 *                MEM_OK           成功
 *                MEM_INVALID_MEM  无效内存区域。此时指定内存区域可能不会真正
 *                                 得到释放。
 *                MEM_DOUBLE_FREE  多次释放同一内存区域
 *        Notes:  无  
 *****************************************************************************/
#define mem_free(p) \
mem_free_ex(syscall(SYS_gettid), __LINE__, MEM_SRC_FILE_NO, (p))

/******************************************************************************
 *         Name:  mem_get_big_block_size
 *  Description:  诊断接口。
 *                返回使用中的大块内存的总量。
 *        Input:  
 *                无
 *       Output:
 *                无
 *       Return:  
 *                大块内存使用总量
 *        Notes:  无  
 *****************************************************************************/
size_t 
mem_get_big_block_size(void);

/******************************************************************************
 *         Name:  mem_get_pool_size
 *  Description:  诊断接口。
 *                返回使用中的小块内存池的大小。包括所有空闲块和使用块。
 *        Input:  
 *                无
 *       Output:
 *                无
 *       Return:  
 *                小块内存池大小
 *        Notes:  无  
 *****************************************************************************/
size_t 
mem_get_pool_size(void);

/******************************************************************************
 *         Name:  mem_tr_handler
 *  Description:  追踪头部处理函数原型。
 *        Input:  
 *                head:  内存块追踪头部信息
 *                arg:   用户定义参数
 *       Output:
 *                无
 *       Return:  
 *                无
 *        Notes:  用户定义的回调函数
 *****************************************************************************/
typedef LC_MEM_CALLBACK void 
(*mem_tr_handler)(const mem_tr_head_t *head, void *arg);

/******************************************************************************
 *         Name:  mem_dump_trace_info
 *  Description:  诊断接口。
 *                Dump出所有使用中内存块的追踪头部。具体的dump方式由用户模块
 *                指定。
 *                本函数会遍历所有使用中的具有追踪头部的内存块，并对每条追踪
 *                头部记录都调用一次用户指定的回调函数来处理。
 *        Input:  
 *                handler:  回调函数。用户指定用来处理每一条追踪头部记录。
 *                arg:      用户指定输入参数
 *       Output:
 *                无
 *       Return:  
 *                无
 *        Notes:  无  
 *****************************************************************************/
void 
mem_dump_trace_info(mem_tr_handler handler, void *arg);

/******************************************************************************
 *         Name:  mem_is_valid
 *  Description:  诊断接口。
 *                判断给定地址内存是否有效。
 *        Input:  
 *                p:  待判定的内存地址指针
 *       Output:
 *                无
 *       Return:  
 *                TRUE:        有效
 *                FALSE:       无效
 *        Notes:  无  
 *****************************************************************************/
lc_bool 
mem_is_valid(void *p);

/******************************************************************************
 *         Name:  mem_get_tr_head
 *  Description:  诊断接口。
 *                获取给定地址内存的追踪头部。
 *        Input:  
 *                p:  内存地址指针。为NULL时返回NULL。
 *       Output:
 *                无
 *       Return:  
 *                NULL:    失败。原因可能是给出了空指针，或者给出的地址指向
 *                         了没有开启追踪功能的小块内存
 *                其他值:  指向给定内存的追踪头部
 *        Notes:  无  
 *****************************************************************************/
mem_tr_head_t * 
mem_get_tr_head(void *p);

#endif
