/***************************************************************************************
 *
 *
 *       Filename:  wm_extern.h
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  2010年07月31日
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  lindong
 *      Copyright:  Copyright (c) 2009, Xi'an LongQuest Technology LTD,all rights reserved
 *
 ***************************************************************************************/
#ifndef _WM_EXTERN_H_
#define _WM_EXTERN_H_

#include "lc_common.h"
#include "ipc_msg_id_define.h"
#include "lte_sdk.h"

/*************************************error code start****************************/
//成功
#define WM_SUCCESS                   0
//内部错误
#define WM_ERR_INTERNAL             -1
//不支持的操作
#define WM_ERR_NOT_SUPPORTED        -2
//无效参数
#define WM_ERR_INVALID_PARAM        -3
//无效操作
#define WM_ERR_INVALID_OPERATION    -4
//申请内存错
#define WM_ERR_MEMORY_ALLOC         -5
//创建线程错
#define WM_ERR_THREAD_CREATE        -6
/*************************************end****************************/
#define _WM_WCDMA_FEATURE

#define WM_PWR_BUTTON_NAME           "BUTTON_POWER"
#define WM_SWITCH_BUTTON_NAME        "BUTTON_NETWORK"

/**************************tmp nv define****************************************************/
#define WM_CONNECTION_STATUS_TMP "wm_connection_status_tmp"
#define DISCONNECTED "disconnected"
#define CONNECTED "connected"
#define CONNECTING "connecting"
#define DISCONNECTING "disconnecting"
#define WM_WCDMA_CONNECTION_ACTIVE_TMP "wm_wcdma_connection_active_tmp"

#define WM_WAN_MODE_TMP "wm_wan_mode_tmp"
#define WM_WAN_IPADDRESS_TMP "wm_wan_ipaddress_tmp"
#define WM_WAN_GATEWAY_TMP "wm_wan_gateway_tmp"
#define WM_WAN_NETMASK_TMP "wm_wan_netmask_tmp"
#define WM_WAN_DNS_TMP "wm_wan_dns_tmp"

#define WM_ETH_STATUS   "wm_eth_status_tmp"
#define WM_WIFI_STATUS  "wm_wifi_status_tmp"
#define WM_WCDMA_STATUS "wm_wcdma_status_tmp"


/**************************tmp nv end  ****************************************************/

enum wm_iface_rule_type
{
	WM_AUTO,  
	WM_WIFI_ONLY, 
	WM_WCDMA_ONLY,    
	WM_ETH_ONLY,
    WM_RULE_MAX   
};

typedef struct wm_iface_prio_type
{
    lc_uint32 rule_mode;
    char prio[128];
}wm_iface_prio_t;





/***********WM_MSG_NET_STATUS_RSP WM_MSG_NET_STATUS_NOTIFY start*********/

typedef struct
{
    lc_uint32 u32iface_id;
    lc_uint32 u32link_state;
    lc_uint32 u32connect_errno;
}wm_status_t;
/***************************end*****************/



/***************************WM_MSG_STATUS_GET_RSP start*****************/
/*
typedef struct wm_status_tag
{
    lc_int32 i32status;//与系统定义一致
}wm_status_t;
*/
/***************************end*****************/



/********WM_MSG_AUTOCONNECT_GET_RSP WM_MSG_AUTOCONNECT_SET_REQ start*******/
enum wm_connect_mode
{
	WM_CONNET_AUTO,  
	WM_CONNET_MANUAL, 
};

typedef struct wm_auto_connect_tag
{
     lc_uint32 u32auto_connect;//wm_auto_connect_type
} wm_auto_connect_t;
/***************************end*****************/

/******WM_MSG_DIAL_TYPE_GET_RSP WM_MSG_DIAL_TYPE_SET_REQ start*******/
enum wm_dial_type
{
	WM_KEEP_ALIVE,  
	WM_DIAL_DEMAND 
};
typedef struct wm_dial_type_tag
{
    lc_uint32 u32dial_type;//wm_dail_type
    lc_uint32 u32time; // min
}wm_dial_type_t;
/***************************end*****************/

/***********WM_MSG_SWITCH_TIME_SET_REQ WM_MSG_SWITCH_TIME_GET_RSP start*********/

typedef struct switch_time_tag
{
    lc_uint32 u32time;
}switch_time_t;
/***************************end*****************/

/* wm general response
   WM_MSG_CONNECT_RSP,
   WM_MSG_DISCONNECT_RSP,
   WM_MSG_NET_MODE_GET_RSP,
   WM_MSG_NET_MODE_SET_RSP,
   WM_MSG_DIAL_TYPE_SET_RSP,
   WM_MSG_AUTOCONNECT_SET_RSP
*****************/

typedef struct wm_result_tag
{
    lc_int32 i32result;//error code
}wm_result_t;
/***************************end*****************/



/***************************WM_MSG_IP_UPDOWN_NOTIFY start*****************/

enum wm_ip_updown_wan_type
{
    wm_ip_updown_wan_pppd,   //PPPD 拨号上网
	wm_ip_updown_wan_qmi,    // qmi 上网
	wm_ip_updown_wan_dhcp,    // dhcp 方式上网
	wm_ip_updown_wan_static_ip,    // dhcp 方式上网
};

typedef struct wm_connectdisconnect_rsp
{
	lc_uint32 result;  //结构， 0， 失败， 1， 成功
	wm_ip_updown_notify_t ip_updown_notify;

} wm_connectdisconnect_rsp_t;


/***************************end*****************/






#endif


