#include <sys/time.h>
#include <time.h>
#include <pthread.h>

pthread_mutex_t m_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  m_cond = PTHREAD_COND_INITIALIZER;


void 
mkcondtimeout(struct timespec *tsp, int msecs)
{
    struct timeval now;
    
    // 获取当前时间
    gettimeofday(&now, NULL);
    tsp->tv_sec = now.tv_sec;
    tsp->tv_nsec = now.tv_usec * 1000;
    // 加秒数增量
    tsp->tv_sec += msecs / 1000;
    // 加不足1秒的毫秒数增量
    tsp->tv_nsec += (msecs % 1000) *1000 * 1000;
    
    return;
}


void timedwaitEvent(int millisec)
{
    //2-set time;
    struct timespec abstime;
    
    pthread_mutex_lock( &(m_lock));
    
    mkcondtimeout(&abstime, millisec);
    printf("tv_sec:%d\n", abstime.tv_sec);
    
    pthread_cond_timedwait(&m_cond, &(m_lock),&abstime);
    pthread_mutex_unlock( &(m_lock));
}


int main()
{
    while (1)
    {
        timedwaitEvent(2000);
        sleep(5);
    }
    return 0;
}
