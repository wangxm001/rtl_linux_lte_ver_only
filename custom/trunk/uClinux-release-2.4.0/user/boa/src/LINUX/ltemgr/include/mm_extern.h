/***************************************************************************************
 *
 *
 *       Filename:  wm_extern.h
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  2010年07月31日
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  lindong
 *      Copyright:  Copyright (c) 2009, Xi'an LongQuest Technology LTD,all rights reserved
 *
 ***************************************************************************************/
#ifndef _MM_EXTERN_H_
#define _MM_EXTERN_H_

#include "lc_common.h"
#include "ipc_msg_id_define.h"

/*************************************apn start****************************/
#define WEB_APN_LIST_MAX                3072
#define APN_ITEM_MAX                    128
#define APN_ITEM_MAX_NUM                20
/*************************************end****************************/


typedef struct 
{
	lc_uint32         nCode;
	const char        *nVar;
    const char        *apn;
    const char        *username;
    const char        *password;
	lc_uint32         pap_type;
    
    
} plmn_apn_list;

/* 
 * information buffer which is got from QMI_NAS_GET_OPERATOR_NAME_DATA_RESP_MSG_V01 
 * qmi msg for NITZ.
 *
 * */

typedef struct
{
  lc_uint16 year;
  /**<   Year.*/

  lc_uint8 month;
  /**<   Month.*/

  lc_uint8 day;
  /**<   Day.*/

  lc_uint8 hour;
  /**<   Hour.*/

  lc_uint8 minute;
  /**<   Minute.*/

  lc_uint8 second;
  /**<   Second.*/

  lc_uint8 time_zone;

}nitz_info;



typedef struct
{
    lc_uint32 tx_rate;
    lc_uint32 rx_rate;

}modem_net_rate_type;


typedef enum
{
    WM_REG_NONE,
    WM_REG_2G,
    WM_REG_3G,
    WM_REG_4G,
}now_reg_net_type;

typedef enum
{
    NETWORK_LOCKED,
    NETWORK_UNLOCKED,
    NETWORK_UNLOCKED_3,
    UNKNOWN_LOCK_STATUS,
}network_lock_status_type;


enum wm_wcdma_evdo_sub_mode_type
{
    WM_WCDMA_EVDO_MODE_AUTO, //auto
    WM_WCDMA_EVDO_MODE_2G,
    WM_WCDMA_EVDO_MODE_3G,
    WM_WCDMA_EVDO_MODE_4G,//LTE only
    WM_WCDMA_EVDO_MODE_3G_4G,//4g and 3g only
    WM_WCDMA_EVDO_MODE_2G_3G,//2g and 3g only
    WM_WCDMA_EVDO_MODE_234g_4g_pref,  //234g with 4G pref
    WM_WCDMA_EVDO_MODE_234g_32g_pref,  //234g with 32G pref
#ifdef _LTE_ONLY
    WM_WCDMA_EVDO_MODE_ERROR,
#endif

};

/***************************end*****************/

/********WM_MSG_SIGNAL_STRENGTH_RSP WM_MSG_SIGNAL_STRENGTH_NOTIFY start*******/
enum wm_signal_strength_type
{
    WM_SIGNAL_0 = 0,//0格信号
    WM_SIGNAL_1,// 1格信号
    WM_SIGNAL_2,
    WM_SIGNAL_3,
    WM_SIGNAL_4,
    WM_SIGNAL_5,
};
/***************************end*****************/
typedef struct wm_sim_status_get_rsp_tag
{
    lc_uint32 u32sim_status;//wm_sim_status
}wm_sim_status_get_rsp_t;
/***************************end*****************/


enum
{
    MM_SUCCESS,
    MM_FAILED
};

#ifdef _WEBUI_GiD1_FEATURE
    #define WM_WCDMA_GID1_NV               "wm_wcdma_gid1_value"
#endif 

#define WM_WCDMA_SIM_STATUS                "wm_wcdma_modem_status_tmp"//sim state pin state
#define WM_WCDMA_SERVICE_TYPE_NV           "wm_wcdma_service_type_tmp"  //gsm gprs wcdma lte
#define WM_WCDMA_ROAMING_STATUS_NV         "wm_wcdma_roaming_status_tmp"//roaming state
#define WM_WCDMA_PIN_STATUS_NV             "wm_lte_pin_status"
#define WM_WCDMA_SIGNAL_LEVEL_NV           "wm_wcdma_rssi_tmp"
#define WM_WCDMA_RSSI_VALUE_NV           "wm_wcdma_rssi_value_tmp"

#define WM_WCDMA_SINR0_VALUE_NV           "wm_wcdma_sinr0_value_tmp"
#define WM_WCDMA_SINR1_VALUE_NV           "wm_wcdma_sinr1_value_tmp"

#define WM_WCDMA_CINR0_VALUE_NV           "wm_wcdma_cinr0_value_tmp"
#define WM_WCDMA_CINR1_VALUE_NV           "wm_wcdma_cinr1_value_tmp"

#define WM_WCDMA_RSRP_VALUE_NV           "wm_wcdma_rsrp_value_tmp"

#define WM_WCDMA_RSRP0_VALUE_NV           "wm_wcdma_rsrp0_value_tmp"
#define WM_WCDMA_RSRP1_VALUE_NV           "wm_wcdma_rsrp1_value_tmp"

#define WM_WCDMA_RSRQ_VALUE_NV           "wm_wcdma_rsrq_value_tmp"

#define WM_WCDMA_BNDWIDTH_VALUE_NV           "wm_wcdma_bndwidth_value_tmp"
#define WM_WCDMA_BANDS_VALUE_NV           "wm_wcdma_bands_value_tmp"


#define WM_WCDMA_PCI_VALUE_NV           "wm_wcdma_pci_value_tmp"

#define WM_WCDMA_ULFQCY_VALUE_NV           "wm_wcdma_ulfqcy_value_tmp"

#define WM_WCDMA_DLFQCY_VALUE_NV           "wm_wcdma_dlfqcy_value_tmp"

#define WM_WCDMA_CELLID_VALUE_NV           "wm_wcdma_cellid_value_tmp"

#define WM_WCDMA_TXPUSCH_VALUE_NV           "wm_wcdma_txpusch_value_tmp"
#define WM_WCDMA_TXPUCCH_VALUE_NV           "wm_wcdma_txpucch_value_tmp"
#define WM_WCDMA_TXPRACH_VALUE_NV           "wm_wcdma_txprach_value_tmp"
#define WM_WCDMA_TXSRS_VALUE_NV           	"wm_wcdma_txsrs_value_tmp"

#define WM_WCDMA_CURR_PLMN_NV              "wm_wcdma_plmn"
#define WM_WCDMA_APN_LIST_NV               "web_apn_list"
#define WM_WCDMA_APN_LIST_IPV6_NV           "web_apn_list_ipv6"
#define WM_WCDMA_REMAIN_PUK_NV             "wm_wcdma_remain_puk"
#define WM_WCDMA_REMAIN_PIN_NV             "wm_wcdma_remain_pin"
#define WM_ROAMING_ENABLE_NV               "wm_wcdma_roaming_config"//roaming on/off
#define WM_WCDMA_IMEI_NV                   "wm_wcdma_imei_nv"
#define WM_WCDMA_SN_NV                      "wm_wcdma_sn_nv"
#define WM_WCDMA_IMSI_NV                    "wm_wcdma_imsi_nv"
#define WM_WCDMA_MSISDN_NV                  "wm_wcdma_msisdn_nv"
#define WM_NETSET_RESULT_NV                 "wm_netset_result_tmp"
#define WM_WCDMA_CONNECTION_ACTIVE_TMP      "wm_wcdma_connection_active_tmp"
#define WM_WCDMA_MODEMVERSION_TMP           "wm_wcdma_modem_version_tmp"
#define WM_WCDMA_VOLUME_VALUE_TMP           "wm_wcdma_volume_value_tmp"
#define WM_WCDMA_VOLUME_VALUE_TMP_BAK           "wm_wcdma_volume_value_tmp_bak"
#define SMS_DEV_NEW_MSG_POPUP_NV    "sms_dev_new_msg_popup"
#define SMS_SIM_NEW_MSG_POPUP_NV    "sms_sim_new_msg_popup"
#define SMS_VOICE_MAIL_NOTIFY       "sms_voicemail_notify"
#define WM_NETWORK_LOCK_ERRNUM      "wm_network_lock_errnum"
#define WM_GCT_CELL_ID_TMP_NV  "wm_gct_cell_id"
#define WM_GCT_LTE_BAND_TMP_NV  "wm_gct_lte_band"
#define WM_GCT_LTE_CHANNEL_TMP_NV  "wm_gct_lte_channel"


#ifdef _WM_CELL_LOCK_FEATURE

#define WM_CELL_LOCK_NV                   "wm_cell_lock_tmp"
#define WM_CURRENT_ONLINE_TIME_NV         "wm_current_online_time_tmp"
#define WM_TOTAL_ONLINE_TIME_NV         "wm_total_online_time_tmp"
#define WM_TOTAL_CALL_TIME_NV         "wm_total_call_time_tmp"
#define WM_CURRENT_ONLINE_CELL_LOCK_MAX    1800
#define WM_TOTAL_ONLINE_CELL_LOCK_MAX      3600
#define WM_TOTAL_CALL_CELL_LOCK_MAX      3

#endif

#ifdef _DEFINE_PRIORITY_TDD_REGISTER
#define WM_E_UTRAN_NV                    "wm_e_utran_cell_identifier"
#define WM_LTE_FDD_TDD_NV                "wm_tdd_fdd_flag_nv"
#endif



typedef enum
{
	WM_WCDMA_MODEM_MODE_START,
	WM_WCDMA_MODEM_MODE_STOP,
	WM_WCDMA_MODEM_MODE_SLEEP,
	WM_WCDMA_MODEM_MODE_UPGRADE,
	WM_WCDMA_MODEM_MODE_INVALIDE,

}wm_modem_mode_state_e;


typedef enum
{
	WCDMA_SERVICE_NOREGISTERED,
    WCDMA_SERVICE_REGISTERED,
	WCDMA_SERVICE_SERCHING,
    WCDMA_SERVICE_REGISTRATION_DENIED,
	WCDMA_SERVICE_REGISTERED_UNKNOWN,


}wm_wcdma_service_state_e;



/*WM_MSG_ROAMING_GET_RSP*/
typedef struct get_roamcntl_rsp_tag
{
	int err_code;
	int value;

}get_roamcntl_rsp_t;

/*WM_MSG_ROAMING_SET_REQ*/
typedef struct set_roamcntl_rep_tag
{
	int value;
}set_roamcntl_req_t;

typedef struct set_roamcntl_rsp_tag
{

	int err_code;

}set_roamcntl_rsp_t;

/*WM_MSG_DUALMODE_GET_RSP*/
typedef struct get_nettype_rsp_tag
{
	int err_code;
	int nettype;
}get_nettype_rsp_t;

/*WM_MSG_DUALMODE_SET_REQ*/
typedef struct set_nettype_req_tag
{
	int nettype;
}set_nettype_req_t;


/*WM_MSG_NET_SET_REQ*/
typedef struct set_selmod_req_tag
{
	int mode;
	unsigned short int mnc;
	unsigned short int mcc;
	unsigned  int rat;
    unsigned  int rrat;

}set_selmod_req_t;

/*WM_MSG_NET_GET_RSP*/
typedef struct get_selmod_rsp_tag
{
    int err_code;
	int mode;
}get_selmod_rsp_t;

typedef struct plmn_list_info_tag
{
	unsigned char identity[16];   //save at cmd return value's <plmn(6 bit)> in "identity", if plmn is 5 bit,then use char 'F' to end.
	unsigned short int mnc;   //use at cmd return value's <plmn> to caculate mnc and save in "mnc".
	unsigned short int mcc;   //use at cmd return value's <plmn> to caculate mmc and save in "mcc".
	int rat;     //use at cmd return value's <act> to decide the net mode and save in "rat".
    int rrat;     //save at cmd return value's <act> int "rrat".
	int forbidden;    //use at cmd return value's <stat> to decide the "forbidden".
	int current;   //use at cmd return value's <stat> to decide the "current".
	int available;  //use at cmd return value's <stat> to decide the "available".
}plmn_list_info_t;
#define WM_NET_SEARCH_LIST_MAX 10
/*WM_MSG_NET_SEARCH_REQ*/
typedef struct get_netlist_rsp_tag
{
	int err_code;
	int num;
	plmn_list_info_t info[WM_NET_SEARCH_LIST_MAX];
}get_netlist_rsp_t;






typedef struct mm_sms_send_tag
{
    int sms_len;
    char data[0];
} mm_sms_send_t;
typedef struct mm_sms_save_tag
{
    int sms_len;
    int flag;
    char data[0];
} mm_sms_save_t;
enum
{
	WM_MANUL_APN,
	WM_AUTO_APN
};
#endif

