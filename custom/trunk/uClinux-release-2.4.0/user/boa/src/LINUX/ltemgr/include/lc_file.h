/*****************************************************************
 *
 *
 *     Filename:  lc_file.h
 *
 *    Description:  include the interface api to operate file and used by application which operate file
 *
 *        Version:  1.0
 *        Created:  
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  wangdaishun
 *      Copyright:  Xi'an LongQuest Technology LTD,all rights reserved
 *
 *******************************************************************/

#ifndef __LC_FILE_H
#define __LC_FILE_H

/*****************************************************************
 *
 *
 *             include file 
 *
 *
 ****************************************************************/
#include <ctype.h>
#include <stdio.h>
#include <errno.h>
#include <syslog.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <unistd.h>

#include "lc_common.h"
#include "lc_string.h"

/*****************************************************************
 *
 *
 *             define parameters
 *
 *
 ****************************************************************/
#define INPUT_PARAM_ERROR -2

#define TARGET_EXIST 0
#define TARGET_NOT_EXIST -1

#define COS_OPERATE_SUCCESS 0
#define COS_OPERATE_FAIL -1

#define SIZE_BOUND_VAL 0
#define FILE_LINE_SIZE 256



/*****************************************************************
 *
 *
 *             file interface api  declare
 *
 *
 ****************************************************************/

/*fopen function*/
FILE *lc_fopen(const lc_char *path , const lc_char *mode);

/*fclose function*/
lc_int32 lc_fclose(FILE *stream);

/*fputc function*/
lc_int32 lc_fputc(lc_int32 ch , FILE *stream);

/*fgetc function*/
lc_int32 lc_fgetc(FILE *stream);

/*fputs function*/
lc_int32 lc_fputs(const lc_char *str, FILE *stream);

/*fgets function*/
lc_char *lc_fgets(lc_char *str, lc_int32 size , FILE *stream);

/*fread function*/
lc_int32 lc_fread(void *ptr, lc_int32 size , lc_int32 nmemb , FILE *stream);

/*fwrite function*/
lc_int32 lc_fwrite(const void *ptr, lc_int32 size , lc_int32 nmemb , FILE *stream);

/*fprintf function*/
lc_int32 lc_fprintf (FILE *stream, const lc_char *format, ...);

/*access function*/
lc_int32 lc_access(const lc_char *pathname, lc_int32 mode);

/*fseek function*/
lc_int32 lc_fseek (FILE *stream, long offset, lc_int32 whence);

/*ftell function*/
long lc_ftell (FILE *stream);

/*rewind function*/
void lc_rewind (FILE *stream);

/*check the file is exist or not*/
lc_int32 lc_file_exist (const lc_char *path);

/*check the dir is exist or not*/
lc_int32 lc_dir_exist (const lc_char *path);

/*read any line of the file*/
lc_int32 lc_strread_fileline (const lc_char *path , lc_int32 line , lc_char *buffer , lc_int32 *buf_size);

#endif


