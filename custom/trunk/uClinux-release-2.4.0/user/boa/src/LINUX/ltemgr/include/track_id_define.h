/***************************************************************************************
 *
 *
 *       Filename:  track_id_define.h
 *
 *    Description:  define global exception id
 *
 *        Version:  1.0
 *        Created:  05/24/2010 10:30:54 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  zhangzhitong
 *      Copyright:  Copyright (c) 2010, Xi'an LongQuest Technology LTD,all rights reserved
 *
 ***************************************************************************************/

#ifndef TRACK_ID_DEFINE_H
#define TRACK_ID_DEFINE_H

#define TRACK_ID_BASE 1

//id for SDK
#define TRACK_SDK_ID_START TRACK_ID_BASE
#define TRACK_SDK_ID_END (TRACK_SDK_ID_START + 1000)

//id for all apps
#define TRACK_APP_ID_START (TRACK_SDK_ID_END + 1)

#define TRACK_SC_ID_MODE_CHG_ERROR     (TRACK_APP_ID_START+1)
#define TRACK_SC_ID_MODE_CHG_TIMEOUT   (TRACK_APP_ID_START+2)
#define TRACK_SC_ID_INIT_ERROR         (TRACK_APP_ID_START+3)

#define TRACK_APP_ID_END (TRACK_APP_ID_START + 0x7FFFFFFF)

#endif
