/******************************************************************************
 *
 *       Filename:  lc_ipc.h
 *        
 *    Description:  IPC模块头文件
 *                  IPC模块分为两个子模块：进程间消息处理子模块和线程间消息队列
 *                  子模块。  
 *                  这里分别声明了两个子模块的初始化、终结化、发送、接收和诊断
 *                  等接口。
 *
 *        Version:  1.0
 *        Created:  05/24/2010 09:50:25 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  徐扬   Xu Yang (Xuy), 
 *        Company:  Xi'an LongQuest Technology LTD
 *      Copyright:  Copyright (c) 2010, Xu Yang, all rigths reserved.
 *
 *****************************************************************************/

#ifndef LC_IPC_H
#define LC_IPC_H

#include <sys/un.h>

#include "lc_common.h"
#include "lc_mem.h"
#include "lc_list.h"
#include "lc_pthread.h"


// 重命名链表数据类型名
typedef list_node lc_list_node_t;
typedef list_head lc_list_head_t;

// IPC模块返回值定义
enum lc_sdk_ipc_ret_value
{
    IPC_OK = 0,        // 成功
    IPC_INVALID_PARA,  // 无效参数
    IPC_NOT_INIT,      // 未初始化
    IPC_NOT_FOUND,     // 获取失败
    IPC_INVALID_MSG,   // 校验失败的无效消息
    IPC_NOT_ENOUGH,    // 消息缓冲区空间不足
    IPC_REJECT,        // 注册请求被拒绝
    IPC_TIMEOUT,       // 超时
    IPC_MQ_FULL,       // 消息队列满
    IPC_MQ_EMPTY,      // 消息队列空
    IPC_SYS_ERR,       // 系统调用错误
    IPC_INNER_ERR,     // 内部其他错误
    IPC_RET_BND,       // IPC返回值定义边界
};

// 消息优先级值定义
enum lc_sdk_ipc_msg_priority
{
    IPC_MSG_PRI_COMM = 0,  // 普通优先级
//    IPC_MSG_PRI_HIGH,      // 高优先级
//    IPC_MSG_PRI_DIAG,      // 独立优先级诊断
    IPC_MSG_PRI_BND,       // 优先级定义边界
};

// 进程间消息头部结构
typedef struct lc_sdk_ipc_msg_head
{
    lc_uint32 msg_type;    // 消息类型
    lc_uint32 msg_id;      // 消息ID
    lc_uint32 length;      // 消息长度，不包含头部
    lc_uint32 session_id;  // Session ID
} ipc_msg_head_t;

// 线程间消息头部结构
typedef struct lc_sdk_ipc_mq_head
{
    lc_uint32 msg_type;  // 消息类型
    lc_uint32 msg_id;    // 消息ID
} ipc_mq_head_t;  

// 消息队列管理节点结构
typedef struct lc_sdk_ipc_mq_mgt_node
{
    lc_uint32 length;              // 队列长度
    lc_uint32 msgh_num;            // 高优先级消息数量
    lc_uint32 msgc_num;            // 普通优先级消息数量
    pthread_mutex_t mutex;         // 队列访问互斥量
    pthread_cond_t  send_waitor;   // 投放等待条件变量
    pthread_cond_t  recv_waitor;   // 获取等待条件变量
    lc_list_head_t  *mq_head;      // 指向消息队列存储区开头
    lc_list_node_t  *last_high;    // 最后一个高优先级消息节点指针
} ipc_mq_mgt_node_t;
// 提供给用户模块用于操作具体队列的标识
typedef lc_list_node_t *ipc_mq_cb_pt;

// 消息队列使用情况结构
typedef struct lc_sdk_ipc_mq_usg 
{
    lc_uint32 length;    // 消息队列最大长度
    lc_uint32 msgh_num;  // 消息队列中高优先级消息的数目
    lc_uint32 msgc_num;  // 消息队列中普通优先级消息的数目
} mq_usg_t;

// 消息类型值定义
enum lc_sdk_ipc_msg_type
{
    IPC_ASY_MSG = 0,  // 异步消息
    IPC_SYN_REQ,      // 同步请求
    IPC_SYN_RSP,      // 同步响应
    IPC_INNER_MQ,     // 进程内部消息队列消息
    IPC_MSG_TYPE_BND, // 消息类型定义边界
};

// 消息地址结构
typedef struct lc_sdk_ipc_addr
{
    lc_uint32 pri;            // 优先级
    struct sockaddr_un addr;  // Unix Socket地址
} ipc_addr_t;


// 同步消息发送等待链节点结构
typedef struct lc_sdk_ipc_waitor_node
{
    lc_uint32       session_id;  // Session ID
    pthread_mutex_t mutex;       // 条件变量用互斥量
    pthread_cond_t  waitor;      // 等待条件变量
    lc_uint8        *buff;       // 消息缓冲区指针
    size_t          out_len;     // 消息缓冲区长度
    size_t          act_len;     // 消息实际长度
} ipc_waitor_node_t;

// 回调函数标记宏
#define LC_IPC_CALLBACK
// 无限等待标志
#define IPC_TIME_INFINITY         -1
// 消息队列实例的最大数目
#define IPC_MQ_MAX_NUM            64
// 消息缓冲区最大长度
#define IPC_MSG_BUFF_MAX_LEN      1024
// 用户消息缓冲区最大长度
#define IPC_MSG_USR_BUFF_MAX_LEN  \
    (IPC_MSG_BUFF_MAX_LEN - sizeof(ipc_msg_head_t))
// 定义了IPC消息依据几字节进行字节对齐调整
#define IPC_MSG_ALIGN 4
// 字节对齐向上调整
#define ROUND_UP(n) (((n) + IPC_MSG_ALIGN - 1) & ~(IPC_MSG_ALIGN - 1))

/******************************************************************************
 *         Name:  IPC_MSG_GET_TYPE
 *  Description:  获取给定指针指向消息的消息类型。
 *        Input:  
 *                msg_hdr_ptr:  消息指针，指向消息起始处。
 *       Return:  消息类型，失败时返回IPC_MSG_TYPE_BND。
 *        Notes:  无  
 *****************************************************************************/
#define IPC_MSG_GET_TYPE(msg_ptr) \
(NULL != (msg_ptr) ? *((lc_uint32 *)(msg_ptr)) : IPC_MSG_TYPE_BND)

/******************************************************************************
 *         Name:  IPC_MSG_GET_ID
 *  Description:  获取给定指针指向消息的ID。
 *        Input:  
 *                msg_hdr_ptr:  消息指针，指向消息起始处。
 *       Return:  消息ID，失败时返回0。
 *        Notes:  无  
 *****************************************************************************/
#define IPC_MSG_GET_ID(msg_ptr) \
(NULL != (msg_ptr) ? *((lc_uint32 *)(msg_ptr) + 1) : 0)

/******************************************************************************
 *         Name:  IPC_MSG_GET_DATA
 *  Description:  获取给定指针指向消息的消息体起始地址指针。
 *        Input:  
 *                msg_hdr_ptr:  消息指针，指向消息起始处。
 *       Return:  消息的消息体起始地址指针，失败时返回NULL。
 *        Notes:  无  
 *****************************************************************************/
#define IPC_MSG_GET_DATA(msg_ptr) \
(NULL != (msg_ptr) \
 ? ((void *)(IPC_INNER_MQ == *((lc_uint32 *)(msg_ptr)) \
             ? ((lc_uint8 *)(msg_ptr) + sizeof(ipc_mq_head_t)) \
             : ((lc_uint8 *)(msg_ptr) + sizeof(ipc_msg_head_t)))) \
 : NULL)

/******************************************************************************
 *         Name:  IPC_MSG_MALLOC
 *  Description:  根据给定长度申请一块进程间消息的空间。
 *        Input:  
 *                len:  消息体长度，不包含消息头。
 *       Return:  申请成功的内存空间地址指针，失败时返回NULL。
 *        Notes:  无  
 *****************************************************************************/
#define IPC_MSG_MALLOC(len) \
ipc_memory_assign((len), IPC_ASY_MSG)

/******************************************************************************
 *         Name:  IPC_MSG_FILL_COMM_HDR
 *  Description:  填充给定指针指向进程间消息的消息头。
 *        Input:  
 *                msg_hdr_ptr:  消息指针，指向待填充消息消息头。
 *                id:           消息ID
 *                len:          消息长度，不包含消息头。
 *       Return:  无
 *        Notes:  非响应类型的消息
 *****************************************************************************/
#define IPC_MSG_FILL_COMM_HDR(msg_hdr_ptr, id, len) \
{ \
    if (NULL != (msg_hdr_ptr)) \
    { \
        memset(msg_hdr_ptr, 0x00, sizeof(ipc_msg_head_t)); \
        ((ipc_msg_head_t *)(msg_hdr_ptr))->msg_id = id; \
        ((ipc_msg_head_t *)(msg_hdr_ptr))->length = len; \
        ipc_gen_seesion_id((ipc_msg_head_t *)(msg_hdr_ptr)); \
    } \
}

/******************************************************************************
 *         Name:  IPC_MSG_FILL_RSP_HDR
 *  Description:  填充给定指针指向进程间响应消息的消息头。
 *        Input:  
 *                msg_hdr_ptr:  消息指针，指向待填充响应消息消息头。
 *                id:           消息ID
 *                len:          消息长度，不包含消息头。
 *                msg_rsp_hdr_ptr:  消息指针，指向已有的请求消息消息头。
 *       Return:  无
 *        Notes:  无
 *****************************************************************************/
#define IPC_MSG_FILL_RSP_HDR(msg_rsp_hdr_ptr, id, len, msg_req_hdr_ptr) \
{ \
    if (NULL != (msg_rsp_hdr_ptr) && NULL != (msg_req_hdr_ptr)) \
    { \
        memset(msg_rsp_hdr_ptr, 0x00, sizeof(ipc_msg_head_t)); \
        if (IPC_SYN_REQ == ((ipc_msg_head_t *)(msg_req_hdr_ptr))->msg_type) \
            ((ipc_msg_head_t *)(msg_rsp_hdr_ptr))->msg_type = IPC_SYN_RSP; \
        ((ipc_msg_head_t *)(msg_rsp_hdr_ptr))->msg_id = id; \
        ((ipc_msg_head_t *)(msg_rsp_hdr_ptr))->length = len; \
        ((ipc_msg_head_t *)(msg_rsp_hdr_ptr))->session_id = \
            ((ipc_msg_head_t *)(msg_req_hdr_ptr))->session_id; \
    } \
}

/******************************************************************************
 *         Name:  IPC_MSG_GET_SRC_ADDR
 *  Description:  获取给定指针指向进程间消息的源地址起始地址指针。
 *        Input:  
 *                msg_hdr_ptr:  消息指针，指向待填充响应消息消息头。
 *       Return:  消息的源地址起始地址指针，失败时返回NULL。
 *        Notes:  无
 *****************************************************************************/
#define IPC_MSG_GET_SRC_ADDR(msg_ptr) \
(NULL != msg_ptr \
 ? (IPC_INNER_MQ != *((lc_uint32 *)(msg_ptr)) \
    ? (ipc_addr_t *)((lc_uint8 *)(msg_ptr) \
        + ROUND_UP(((ipc_msg_head_t *)(msg_ptr))->length) \
        + sizeof(ipc_msg_head_t)) \
    : NULL) \
 : NULL)

/******************************************************************************
 *         Name:  IPC_MQ_MALLOC
 *  Description:  根据给定长度申请一块消息队列消息的空间。
 *        Input:  
 *                len:  消息体长度，不包含消息头。
 *       Return:  申请成功的内存空间地址指针，失败时返回NULL。
 *        Notes:  无
 *****************************************************************************/
#define IPC_MQ_MALLOC(len) \
ipc_memory_assign((len), IPC_INNER_MQ)

/******************************************************************************
 *         Name:  IPC_MQ_FILL_HDR
 *  Description:  填充给定指针指向线程间消息的消息头。
 *        Input:  
 *                msg_hdr_ptr:  消息指针，指向待填充响应消息消息头。 
 *                id:           消息ID
 *       Return:  无
 *        Notes:  无
 *****************************************************************************/
#define IPC_MQ_FILL_HDR(msg_hdr_ptr, id) \
{ \
    if (NULL != (msg_hdr_ptr)) \
    { \
        ((ipc_msg_head_t *)(msg_hdr_ptr))->msg_id = id; \
    } \
}


/******************************************************************************
 *         Name:  ipc_mq_init
 *  Description:  初始化消息队列管理结构。主要内容是初始化消息队列管理节点链的
 *                访问控制互斥量。
 *        Input:  
 *                无
 *       Output:
 *                无
 *       Return:  
 *                无
 *        Notes:  无  
 *****************************************************************************/
void 
ipc_mq_init(void);

/******************************************************************************
 *         Name:  ipc_mq_create
 *  Description:  创建一个消息队列。
 *        Input:  
 *                length:  消息队列长度。可以指定为0，指定为0时使用默认值256。
 *       Output:
 *                mq_cb:   标识消息队列的控制块
 *       Return:  
 *                IPC_OK            成功
 *                IPC_INVALID_PARA  无效参数
 *                IPC_NOT_INIT      未初始化 
 *                IPC_SYS_ERR       系统调用错误
 *                IPC_INNER_ERR     内部其他错误 
 *        Notes:  无  
 *****************************************************************************/
lc_uint32 
ipc_mq_create(lc_uint32 length, ipc_mq_cb_pt *mq_cb);

/******************************************************************************
 *         Name:  ipc_mq_destroy
 *  Description:  销毁所有消息队列。
 *        Input:  
 *                无
 *       Output:
 *                无
 *       Return:  
 *                无
 *        Notes:  进程终止前调用，请确保所有使用消息队列的线程都已经终止。  
 *****************************************************************************/
void 
ipc_mq_destroy(void);

/******************************************************************************
 *         Name:  ipc_mq_send
 *  Description:  向指定消息队列中投入一个消息。
 *        Input:  
 *                mq_cb:     标识消息队列的控制块
 *                msg_hdr:   消息的起始地址指针
 *                priority:  消息优先级。这里只能指定IPC_MSG_PRI_COMM或者
 *                           IPC_MSG_PRI_HIGH，指定IPC_MSG_PRI_DIAG将返回错误。
 *                timeout:   超时时间，单位为毫秒（ms）。超时时间可以指定为下面
 *                           几种值来控制不可投放时的行为：
 *                             1. 正值 以指定值为超时时间的阻塞投放
 *                             2. 0    立刻返回，即异步投放
 *                             3. IPC_TIME_INFINITY  无限，即永远阻塞
 *       Output:
 *                无
 *       Return:  
 *                IPC_OK            成功
 *                IPC_INVALID_PARA  无效参数
 *                IPC_MQ_FULL       消息队列满
 *                IPC_TIMEOUT       超时
 *                IPC_SYS_ERR       系统调用错误
 *                IPC_INNER_ERR     内部其他错误  
 *        Notes:  无  
 *****************************************************************************/
lc_uint32 
ipc_mq_send(const ipc_mq_cb_pt mq_cb, 
            const void *msg_hdr, 
            lc_uint32 priotiry, 
            lc_int32 timeout);

/******************************************************************************
 *         Name:  ipc_mq_recv
 *  Description:  从指定消息队列中获取一个消息。
 *        Input:  
 *                mq_cb:     标识消息队列的控制块
 *                timeout:   超时时间，单位为毫秒（ms）。超时时间可以指定为下面
 *                           几种值来控制不可获取时的行为：
 *                             1. 正值 以指定值为超时时间的阻塞获取
 *                             2. 0    立刻返回，即异步获取
 *                             3. IPC_TIME_INFINITY  无限，即永远阻塞
 *       Output:
 *                无
 *       Return:  
 *                IPC_OK            成功
 *                IPC_INVALID_PARA  无效参数
 *                IPC_MQ_EMPTY      消息队列空
 *                IPC_TIMEOUT       超时
 *                IPC_SYS_ERR       系统调用错误
 *                IPC_INNER_ERR     内部其他错误  
 *        Notes:  无  
 *****************************************************************************/
lc_uint32 
ipc_mq_recv(const ipc_mq_cb_pt mq_cb, 
            void **msg_hdr, 
            lc_int32 timeout);

/******************************************************************************
 *         Name:  ipc_mq_remove
 *  Description:  从指定消息队列中移除所有指定消息ID的消息。如果队列为空或者没
 *                有找到指定ID的消息，也成功返回。
 *        Input:  
 *                mq_cb:     标识消息队列的控制块
 *                msg_id:    待移除消息的消息ID
 *       Output:
 *                无
 *       Return:  
 *                IPC_OK            成功
 *                IPC_INVALID_PARA  无效参数
 *                IPC_SYS_ERR       系统调用错误
 *                IPC_INNER_ERR     内部其他错误  
 *        Notes:  无  
 *****************************************************************************/
lc_uint32 
ipc_mq_remove(const ipc_mq_cb_pt mq_cb, lc_uint32 msg_id);

/******************************************************************************
 *         Name:  ipc_mq_usg_state
 *  Description:  获取本进程消息队列使用情况。
 *        Input:  
 *                num:       数组usg_stat的长度
 *       Output:
 *                usg_stat:  记录各个消息队列使用情况的数组。输入时数组长度应该
 *                           指定为消息队列的最大数目：IPC_MQ_MAX_NUM。
 *                           输出时则为消息队列的实际数目，遍历数组即可以得知所
 *                           有消息队列的使用情况。请参考5.7。
 *                num:       消息队列的实际数目
 *       Return:  
 *                IPC_OK            成功
 *                IPC_INVALID_PARA  无效参数
 *                IPC_NOT_INIT      未初始化
 *                IPC_NOT_ENOUGH    缓冲区空间不足
 *                IPC_SYS_ERR       系统调用错误
 *                IPC_INNER_ERR     内部其他错误  
 *        Notes:  无  
 *****************************************************************************/
lc_uint32 
ipc_mq_usg_state(mq_usg_t *usg_stat, lc_uint32 *num);

/******************************************************************************
 *         Name:  ipc_msg_handler
 *  Description:  接收消息处理函数原型。
 *                用户自定义的处理接收到的进程间消息的函数，函数名由用户模块
 *                定义，实现也由用户模块完成。 
 *        Input:  
 *                buff:  消息缓冲区。存放接收到的进程间消息
 *                len:   缓冲区中消息长度，不包含消息头
 *       Output:
 *                无
 *       Return:  
 *                IPC模块并不关心用户定义函数的运行情况，此返回值只用于指导IPC
 *                模块接下来的行为：
 *                  TRUE:   消息投入消息队列
 *                  FALSE:  消息不投入消息队列
 *        Notes:  用户定义的回调函数
 *                如果不允许IPC模块在处理完成后继续默认处理，则用户必须负责消息
 *                缓冲区的释放。
 *****************************************************************************/
typedef LC_IPC_CALLBACK lc_bool (*ipc_msg_handler)(lc_uint8 *buff, size_t len);

/******************************************************************************
 *         Name:  ipc_reg_handler
 *  Description:  注册消息处理函数原型。
 *                用户自定义的处理接收到的注册请求消息的函数，函数名由用户模块
 *                定义，实现也由用户模块完成。 
 *        Input:  
 *                msg_id:  请求注册的消息ID
 *       Output:
 *                无
 *       Return:  
 *                IPC模块并不关心用户定义函数的运行情况，此返回值只用于指导IPC
 *                模块接下来的行为：
 *                  TRUE:   完成注册
 *                  FALSE:  拒绝注册
 *        Notes:  用户定义的回调函数
 *****************************************************************************/
typedef LC_IPC_CALLBACK lc_bool (*ipc_reg_handler)(lc_uint32 msg_id);

/******************************************************************************
 *         Name:  ipc_msg_init
 *  Description:  进程间消息处理子模块初始化方法。需要在用户模块使用其他消息
 *                接口前调用，主要用于创建Socket、地址绑定和创建接收线程等。
 *        Input:  
 *                module_id:  客户模块ID
 *                mq_cb:      标识用于投放消息的消息队列控制块。消息队列控制块
 *                            为创建消息队列时的返回值，作为Server端的模块在调
 *                            用本接口之前需要创建一个消息队列来存放接收到的进
 *                            程间消息。
 *                            可以通过指定为NULL来表示不使用消息队列
 *                msg_handler:  用户指定的接收消息处理函数指针
 *                reg_handler:  用户指定的注册消息处理函数指针 
 *                is_support_multi_processes:  是否支持多进程
 *                              TRUE:   支持多进程。此模式通信地址不固定
 *                                         无法作为Server端。
 *                              FALSE:  不支持多进程。
 *       Output:
 *                无
 *       Return:  
 *                IPC_OK            成功
 *                IPC_INVALID_PARA  无效参数
 *                IPC_SYS_ERR       系统调用错误
 *                IPC_INNER_ERR     内部其他错误 
 *        Notes:  无  
 *****************************************************************************/
lc_uint32 
ipc_msg_init(lc_uint32 module_id, 
             const ipc_mq_cb_pt mq_cb, 
             ipc_msg_handler msg_handler, 
             ipc_reg_handler reg_handler,
             lc_bool is_support_multi_processes);

/******************************************************************************
 *         Name:  ipc_msg_fin
 *  Description:  进程间消息处理子模块终结化方法。用户模块不再使用任何消息接口
 *                后（例如，模块退出前）调用，主要用于销毁各种资源。
 *        Input:  
 *                无 
 *       Output:
 *                无
 *       Return:  无  
 *        Notes:  无
 *****************************************************************************/
void 
ipc_msg_fin(void);

/******************************************************************************
 *         Name:  ipc_msg_send_asy
 *  Description:  进程间异步消息发送方法。
 *        Input:  
 *                dest_addr:  目标地址
 *                buff:       消息缓冲区。存放待发送的进程间消息。
 *                len:        缓冲区中消息的长度，不包含消息头。
 *       Output:
 *                无
 *       Return:  
 *                IPC_OK            成功
 *                IPC_INVALID_PARA  无效参数
 *                IPC_NOT_INIT      未初始化
 *                IPC_INVALID_MSG   无效消息。可能的原因是消息类型无效或者声明
 *                                  的消息长度与实际不符。
 *                IPC_SYS_ERR       系统调用错误
 *                IPC_INNER_ERR     内部其他错误  
 *        Notes:  无  
 *****************************************************************************/
lc_uint32 
ipc_msg_send_asy(const ipc_addr_t *dest_addr, 
                 const lc_uint8 *buff, 
                 size_t len);

/******************************************************************************
 *         Name:  ipc_msg_send_syn
 *  Description:  进程间同步消息发送方法。
 *        Input:  
 *                dest_addr:  目标地址
 *                in_buff:    消息缓冲区。存放待发送的进程间消息。
 *                in_len:     缓冲区中消息的长度，不包含消息头。
 *                timeout:    超时时间，单位为毫秒（ms）。
 *                out_len:    消息缓冲区的长度减去进程间消息头的长度  
 *       Output:
 *                out_buff:   消息缓冲区。存放待接收到的进程间消息。
 *                out_len:    缓冲区中消息的实际长度，不包含消息头。 
 *       Return:  
 *                IPC_OK            成功
 *                IPC_INVALID_PARA  无效参数
 *                IPC_NOT_INIT      未初始化
 *                IPC_INVALID_MSG   无效消息。可能的原因是消息类型无效或者声明
 *                                  的消息长度与实际不符。
 *                IPC_NOT_ENOUGH    消息缓冲区空间不足
 *                IPC_TIMEOUT       超时  
 *                IPC_SYS_ERR       系统调用错误
 *                IPC_INNER_ERR     内部其他错误  
 *        Notes:  无  
 *****************************************************************************/
lc_uint32 
ipc_msg_send_syn(const ipc_addr_t *dest_addr, 
                 const lc_uint8 *in_buff, 
                 size_t in_len, 
                 lc_uint32 timeout, 
                 lc_uint8 *out_buff, 
                 size_t *out_len);
                 
/******************************************************************************
 *         Name:  ipc_register_notify
 *  Description:  向指定模块注册通知消息。
 *        Input:  
 *                dest_addr:  注册通知的目标模块地址
 *                msg_id:     需要注册的通知消息ID 
 *                timeout:    超时时间，单位为毫秒（ms）。                 
 *       Output:
 *                无
 *       Return:  
 *                IPC_OK            成功
 *                IPC_INVALID_PARA  无效参数
 *                IPC_NOT_INIT      未初始化 
 *                IPC_REJECT        注册请求被拒绝 
 *                IPC_TIMEOUT       超时                 
 *                IPC_SYS_ERR       系统调用错误                 
 *                IPC_INNER_ERR     内部其他错误  
 *        Notes:  如果指定高优先级发送，则将来目标模块也会使用高优先级来发送
 *                通知。   
 *****************************************************************************/
lc_uint32 
ipc_register_notify(const ipc_addr_t *dest_addr,  
                    lc_uint32 msg_id,
                    lc_uint32 timeout);
                    
/******************************************************************************
 *         Name:  ipc_deregister_notify
 *  Description:  向指定模块反注册通知消息。
 *        Input:  
 *                dest_addr:  注册通知的目标模块地址
 *                msg_id:     需要反注册的通知消息ID 
 *       Output:
 *                无
 *       Return:  
 *                IPC_OK            成功
 *                IPC_INVALID_PARA  无效参数
 *                IPC_NOT_INIT      未初始化  
 *                IPC_SYS_ERR       系统调用错误                 
 *                IPC_INNER_ERR     内部其他错误  
 *        Notes:  无  
 *****************************************************************************/
lc_uint32 
ipc_deregister_notify(const ipc_addr_t *dest_addr, 
                      lc_uint32 msg_id);
                      
/******************************************************************************
 *         Name:  ipc_msg_notify
 *  Description:  向注册了的模块发送通知消息。
 *        Input:  
 *                buff:       消息缓冲区。存放待发送的通知消息。
 *                len:        缓冲区中消息的长度，不包含消息头。 
 *       Output:
 *                无
 *       Return:  
 *                IPC_OK            成功
 *                IPC_INVALID_PARA  无效参数
 *                IPC_NOT_INIT      未初始化  
 *                IPC_SYS_ERR       系统调用错误                 
 *                IPC_INNER_ERR     内部其他错误  
 *        Notes:  无  
 *****************************************************************************/
lc_uint32 
ipc_msg_notify(const lc_uint8 *buff, size_t len);

/******************************************************************************
 *         Name:  ipc_get_addr_by_id
 *  Description:  通过模块ID获取通信地址。
 *        Input:  
 *                module_id:  模块ID
 *                priority:   消息优先级
 *       Output:
 *                addr:       模块通信地址
 *       Return:  
 *                IPC_OK            成功
 *                IPC_INVALID_PARA  无效参数
 *                IPC_NOT_FOUND     无对应通信地址
 *                IPC_INNER_ERR     内部其他错误  
 *        Notes:  无  
 *****************************************************************************/
lc_uint32 
ipc_get_addr_by_id(lc_uint32 module_id, 
                   lc_uint32 priority, 
                   ipc_addr_t *addr);

/******************************************************************************
 *         Name:  ipc_diag_msg_receive
 *  Description:  进程间消息接收方法。
 *        Input:  
 *                out_len:    消息缓冲区的长度减去进程间消息头的长度  
 *       Output:
 *                out_buff:   消息缓冲区。存放待接收到的进程间消息。
 *                out_len:    缓冲区中消息的实际长度，不包含消息头。 
 *                src_addr:   通信源地址 
 *       Return:  
 *                IPC_OK            成功
 *                IPC_INVALID_PARA  无效参数
 *                IPC_NOT_INIT      未初始化
 *                IPC_INVALID_MSG   无效消息。可能的原因是消息类型无效或者声明
 *                                  的消息长度与实际不符。
 *                IPC_NOT_ENOUGH    消息缓冲区空间不足 
 *                IPC_SYS_ERR       系统调用错误
 *                IPC_INNER_ERR     内部其他错误  
 *        Notes:  仅供诊断模块使用，其他用户模块请不要调用。  
 *****************************************************************************/
lc_uint32 
ipc_diag_msg_receive(lc_uint8 *out_buff, 
                     size_t *out_len, 
                     ipc_addr_t *src_addr);


/******************************************************************************
 *         Name:  ipc_gen_seesion_id
 *  Description:  生成唯一的Session ID。
 *        Input:  
 *                msg_hdr_ptr:  消息指针，指向待填充消息消息头。  
 *       Output:
 *                msg_hdr_ptr:  消息指针，指向待填充消息消息头。
 *       Return:  
 *                无  
 *        Notes:  仅供工具宏使用，用户模块请不要调用。  
 *****************************************************************************/
inline void  
ipc_gen_seesion_id(ipc_msg_head_t *msg_hdr_ptr);

/******************************************************************************
 *         Name:  ipc_memory_assign
 *  Description:  Assign message memory.
 *        Input:  
 *                len:   长度
 *                type:  消息类型
 *       Output:
 *                无
 *       Return:  
 *                无  
 *        Notes:  仅供工具宏使用，用户模块请不要调用。  
 *****************************************************************************/
void *ipc_memory_assign(size_t len, lc_uint32 type);

#endif
