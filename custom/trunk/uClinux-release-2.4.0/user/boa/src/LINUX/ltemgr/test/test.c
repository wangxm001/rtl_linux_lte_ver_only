#include <stdio.h>
#include "lte_sdk.h"

lct_config_s test_config;

lc_bool test_msg_handler(callback_msg_data_s* msg)
{

    int msg_id;
    int i;
    link_status_evt_t *plink_status;
    iface_status_evt_t* piface_status;
    char* ptr;

    if(msg == NULL)
    {
        printf("recv NULL msg\n");
        return FALSE;
    }
    msg_id = msg->msg_id;
    printf("test recv msg:0x%08X \n", msg_id);
    switch(msg_id)
    {
        case WM_MSG_LTE_SDK_LINK_STATUS_NOTIFY:
        {
            printf("WM_MSG_LTE_SDK_LINK_STATUS_NOTIFY\n");
            if(msg->result != OK)
            {
                printf("notfiy WM_MSG_LTE_SDK_LINK_STATUS_NOTIFY error\n");
                break;
            }

            plink_status = (link_status_evt_t*)msg->data; 
            
            printf("link status:%s %s %s\n", 
                (plink_status->link_status == WM_CONNECTED? "WM_CONNECTED":"WM_DISCONNECTED"), 
                (plink_status->ip.u32updown==WM_IP_UP?"WM_IP_UP":"WM_IP_DOWN"),
                (plink_status->iface_id==WM_WCDMA?"WM_WCDMA":"Other type")
            );
            if(plink_status->link_status == WM_CONNECTED)
            {
                printf("interface:%s\n", plink_status->ip.wan_ifname);
                printf("ip:%s\n", plink_status->ip.wan_ip);
                printf("gateway:%s\n", plink_status->ip.gateway);
                printf("dns1:%s\n", plink_status->ip.dns1);
                printf("dns2:%s\n", plink_status->ip.dns2);
                printf("mask:%s\n", plink_status->ip.mask);
            }
            break;
        }
        case WM_MSG_LTE_SDK_IFACE_STATUS_NOTIFY:
        {
            printf("WM_MSG_LTE_SDK_IFACE_STATUS_NOTIFY\n");
            if(msg->result != OK)
            {
                printf("notfiy WM_MSG_LTE_SDK_IFACE_STATUS_NOTIFY error\n");
                break;
            }            
            piface_status = (iface_status_evt_t*)msg->data;
            
            printf("iface status:%s\n", (piface_status->link.iface_status == WM_READY? "WM_READY": "WM_NOT_READY"));
            break;
        }
        case WM_MSG_LTE_SDK_MODEM_REPORT_NOTIFY:
        {
#if 0            
            printf("WM_MSG_LTE_SDK_MODEM_REPORT_NOTIFY\n");
            if(msg->result != OK)
            {
                printf("notfiy WM_MSG_LTE_SDK_MODEM_REPORT_NOTIFY error\n");
                break;
            }            
            piface_status = (iface_status_evt_t*)msg->data;
            
            printf("modem report:%s\n", msg->data);            
#endif            
            break;
        }
        default:
        {
            printf("error msg id\n");
            break;
        }
    }
    return TRUE;
}
int test_init()
{
    sprintf(test_config.name, "test_sdk");
    test_config.msg_func=test_msg_handler;
    test_config.log_enable =1;

    if(lct_module_init(&test_config) < 0)
    {
        printf("lte sdk init error\n");
        return -1;
    }
    return 0;
}


void test_getimsi()
{
    char buff[512]={0};
    if(lct_net_imsi_get_req(buff, sizeof(buff)) < 0)
    {
        printf("get test imsi error\n");
        return;
    }
    printf("test imsi:%s\n", buff);
}

void test_getsignal()
{
    char buff[512]={0};
    if(lct_net_signal_level_get_req(buff, sizeof(buff)) < 0)
    {
        printf("get test signal error\n");
        return;
    }
    printf("test signal:%s\n", buff);
}

void test_getimei()
{
    char buff[512]={0};
    if(lct_net_imei_get_req(buff, sizeof(buff)) < 0)
    {
        printf("get test imei error\n");
        return;
    }
    printf("test imei:%s\n", buff);
}

void test_get_sim_status()
{
    char buff[512]={0};
    if(lct_sim_status_get_req(buff, sizeof(buff)) < 0)
    {
        printf("get test imei error\n");
        return;
    }
    printf("test imei:%s\n", buff);
}

void test_modem_at()
{
    char buff[128]={0};
    printf("please input at:");
    while(fgets(buff, sizeof(buff)-sizeof(mm_at_req_t), stdin) && isspace(buff[0]));
    
    if(lct_ext_at_req(buff, buff,sizeof(buff)) < 0)
    {
        printf("send at cmd error\n");
        return;
    }
    printf("test modem at :%s\n", buff);
}

void test_interface_status()
{
    iface_status_evt_t req;
    printf("please select ETH interface status[ 0: WM_NOT_READY 1:WM_READY]:");
    req.link.iface_id = WM_ETH;
    req.link.link_status = WM_LINK_STA_CLEAR;
    scanf("%d", &req.link.iface_status);

    if(lct_inface_status_req(&req) < 0)
    {
        printf("send interface status error\n");
        return;
    }
    printf("send interface status ok\n");
}

void test_link_status()
{
    iface_status_evt_t req;
    printf("please select ETH link status[ 0: WM_DISCONNECTED 2:WM_CONNECTED]:");
    req.link.iface_id = WM_ETH;
    req.link.iface_status = WM_WARNING_CLEAR;
    scanf("%d", &req.link.link_status);

    if(lct_inface_status_req(&req) < 0)
    {
        printf("send link status error\n");
        return;
    }
    printf("send link status ok\n");
}

void test_link_status1()
{
    int req;
    printf("please select LTE link status[ 0: WM_DISCONNECTED 2:WM_CONNECTED]:");
    scanf("%d", &req);

    if(lct_net_set_status_test_req(&req) < 0)
    {
        printf("send lte link status error\n");
        return;
    }
    printf("send lte link status ok\n");
}


void test_dev_status()
{
	char ifname[64] = {0};
    struct user_net_device_stats req ={0};
    printf("please select enter ifname [ex:4g0]:");
    scanf("%s", ifname);
    
    if(lct_readlist_proc(ifname, &req) < 0)
    {
        printf("get volume status error\n");
        return;
    }
    printf("rxbytes:%llu txbytes:%llu\n", req.rx_bytes, req.tx_bytes);
}


void test_net_pref()
{
    set_pref_nettype_req_t req;
    printf("please set net pref[ 0: auto 1:manual]:");
    scanf("%d", &req.pref_nettype);

    if(lct_net_set_pref_req(&req) < 0)
    {
        printf("set network pref error\n");
        return;
    }
    printf("set network pref ok\n");
}


void test_set_apn()
{
    if(lct_apn_set_req() < 0)
    {
        printf("set apn error\n");
        return;
    }
    printf("set apn ok\n");
}

void test_sim_enable_pin()
{
    enable_pin_req_t req;
    printf("please enter pin code:");
    scanf("%s", &req.pin);

    if(lct_enable_pin(&req) < 0)
    {
        printf("enable pin error\n");
        return;
    }
    printf("enable pin ok\n");
}

void test_sim_disenable_pin()
{
    unlock_pin_req_t req;
    printf("please enter pin code:");
    scanf("%s", &req.pin);

    if(lct_disenable_pin(&req) < 0)
    {
        printf("disenable pin error\n");
        return;
    }
    printf("disenable pin ok\n");
}

void test_sim_modify_pin()
{
    change_pin_req_t req;
    printf("please enter old pin code:");
    scanf("%s", &req.old_pin);
    printf("please enter new pin code:");
    scanf("%s", &req.new_pin);    

    if(lct_modify_pin(&req) < 0)
    {
        printf("modify pin error\n");
        return;
    }
    printf("modify pin ok\n");
}

void test_sim_modify_pin_en()
{
    unsigned char req;
    unsigned int val;
    printf("please enter modify pin Enable or Disable:[0: Disable 1:Enable]");
    scanf("%d", &val);
    req = (unsigned char)val;

    if(lct_modify_pin_en(&req) < 0)
    {
        printf("enable modify pin error\n");
        return;
    }
    printf("enable pin modify ok\n");
}



int main(int argc, char** argv)
{
    int run = 1;
    int option;
    if( test_init() < 0)
    {
        return -1;
    }

    while(run)
    {
        printf("-1.exit\n");
        printf("0.get imsi\n");
        printf("1.get signal level [0:nosignal 1:level 1 2: level 2 ...]\n");
        printf("2.get imei\n");
        printf("3.get sim status\n");
        printf("4.connect\n");
        printf("5.disconnect\n");
        printf("6.modem at\n");
        printf("7.ETH interface status [0:WM_NOT_READY 1:WM_READY]\n");
        printf("8:set apn\n");
        printf("9:set net pref\n");
        printf("10.ETH link status [ 0: WM_DISCONNECTED 2:WM_CONNECTED]\n");
        printf("11.volume status:\n");
        printf("12.lte link status [ 0: WM_DISCONNECTED 2:WM_CONNECTED]\n");
        printf("13.enable pin\n");
        printf("14.disenable pin\n");
        printf("15.modified pin\n");
        printf("16.enabled modified pin[ 0: modify disable 1: modified enable]\n");
        

        if(scanf("%d", &option) == 0)
        {
            scanf("%*s");            
            continue;
        }
        switch(option)
        {
            case 0:
            {
                test_getimsi();
                break;
            }
            case 1:
            {
                test_getsignal();
                break;
            }
            case 2:
            {
                test_getimei();
                break;
            }
            case 3:
            {
                test_get_sim_status();
                break;
            }
            case 4:
            {
                lct_connect();
                break;
            }
            case 5:
            {
                lct_disconnect();
                break;
            }
            case 6:
            {
                test_modem_at(); 
                break;
            }
            case 7:
            {
               test_interface_status();
               break;
            }
            case 8:
            {
                test_set_apn();
                break;                
            }
            case 9:
            {
               test_net_pref();
               break;
            }
            case 10:
            {
               test_link_status();
               break;
            }
            case 11:
            {
               test_dev_status();
               break;
            }
            case 12:
            {
               test_link_status1();
               break;
            }
            case 13:
            {
				test_sim_enable_pin();
				break;
			}
			case 14:
			{
				test_sim_disenable_pin();
				break;
			}
			case 15:
			{
				test_sim_modify_pin();
				break;
			}
			case 16:
			{
				test_sim_modify_pin_en();
				break;
			}
            case -1:
            {
                run = 0;
                break;
            }
        }
    }
    return 0;
}
