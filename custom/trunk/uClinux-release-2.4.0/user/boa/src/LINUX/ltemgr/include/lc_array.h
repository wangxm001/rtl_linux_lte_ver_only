#ifndef __COS_ARRAY_H
#define __COS_ARRAY_H

#include <ctype.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <syslog.h>

#include <stdlib.h>
#include <string.h>

#include "cos_common.h"

void  lc_array_insert ( void *array,
                  lc_uint32 elem_size,
                  lc_uint32 count,
                  lc_uint32 pos,
                  const void *value);

void  lc_array_erase ( void *array,
                  lc_uint32 elem_size,
                  lc_uint32 count,
                  lc_uint32 pos);

lc_int32  lc_array_find( const void *array, 
                lc_uint32 elem_size, 
                lc_uint32 count, 
                lc_int32 (*matching)(const void *value),
                void **result);

#endif

