#ifndef __COS_HASH_H
#define __COS_HASH_H

#include <ctype.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <syslog.h>

#include <stdlib.h>
#include <string.h>

#include "lc_common.h"

#define HASHTAB_MAX_NODES    0xffffffff

//typedef lc_int32 (* hash_value)(struct hashtab *h, const void *key) ;

//typedef lc_int32 (* keycmp)(struct hashtab *h, const void *key1, const void *key2) ;


struct hashtab_node 
{
    void *key;
    void *datum;
    struct hashtab_node *next;
};

struct hashtab 
{
    struct hashtab_node **htable;    /* hash table */
    lc_uint32 size;            /* number of slots in hash table */
    lc_uint32 nel;            /* number of elements in hash table */
    lc_uint32 (*hash_value)(struct hashtab *h, const void *key);    /* hash function */
    lc_int32 (*keycmp)(struct hashtab *h, const void *key1, const void *key2);    /* key comparison function */
};

struct hashtab_info 
{
    lc_uint32 slots_used;
    lc_uint32 max_chain_len;
};

/*
 * Creates a new hash table with the specified characteristics.
 *
 * Returns NULL if insufficent space is available or
 * the new hash table otherwise.
 */
 #if 0
struct hashtab *lc_hashtab_create
(
    hash_value create_hash_value,
    keycmp hash_key_cmp,
    lc_uint32 size
);
#else
struct hashtab *lc_hashtab_create
(
                    lc_uint32 (*hash_value)(struct hashtab *h, const void *key),
                    lc_int32 (*keycmp)(struct hashtab *h, const void *key1, const void *key2),
                    lc_uint32 size
);
#endif


/*
 * Inserts the specified (key, datum) pair into the specified hash table.
 *
 * Returns -ENOMEM on memory allocation error,
 * -EEXIST if there is already an entry with the same key,
 * -EINVAL for general errors or
  0 otherwise.
 */
int lc_hashtab_insert(struct hashtab *h, void *k, void *d);

/*
 * Searches for the entry with the specified key in the hash table.
 *
 * Returns NULL if no entry has the specified key or
 * the datum of the entry otherwise.
 */
void *lc_hashtab_search(struct hashtab *h, const void *k);

/*
 * Destroys the specified hash table.
 */
void lc_hashtab_destroy(struct hashtab *h, 
                        void (*keyfree)(void *key), 
                        void (*datafree)(void *data));

/*
 * Applies the specified apply function to (key,datum,args)
 * for each entry in the specified hash table.
 *
 * The order in which the function is applied to the entries
 * is dependent upon the internal structure of the hash table.
 *
 * If apply returns a non-zero status, then hashtab_map will cease
 * iterating through the hash table and will propagate the error
 * return to its caller.
 */
lc_int32 lc_hashtab_map(struct hashtab *h,
        lc_int32 (*apply)(void *k, void *d, void *args),
        void *args);

/* Fill info with some hash table statistics */
void lc_hashtab_stat(struct hashtab *h, struct hashtab_info *info);




#endif
