/***************************************************************************************
 *
 *
 *       Filename:  nvmanager.h
 *
 *    Description:  Header for nvram
 *
 *        Version:  1.0
 *        Created:  07/15/2009 07:59:45 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Cao Gang
 *      Copyright:  Copyright (c) 2009, Xi'an LongQuest Technology LTD,all rights reserved
 *
 ***************************************************************************************/

#ifndef __NVMANAGER_H__
#define __NVMANAGER_H__
#include "liblog.h"
#ifdef DEBUG
#ifdef MYPRINT

/* Print directly to the console */
#if 0
#define NVDBG(fmt, args...) do {    \
    FILE *fp = fopen("/dev/console", "w");    \
    if (fp) {    \
        fprintf(fp, fmt, ## args);    \
        fclose(fp);    \
    }    \
} while (0)
#else
#define NVDBG(fmt, args...) do {    \
        fprintf(stdout, fmt, ## args);    \
} while (0)
#endif

#else    /*MYPRINT*/

#define NVDBG(formats, args...) \
  log_print(LC_LOG_DEBUG, "[%s:%d]"formats"\n", __FILE__, __LINE__, ##args);
#endif    /*MYPRINT*/
#else    /*DEBUG*/
#define NVDBG(formats, args...)
#endif    /*DEBUG*/

#include "lc_common.h"

#define DEFAULTNV             "/2ndfile/nv/default.cfg"
#define UPDATENV              "/2ndfile/nv/default.cfg"
#define CURRENTNV             "/2ndfile/nv/currentnv/"
#define BACKUPNV              "/2ndfile/nv/backupnv/"
#define NV_FIN_FLAG           "/2ndfile/nvflage"

#define RAM_CURRENT_NV        "/tmp/nv/"

#define FACTORY_DEFAULT_NV    "/dev/mtdblock2"
//#define FACTORY_DEFAULT_NV    "/2ndfile/factory/factory_nv"
/*Pacino.cao . update factory partition*/
#define FACTORY_CURRENT_NV    "/tmp/factory.cfg"
#define FACTORY_CURRENT_PATH  "/tmp/factory/"

#define MAXNAMELEN 128             /* include '\0' */
#define MAXVALUELEN 4096          /* include '\0' */
#define MAX_FACTORY_NAMELEN 32    /* include '\0' */
#define MAX_FACTORY_VALUELEN 64   /* include '\0' */
#define MAX_FACTORY_SIZE    0x10000   /*64*1024*/

/*Pacino.cao . update factory partition size*/

/*attributes available, a combined attribute is available*/
#define NV_ATTRIBUTE_RESET         0x00000001        /* the item should be reset on a router setting reset */
#define NV_ATTRIBUTE_USER          0x00000002        /* the item should be configurable and can be exported and imported in webUI */
#define NV_ATTRIBUTE_UPDATE        0x00000008        /* the item should be imported on firmware updating */

/*access control flags available*/
#define NV_ACF_NOPERM 0    /* the item can not be accessed by AT parser */
#define NV_ACF_RDWR 1      /* the item can be read and wrote by AT parser */
#define NV_ACF_RDONLY 2    /* the item can only be read by AT parser */
#define NV_ACF_WRONLY 3    /* the item can only be wrote by AT parser */

typedef enum {
    NV_OK = 0,
    NV_NOTFOUND,
    NV_NOMEM,
    NV_EFILE,
    NV_EINVAL,
    NV_ERROR,
    NV_NOMATCH,
    NV_MAX
}nv_errorcode;

typedef struct tag_fnvnode_t
{
    lc_int8 factory_name[MAX_FACTORY_NAMELEN];
    lc_int8 factory_value[MAX_FACTORY_VALUELEN];
}fnvnode_t, *pfnvnode_t;

/********************************************************************************
 *
 *         Name:  nvm_init
 *  Description:  This is the initialization function of database, and should be 
 *                called before any other database function. In this function, 
 *                all NV item in database will be checked.This function will set 
 *                file mode creation mask to 0.
 *        Parms:  None
 *       Return:
 *                NV_OK - Initialize database successfully
 *                NV_ERROR - Some error occured in the initialization process
 *        Notes:
 ********************************************************************************/

nv_errorcode nvm_init(void);

/********************************************************************************
 *
 *         Name:  nvm_readnv
 *  Description:  Read current value of a NV item
 *        Parms:
 *                nvname - NV item name
 *                buff - Buffer used to receive NV item value
 *                bufflen - Size of buff
 *       Return:
 *                NV_OK - Get NV value successfully
 *                NV_NOTFOUND - No such NV item in database
 *                NV_NOMEM - The buff length is not enough for current value
 *                NV_ERROR - There are some error on the database
 *        Notes:
 ********************************************************************************/

nv_errorcode nvm_readnv(const lc_int8 *nvname, lc_int8 *buff, size_t bufflen);

/********************************************************************************
 *
 *         Name:  nvm_writenv
 *  Description:  Update a NV item value.This function will set file mode creation
 *                mask to 0.
 *        Parms:
 *                nvname - NV item name
 *                buff - NV item value needs to write to database
 *                datalen - Length of NV item value, include terminal '\0'
 *       Return:
 *                NV_OK - Save the NV item value successfully
 *                NV_NOTFOUND - o such NV item in database
 *                NV_EINVAL - Invalid NV item value
 *                NV_ERROR - There are some error on the database
 *        Notes:
 ********************************************************************************/

nv_errorcode nvm_writenv(const lc_int8 *nvname, const lc_int8 *buff, size_t datalen);

/********************************************************************************
 *
 *         Name:  nvm_import
 *  Description:  Import some NV items to database, and only items exist in the
 *                default item list can be imported.This function will set file
 *                mode creation mask to 0.
 *        Parms:
 *                filename - The file which includes some NV items need to import
 *                to the database.NV name and NV value (No NV Attribute) are saved
 *                in the file, separated by certain separator, ?????? NV items,
 *                which consist of NV name and NV value pairs, are separated by 
 *                LF (0x0A)
 *                attribute - The attribute of these items in the file, only if 
 *                this attribute is consistent with the attribute saved in default
 *                item list, the item can be import
 *                needcrc - Whether a CRC should be included in the file
 *       Return:
 *                NV_OK - Import NV items successfully
 *                NV_NOTFOUND - An NV item is not found in default list
 *                NV_EFILE - The file needs to import has some error
 *                NV_ERROR - The database has some error
 *        Notes:
 ********************************************************************************/

nv_errorcode 
nvm_import(const lc_int8 *filename, lc_uint32 attribute, lc_int32 needcrc, lc_int32 checksku);

/********************************************************************************
 *
 *         Name:  nvm_export
 *  Description:  Export NV items of certain attribute to a file
 *        Parms:
 *                filename - Export these NV items to this file
 *                attribute - Items included in this attribute will be exported
 *                needcrc - Whether a CRC should be included in the file
 *       Return:
 *                NV_OK - Export NV items to the file successfully
 *                NV_EFILE - The file needs to export to can not be open
 *                NV_ERROR - The database has some error
 *        Notes:
 ********************************************************************************/

nv_errorcode nvm_export(const lc_int8 *filename, lc_uint32 attribute, lc_int32 needcrc);

/********************************************************************************
*
*         Name:  nvm_routerreset
*  Description:  reset NV items with reset attribute from the backup file
*        Parms:  None
*       Return:
*                NV_OK - Import NV items successfully
*                NV_NOTFOUND - An NV item is not found in default list
*                NV_EFILE - The file needs to import can not be open
*                NV_ERROR - The database has some error
*        Notes:
********************************************************************************/

nv_errorcode nvm_routerreset(void);

/********************************************************************************
 *
 *         Name:  nvm_clear
 *  Description:  Remove all current NV items from database
 *        Parms:  NONE
 *       Return:
 *                NV_OK -- Always return NV_OK
 *        Notes:
 ********************************************************************************/

nv_errorcode nvm_clear (void);

/********************************************************************************
 *
 *         Name:  nvm_fin
 *  Description:  create fin flage
 *        Parms:  NONE
 *       Return:
 *                NV_OK -- return OK
 *                NV_ERROR -- error
 *        Notes:
 ********************************************************************************/

nv_errorcode nvm_fin (void);

/********************************************************************************
*
*         Name:  nvm_match
*  Description:  match CURRENTNV items
*        Parms:  const char *name, const char *value
*       Return:
*                NV_OK -- match return NV_OK
*        Notes:
********************************************************************************/

nv_errorcode nvm_match(const lc_int8 *name, const lc_int8 *value);

/********************************************************************************
 *
 *         Name:  nvm_parsefile
 *  Description:  Get an item value in imported file
 *        Parms:
 *                filename - The file which includes some NV items need to import
 *                to the database.NV name and NV value (No NV Attribute) are saved
 *                in the file, separated by certain separator, ?????? NV items,
 *                which consist of NV name and NV value pairs, are separated by 
 *                LF (0x0A).
 *                nvname - NV item name
 *                buff - Buffer used to receive NV item value
 *                bufflen - Size of buff
 *                needcrc - Whether a CRC is included in the file
 *       Return:
 *                NV_OK - Get NV value successfully
 *                NV_NOTFOUND - No such NV item in the file
 *                NV_EFILE - The file has some error
 *                NV_NOMEM - The buff length is not enough for current value
 *        Notes:
 ********************************************************************************/
nv_errorcode nvm_parsefile (const lc_int8 *filename, const lc_int8 *nvname, lc_int8 *buff,
                           size_t bufflen, lc_int32 needcrc);

/********************************************************************************
 *
 *         Name:  nvmram_init
 *  Description:  This is the initialization function of database, and should be 
 *                called before any other database function. In this function, 
 *                all NV item in database will be checked.This function will set 
 *                file mode creation mask to 0.
 *        Parms:  None
 *       Return:
 *                NV_OK - Initialize database successfully
 *                NV_ERROR - Some error occured in the initialization process
 *        Notes:
 ********************************************************************************/

nv_errorcode nvmram_init(void);

/********************************************************************************
 *
 *         Name:  nvmram_readnv
 *  Description:  Read current value of a NV item
 *        Parms:
 *                nvname - NV item name
 *                buff - Buffer used to receive NV item value
 *                bufflen - Size of buff
 *       Return:
 *                NV_OK - Get NV value successfully
 *                NV_NOTFOUND - No such NV item in database
 *                NV_NOMEM - The buff length is not enough for current value
 *                NV_ERROR - There are some error on the database
 *        Notes:
 ********************************************************************************/

nv_errorcode nvmram_readnv(const lc_int8 *nvname, lc_int8 *buff, size_t bufflen);

/********************************************************************************
 *
 *         Name:  nvmram_writenv
 *  Description:  Update a NV item value.This function will set file mode creation
 *                mask to 0.
 *        Parms:
 *                nvname - NV item name
 *                buff - NV item value needs to write to database
 *                datalen - Length of NV item value, include terminal '\0'
 *       Return:
 *                NV_OK - Save the NV item value successfully
 *                NV_NOTFOUND - o such NV item in database
 *                NV_EINVAL - Invalid NV item value
 *                NV_ERROR - There are some error on the database
 *        Notes:
 ********************************************************************************/

nv_errorcode nvmram_writenv(const lc_int8 *nvname, const lc_int8 *buff, size_t datalen);

/********************************************************************************
 *
 *         Name:  nvmram_clear
 *  Description:  Remove all current NV items from database
 *        Parms:  NONE
 *       Return:
 *                NV_OK -- Always return NV_OK
 *        Notes:
 ********************************************************************************/

nv_errorcode nvmram_clear (void);

/********************************************************************************
*
*         Name:  nvram_match
*  Description:  match TMPCURRENTNV items
*        Parms:  const char *name, const char *value
*       Return:
*                NV_OK -- match return NV_OK
*        Notes:
********************************************************************************/

nv_errorcode nvram_match(const lc_int8 *name, const lc_int8 *value);

/********************************************************************************
*
*         Name:  nvmfactory_init
*  Description:  This is the initialization function of database, and should be 
*                called before any other database function. In this function, 
*                all NV item in database will be checked.This function will set 
*                file mode creation mask to 0.
*        Parms:  None
*       Return:
*                NV_OK - Initialize database successfully
*                NV_ERROR - Some error occured in the initialization process
*        Notes:
********************************************************************************/

nv_errorcode nvmfactory_init(void);

/********************************************************************************
*
*         Name:  nvmfactory_readnv
*  Description:  Read FACTORYCURRENTNV of a NV item
*        Parms:
*                nvname - NV item name
*                buff - Buffer used to receive NV item value
*                bufflen - Size of buff
*       Return:
*                NV_OK - Get NV value successfully
*                NV_NOTFOUND - No such NV item in database
*                NV_NOMEM - The buff length is not enough for current value
*                NV_ERROR - There are some error on the database
*        Notes:
********************************************************************************/

nv_errorcode nvmfactory_readnv(const lc_int8 *nvname, lc_int8 *buff, size_t bufflen);

/********************************************************************************
*
*         Name:  nvmfactory_writenv
*  Description:  Update a NV item value.This function will set file mode creation
*                mask to 0.
*        Parms:
*                nvname - NV item name
*                buff - NV item value needs to write to database
*                datalen - Length of NV item value, include terminal '\0'
*       Return:
*                NV_OK - Save the NV item value successfully
*                NV_NOTFOUND - o such NV item in database
*                NV_EINVAL - Invalid NV item value
*                NV_ERROR - There are some error on the database
*        Notes:
********************************************************************************/

nv_errorcode nvmfactory_writenv(const lc_int8 *nvname, const lc_int8 *buff, size_t datalen);

/********************************************************************************
*
*         Name:  nvmfactory_erase
*  Description:  Remove all current NV items from database
*        Parms:  NONE
*       Return:
*                NV_OK -- return NV_OK
*                NV_ERROR     -- return NV_ERROR
*        Notes:
********************************************************************************/

nv_errorcode nvmfactory_erase(void);

/********************************************************************************
*
*         Name:  nvmfactory_sync
*  Description:  sync all current NV items from database
*        Parms:  NONE
*       Return:
*                NV_OK -- return NV_OK
*                NV_ERROR     -- return NV_ERROR
*        Notes:
********************************************************************************/

nv_errorcode nvmfactory_sync(void);

/********************************************************************************
*
*         Name:  nvmfactory_destory
*  Description:  destory all current NV items from database
*        Parms:  NONE
*       Return:
*                NV_OK -- return NV_OK
*                NV_ERROR     -- return NV_ERROR
*        Notes:
********************************************************************************/

nv_errorcode nvmfactory_destory(void);

#endif //__NVMANAGER_H__

