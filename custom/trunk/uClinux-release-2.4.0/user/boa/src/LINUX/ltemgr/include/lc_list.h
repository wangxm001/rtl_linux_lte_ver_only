#ifndef __COS_LIST_H
#define __COS_LIST_H


/*****************************************************************
 *
 *
 *     Filename:  lc_list.h
 *
 *    Description:  this file package some  API of string oprition. 
 *
 *        Version:  1.0
 *        Created:  
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  daishenglong
 *      Copyright:  Xi'an LongQuest Technology LTD,all rights reserved
 *
 *******************************************************************/


#include <ctype.h>
#include <stdio.h>
#include <errno.h>
#include <netdb.h>
#include <syslog.h>
#include <stdlib.h>
#include <string.h>
#include "lc_common.h"

/*list error */
typedef enum list_err
{
    /*operation success*/
    LIST_SUCCESS=0,
    /*input parameter is error or the input parameter is null point*/
    LIST_PARAMETER_ERROR,
    /*malloc memory buf failed*/
    LIST_MEMORY_ERROR,
    /*the list is empty*/
    LIST_EMPTY_ERROR,
    /*the list only one node, can not do delete next node or delete
     * prev node operation*/
    LIST_ONLY_ONE_NODE_ERROR,
    /*to do delete prev node failed*/
    LIST_NODE_BEFORE_NULL_ERROR,
    /*delete next node failed*/
    LIST_NODE_AFTER_NULL_ERROR,
    /*create list head failed*/
    LIST_CREATE_HEAD_ERROR,
    /*match two node failed*/
    LIST_NODE_MATCH_ERROR,
    /* traverse list failed*/
    LIST_TRAVERSE_ERROR,
    /* add list node failed*/
    LIST_ADD_NODE_ERROR,
    /* delete list node failed*/
    LIST_DEL_NODE_ERROR,
    /*get the specify list node failed*/
    LIST_GET_NODE_ERROR,
    
}list_err;

/*list node struct*/
typedef struct list_node 
{
    /* add node_valide_flag for tiver */
    lc_int32 node_valid_flag;  
    /*the list node data buf*/
    void * data;
    /*the next node based on current node*/
    struct list_node *next;
    /*the previous node based on current node*/
    struct list_node *prev;
}list_node;


/*list head struct*/
typedef struct list_head
{
    /*list node number*/
    lc_int32 count;
    /* first node of the list*/
    list_node *first;
    /*last node of the list*/
    list_node *last;
}list_head;

/********************************************************************
 *
 *         Name:  match
 *  Description:  to compare two list node whether they are matched.
 *        Input: 
 *                 node_data : list node data pointer
 *                 data : this is an list node pointer or other data
 *                         pointer;
           
 *       Output:  
 *       Return: list_err
 *       Notes: other modules should implement this funcion for find
 *       one node .
********************************************************************/
typedef list_err (* match)(void * node_data, void * data) ;

/********************************************************************
 *
 *         Name:  data_free
 *  Description:  to free the list node data memory buf.
 *        Input: 
 *                 data: list node data pointer
 *       Output:  
 *       Return: list_err
 *       Notes: other modules should implement this funcion for find
 *       one node .
********************************************************************/
typedef list_err (* data_free)(void * data) ;


/********************************************************************
 *
 *         Name:  list_traverse_process
 *  Description: when to traverse list , this function will be used 
 *               to process list node
 *        Input: 
 *                snode : list node;
 *                data : reserver for other function;
 *       Output:  
 *       Return: list_err  . If return value is not LIST_SUCCESS, then
 *                will stop traverse .
 *       Notes: other modules should implement this funcion for traverse 
 *              process .
********************************************************************/
typedef list_err (* list_traverse_process)(list_node * node, void * res_data);


/********************************************************************
 *
 *         Name:  lc_list_create
 *  Description:  to create a list head.
 *        Input: NULL
 *       Output: NULL  
 *       Return: list head
 *       Notes: If the return value is an NULL pointer , then create 
 *       list head failed
 *              
********************************************************************/
list_head * lc_list_create (void);

/********************************************************************
 *
 *         Name:  lc_list_add_at_head
 *  Description: insert list node at list head.
 *        Input: 
 *               node: list node , before insert it , user should
 *                         create a list node.
 *                head: list head, before inset list node, the list 
 *                          head should had been created
 *       Output: NULL  
 *       Return: list_err
 *       Notes:
 *              
********************************************************************/
list_err lc_list_add_at_head(list_head *head,list_node *node);

/********************************************************************
 *
 *         Name:  lc_list_add_at_tail
 *  Description: insert list node at list tail.
 *        Input: 
 *               node: list node , before insert it , user should
 *                         create a list node.
 *                head: list head, before inset list node, the list 
 *                          head should had been created
 *       Output: NULL  
 *       Return: list_err
 *       Notes:
 *              
********************************************************************/
list_err lc_list_add_at_tail(list_head *head,list_node *node);

/********************************************************************
 *
 *         Name:  lc_list_add_before
 *  Description: insert list node before the specify node.
 *        Input: 
 *                 cur_node: the specify node
 *               node: insert node , before insert it , user should
 *                         create it.
 *                head: list head, before inset node, the list 
 *                          head should had been created
 *       Output: NULL  
 *       Return: list_err
 *       Notes:
 *              
********************************************************************/
list_err lc_list_add_before(list_head *head,list_node *cur_node,list_node *node);

/********************************************************************
 *
 *         Name:  lc_list_add_before
 *  Description: insert list node behind the specify node.
 *        Input: 
 *                 cur_node: the specify node
 *               node: insert node , before insert it , user should
 *                         create it.
 *                head: list head, before inset node, the list 
 *                          head should had been created
 *       Output: NULL  
 *       Return: list_err
 *       Notes:
 *              
********************************************************************/
list_err lc_list_add_next(list_head *head,list_node *cur_node,list_node *node);


/********************************************************************
 *
 *         Name:  lc_list_find
 *  Description: to search for the target node on  list .
 *        Input: 
 *                head: list head,
 *                find_match: user define  the find_match function .
 *                data: match data, this can be a list node pointer or 
 *                other data    pointer; 
 *       Output: NULL  
 *       Return: list_node
 *       Notes:
 *              
********************************************************************/
list_node * lc_list_find(list_head *head,match find_match,void *data);


/********************************************************************
 *
 *         Name:  lc_list_tarverse
 *  Description: to traverse  list .
 *        Input: 
 *                head: list head,
 *                traverse_process: user define traverse process function .
 *                res_data: reserver for other function. If user can input
 *                        other parameter to traverse_process function by 
 *                        res_data
 *                        
 *       Output: NULL  
 *       Return: list_err
 *       Notes: you can 
 *              
********************************************************************/
list_err lc_list_tarverse(list_head *head,list_traverse_process 
                          traverse_process,void * res_data);

/********************************************************************
 *
 *         Name:  lc_list_del_from_head
 *  Description: delete the first node from list .
 *        Input: 
 *                     head: list head
 *       Output: ret_err: list_err   
 *       Return: list_node *: success
 *               NULL: error.
 *       Notes: The user must free the list node which lc_list_del_from
 *              head() function returned
 *              
********************************************************************/
list_node * lc_list_del_from_head(list_head *head,lc_int32 * ret_err);



/********************************************************************
 *
 *         Name:  lc_list_del_from_tail
 *  Description: delete the tail node of list .
 *        Input: 
 *                     head: list head
 *                     
 *       Output:   ret_err: list_err     
 *       Return: list_node * : success
 *                NULL:  failed
 *       Notes: The user must free the list node
 *              
********************************************************************/
list_node * lc_list_del_from_tail(list_head *head,lc_int32 * ret_err);


/********************************************************************
 *
 *         Name:  lc_list_del_prev
 *  Description: delete the specify before node from list .
 *        Input:
 *                     head: list head
 *                     node:specify node
 *                     
 *       Output:  ret_err: list_err    
 *       Return: list_node * :SUCCESS
 *                NULL:failed
 *       Notes: User must free the list node
 *              
********************************************************************/
list_node * lc_list_del_prev(list_head *head,list_node *node,lc_int32 * ret_err);


/********************************************************************
 *
 *         Name:  lc_list_del_next
 *  Description: delete the node which behind the specify node of list .
 *        Input: 
 *                   node:specify node
 *                     head: list head
 *                     
 *       Output: ret_err: list_err    
 *       Return: list_node *: SUCCESS
 *               NULL: failed
 *       Notes: User must free the list node.
 *              
********************************************************************/
list_node * lc_list_del_next(list_head *head,list_node *node,lc_int32 * ret_err);


/********************************************************************
 *
 *         Name:  lc_list_del_node
 *  Description: delete the specify node from list .
 *        Input: 
 *                   node:specify node
 *                     head: list head
 *    
 *       Return: list_err:
 *       Notes: User must free this list node.
 *              
********************************************************************/
list_err  lc_list_del_node(list_head *head,list_node *node);

/********************************************************************
 *
 *         Name:  lc_list_query_num
 *  Description: get list node number .
 *        Input:    
 *                head: list head
 *       Output: NULL  
 *       Return: list_err
 *       Notes:  
 *              
********************************************************************/
lc_int32 lc_list_query_num (list_head *head);



/********************************************************************
 *
 *         Name:  lc_list_get_node
 *  Description: get the specify node.
 *        Input:    
 *                head: list head
 *                node_index:node index:start form 0
 *                    
 *       Output: ret_err: list_err  
 *       Return: list_node
 *       Notes:  
 *              
********************************************************************/
list_node * lc_list_get_node (list_head *head,lc_int32 node_index,lc_int32 * ret_err);


/********************************************************************
 *
 *         Name:  lc_list_destroy
 *  Description: destroy list .
 *        Input:    
 *                head: list head
 *                 node_data_free:this funciton used to release 
 *                     the list node data. User should implement 
 *                     this funciont based on the list node.
 *       Output: NULL  
 *       Return: list_err
 *       Notes:  
 *              
********************************************************************/
list_err lc_list_destroy (list_head *head,data_free node_data_free);


#endif

