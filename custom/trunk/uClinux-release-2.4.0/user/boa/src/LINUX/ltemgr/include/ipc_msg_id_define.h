/******************************************************************************
 *
 *       Filename:  ipc_msd_id_define.h
 *
 *    Description:  IPC消息ID定义
 *
 *        Version:  1.0
 *        Created:  05/21/2010 10:03:24 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  徐扬   Xu Yang (Xuy),
 *        Company:  Xi'an LongQuest Technology LTD
 *      Copyright:  Copyright (c) 2010, Xu Yang, all rigths reserved.
 *
 *****************************************************************************/

#ifndef IPC_MSG_ID_DEFINE_H
#define IPC_MSG_ID_DEFINE_H

// 用于判断进程间消息所属模块的掩码
// 逻辑式
//   XX_MSG_ID_START == msg_id && IPC_APP_MSG_MASK
// 成立即表明msg_id消息属于模块XX
#define IPC_APP_MSG_MASK         0xFFF00000

// IPC消息ID起始
#define IPC_MSG_ID_BASE          0x00000000

// IPC底层保留消息
#define IPC_ID_INNER_START       IPC_MSG_ID_BASE
// IPC消息接收线程停止消息
#define IPC_ID_RECV_THREAD_STOP  (IPC_MSG_ID_BASE + 0x001)

// IPC通知注册消息
#define IPC_ID_NOTIFY_REG        (IPC_MSG_ID_BASE + 0x002)
// IPC通知反注册消息
#define IPC_ID_NOTIFY_DEREG      (IPC_MSG_ID_BASE + 0x003)

#define IPC_ID_INNER_END         (IPC_ID_INNER_START + 0x10000000 - 1)



/****************** 模块进程间消息ID定义开始 *********************************/
/************ 每模块拥有低5个十六进制数的消息ID空间 **************************/

// System Control消息ID起始值
#define SC_MSG_ID_START IPC_ID_INNER_END + 1
#define SC_STATE_CHANGE_REQ            (SC_MSG_ID_START+0x1)//状态切换请求
#define SC_STATE_CHANGE_RSP            (SC_MSG_ID_START+0x2)//状态切换响应
#define SC_POWERON_NOTIFY              (SC_MSG_ID_START+0x3)//app上电通知
#define SC_POWEROFF_REQ                (SC_MSG_ID_START+0x4)//关电请求
#define SC_POWEROFF_RSP                (SC_MSG_ID_START+0x5)//关电请求
#define SC_PRE_STATE_CHANGE_REQ        (SC_MSG_ID_START+0x6)//pre state change request
#define SC_STATE_QUERY_REQ             (SC_MSG_ID_START+0x7)//状态查询请求
#define SC_STATE_QUERY_RSP             (SC_MSG_ID_START+0x8)//状态查询响应
#define SC_REGISTER_TIME_OUT           (SC_MSG_ID_START+0x9)//注册定时器超时
#define SC_PET_DOG_TIME_OUT            (SC_MSG_ID_START+0xa)//喂狗定时器超时
#define SC_SETTING_RESET_REQ           (SC_MSG_ID_START+0xb)//router setting reset request
#define SC_SETTING_RESET_RSP           (SC_MSG_ID_START+0xc)//router setting reset response
#define SC_SET_WORK_MODE_REQ           (SC_MSG_ID_START+0xd)//set sys work mode resquest
#define SC_SET_WORK_MODE_RSP           (SC_MSG_ID_START+0xe)//set sys work mode response
#define SC_STATE_CHANGE_FAILED_NOTIFY  (SC_MSG_ID_START+0xf)//nodify an state change failed event
#define SC_SETTING_RESET_NOTIFY        (SC_MSG_ID_START+0x10)//nodify an state change failed event
#define SC_POLL_SLEEP_NOTIFY                  (SC_MSG_ID_START+0x11)  
#define SC_MSG_LAST                    (SC_MSG_ID_START+0x12)//最后一条有效     
#define SC_SLEEP_TIME_OUT              (SC_MSG_ID_START+0x13) 
#define SC_SYS_WORK_MODE_SLEEP         (SC_MSG_ID_START+0x14) 
#define SC_SYS_ANTENNA_MODE_SET_REQ    (SC_MSG_ID_START+0x15) 
#define SC_SYS_ANTENNA_MODE_SET_RSP    (SC_MSG_ID_START+0x16) 
#define SC_MSG_END                     (SC_MSG_ID_START+0x64)
// System Control消息ID结束值
#define SC_MSG_ID_END   SC_MSG_ID_START + 0x00100000 - 1

/************** Device Manager模块进程间消息ID定义开始 ************************/
// 用于判断Device Manager子模块消息的掩码
#define DM_MSG_SUB_MASK  0x0001F000
// 用于判断Device Manager请求消息的掩码
#define DM_MSG_REQ_MASK  0x00020000
// 用于判断Device Manager响应消息的掩码
#define DM_MSG_RSP_MASK  0x00040000
// 用于判断Device Manager通知消息的掩码
#define DM_MSG_NOTIFY_MASK  0x00080000
// Device Manager消息ID起始值
#define DM_MSG_ID_START SC_MSG_ID_END + 1

// 子模块消息ID起始值偏移量为DM头文件中子模块定义的枚举值
// Battery Manager子模块消息ID起始值
#define DM_BATT_MGR_MSG_ID_START \
(DM_MSG_ID_START + (0x01 << 12))
/*         Name:  电量等级获取请求响应
 *  Description:  获取当前电量等级。
 *                同时可以获取到有无电池，电量实际百分比和当前的电量警告等级。
 *      Request:
 *                无
 *     Response:
 *                dm_batt_cap_get_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  无
 */
#define DM_BATT_CAP_LV_GET_REQ \
((DM_BATT_MGR_MSG_ID_START + 0x001) | DM_MSG_REQ_MASK)
#define DM_BATT_CAP_LV_GET_RSP \
((DM_BATT_MGR_MSG_ID_START + 0x002) | DM_MSG_RSP_MASK)
/*         Name:  电池信息获取请求响应
 *  Description:  获取电池当前各项数值信息。
 *                同时可以获取到有无电池。
 *      Request:
 *                无
 *     Response:
 *                dm_batt_info_get_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  无
 */
#define DM_BATT_INFO_GET_REQ \
((DM_BATT_MGR_MSG_ID_START + 0x003) | DM_MSG_REQ_MASK)
#define DM_BATT_INFO_GET_RSP \
((DM_BATT_MGR_MSG_ID_START + 0x004) | DM_MSG_RSP_MASK)
/*         Name:  阈值设置请求响应
 *  Description:  设置电池警告的阈值。
 *                可以设置电量警告的阈值，阈值将写入NV。
 *      Request:
 *                dm_batt_thrs_set_req_t
 *     Response:
 *                dm_batt_thrs_set_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_INVALID_PARA  // 无效参数
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  无
 */
#define DM_BATT_THRS_SET_REQ \
((DM_BATT_MGR_MSG_ID_START + 0x005) | DM_MSG_REQ_MASK)
#define DM_BATT_THRS_SET_RSP \
((DM_BATT_MGR_MSG_ID_START + 0x006) | DM_MSG_RSP_MASK)
/*         Name:  阈值获取请求响应
 *  Description:  获取电池警告的阈值。
 *                可以获取电量警告的阈值，阈值读取自NV。
 *      Request:
 *                dm_batt_thrs_get_req_t
 *     Response:
 *                dm_batt_thrs_get_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_INVALID_PARA  // 无效参数
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  无
 */
#define DM_BATT_THRS_GET_REQ \
((DM_BATT_MGR_MSG_ID_START + 0x007) | DM_MSG_REQ_MASK)
#define DM_BATT_THRS_GET_RSP \
((DM_BATT_MGR_MSG_ID_START + 0x008) | DM_MSG_RSP_MASK)
/*         Name:  充电状态获取请求响应
 *  Description:  获取当前充电状态。
 *                同时可以获取到有无电池。
 *      Request:
 *                无
 *     Response:
 *                dm_batt_chg_stat_get_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  无
 */
#define DM_BATT_CHG_STAT_GET_REQ \
((DM_BATT_MGR_MSG_ID_START + 0x009) | DM_MSG_REQ_MASK)
#define DM_BATT_CHG_STAT_GET_RSP \
((DM_BATT_MGR_MSG_ID_START + 0x00A) | DM_MSG_RSP_MASK)
/*         Name:  电池电量等级变化通知
 *  Description:  电量等级发生变化时的通知。
 *       Notify:
 *                dm_batt_cap_lv_notify_t
 *        Notes:  无
 */
#define DM_BATT_CAP_LV_NOTIFY \
((DM_BATT_MGR_MSG_ID_START + 0x801) | DM_MSG_NOTIFY_MASK)
/*         Name:  电池电量警告等级变化通知
 *  Description:  电量警告等级发生变化时的通知。
 *       Notify:
 *                dm_batt_cap_warn_notify_t
 *        Notes:  无
 */
#define DM_BATT_CAP_WARN_NOTIFY \
((DM_BATT_MGR_MSG_ID_START + 0x802) | DM_MSG_NOTIFY_MASK)
/*         Name:  电池充电状态变化通知
 *  Description:  充电状态发生变化时的通知。
 *       Notify:
 *                dm_batt_chg_stat_notify_t
 *        Notes:  无
 */
#define DM_BATT_CHG_STAT_NOTIFY \
((DM_BATT_MGR_MSG_ID_START + 0x803) | DM_MSG_NOTIFY_MASK)

/*         Name:  sleep warkup notify
 *  Description:  sleep warkup notify
 *       Notify:
 *                dm_rtc_alarm_notify_t
 *        Notes:  null
 */
#define DM_RTC_ALARM_NOTIFY \
((DM_BATT_MGR_MSG_ID_START + 0x804) | DM_MSG_NOTIFY_MASK)

// Button Manager子模块消息ID起始值
#define DM_BTN_MGR_MSG_ID_START \
(DM_MSG_ID_START + (0x02 << 12))
/*         Name:  Button状态获取请求响应
 *  Description:  获取指定Button的当前状态。
 *      Request:
 *                dm_btn_stat_get_req_t
 *     Response:
 *                dm_btn_stat_get_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_INVALID_PARA  // 无效参数
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  无
 */
#define DM_BTN_STAT_GET_REQ \
((DM_BTN_MGR_MSG_ID_START + 0x001) | DM_MSG_REQ_MASK)
#define DM_BTN_STAT_GET_RSP \
((DM_BTN_MGR_MSG_ID_START + 0x002) | DM_MSG_RSP_MASK)
/*         Name:  Button down事件通知
 *  Description:  按钮按键按下/拨档开关打开时的通知。
 *       Notify:
 *                dm_btn_action_notify_t
 *        Notes:  无
 */
#define DM_BTN_DOWN_NOTIFY \
((DM_BTN_MGR_MSG_ID_START + 0x801) | DM_MSG_NOTIFY_MASK)
/*         Name:  Button up事件通知
 *  Description:  按钮按键弹起/拨档开关关闭时的通知。
 *       Notify:
 *                dm_btn_action_notify_t
 *        Notes:  无
 */
#define DM_BTN_UP_NOTIFY \
((DM_BTN_MGR_MSG_ID_START + 0x802) | DM_MSG_NOTIFY_MASK)
/*         Name:  Button Short事件通知
 *  Description:  按钮按键短按时的通知。
 *       Notify:
 *                dm_btn_action_notify_t
 *        Notes:  无
 */
#define DM_BTN_SHORT_NOTIFY \
((DM_BTN_MGR_MSG_ID_START + 0x803) | DM_MSG_NOTIFY_MASK)
/*         Name:  Button press事件通知
 *  Description:  按钮按键长按时的通知。
 *       Notify:
 *                dm_btn_action_notify_t
 *        Notes:  无
 */
#define DM_BTN_PRESS_NOTIFY \
((DM_BTN_MGR_MSG_ID_START + 0x804) | DM_MSG_NOTIFY_MASK)
/*         Name:  Button double click事件通知
 *  Description:  按钮按键双击时的通知。
 *       Notify:
 *                dm_btn_action_notify_t
 *        Notes:  无
 */
#define DM_BTN_DCLICK_NOTIFY \
((DM_BTN_MGR_MSG_ID_START + 0x805) | DM_MSG_NOTIFY_MASK)
/*         Name:  Button Combo事件通知
 *  Description:  按钮与功能按钮同时按下的通知。
 *       Notify:
 *                dm_btn_action_notify_t
 *        Notes:  无
 */
#define DM_BTN_COMBO_NOTIFY \
((DM_BTN_MGR_MSG_ID_START + 0x806) | DM_MSG_NOTIFY_MASK)


#define DM_SLIC_EVENT_NOTIFY \
  ((DM_BTN_MGR_MSG_ID_START + 0x807) | DM_MSG_NOTIFY_MASK)
                                            // LED Manager子模块消息ID起始值
#define DM_LED_MGR_MSG_ID_START \
(DM_MSG_ID_START + (0x03 << 12))
/*         Name:  单LED灯操作请求响应
 *  Description:  对指定的单个LED灯进行操作。
 *      Request:
 *                dm_led_op_req_t
 *     Response:
 *                dm_led_op_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_INVALID_PARA  // 无效参数
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_BUSY          // DM进程无法处理本次请求
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  单LED灯的操作优先级低于模式操作。如果指定的LED灯正在参与模式
 *                则会返回忙。
 */
#define DM_LED_OP_REQ \
((DM_LED_MGR_MSG_ID_START + 0x001) | DM_MSG_REQ_MASK)
#define DM_LED_OP_RSP \
((DM_LED_MGR_MSG_ID_START + 0x002) | DM_MSG_RSP_MASK)
/*         Name:  LED灯模式操作请求响应
 *  Description:  依照指定模式对LED灯进行操作。
 *      Request:
 *                dm_led_op_req_t
 *     Response:
 *                dm_led_op_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_INVALID_PARA  // 无效参数
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_BUSY          // DM进程无法处理本次请求
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  此接口将在模式成功开始后立刻返回。
 */
#define DM_LED_MODE_REQ \
((DM_LED_MGR_MSG_ID_START + 0x003) | DM_MSG_REQ_MASK)
#define DM_LED_MODE_RSP \
((DM_LED_MGR_MSG_ID_START + 0x004) | DM_MSG_RSP_MASK)

// Power Manager子模块消息ID起始值
#define DM_PWR_MGR_MSG_ID_START \
DM_MSG_ID_START + (0x04 << 12)
/*         Name:  充电器类型获取请求响应
 *  Description:  获取当前充电器的类型。
 *      Request:
 *                无
 *     Response:
 *                dm_pwr_charger_type_get_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  无
 */
#define DM_PWR_CHARGER_TYPE_GET_REQ \
((DM_PWR_MGR_MSG_ID_START + 0x001) | DM_MSG_REQ_MASK)
#define DM_PWR_CHARGER_TYPE_GET_RSP \
((DM_PWR_MGR_MSG_ID_START + 0x002) | DM_MSG_RSP_MASK)
/*         Name:  模块电源操作请求响应
 *  Description:  对模块的电源进行操作。
 *      Request:
 *                dm_pwr_mod_op_req_t
 *     Response:
 *                dm_pwr_mod_op_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_INVALID_PARA  // 无效参数
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  无
 */
#define DM_PWR_MOD_OP_REQ \
((DM_PWR_MGR_MSG_ID_START + 0x003) | DM_MSG_REQ_MASK)
#define DM_PWR_MOD_OP_RSP \
((DM_PWR_MGR_MSG_ID_START + 0x004) | DM_MSG_RSP_MASK)
/*         Name:  充电器类型变化通知
 *  Description:  充电器类型发生变化时的通知。
 *       Notify:
 *                dm_pwr_charger_type_notify_t
 *        Notes:  无
 */
#define DM_PWR_CHARGER_TYPE_NOTIFY \
((DM_PWR_MGR_MSG_ID_START + 0x801) | DM_MSG_NOTIFY_MASK)

// Sound Manager子模块消息ID起始值
#define DM_SND_MGR_MSG_ID_START \
(DM_MSG_ID_START + (0x05 << 12))
/*         Name:  蜂鸣器单独操作请求响应
 *  Description:  对蜂鸣器进行单独操作。
 *      Request:
 *                dm_snd_beep_op_req_t
 *     Response:
 *                dm_snd_beep_op_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_INVALID_PARA  // 无效参数
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_BUSY          // DM进程无法处理本次请求
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  蜂鸣器单独的操作优先级低于模式操作。如果蜂鸣器正在执行模式
 *                则会返回忙。
 */
#define DM_SND_BEEP_OP_REQ \
((DM_SND_MGR_MSG_ID_START + 0x001) | DM_MSG_REQ_MASK)
#define DM_SND_BEEP_OP_RSP \
((DM_SND_MGR_MSG_ID_START + 0x002) | DM_MSG_RSP_MASK)
/*         Name:  蜂鸣器模式操作请求响应
 *  Description:  依照指定模式对蜂鸣器进行操作。
 *      Request:
 *                dm_snd_beep_mode_req_t
 *     Response:
 *                dm_snd_beep_mode_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_INVALID_PARA  // 无效参数
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_BUSY          // DM进程无法处理本次请求
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  此接口将在模式成功开始后立刻返回。
 */
#define DM_SND_BEEP_MODE_REQ \
((DM_SND_MGR_MSG_ID_START + 0x003) | DM_MSG_REQ_MASK)
#define DM_SND_BEEP_MODE_RSP \
((DM_SND_MGR_MSG_ID_START + 0x004) | DM_MSG_RSP_MASK)

// Storage Manager子模块消息ID起始值
#define DM_STG_MGR_MSG_ID_START \
(DM_MSG_ID_START + (0x06 << 12))
/*         Name:  外部扩展存储设备信息获取请求响应
 *  Description:  获取当前存在的外部扩展存储设备信息。
 *      Request:
 *                无
 *     Response:
 *                dm_stg_info_get_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  无
 */
#define DM_STG_INFO_GET_REQ \
((DM_STG_MGR_MSG_ID_START + 0x001) | DM_MSG_REQ_MASK)
#define DM_STG_INFO_GET_RSP \
((DM_STG_MGR_MSG_ID_START + 0x002) | DM_MSG_RSP_MASK)
/*         Name:  外部扩展存储设备变化通知
 *  Description:  外部扩展存储设备发生变化时的通知。
 *       Notify:
 *                dm_stg_plug_notify_t
 *        Notes:  无
 */
#define DM_STG_PLUG_NOTIFY \
((DM_STG_MGR_MSG_ID_START + 0x801) | DM_MSG_NOTIFY_MASK)
/*         Name:  外部扩展存储设备变化通知（DM内部使用）
 *  Description:  外部扩展存储设备发生变化时的通知。
 *                由DM命令行通知给DM守护进程
 *       Notify:
 *                dm_stg_dev_inner_info_t
 *        Notes:  其他模块不需注册和处理此通知
 */
#define DM_STG_PLUG_INNER_NOTIFY \
((DM_STG_MGR_MSG_ID_START + 0x802) | DM_MSG_NOTIFY_MASK)

// Temperature Manager子模块消息ID起始值
#define DM_TEMP_MGR_MSG_ID_START \
(DM_MSG_ID_START + (0x07 << 12))
/*         Name:  温度信息获取请求响应
 *  Description:  获取当前温度信息。
 *                可以同时获取到警告等级和具体温度值。
 *      Request:
 *                无
 *     Response:
 *                dm_temp_info_get_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  无
 */
#define DM_TEMP_INFO_GET_REQ \
((DM_TEMP_MGR_MSG_ID_START + 0x001) | DM_MSG_REQ_MASK)
#define DM_TEMP_INFO_GET_RSP \
((DM_TEMP_MGR_MSG_ID_START + 0x002) | DM_MSG_RSP_MASK)
/*         Name:  阈值设置请求响应
 *  Description:  设置温度警告的阈值。
 *                可以设置温度警告的阈值，阈值将写入NV。
 *      Request:
 *                dm_temp_thrs_set_req_t
 *     Response:
 *                dm_temp_thrs_set_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_INVALID_PARA  // 无效参数
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  无
 */
#define DM_TEMP_THRS_SET_REQ \
((DM_TEMP_MGR_MSG_ID_START + 0x003) | DM_MSG_REQ_MASK)
#define DM_TEMP_THRS_SET_RSP \
((DM_TEMP_MGR_MSG_ID_START + 0x004) | DM_MSG_RSP_MASK)
/*         Name:  阈值获取请求响应
 *  Description:  获取温度警告的阈值。
 *                可以获取温度警告的阈值，阈值读取自NV。
 *      Request:
 *                dm_batt_thrs_set_req_t
 *     Response:
 *                dm_batt_thrs_set_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_INVALID_PARA  // 无效参数
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  无
 */
#define DM_TEMP_THRS_GET_REQ \
((DM_TEMP_MGR_MSG_ID_START + 0x005) | DM_MSG_REQ_MASK)
#define DM_TEMP_THRS_GET_RSP \
((DM_TEMP_MGR_MSG_ID_START + 0x006) | DM_MSG_RSP_MASK)
/*         Name:  温度警告等级变化通知
 *  Description:  温度警告等级发生变化时的通知。
 *       Notify:
 *                dm_temp_warn_notify_t
 *        Notes:  无
 */
#define DM_TEMP_WARN_NOTIFY \
((DM_TEMP_MGR_MSG_ID_START + 0x800) | DM_MSG_NOTIFY_MASK)

#define DM_TEMP_CHANGE_NOTIFY \
((DM_TEMP_MGR_MSG_ID_START + 0x801) | DM_MSG_NOTIFY_MASK)

#define DM_ETH_LINK_STATE_NOTIFY \
((DM_TEMP_MGR_MSG_ID_START + 0x802) | DM_MSG_NOTIFY_MASK)
// USB Manager子模块消息ID起始值
#define DM_USB_MGR_MSG_ID_START \
(DM_MSG_ID_START + (0x08 << 12))
/*         Name:  USB设备信息获取请求响应
 *  Description:  获取当前存在的USB设备信息。
 *      Request:
 *                dm_usb_info_get_req_t
 *     Response:
 *                dm_usb_info_get_rsp_t
 *       Return:
 *                DM_OK            // 成功
 *                DM_UNSUPPORT     // 不支持的接口消息
 *                DM_INVALID_PARA  // 无效参数
 *                DM_NOT_STABLE    // DM进程未处于一个稳定可用的状态
 *                DM_INNER_ERR     // DM进程内部其他错误
 *        Notes:  无
 */
#define DM_USB_INFO_GET_REQ \
((DM_USB_MGR_MSG_ID_START + 0x001) | DM_MSG_REQ_MASK)
#define DM_USB_INFO_GET_RSP \
((DM_USB_MGR_MSG_ID_START + 0x002) | DM_MSG_RSP_MASK)

/* added by liujunan for sd share mode:ucard or web share */
#define DM_USB_MODE_NOTIFY \
((DM_USB_MGR_MSG_ID_START + 0x003) | DM_MSG_REQ_MASK)
/*pacino.cao 2012-0504 support force power off and recovery state*/
/*         Name:  USB Commands 
 *  Description:  USB Commands notify  
 *       Notify:
 *                NULL
 *        Notes:  无
 */
#define DM_USB_CMD_FORCE_POWER_OFF_NOTIFY \
((DM_USB_MGR_MSG_ID_START + 0x701) | DM_MSG_NOTIFY_MASK)
#define DM_USB_CMD_FORCE_RECOVERY_NOTIFY \
((DM_USB_MGR_MSG_ID_START + 0x702) | DM_MSG_NOTIFY_MASK)


/*         Name:  USB设备变化通知
 *  Description:  USB设备发生变化时的通知。
 *       Notify:
 *                dm_usb_plug_notify_t
 *        Notes:  无
 */
#define DM_USB_PLUG_NOTIFY \
((DM_USB_MGR_MSG_ID_START + 0x801) | DM_MSG_NOTIFY_MASK)
/*
#define DM_ETH_LINK_STATE_NOTIFY \
    ((DM_USB_MGR_MSG_ID_START + 0x802) | DM_MSG_NOTIFY_MASK)
*/    
// Device Manager消息ID结束值
#define DM_MSG_ID_END   DM_MSG_ID_START + 0x00100000 - 1
/************** Device Manager模块进程间消息ID定义结束 ************************/

// User Interface Manager消息ID起始值
#define UI_MSG_ID_START DM_MSG_ID_END + 1

#define UI_WEBNV_CHANGE_REQ    (UI_MSG_ID_START + 1)      //web nv change req
#define UI_WEB_SETUP_REQ       (UI_MSG_ID_START + 2)      //web setup req
#define UI_FACTORY_REQ         (UI_MSG_ID_START + 3)      //factory opt req
#define UI_UPGRADE_REQ         (UI_MSG_ID_START + 4)      //upgrade opt req
#define UI_WIFI_NOTIFY         (UI_MSG_ID_START + 5)      //wifi button notify
#define UI_TIMEROUT_SC_NOTIFY  (UI_MSG_ID_START + 6)      //ui_timerout sc_notify
#define  UI_INNER_STATE_QUERY_REQ  (UI_MSG_ID_START + 7)  //ui_inner_state_query_t
#define  UI_INNER_BLACKLIGHT_QUERY_REQ  (UI_MSG_ID_START + 8)  
#define  UI_INNER_DOG_PET          (UI_MSG_ID_START + 9)           

// User Interface Manager消息ID结束值
#define UI_MSG_ID_END   UI_MSG_ID_START + 0x00100000 - 1
/************** GUI Manager模块进程间消息ID定义结束 ************************/

// Network Manager消息ID起始值
#define NM_MSG_ID_START UI_MSG_ID_END + 1

#define    NM_MSG_WAN_UP_REQ       NM_MSG_ID_START +1
#define    NM_MSG_WAN_UP_RSP       NM_MSG_ID_START +2
#define    NM_MSG_WAN_DOWN_REQ     NM_MSG_ID_START +3
#define    NM_MSG_WAN_DOWN_RSP     NM_MSG_ID_START +4
#define    NM_MSG_WAN_DO_REQ       NM_MSG_ID_START +5
#define    NM_MSG_WAN_DO_RSP       NM_MSG_ID_START +6
#define    NM_MSG_USER_NUM_REQ     NM_MSG_ID_START +7
#define    NM_MSG_USER_NUM_RSP     NM_MSG_ID_START +8
#define    NM_MSG_USER_NUM_NOTIFY  NM_MSG_ID_START +9
#define    NM_MSG_STOP_NOTTFY      NM_MSG_ID_START +10
#define    NM_MSG_WDTIMER_OUT      NM_MSG_ID_START +11
#define    NM_MSG_WPS_EVENT_NOTIFY NM_MSG_ID_START +12
#define    NM_MSG_LAN_UP_NOTIFY    NM_MSG_ID_START +13
#define    NM_MSG_WPS_EVENT_GET_REQ NM_MSG_ID_START +14
#define    NM_MSG_WPS_EVENT_GET_RSP NM_MSG_ID_START +15
#define    NM_MSG_WPS_STATUS_REPORT_MSG NM_MSG_ID_START+16

//#define    NM_MSG_LAN_UP_NOTIFY     NM_MSG_ID_START +17
#define    NM_MSG_LAN_DOWN_NOTIFY   NM_MSG_ID_START +18
#define    NM_MSG_LAN_CHANGE_NOTIFY NM_MSG_ID_START +19

// Networ Manager消息ID结束值
#define NM_MSG_ID_END   NM_MSG_ID_START + 0x00100000 - 1

// WAN Manager消息ID起始值
#define WM_MSG_ID_START NM_MSG_ID_END + 1
//获取当前无线模块信息请求

//WAN¿¿¿¿¿
#define WM_MSG_WMMSG_BEGIN WM_MSG_ID_START
//¿¿¿¿ WMM¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿
#define WM_MSG_CONNECT_REQ (WM_MSG_WMMSG_BEGIN + 1)
//¿¿¿¿¿ ¿¿¿¿¿¿¿¿¿¿
#define WM_MSG_CONNECT_RSP (WM_MSG_WMMSG_BEGIN + 2)
//¿¿¿¿ WMM¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿
#define WM_MSG_DISCONNECT_REQ (WM_MSG_WMMSG_BEGIN + 3)
//¿¿¿¿¿¿¿¿¿¿
#define WM_MSG_DISCONNECT_RSP (WM_MSG_WMMSG_BEGIN + 4)
//¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿¿WMM,WMM¿notify UI
#define WM_MSG_IP_UPDOWN_NOTIFY (WM_MSG_WMMSG_BEGIN + 5)
//UI¿¿¿¿¿¿¿¿¿¿¿ETH, WIFI, WCDMA
#define WM_MSG_NET_MODE_GET_REQ (WM_MSG_WMMSG_BEGIN + 6)
//wmm ¿¿UI¿¿¿¿¿¿¿¿¿¿
#define WM_MSG_NET_MODE_GET_RSP (WM_MSG_WMMSG_BEGIN + 7)
//¿¿¿¿¿¿¿¿
#define WM_MSG_NET_MODE_SET_REQ (WM_MSG_WMMSG_BEGIN + 8)
//¿¿¿¿¿¿¿¿
#define WM_MSG_NET_MODE_SET_RSP (WM_MSG_WMMSG_BEGIN +9)
//¿¿¿¿¿¿¿¿
#define WM_MSG_NET_STATUS_NOTIFY (WM_MSG_WMMSG_BEGIN + 10)
//¿¿¿¿¿¿¿¿
#define WM_MSG_NET_STATUS_REQ (WM_MSG_WMMSG_BEGIN + 11)
//¿¿¿¿¿¿¿¿
#define WM_MSG_NET_STATUS_RSP (WM_MSG_WMMSG_BEGIN + 12)
#define WM_MSG_DIAL_TYPE_SET_REQ (WM_MSG_WMMSG_BEGIN + 13)
//¿¿WCDMA¿¿¿¿¿¿
#define WM_MSG_DIAL_TYPE_SET_RSP (WM_MSG_WMMSG_BEGIN + 14)
//¿¿WCDMA¿¿¿¿¿¿
#define WM_MSG_DIAL_TYPE_GET_REQ (WM_MSG_WMMSG_BEGIN + 15)
//¿¿WCDMA¿¿¿¿¿¿
#define WM_MSG_DIAL_TYPE_GET_RSP (WM_MSG_WMMSG_BEGIN + 16)
//¿¿WCDMA¿¿¿¿¿¿
#define WM_MSG_AUTOCONNECT_SET_REQ (WM_MSG_WMMSG_BEGIN + 17)
//¿¿WCDMA¿¿¿¿¿¿
#define WM_MSG_AUTOCONNECT_SET_RSP (WM_MSG_WMMSG_BEGIN + 18)
//¿¿WCDMA¿¿¿¿¿¿
#define WM_MSG_AUTOCONNECT_GET_REQ (WM_MSG_WMMSG_BEGIN + 19)
//¿¿WCDMA¿¿¿¿¿¿
#define WM_MSG_AUTOCONNECT_GET_RSP (WM_MSG_WMMSG_BEGIN + 20)

#define WM_MSG_IFACE_RULE_NOTIFY       (WM_MSG_WMMSG_BEGIN + 21)
//connection & disconnect msg notify
#define WM_MSG_LINK_STATUS_NOTIFY       (WM_MSG_WMMSG_BEGIN + 22)
//interface msg notify
#define WM_MSG_IFACE_STATUS_NOTIFY       (WM_MSG_WMMSG_BEGIN + 23)

#define WM_MSG_SWITCH_TIME_SET_REQ      (WM_MSG_WMMSG_BEGIN + 24)
#define WM_MSG_SWITCH_TIME_SET_RSP      (WM_MSG_WMMSG_BEGIN + 25)
#define WM_MSG_SWITCH_TIME_GET_REQ      (WM_MSG_WMMSG_BEGIN + 26)
#define WM_MSG_SWITCH_TIME_GET_RSP      (WM_MSG_WMMSG_BEGIN + 27)
//mm inner msg to handle lte_cell_lock reapetedly
#define WM_MSG_LTE_CELL_LOCK_HANDLE_NOTIFY (WM_MSG_WMMSG_BEGIN + 28)
#define WM_MSG_LTE_CELL_SEARCH_REQ          (WM_MSG_WMMSG_BEGIN + 29)
#define WM_MSG_LTE_CELL_SEARCH_RSP          (WM_MSG_WMMSG_BEGIN + 30)
#define WM_MSG_CELL_LIST_CHANGE_NOTIFY      (WM_MSG_WMMSG_BEGIN +31)
//device and sim mult lock
#define WM_MULT_UNLOCK_REQ                  (WM_MSG_WMMSG_BEGIN + 32)
#define WM_MULT_UNLOCK_RSP                  (WM_MSG_WMMSG_BEGIN + 33)
#define EM_IP_UPDOWN_NOTIFY      (WM_MSG_WMMSG_BEGIN + 30)
#define EM_ETH_WORK_MODE_CHANGE_NOTIFY      (WM_MSG_WMMSG_BEGIN + 31)
#define EM_ETH_STATUS_REQ      (WM_MSG_WMMSG_BEGIN + 32)
#define EM_ETH_STATUS_RSP      (WM_MSG_WMMSG_BEGIN + 33)
#define WM_MSG_PRIORITY_CHANGE_REQ       (WM_MSG_WMMSG_BEGIN + 34)
#define WM_MSG_PRIORITY_CHANGE_RSP       (WM_MSG_WMMSG_BEGIN + 35)

#define WM_MSG_WMMSG_END WM_MSG_WMMSG_BEGIN + 0x50 - 1  

#define WM_MSG_RADIO_STATUS_REQ  (WM_MSG_WMMSG_END + 1)
//获取当前无线模块信息响应消息
#define WM_MSG_RADIO_STATUS_RSP  (WM_MSG_WMMSG_END + 2)
//获取当前无线模块信号强度请求
#define WM_MSG_SIGNAL_STRENGTH_REQ (WM_MSG_WMMSG_END + 3)
//获取当前无线模块信号强度响应
#define WM_MSG_SIGNAL_STRENGTH_RSP (WM_MSG_WMMSG_END + 4)
//获取当前无线模块信号强度(DB)请求
#define WM_MSG_SIGNAL_DB_REQ  (WM_MSG_WMMSG_END + 5)
//获取当前无线模块信号强度(DB)响应
#define WM_MSG_SIGNAL_DB_RSP  (WM_MSG_WMMSG_END + 6)
//获得WM当前的系统状态请求
#define WM_MSG_STATUS_GET_REQ (WM_MSG_WMMSG_END + 7)
//获得WM当前的系统状态响应
#define WM_MSG_STATUS_GET_RSP (WM_MSG_ID_START + 8)
//当前无线模块信息通知消息
#define WM_MSG_RADIO_STATUS_NOTIFY (WM_MSG_WMMSG_END + 9)
//当前无线模块信号强度通知消息
#define WM_MSG_SIGNAL_STRENGTH_NOTIFY (WM_MSG_ID_START + 10)
//获得网络优先级
#define WM_MSG_DUALMODE_GET_REQ (WM_MSG_WMMSG_END + 11)
//获得网络优先级响应消息
#define WM_MSG_DUALMODE_GET_RSP (WM_MSG_WMMSG_END + 12)
//设置网络优先级请求消息
#define WM_MSG_DUALMODE_SET_REQ (WM_MSG_WMMSG_END + 13)
//设置网络优先级响应消息
#define WM_MSG_DUALMODE_SET_RSP (WM_MSG_WMMSG_END + 14)
//获得拨号优先级请求消息
#define WM_MSG_DUALWAN_PREFERRNCE_GET_REQ (WM_MSG_WMMSG_END + 15)
//获得拨号优先级响应消息
#define WM_MSG_DUALWAN_PREFERRNCE_GET_RSP (WM_MSG_WMMSG_END + 16)
//设置拨号优先级请求
#define WM_MSG_DUALWAN_PREFERRNCE_SET_REQ (WM_MSG_WMMSG_END + 17)
//设置拨号优先级响应
#define WM_MSG_DUALWAN_PREFERRNCE_SET_RSP (WM_MSG_WMMSG_END + 18)


#define WM_MSG_ROAMING_GET_REQ (WM_MSG_WMMSG_END + 27)
#define WM_MSG_ROAMING_GET_RSP (WM_MSG_WMMSG_END + 28)
//设置自动拨号请求消息
#define WM_MSG_ROAMING_SET_REQ (WM_MSG_WMMSG_END + 29)
//设置自动拨号响应消息
#define WM_MSG_ROAMING_SET_RSP (WM_MSG_WMMSG_END + 30)
// SIM卡状态信息获取请求消息
#define WM_MSG_SIMSTATUS_GET_REQ (WM_MSG_WMMSG_END + 31)
// SIM卡状态信息获取响应消息
#define WM_MSG_SIMSTATUS_GET_RSP (WM_MSG_WMMSG_END + 32)
// SIM卡状态信息获取请求消息
#define WM_MSG_PIN_UNLOCK_REQ (WM_MSG_WMMSG_END + 33)
// SIM卡状态信息获取响应消息
#define WM_MSG_PIN_UNLOCK_RSP (WM_MSG_WMMSG_END + 34)
// PIN码状态信息获取请求消息
#define WM_MSG_PIN_STATUS_SET_REQ (WM_MSG_WMMSG_END + 35)
// PIN码状态信息获取响应消息
#define WM_MSG_PIN_STATUS_SET_RSP (WM_MSG_WMMSG_END + 36)
// PIN码状态信息获取请求消息
#define WM_MSG_PIN_STATUS_GET_REQ (WM_MSG_WMMSG_END + 37)
// PIN码状态信息获取响应消息
#define WM_MSG_PIN_STATUS_GET_RSP (WM_MSG_WMMSG_END + 38)
//获得自动匹配到的APN请求消息
#define WM_MSG_ATUOMATCH_APN_REQ (WM_MSG_WMMSG_END + 39)
//获得自动匹配到的APN响应消息
#define WM_MSG_ATUOMATCH_APN_RSP (WM_MSG_WMMSG_END + 40)
//获取SN请求消息
#define WM_MSG_SN_GET_REQ (WM_MSG_WMMSG_END + 41)
//获取SN响应消息
#define WM_MSG_SN_GET_RSP (WM_MSG_WMMSG_END + 42)
//设置SN请求消息
#define WM_MSG_SN_SET_REQ (WM_MSG_WMMSG_END + 43)
//设置SN响应消息
#define WM_MSG_SN_SET_RSP (WM_MSG_WMMSG_END + 44)
//获取拨号配置信息请求消息
#define WM_MSG_PROFILE_GET_REQ (WM_MSG_WMMSG_END + 45)
//获取拨号配置信息响应消息
#define WM_MSG_PROFILE_GET_RSP (WM_MSG_WMMSG_END + 46)
//获取MAC请求消息
#define WM_MSG_MAC_GET_REQ (WM_MSG_WMMSG_END + 47)
//获取MAC响应消息
#define WM_MSG_MAC_GET_RSP (WM_MSG_WMMSG_END + 48)
//PIN码修改请求消息
#define WM_MSG_PIN_MODIFY_REQ (WM_MSG_WMMSG_END + 49)
//PIN码修改请求消息
#define WM_MSG_PIN_MODIFY_RSP (WM_MSG_WMMSG_END + 50)

//PIN码修改enable请求消息
#define WM_MSG_PIN_MODIFY_EN_REQ (WM_MSG_WMMSG_END + 51)
//PIN码修改enable请求消息
#define WM_MSG_PIN_MODIFY_EN_RSP (WM_MSG_WMMSG_END + 52)


//keepalive or demand get request
//#define WM_MSG_DIAL_TYPE_GET_REQ (WM_MSG_ID_START + 51)
//keepalive or demand get response
//#define WM_MSG_DIAL_TYPE_GET_RSP (WM_MSG_ID_START + 52)
//keepalive or demand set request
//#define WM_MSG_DIAL_TYPE_SET_REQ (WM_MSG_ID_START + 53)
//keepalive or demand set response
//#define WM_MSG_DIAL_TYPE_SET_RSP (WM_MSG_ID_START + 54)
//only odd mode can send WM_MSG_NET_SEARCH_REQ
#define WM_MSG_NET_SEARCH_REQ (WM_MSG_WMMSG_END + 55)
//response of net search request
#define WM_MSG_NET_SEARCH_RSP (WM_MSG_WMMSG_END + 56)
//set net request
#define WM_MSG_NET_SET_REQ (WM_MSG_WMMSG_END + 57)
//set net response
#define WM_MSG_NET_SET_RSP (WM_MSG_WMMSG_END + 58)
//at parser used at request
#define WM_MSG_AT_REQ  (WM_MSG_WMMSG_END + 59)
//at parser used at response
#define WM_MSG_AT_RSP  (WM_MSG_WMMSG_END + 60)
// SIM card information notify
#define WM_MSG_SIMSTATUS_NOTIFY (WM_MSG_WMMSG_END + 61)
// PIN state information notify
#define WM_MSG_PIN_STATUS_NOTIFY (WM_MSG_WMMSG_END + 62)

//#define WM_MSG_IP_UPDOWN_NOTIFY (WM_MSG_ID_START + 63)
//release or capture modem AT serial port request
#define WM_MSG_MODEM_AT_REQ (WM_MSG_WMMSG_END + 64)
//release or capture modem AT serial port request
#define WM_MSG_MODEM_AT_RSP (WM_MSG_WMMSG_END + 65)
#define WM_MSG_GET_PLMN_STATE_REQ (WM_MSG_WMMSG_END + 66)
// SIM卡状态信息获取请求消息
#define WM_MSG_PUK_UNLOCK_REQ (WM_MSG_WMMSG_END + 69)
// SIM卡状态信息获取响应消息
#define WM_MSG_PUK_UNLOCK_RSP (WM_MSG_WMMSG_END + 70)
// get modem version request
#define WM_MSG_VERSION_GET_REQ (WM_MSG_WMMSG_END + 71)
// get modem version response
#define WM_MSG_VERSION_GET_RSP (WM_MSG_WMMSG_END + 72)
#define WM_MSG_SERVICE_GET_REQ (WM_MSG_WMMSG_END + 73)
#define WM_MSG_SERVICE_GET_RSP (WM_MSG_WMMSG_END + 74)
#define WM_MSG_NET_GET_REQ     (WM_MSG_WMMSG_END + 75)
#define WM_MSG_NET_GET_RSP    (WM_MSG_WMMSG_END + 76)
#define WM_MSG_PIN_ENABLE_REQ  (WM_MSG_WMMSG_END + 77)
#define WM_MSG_PIN_ENABLE_RSP  (WM_MSG_WMMSG_END + 78)
#define WM_MSG_PIN_DISABLE_REQ  (WM_MSG_WMMSG_END + 79)
#define WM_MSG_PIN_DISABLE_RSP  (WM_MSG_WMMSG_END + 80)
#define WM_MSG_SERVICE_NOTIFY   (WM_MSG_WMMSG_END + 81)

#define SMSPB_MSG_PB_REQ           (WM_MSG_WMMSG_END + 82)
#define SMSPB_MSG_PB_RSP           (WM_MSG_WMMSG_END + 83)
#define WM_MSG_DUALMODE_SET_PREF_REQ   (WM_MSG_WMMSG_END + 84)
#define WM_MSG_DUALMODE_SET_PREF_RSP   (WM_MSG_WMMSG_END + 85)
#define WEB_UI_WMS_NOTIFY         (WM_MSG_WMMSG_END + 86)
#define SMSPB_MSG_SMS_RSP            (WM_MSG_WMMSG_END + 87)
#define SMSPB_SMS_STATE_NOTIFY            (WM_MSG_WMMSG_END + 88)
#define SMSPB_SMS_STATE_REQ            (WM_MSG_WMMSG_END + 89)
#define SMSPB_SMS_STATE_RSP            (WM_MSG_WMMSG_END + 90)
#define WM_SET_DEFAULT_APN_REQ        (WM_MSG_WMMSG_END + 91)
#define WM_SET_DEFAULT_APN_RSP        (WM_MSG_WMMSG_END + 92)
#define WM_MSG_DUALMODE_GET_PREF_REQ   (WM_MSG_WMMSG_END + 93)
#define WM_MSG_DUALMODE_GET_PREF_RSP   (WM_MSG_WMMSG_END + 94)
#define WM_MSG_NEWSMS_NOTIFY   (WM_MSG_WMMSG_END + 95)
#define WM_MSG_SEND_SMS_REQ   (WM_MSG_WMMSG_END + 96)
#define WM_MSG_SEND_SMS_RSP   (WM_MSG_WMMSG_END + 97)
#define WM_MSG_SAVE_SMS_REQ   (WM_MSG_WMMSG_END + 98)
#define WM_MSG_SAVE_SMS_RSP   (WM_MSG_WMMSG_END + 99)
//modem at comand notify
#define WM_MSG_MODEM_NOTIFY   (WM_MSG_WMMSG_END + 100)
//modem registe notify
#define WM_MSG_MODEM_REGSTATUS_NOTIFY   (WM_MSG_WMMSG_END + 101)
#define WM_MSG_SMS_LED_REQ   (WM_MSG_WMMSG_END + 102)
#define WM_MSG_SMS_LED_RSP   (WM_MSG_WMMSG_END + 103)
#define WM_MSG_MODEM_NITZ_REQ   (WM_MSG_WMMSG_END + 104)
#define WM_MSG_MODEM_NITZ_NOTIFY   (WM_MSG_WMMSG_END + 105)
#define WM_MSG_MODEM_RATES_REQ   (WM_MSG_WMMSG_END + 106)
#define WM_MSG_MODEM_RATES_RSP   (WM_MSG_WMMSG_END + 107)
#define WM_NETWORK_UNLOCK_REQ   (WM_MSG_WMMSG_END + 108)
#define WM_NETWORK_UNLOCK_RSP   (WM_MSG_WMMSG_END + 109)

#define WM_MSG_GET_IMSI_REQ   (WM_MSG_WMMSG_END + 110)
#define WM_MSG_GET_IMSI_RSP   (WM_MSG_WMMSG_END + 111)

#define WM_MSG_IFACE_UPORDOWN_REQ   (WM_MSG_WMMSG_END + 112)
#define WM_MSG_IFACE_UPORDOWN_RSP   (WM_MSG_WMMSG_END + 113)

#define WM_MSG_CLEAR_PEAK_REQ   (WM_MSG_WMMSG_END + 114)
#define WM_MSG_CLEAR_PEAK_RSP   (WM_MSG_WMMSG_END + 115)



// WAN Manager消息ID结束值
#define WM_MSG_ID_END   WM_MSG_ID_START + 0x00100000 - 1

// Network Storage消息ID起始值
#define NS_MSG_ID_START WM_MSG_ID_END + 1
#define NS_SVR_CNTL_REQ NS_MSG_ID_START + 1
#define NS_SVR_CNTL_RSP NS_MSG_ID_START + 2
#define NS_TEST_DEV_REQ NS_MSG_ID_START + 3
#define NS_TEST_DEV_RSP NS_MSG_ID_START + 4
#define NS_DEV_FORMAT_REQ NS_MSG_ID_START + 5
#define NS_DEV_FORMAT_RSP NS_MSG_ID_START + 6
#define NS_GET_DEVS_REQ NS_MSG_ID_START + 7
#define NS_GET_DEVS_RSP NS_MSG_ID_START + 8
// Network Storage消息ID结束值
#define NS_MSG_ID_END   NS_MSG_ID_START + 0x00100000 - 1

// AT Parser消息ID起始值
#define AT_MSG_ID_START NS_MSG_ID_END + 1
// AT Parser消息ID结束值
#define AT_MSG_ID_END   AT_MSG_ID_START + 0x00100000 - 1

// Record Manager消息ID起始值
#define RM_MSG_ID_START AT_MSG_ID_END + 1
// clear record file msg id
#define RM_MSG_RECORD_CLEAR              RM_MSG_ID_START+1 
// clear wan/lan static msg id
#define RM_MSG_STATISTIC_CLEAR           RM_MSG_ID_START+2
// record msg msg id
#define RM_MSG_RECORD_REQ                RM_MSG_ID_START+3
// wan connect to internet
#define RM_MSG_WAN_CONNECT              RM_MSG_ID_START+4
// wan disconnect from internet
#define RM_MSG_WAN_DISCONNECT              RM_MSG_ID_START+5
// log message msg id
#define RM_MSG_LOG_REQ                RM_MSG_ID_START+6
// clear record file response msg id
#define RM_MSG_RECORD_RSP              RM_MSG_ID_START+7
//notify rm crond reboot config update
#define RM_MSG_CROND_CFG_NOTIFY              RM_MSG_ID_START+8
//notify rm system time  config update
#define RM_MSG_SYSTEM_TIME_CFG_NOTIFY              RM_MSG_ID_START+9

#define RM_MSG_CELL_DWELL_TIMEOUT_NOTIFY              RM_MSG_ID_START+10




// Record Manager消息ID结束值
#define RM_MSG_ID_END   RM_MSG_ID_START + 0x00100000 - 1

// Upgrade消息ID起始值
#define UP_MSG_ID_START RM_MSG_ID_END + 1
// Upgrade消息ID结束值
#define UP_MSG_ID_END   UP_MSG_ID_START + 0x00100000 - 1

// Diagnosis消息ID起始值
#define DIAG_MSG_ID_START UP_MSG_ID_END + 1
#define DIAG_GET_SYS_REQ  DIAG_MSG_ID_START+1
#define DIAG_GET_SYS_RSP  DIAG_MSG_ID_START+2
#define DIAG_GET_PCB_REQ  DIAG_MSG_ID_START+3
#define DIAG_GET_PCB_RSP  DIAG_MSG_ID_START+4
#define DIAG_GET_BOARD_INFO_REQ  DIAG_MSG_ID_START+5
#define DIAG_GET_BOARD_INFO_RSP  DIAG_MSG_ID_START+6
#define DIAG_SOFT_TEST_REQ  DIAG_MSG_ID_START+7
#define DIAG_SOFT_TEST_RSP  DIAG_MSG_ID_START+8
#define DIAG_PRAV_GET_PCB_REQ DIAG_MSG_ID_START+9
#define DIAG_PRAV_GET_PCB_RSP DIAG_MSG_ID_START+10
#define DIAG_PRAV_GET_BOARD_INFO_REQ DIAG_MSG_ID_START+11
#define DIAG_PRAV_GET_BOARD_INFO_RSP DIAG_MSG_ID_START+12
#define DIAG_PRAV_SOFT_TEST_REQ DIAG_MSG_ID_START+13
#define DIAG_PRAV_SOFT_TEST_RSP DIAG_MSG_ID_START+14
#define DIAG_STATE_PING_REQ            (DIAG_MSG_ID_START+15)
#define DIAG_STATE_SYSCHK_REQ          (DIAG_MSG_ID_START+16)
#define DIAG_STATE_WIRCHK_REQ          (DIAG_MSG_ID_START+17)
#define DIAG_STATE_RSP                 (DIAG_MSG_ID_START+18)
#define DIAG_STATE_EXPORT_REQ          (DIAG_MSG_ID_START+19)
#define DIAG_LNK_EUR_REQ          (DIAG_MSG_ID_START+20)
#define DIAG_HIDE_LNK_EUR_REQ          (DIAG_MSG_ID_START+21)

// Diagnosis消息ID结束值
#define DIAG_MSG_ID_END   DIAG_MSG_ID_START + 0x00100000 - 1

// Watchdog消息ID起始值
#define DOG_MSG_ID_START   DIAG_MSG_ID_END + 1

#define DOG_PET_DOG_REQ         DOG_MSG_ID_START+1 //喂狗请求
#define DOG_ENABLE_DOG_REQ      DOG_MSG_ID_START+2 //使能狗请求
#define DOG_DISABLE_DOG_REQ     DOG_MSG_ID_START+3 //禁止狗请求
#define DOG_POLL_CHECK_TIME_OUT DOG_MSG_ID_START+4 //轮询定时器
#define DOG_MSG_LAST            DOG_MSG_ID_START+5
// Watchdog消息ID结束值
#define DOG_MSG_ID_END    DOG_MSG_ID_START + 0x00100000 - 1

// Log Manager消息ID起始值
#define LOG_MSG_ID_START  DOG_MSG_ID_END + 1
// Log Manager消息ID结束值
#define LOG_MSG_ID_END    LOG_MSG_ID_START + 0x00100000 - 1

/* WLAN Manager, for detailed definitions please see wlan_extern.h */
#define WLAN_MSG_ID_START DOG_MSG_ID_END + 1
#define WLAN_MSG_ID_END   WLAN_MSG_ID_START + 0x00100000 - 1

/* WLAN Manager, for detailed definitions please see wlan_extern.h */


#define CM_MSG_ID_START WLAN_MSG_ID_END+1

#define CM_CFG_UPDATE_REQ  CM_MSG_ID_START+1
#define CM_CFG_UPDATE_RSP  CM_MSG_ID_START+2
#define CM_MSG_MODEM_NOTIFY  CM_MSG_ID_START+3

#define CM_NET_CFCW_STATUS_REQ CM_MSG_ID_START+4
#define CM_NET_CFCW_STATUS_RSP CM_MSG_ID_START+5
#define CM_CLIR_CFG_UPDATE_REQ CM_MSG_ID_START+6
#define CM_CLIR_CFG_UPDATE_RSP CM_MSG_ID_START+7
#define CM_SET_CALL_OUT_STATIC_REQ CM_MSG_ID_START+8
#define CM_SET_CALL_OUT_STATIC_RSP CM_MSG_ID_START+9
#define CM_SET_CALL_IN_STATIC_REQ CM_MSG_ID_START+10
#define CM_SET_CALL_IN_STATIC_RSP CM_MSG_ID_START+11
#define CM_SET_CALL_CLIP_ID_REQ CM_MSG_ID_START+12
#define CM_SET_CALL_CLIP_ID_RSP CM_MSG_ID_START+13
#define CM_CALL_WAITING_REQ CM_MSG_ID_START+14
#define CM_CALL_WAITING_RSP CM_MSG_ID_START+15



#define CM_MSG_ID_END CM_MSG_ID_START+0x00100000-1


/****************** 模块进程间消息ID定义结束 *********************************/


// 消息队列消息
// 此范围留给各模块自己定义
#define IPC_ID_MQ_START     IPC_MSG_ID_END - 0x10000000 + 1
#define IPC_ID_MQ_END       (IPC_ID_TEST_START - 1)

// 测试用消息范围
#define IPC_ID_TEST_START   (IPC_MSG_ID_END - 0x01000000 + 1)
#define IPC_MSG_ID_END      0xFFFFFFFF

#endif


