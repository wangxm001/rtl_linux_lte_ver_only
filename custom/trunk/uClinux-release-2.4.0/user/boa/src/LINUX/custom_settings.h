#ifndef INCLUDE_CUSTOM_SETTINGS_H
#define INCLUDE_CUSTOM_SETTINGS_H


#if defined(CONFIG_USER_CUSTOM_UKB)

  #define	CTS_SNMPSYSDESCR         "LTE Router"
	#define	CTS_SNMPSYSNAME          "VH510B"
	
	#define CTS_NTP_ENABLE           "1"
	#define CTS_NTP_ZONE_INDWX       "315"
	#define CTC_NTP_SERVER1           "130.149.17.8"
	#define CTC_NTP_SERVER2           ""
	#define CTC_NTP_SERVER_ID         "1"
	
	#define CTC_SSID                  "Relish_Home_123456"
	#define CTC_WLAN_ENCRYPT          "4"
	#define CTC_WLAN_WPACIPHER        "0"
	#define CTC_WLAN_WPA2CIPHER       "2"
	#define CTC_WLAN_WPAPSK           "12345678"
	#define CTC_WLAN_SHORTGI           "1"
	
	#define CTC_ACS_URL              "http://81.91.205.39:10301/acs/"
	#define CTC_ACS_OLD              "http://81.91.205.39:10301/acs/"
	#define CTC_ACS_USERNAME          "user_acs"
	#define CTC_ACS_PASSWD            "sps6wCf5OR"
	#define CTC_ACS_INFORM_INTERVAL   "86400"
	#define CTC_CONNREQ_ACS_USERNAME  ""
	#define CTC_CONNREQ_ACS_PASSWD    ""
	#define CTC_CONNREQ_ACS_PORT      "7547"
	
	#define CTC_WCS_CONFIGURED         "1"
	#define CTC_WCS_AUTH               "32"
	#define CTC_WCS_ENC                "8"
	#define CTC_WCS_MANUAL_ENB         "1"
	#define CTC_WCS_PSK                "12345678"
	
	#define CTC_V6_OTHERCONFIGFLAG     "0"
	
	#define CTC_VAP_NAME               "Relish_Home_Guest4"
	
	#define CTC_IPV6_ENABLE            "1"
	
	#define CTC_SUSERNAME              "UKBadmin"
	#define CTC_SPASSWORD              "H97ENUjqnG"
	#define CTC_DEFAULT_LANG           "en"
	
#elif defined(CONFIG_USER_CUSTOM_UMNIAH)

  #define	CTS_SNMPSYSDESCR         "LTE Router"
	#define	CTS_SNMPSYSNAME          "LT67"
	
	#define CTS_NTP_ENABLE            "1"
	#define CTS_NTP_ZONE_INDWX        "214"
	#define CTC_NTP_SERVER1           "0.asia.pool.ntp.org"
	#define CTC_NTP_SERVER2           "130.149.17.8"
	#define CTC_NTP_SERVER_ID         "0"
	
	#define CTC_SSID                  "Umniah_12345"
	#define CTC_WLAN_ENCRYPT          "6"
	#define CTC_WLAN_WPACIPHER        "3"
	#define CTC_WLAN_WPA2CIPHER       "3"
	#define CTC_WLAN_WPAPSK           "12345678"
	#define CTC_WLAN_SHORTGI           "1"
	
	#define CTC_ACS_URL               "http://acs.umniah.com:10301"
	#define CTC_ACS_OLD               "http://acs.umniah.com:10301"
	#define CTC_ACS_USERNAME          "acsumniah"
	#define CTC_ACS_PASSWD            "cj86jo6$"
	#define CTC_ACS_INFORM_INTERVAL   "1200"
	#define CTC_CONNREQ_ACS_USERNAME  "acsumniah"
	#define CTC_CONNREQ_ACS_PASSWD    "cj86jo6$"
	#define CTC_CONNREQ_ACS_PORT      "10301"
	
	#define CTC_WCS_CONFIGURED         "1"
	#define CTC_WCS_AUTH               "34"
	#define CTC_WCS_ENC                "12"
	#define CTC_WCS_MANUAL_ENB         "1"
	#define CTC_WCS_PSK                "12345678"
	
	#define CTC_V6_OTHERCONFIGFLAG     "0"
	#define CTC_VAP_NAME               "Umniah_Guest4"
	
	#define CTC_IPV6_ENABLE            "0"
	#define CTC_SUSERNAME              "admin"
	#define CTC_SPASSWORD              "LTE@LT67"
	#define CTC_DEFAULT_LANG           "ar"
#elif defined(CONFIG_USER_CUSTOM_ZAK)

	#define	CTS_SNMPSYSDESCR         "LTE Router"
	#define	CTS_SNMPSYSNAME          "VH510B"
	
	#define CTS_NTP_ENABLE            "0"
	#define CTS_NTP_ZONE_INDWX        "259"
	#define CTC_NTP_SERVER1            "203.117.180.36"
	#define CTC_NTP_SERVER2           "220.130.158.52"
	#define CTC_NTP_SERVER_ID         "0"
	
	#define CTC_SSID                  "LT67_12345"
	#define CTC_WLAN_ENCRYPT          "4"
	#define CTC_WLAN_WPACIPHER        "0"
	#define CTC_WLAN_WPA2CIPHER       "2"
	#define CTC_WLAN_WPAPSK           "12345678"
	#define CTC_WLAN_SHORTGI           "1"
	
	#define CTC_ACS_URL               "http://"
	#define CTC_ACS_OLD               "http://"
	#define CTC_ACS_USERNAME          "username"
	#define CTC_ACS_PASSWD            "password"
	#define CTC_ACS_INFORM_INTERVAL   "300"
	#define CTC_CONNREQ_ACS_USERNAME  ""
	#define CTC_CONNREQ_ACS_PASSWD    ""
	#define CTC_CONNREQ_ACS_PORT      "7547"
	
	#define CTC_WCS_CONFIGURED         "1"
	#define CTC_WCS_AUTH               "32"
	#define CTC_WCS_ENC                "8"
	#define CTC_WCS_MANUAL_ENB         "1"
	#define CTC_WCS_PSK                "12345678"
	
	#define CTC_V6_OTHERCONFIGFLAG     "0"
	#define CTC_VAP_NAME               "LT67_Guest4"
	
	#define CTC_IPV6_ENABLE            "0"
	#define CTC_SUSERNAME              "admin"
	#define CTC_SPASSWORD              "system"
	#define CTC_DEFAULT_LANG           "en"
#else

  #define	CTS_SNMPSYSDESCR        "Router"
	#define	CTS_SNMPSYSNAME         "Modem/Router"
	#define CTS_NTP_ENABLE           "0"
	#define CTS_NTP_ZONE_INDWX       "259"
	#define CTC_NTP_SERVER1          "203.117.180.36"
	#define CTC_NTP_SERVER2           "220.130.158.52"
	#define CTC_NTP_SERVER_ID         "0"
	
	#define CTC_SSID                 "RTL867x-ADSL"
	#define CTC_WLAN_ENCRYPT          "0"
	#define CTC_WLAN_WPACIPHER        "1"
	#define CTC_WLAN_WPA2CIPHER       "2"
	#define CTC_WLAN_WPAPSK           "12345678"
	#define CTC_WLAN_SHORTGI           "0"
	
	#define CTC_ACS_URL               ""
	#define CTC_ACS_OLD               ""
	#define CTC_ACS_USERNAME          "username"
	#define CTC_ACS_PASSWD            "password"
	#define CTC_ACS_INFORM_INTERVAL   "300"
	#define CTC_CONNREQ_ACS_USERNAME  ""
	#define CTC_CONNREQ_ACS_PASSWD    ""
	#define CTC_CONNREQ_ACS_PORT      "7547"
	
	#define CTC_WCS_CONFIGURED         "1"
	#define CTC_WCS_AUTH               "1"
	#define CTC_WCS_ENC                "1"
	#define CTC_WCS_MANUAL_ENB         "0"
	#define CTC_WCS_PSK                "\0"
	
	#define CTC_V6_OTHERCONFIGFLAG     "1"
	#define CTC_VAP_NAME               "RTL867x_Guest4"
	
	#define CTC_IPV6_ENABLE            "1"
	#define CTC_SUSERNAME              "admin"
	#define CTC_SPASSWORD              "system"
	#define CTC_DEFAULT_LANG           "en"
#endif


#endif //INCLUDE_CUSTOM_SETTINGS_H
