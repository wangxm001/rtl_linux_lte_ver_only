<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="all.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
<link rel="stylesheet" type="text/css" href="../css/globals.css">
<link href="../css/user2.css" rel="stylesheet" />
<script type="text/javascript"  src="../language/js/wan.js"></script>

<script src='../ui_js/jquery.js' type='text/javascript'></script>
<SCRIPT type="text/javascript" src="../ui_js/jquery.qtip.js"></SCRIPT>
<script type="text/javascript"  src="../js/globals.js"></script>

<title>Inter network</title>
<script type='text/javascript'>
nvram = {
	sim_data: '<% getInfo("modemstatus"); %>',
	connect_mode: '<% checkWrite("webconnectmode"); %>',
	language: 'en'
};


function init()
{
	switch (nvram.sim_data)
	{
		case "not_invalid":
			$("#modem_show_txt").html(getalert("alert_wan_not_invalid_sim"));
			$("#modem_show_txt").attr("alt","alert_wan_not_invalid_sim");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			return;
		case "no_simcard":
			$("#modem_show_txt").html(getalert("alert_wan_nosim"));
			$("#modem_show_txt").attr("alt","alert_wan_nosim");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			return;
		case "not_ready":
			$("#modem_show_txt").html(getalert("alert_wan_modem_notready"));
			$("#modem_show_txt").attr("alt","alert_wan_modem_notready");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			return;
		case "sim_ready":
		case "pin_disable":
		case "pin_enable":
			$("#user_data").hide();
			$("input[name=ipt_connect_mode][value="+nvram.connect_mode+"]").attr("checked",'checked');
			get_connect_status();
			break;
		case "need_pin":
			$("#modem_show_txt").html(getalert("alert_wan_pinblock"));
			$("#modem_show_txt").attr("alt","alert_wan_pinblock");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			break;
		case "need_puk":
			$("#modem_show_txt").html(getalert("alert_wan_pukblock"));
			$("#modem_show_txt").attr("alt","alert_wan_pukblock");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			break;
		case "puk_lock":
			$("#modem_show_txt").html(getalert("alert_wan_puklock"));
			$("#modem_show_txt").attr("alt","alert_wan_puklock");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			break;
		default:
			$("#modem_show_txt").html(getalert("alert_wan_modem_error"));
			$("#modem_show_txt").attr("alt","alert_wan_modem_error");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			return;
	}
    init_language();

}

</script>
</head>
<body onload="getxml(language_wan);" >
<div id="user_all" onkeydown="javascript:capchar(event)" style="padding:0 1em 0 1em;">
  <div id="user_data" style="display:none">
    <div id="user_table" >
      <form action="/boaform/admin/form4GConfsetconnectmode" method="POST" name="form4gconfsetconnectmode" id="form4gconfsetconnectmode">
        <input type="hidden" name="submit-url" value="/admin/lct_internet_mode.asp">
        <input type="hidden" id = "ipt_dial_mode_idletime_value" name="ipt_dial_mode_idletime_value">
        <div class="line">
			<div class="">
				  			
				<div class="line">
					<div class="s-12 l-4 line_form">		   
						<label class="name" id="td_connect_mode"></label>
					</div>
					<div class="s-12 l-4 line_form">						
						<input name="ipt_connect_mode" type="radio" value="0"/><span id="sp_connect_mode_auto"></span>
						<input name="ipt_connect_mode" type="radio" value="1"/><span id="sp_connect_mode_manual"></span>
					</div>					 
				</div>	
			</div>
        </div>
      </form>
	  <form action="/boaform/admin/form4Ggetwanstatus" method="POST" name="form4ggetwanstatus" id="getnetworkconnectstatus">
		</form>
    </div>

    <div class="line" >
		<div class="margin">
		    <div class="s-12 l-2">
                <div class="button" onClick="wanmode_submit();"><span id="sp_save"></span></div>
            </div>
			<div class="s-12 l-2">
                <div class="button" onclick="window.location=location;"><span id="sp_cancel"></span></div>
            </div>
		</div>
    </div>
    </div>
  </div>
  <br class="clearfloat"/>
</div>

<div id="modem_shoe_div" align="center" style="display:none;"> 
    <div class="line">
		<div class="margin">
		    <div class="line">
				<div class="s-12 l-1">
					<label class="name">&nbsp;</label>
				</div>
			</div>
			<div class="line">
				<div class="s-12 l-1">
					<label class="name">&nbsp;</label>
				</div>
			</div>
			<div class="line">
				<div class="s-12 l-1">
					<label class="name">&nbsp;</label>
				</div>
			</div>
			<div class="line">
				<div class="s-12 l-1">
					<label class="name">&nbsp;</label>
				</div>
			</div>
			<div class="line">
				<div class="s-12 l-1">
					<label class="name">&nbsp;</label>
				</div>
				<div  class="s-12 l-10 line_form">
					<label  class="name" id="modem_show_txt"></label>
				</div>
			</div>
		</div>		
    </div>
</div>

<div id="div_process_bar" align="center" style="display:none">
	<div class="line">  
		<div class="margin">
			<div  class="s-12 l-4 line_form">
				<label  class="name" id="sp_wait_process"></label>
			</div>
		</div>
	</div>
</div>
<script>
function get_connect_status()
{
	$.ajax({
	url: '/boaform/admin/form4Ggetwanstatus',
	data: $('#getnetworkconnectstatus').serialize(),
	type: 'POST',
	complete: function (xhr)
	{
		if (''!=xhr.responseText && xhr.responseText!=null)
		{
			var tmp = xhr.responseText.split(",");
			is_connectd_status = tmp[1];
			if(tmp[1] =="2")
			{
				$("#modem_show_txt").html(getalert("alert_wan_connected"));
				$("#modem_show_txt").attr("alt","alert_wan_connected");
				$("#user_data").hide();
				$("#modem_shoe_div").show();
			}
			else if(tmp[1] =="3")
			{
				$("#modem_show_txt").html(getalert("alert_wan_connecting"));
				$("#modem_show_txt").attr("alt","alert_wan_connecting");
				$("#user_data").hide();
				$("#modem_shoe_div").show();
			}
			else if(tmp[1] =="1")
			{
				$("#modem_show_txt").html(getalert("alert_wan_connecting"));
				$("#modem_show_txt").attr("alt","alert_wan_connecting");
				$("#user_data").hide();
				$("#modem_shoe_div").show();
			}
			else
			{
				$("#user_data").show();
				$("#modem_shoe_div").show();
			}
		}
	}
	});
}


function wanmode_submit()
{

	$.ajax({
	url: '/boaform/admin/form4GConfsetconnectmode',
	data: $('#form4gconfsetconnectmode').serialize(),
	type: 'POST',
	complete: function (xhr)
	{
		window.location.reload();
	}
	});
}
</script>
</body>
</html>
