
var supportted_languages = new Array("en", "ar");// this is for language ding zhi  2012-08-01  wangweizhou

var g_luangauge_count = 13;
var g_luanuage_list_height = "300px"; 
var g_langList = "";

function creatLangList()
{
    if (jQuery.isArray(supportted_languages)) 
    {
        if(supportted_languages.length > 20)
        {
            $("#language_list").css({"overflow-x": "hidden", "overflow-y": "scroll"});
        }
		
        $.each(supportted_languages, function(n, value){
            g_langList += "<span><a href = 'javascript:void(0)' onclick=\"javascript:changeLang('lang','" + value + "')\">" + get_language_value("op_longuage_"+value) + "</a></span>";
        });
    }
}

function changeLang(key, val)
{
    if (nvram.language == val)   // If the selected language is equal to the current default language, then exit. add by lyy in 20121107
	{
	    return;
	}
	
	var language_back = document.lct_language_change.language.value;
	document.lct_language_change.language.value = val;
	$.ajax({
		url: '/cgi-bin/ajax_set.cgi',
		data: $('#lct_language_change').serialize(),
		type: 'post',
		complete: function (xhr) 
		{ 
		    if (''!=xhr.responseText && xhr.responseText!=null && xhr.responseText=="ok")
			{
			    nvram.language = val;
				init_language();
				if(window.frames.length > 0)
				{
					for(var i = 0; i < window.frames.length; i++)
					{
					    if (typeof(window.frames[i].nvram) != "undefined")
						{
							window.frames[i].nvram.language  = val;
							window.frames[i].init_language();
							if(window.frames[i].frames.length > 0)
							{
								if (typeof(window.frames[i].frames[0].nvram) != "undefined")
								{
									window.frames[i].frames[0].nvram.language  = val;
									window.frames[i].frames[0].init_language();
								}
							}
						}
					}
				}
			}
			else
			{
			    document.lct_language_change.language.value = language_back;
				$("#lang").val(get_language_value("op_longuage_"+nvram.language));
			    lc_alert(getalert("alert_change_language_fail"));
			}
		}
	});
} 


//Drop down select
function drop_down_select(touchElement, _select){
    touchElement.focus(function(){ $(this).attr("readonly", "readonly"); });
    
    //select show
    touchElement.click(function(){
        _select.slideToggle(100, function(){
            if($.browser.msie && ($.browser.version == "6.0") && _select.children("li,span").size() == 0)
            {
                _select.css({height:"1px"});
            }
            if($(this).attr("id") == "language_list")
            {
                if(g_luangauge_count >= $(this).children().size())
                {
                    $(this).css({overflow:"hidden"});
                }
                else
                {
                    $(this).css({height:g_luanuage_list_height, display:"block", "overflow-x":"hidden", "overflow-y":"scroll"});
                }
            }
        });
        $(this).blur();        
        _select.css({"z-index": "1000"});
    });
    //select hide
    _select.children().click(function(){
        var optionIndex = _select.children().index(this);
        touchElement.val(_select.children().eq(optionIndex).text());
        _select.slideUp(100);
        return false;
    });
	
	var elementId = touchElement.attr("id");
	$(document).click(function(event){
		if ($(event.target).attr("id") != elementId)
		{
			_select.slideUp(100);
		}
	}); 
	
	
 }
 
 function setLangList(){
	creatLangList();
	$(".lang_option").append(g_langList);
	//Set current language to select
	$("#lang").val(get_language_value("op_longuage_"+nvram.language));
	if ($("#lang").size() > 0)
	{
		drop_down_select($("#lang"), $(".lang_option").eq(0));
	}
}

function initLanguage()
{
    if(nvram.language == "en")
	{
	    $('#langit').hide();
	    $('#langen').show();
	    $('#lang').val('en');
	}
	else
	{
	    $('#langen').hide();
	    $('#langit').show();
	    $('#lang').val('ar');
	}
}

function ChangeLanguageValue()
{
	var value=$('#lang').val();		
	if(value=="en")
	{
	    $('#langit').hide();
	    $('#langen').show();
	    $('#lang').val('zh');
		ChangeLang('lang', $('#lang').val());		
    }
	else
	{
	    $('#langen').hide();
	    $('#langit').show();
	    $('#lang').val('en');
		ChangeLang('lang', $('#lang').val()); 
	}
}
function ChangeLang(key, val)
{
	var language_back = document.lct_language_change.language.value;
	document.lct_language_change.language.value = val;
	$.ajax({
		url: '/cgi-bin/ajax_set.cgi',
		data: $('#lct_language_change').serialize(),
		type: 'post',
		complete: function (xhr) 
		{
		    if (''!=xhr.responseText && xhr.responseText!=null && xhr.responseText=="ok")
			{
		        nvram.language = val;
				init_language();
                if(val=="en")
	            {
	                $('#langit').hide();
	                $('#langen').show();
	                $('#lang').val('en');				   
                }
	            else
	            {
	                $('#langen').hide();
	                $('#langit').show();
	                $('#lang').val('zh'); 					
	            }	
                if(window.frames.length > 0)
				{
					for(var i = 0; i < window.frames.length; i++)
					{
					    if (typeof(window.frames[i].nvram) != "undefined")
						{
							window.frames[i].nvram.language  = val;
							window.frames[i].init_language();
							if(window.frames[i].frames.length > 0)
							{
								if (typeof(window.frames[i].frames[0].nvram) != "undefined")
								{
									window.frames[i].frames[0].nvram.language  = val;
									window.frames[i].frames[0].init_language();
								}
							}
						}
					}
				}				
            }	
		    else
		    {
		        document.lct_language_change.language.value = language_back;
		        lc_alert(getalert("alert_change_language_fail"));
		    }
		}
	});	
}