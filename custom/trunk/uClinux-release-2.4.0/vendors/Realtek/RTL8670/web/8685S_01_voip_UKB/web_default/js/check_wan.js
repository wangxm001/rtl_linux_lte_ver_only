var patrn = /(^(\+)?([0-9\*#P]{1,64})$)/;
var apnStrValid_patrn = /^[0-9a-zA-Z.-]{1,100}$/;
var apn_length = 64;
var apn_length_100 = 100;

function check_APN_profile_length(value)
{
    var len = value.replace(/[^\x00-\xff]/g,"\r\n").length; 

    if(len != value.length)
	{      
		return false;
	}
    else
	{
		if (value.length > apn_length)
		{ 
			return false;
		} 	
    }
	return true;
}

function check_APN_username(keyval)
{
	var ck_apn =  /^[^&\>\"]{0,128}$/;
	if (!ck_apn.test(keyval)) 
	{
		return false;
	}
	else
	{
		return true;
	}
}

function check_APN_apn(s)
{
	if (s=="")
	{
        return true;
    }	
	if (!apnStrValid_patrn.exec(s)) 
	{
		return false; 
	}
	
    return true; 
}

function is_phone_number(s) 
{ 	
	if (s=="")
	{
        return true;
    }	
	if (!patrn.exec(s)) 
	{
		return false; 
	}
	
    return true; 
} 



function check_apn(keyval)
{
	var ck_apn =  /^[^,\>]{0,32}$/;
	if (!ck_apn.test(keyval)) 
	{
		return false;
	}
	else
	{
		return true;
	}

}

function check_profile_apn(keyval)
{
	var ck_apn =  /^[^\>]{0,32}$/;
	if (!ck_apn.test(keyval)) 
	{
		return false;
	}
	else
	{
		return true;
	}

}

function upkey(value)
{
    var str=E("net_connect_profile_name").value;	
    value = value.replace(/\r\n/g,   "\n");
    value = value.replace(/\n/g,   "\r\n");
    var len = value.replace(/[^\x00-\xff]/g,"\r\n").length; 
	var regC = /[^ -~]+/g; 
    var regE = /\D+/g;  

    if(len != value.length)
	{
        
		if (regC.test(str)&&(value.length>8))
		{
			lc_alert("alertwan_wan_profile_name_limit");
			E("net_connect_profile_name").value =value.substr(0,8);
			return false;
		} 

	}
    else
	{

		if(regE.test(str)&&(value.length>30))
		{ 
			E("net_connect_profile_name").value = value.substr(0,30); 
			return false;
		} 	
    }
	return true;
}

function check_apn_ip(val)
{
	ip = '(25[0-5]|2[0-4]\\d|1\\d\\d|\\d\\d|\\d)';
	ipdot = ip + '\\.';
	isIPaddress = new RegExp('^'+ipdot+ipdot+ipdot+ip+'$');
	if (!isIPaddress.test(val))
	{
		return false;
	}
	return true;
}
