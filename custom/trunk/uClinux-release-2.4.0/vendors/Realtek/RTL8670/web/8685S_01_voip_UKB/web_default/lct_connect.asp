<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <link href="all.css" rel="stylesheet" type="text/css" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="pragma" content="no-cache" />
  <meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
  <link rel="stylesheet" type="text/css" href="../css/style.css">
  <link rel="stylesheet" type="text/css" href="../css/globals.css">
  <link href="../css/user2.css" rel="stylesheet" />
  <script type="text/javascript" src="../language/js/wan.js"></script>
  <script src='../ui_js/jquery.js' type='text/javascript'></script>
  <script type="text/javascript" src="../js/globals.js"></script>
  <SCRIPT type="text/javascript" src="../ui_js/jquery.qtip.js"></SCRIPT>
  <script type="text/javascript" src="../js/form.js"></script>
  <script type="text/javascript" src="../js/check_wan.js"></script>
  <title>Inter connect</title>
  <script type='text/javascript'>
   //net_pro_list: name,wether default,index,username,password,apn,auth  
   nvram = {
    nvmodemstatus: '<% getInfo("modemstatus"); %>',
    traffic_data: '<% getInfo("traffic_data"); %>',
    language: 'en'
   };
   var is_connected = 'disconnected';
   var wanstatus = "1";
   function init() {
    //nvram.nvmodemstatus = 'sim_ready';
    switch (nvram.nvmodemstatus) {
     case "not_invalid":
      $("#modem_show_txt").html(getalert("alert_wan_not_invalid_sim"));
      $("#modem_show_txt").attr("alt", "alert_wan_not_invalid_sim");
      $("#user_data").hide();
      $("#modem_shoe_div").show();
      return;
     case "no_simcard":
      $("#modem_show_txt").html(getalert("alert_wan_nosim"));
      $("#modem_show_txt").attr("alt", "alert_wan_nosim");
      $("#user_data").hide();
      $("#modem_shoe_div").show();
      return;
     case "not_ready":
      $("#modem_show_txt").html(getalert("alert_wan_modem_notready"));
      $("#modem_show_txt").attr("alt", "alert_wan_modem_notready");
      $("#user_data").hide();
      $("#modem_shoe_div").show();
      return;
     case "sim_ready":
     case "pin_disable":
     case "pin_enable":
      $("#user_data").hide();
      get_networkstatus();
      get_max_data_rate_period();
      break;
     case "need_pin":
      $("#modem_show_txt").html(getalert("alert_wan_pinblock"));
      $("#modem_show_txt").attr("alt", "alert_wan_pinblock");
      $("#user_data").hide();
      $("#modem_shoe_div").show();
      break;
     case "need_puk":
      $("#modem_show_txt").html(getalert("alert_wan_pukblock"));
      $("#modem_show_txt").attr("alt", "alert_wan_pukblock");
      $("#user_data").hide();
      $("#modem_shoe_div").show();
      break;
     case "puk_lock":
      $("#modem_show_txt").html(getalert("alert_wan_puklock"));
      $("#modem_show_txt").attr("alt", "alert_wan_puklock");
      $("#user_data").hide();
      $("#modem_shoe_div").show();
      break;
     default:
      $("#modem_show_txt").html(getalert("alert_wan_modem_error"));
      $("#modem_show_txt").attr("alt", "alert_wan_modem_error");
      $("#user_data").hide();
      $("#modem_shoe_div").show();
      return;
    }
    init_language();
   }
  </script>
 </head>
 <body onload="getxml(language_wan);">
  <div id="user_all" onkeydown="javascript:capchar(event)" style="padding:0 1em 0 1em;">
   <div id="user_table">
    <div id="user_data" style="display:none">
     <div id="user_table">
      <form action="/boaform/admin/form4GConf" method="POST" name="form4gconf" id="form4gconf">
       <input type="hidden" value="" name="connectstatus" id="connectstatus">
       <div class="line width_100">
        <div class="s-12 l-3 center" style="margin-top:2em;">
         <div style="text-align:center;font-size:30px;">
          <h3 id="td_connection" style="color:#444444"></h3></div>
        </div>
       </div>
       <div class="line width_100">
        <div class="s-12 l-5 center">
         <div id="div_stat_connect" class="button_gray" onClick="home_start_connect_disconnect(this.id);"><span id="sp_connected_button"></span></div>
        </div>
       </div>
      </form>
      <form action="/boaform/admin/form4GConfget" method="GET" name="form4gconfget" id="networkconnectstatus">
      </form>
      <form action="/boaform/admin/form4GTrafficdata" method="GET" name="form4GTrafficdata" id="form4GTrafficdata">
      </form>
      <form action="/boaform/admin/form4GResetdata" method="POST" name="form4gresetdata" id="form4gresetdata">
      </form>
      <form action="/boaform/admin/form4GgetRsetperiod" method="GET" name="form4gresetperiod" id="form4gresetperiod">
      </form>
      <form action="/boaform/admin/form4Gsetperiod" method="POST" name="form4gsetperiod" id="form4gsetperiod">
      	<input type="hidden" value="" name="max_data_rate_period" id="max_data_rate_period">
      </form>
      <div class="l-5 center">
       <table width=100 border=0 class="l-12">
        <tr>
         <td width=50% bgcolor="#0dabe1">
          <font color="#FFFFFF" size=2><b>Type</b></font>
         </td>
         <td width=50% bgcolor="#0dabe1">
          <font color="#FFFFFF" size=2><b>Current Volume</b></font>
         </td>
        </tr>
        <tr id='lte_rx_rxbytes_s' bgcolor="#EEEEEE">
         <td width=50%>
          <font size=2><b>LTE_RX_RxBytes:</b></td>
         <td width=50%>
          <font size=2 id="td_rx_range">
         </td>
        </tr>
        <tr id='lte_rx_packets_s' bgcolor="#EEEEEE">
         <td width=50%>
          <font size=2><b>LTE_RX_Packets:</b></td>
         <td width=50%>
          <font size=2 id="td_rx_package">
         </td>
        </tr>
        <tr id='lte_tx_rxbytes_s' bgcolor="#EEEEEE">
         <td width=50%>
          <font size=2><b>LTE_TX_RxBytes:</b></td>
         <td width=50%>
          <font size=2 id="td_tx_range">
         </td>
        </tr>
        <tr id='lte_tx_packets_s' bgcolor="#EEEEEE">
         <td width=50%>
          <font size=2><b>LTE_TX_Packets:</b></td>
         <td width=50%>
          <font size=2 id="td_tx_package">
         </td>
        </tr>
        <tr id='up_time' bgcolor="#EEEEEE">
         <td width=50%>
          <font size=2><b>Up_Time:</b></td>
         <td width=50%>
          <font size=2 id="td_up_time">
         </td>
        </tr>
        <tr id='lte_max_data_rate_downlink' bgcolor="#EEEEEE">
         <td width=50%>
          <font size=2><b>Max_Data_Rate_Downlink:</b></td>
         <td width=50%>
          <font size=2 id="lte_max_data_rate_downlink_value">
         </td>
        </tr>
        <tr id='lte_max_data_rate_uplink' bgcolor="#EEEEEE">
         <td width=50%>
          <font size=2><b>Max_Data_Rate_Uplink:</b></td>
         <td width=50%>
          <font size=2 id="lte_max_data_rate_uplink_value">
         </td>
        </tr>
        <tr id='lte_max_data_rate_reset' bgcolor="#EEEEEE">
         <td width=50%>
          <font size=2><b>Max_Data_Rate_Reset:</b></td>
         <td width=50%>
          <div id="div_stat_reset" class="button" onClick="reset_data();"><span id="sp_reset_button">Reset</span></div>
         </td>
        </tr>
        <tr id='lte_max_data_rate_period' bgcolor="#EEEEEE">
         <td width=50%>
          <font size=2><b>Max_Data_Rate_Period:</b></td>
         <td width=50%>
          <!--font size=2 id="lte_max_data_rate_period_value"-->
          <input type="text" name="lte_max_data_rate_period_value" id="lte_max_data_rate_period_value" value="" /> 	
         </td>
        </tr>
        <tr id='lte_max_data_rate_period_set' bgcolor="#EEEEEE">
         <td width=50%>
          <font size=2>&nbsp;</td>
         <td width=50%>
           <div id="div_data_rate_period_reset" class="button" onClick="set_max_data_rate_period();"><span id="sp_save_button">save</span></div>
         </td>
        </tr>
        <tr id='lte_current_data_rate_downlink' bgcolor="#EEEEEE">
         <td width=50%>
          <font size=2><b>Current_Data_Rate_Downlink :</b></td>
         <td width=50%>
          <font size=2 id="lte_current_data_rate_downlink_value">
         </td>
        </tr>
        <tr id='lte_current_data_rate_uplink' bgcolor="#EEEEEE">
         <td width=50%>
          <font size=2><b>Current_Data_Rate_Uplink:</b></td>
         <td width=50%>
          <font size=2 id="lte_current_data_rate_uplink_value">
         </td>
        </tr>
       </table>
      </div>
     </div>
    </div>
   </div>
   <!-- div id="user_table" -->
   <div id="user_end">
    <div id="user_end_in">
    </div>
   </div>
  </div>
  <!-- div id="user_data" -->
  <br class="clearfloat" />
  </div>
  <div id="modem_shoe_div" align="center" style="display:none;">
   <div class="line">
    <div class="margin">
     <div class="line">
      <div class="s-12 l-1">
       <label class="name">&nbsp;</label>
      </div>
     </div>
     <div class="line">
      <div class="s-12 l-1">
       <label class="name">&nbsp;</label>
      </div>
     </div>
     <div class="line">
      <div class="s-12 l-1">
       <label class="name">&nbsp;</label>
      </div>
     </div>
     <div class="line">
      <div class="s-12 l-1">
       <label class="name">&nbsp;</label>
      </div>
     </div>
     <div class="line">
      <div class="s-12 l-1">
       <label class="name">&nbsp;</label>
      </div>
      <div class="s-12 l-10 line_form">
       <label class="name" id="modem_show_txt"></label>
      </div>
     </div>
    </div>
   </div>
  </div>
  <div id="div_process_bar" align="center" style="display:none">
   <div class="line">
    <div class="margin">
     <div class="line">
      <div class="s-12 l-1">
       <label class="name">&nbsp;</label>
      </div>
     </div>
     <div class="line">
      <div class="s-12 l-1">
       <label class="name">&nbsp;</label>
      </div>
     </div>
     <div class="line">
      <div class="s-12 l-1">
       <label class="name">&nbsp;</label>
      </div>
     </div>
     <div class="line">
      <div class="s-12 l-1">
       <label class="name">&nbsp;</label>
      </div>
     </div>
     <div class="line">
      <div class="s-12 l-4 line_form">
       <label class="name" id="sp_wait_process"></label>
      </div>
     </div>
     <div class="line">
      <div class="s-12 l-2 center">
      </div>
     </div>
    </div>
   </div>
  </div>
  <script type="text/javascript" language="javascript">
   function get_networkstatus() {
    var lrand = Math.random();
    var sids = parseInt(lrand * 1000000);
    $.ajax({
     url: '/boaform/admin/form4GConfget?sids=' + sids,
     data: $('#networkconnectstatus').serialize(),
     type: 'GET',
     complete: function(xhr) {
      //alert(xhr.responseText);
      if ('' != xhr.responseText && xhr.responseText != null) {
       var tmp = xhr.responseText.split(",");
       if (tmp[0] == "0") {
        $('#div_stat_connect').removeClass("button");
        $('#div_stat_connect').addClass("button_gray");
       } else {
        $('#div_stat_connect').removeClass("button_gray");
        $('#div_stat_connect').addClass("button");
       }
       if (tmp[1] == "0") {
        is_connected = "disconnected";
        $('#sp_connected_button').html(getalert("title_connect_button"));
        $("#connection_image").attr("src", '../images/cn_dis.png');
       } else if (tmp[1] == "2") {
        is_connected = "connected";
        $('#sp_connected_button').html(getalert("title_disconnect_button"));
        $("#connection_image").attr("src", '../images/con_cn.png');
       } else if (tmp[1] == "3") {
        is_connected = "connecting";
        $('#sp_connected_button').html(getalert("title_disconnect_button_disconnecting"));
        $("#connection_image").attr("src", '../images/con_cn.png');
       } else if (tmp[1] == "1") {
        is_connected = "disconnecting";
        $('#sp_connected_button').html(getalert("title_disconnect_button_connecting"));
        $("#connection_image").attr("src", '../images/con_cn.png');
       }
       wanstatus = tmp[0];
       $("#user_data").show();
      }
     }
    });
    $.ajax({
     url: '/boaform/admin/form4GTrafficdata?sids=' + sids,
     data: $('#form4GTrafficdata').serialize(),
     type: 'GET',
     complete: function(xhr) {
      if ('' != xhr.responseText && xhr.responseText != null) {
       var tmp = xhr.responseText;
       if (tmp == "err") {
        $('#td_rx_range').html("");
        $('#td_rx_package').html("");
        $('#td_tx_range').html("");
        $('#td_tx_package').html("");
        $('#td_up_time').html("");
        $('#lte_max_data_rate_downlink_value').html("");
        $('#lte_max_data_rate_uplink_value').html("");
        $('#lte_current_data_rate_downlink_value').html("");
        $('#lte_current_data_rate_uplink_value').html("");
       } else {
        var traffic_value = tmp.split(",");
        if (typeof(traffic_value[0]) == "undefined" || traffic_value[0] == "") {
         $('#td_rx_range').html("0");
        } else {
         $('#td_rx_range').html(traffic_value[0]);
        }
        if (typeof(traffic_value[1]) == "undefined" || traffic_value[1] == "") {
         $('#td_rx_package').html("0");
        } else {
         $('#td_rx_package').html(traffic_value[1]);
        }
        if (typeof(traffic_value[4]) == "undefined" || traffic_value[4] == "") {
         $('#td_tx_range').html("0");
        } else {
         $('#td_tx_range').html(traffic_value[4]);
        }
        if (typeof(traffic_value[5]) == "undefined" || traffic_value[5] == "") {
         $('#td_tx_package').html("0");
        } else {
         $('#td_tx_package').html(traffic_value[5]);
        }
        if (typeof(traffic_value[8]) == "undefined" || traffic_value[8] == "") {
         $('#td_up_time').html("0 s");
        } else {
         $('#td_up_time').html(traffic_value[8] + " s");
        }
        if (typeof(traffic_value[10]) == "undefined" || traffic_value[10] == "") {
         $('#lte_max_data_rate_uplink_value').html("0");
        } else {
         $('#lte_max_data_rate_uplink_value').html(traffic_value[10]);
        }
        if (typeof(traffic_value[7]) == "undefined" || traffic_value[7] == "") {
         $('#lte_current_data_rate_uplink_value').html("0");
        } else {
         $('#lte_current_data_rate_uplink_value').html(traffic_value[7]);
        }
        if (typeof(traffic_value[3]) == "undefined" || traffic_value[3] == "") {
         $('#lte_current_data_rate_downlink_value').html("0");
        } else {
         $('#lte_current_data_rate_downlink_value').html(traffic_value[3]);
        }
        if (typeof(traffic_value[9]) == "undefined" || traffic_value[9] == "") {
         $('#lte_max_data_rate_downlink_value').html("0");
        } else {
         $('#lte_max_data_rate_downlink_value').html(traffic_value[9]);
        }
       }
      }
     }
    });
  /*  $.ajax({
     url: '/boaform/admin/form4GgetRsetperiod?sids=' + sids,
     type: 'GET',
     complete: function(xhr) {
      if ('' != xhr.responseText && xhr.responseText != null) {
       var tmp = xhr.responseText;
       if (tmp == "err") {
     //   $('#lte_max_data_rate_period_value').html("");
        $('#lte_max_data_rate_period_value').val("");
       } else {
      //  $('#lte_max_data_rate_period_value').html(tmp);
       $('#lte_max_data_rate_period_value').val(tmp);
       }
      }
     }
    });
   */ 
    setTimeout('get_networkstatus()', 2000);
   }
   function get_max_data_rate_period(){
   	var lrand = Math.random();
    var sids = parseInt(lrand * 1000000);
   	  $.ajax({
     url: '/boaform/admin/form4GgetRsetperiod?sids=' + sids,
     type: 'GET',
     complete: function(xhr) {
      if ('' != xhr.responseText && xhr.responseText != null) {
       var tmp = xhr.responseText;
       if (tmp == "err") { 
        $('#lte_max_data_rate_period_value').val("");
       } else {
       $('#lte_max_data_rate_period_value').val(tmp);
       }
      }
     }
    });
   }
   function set_max_data_rate_period(){
   	var lrand = Math.random();
    var sids = parseInt(lrand * 1000000);
    var data_rate_period = $('#lte_max_data_rate_period_value').val();
    if((data_rate_period - 0) > 0 && (data_rate_period - 0) <= 65534){
    	$("#max_data_rate_period").val(data_rate_period);

 	 	 $.ajax({
  		   	 url: '/boaform/admin/form4Gsetperiod?sids=' + sids,
  	  		 data: $('#form4gsetperiod').serialize(),    
   	 		 type: 'POST',
   		  		complete: function(xhr) {
     			window.location.reload();
  	  		 }
   		 });
    }else{
    	alert("Data range is 1 ~ 65534");
    }
    
   	
   }
   function home_start_connect_disconnect(id) {
    if (wanstatus == "0") {
     return;
    }
    if (is_connected == "connected") {
     connect_disconnect_internet(0);
    } else if (is_connected == "disconnected") {
     connect_disconnect_internet(1);
    } else if (is_connected == "connecting") {
     connect_disconnect_internet(0);
    }
   }
   function connect_disconnect_internet(val) {
    ajax_connect_disconnect(val);
   }
   function ajax_connect_disconnect(opr) {
    E("connectstatus").value = opr;
    //alert(E("connectstatus").value);
    $.ajax({
     url: '/boaform/admin/form4GConf',
     data: $('#form4gconf').serialize(),
     type: 'POST',
     complete: function(xhr) {
      if ('' != xhr.responseText && xhr.responseText != null) {
       var tmp = xhr.responseText;
       if (tmp == "0") {
        alert("Success");
       } else {
        alert("Failed");
       }
      }
     }
    });
   }
   function reset_data() {
    $.ajax({
     url: '/boaform/admin/form4GResetdata',
     data: $('#form4gresetdata').serialize(),
     type: 'POST',
     complete: function(xhr) {
      if ('' != xhr.responseText && xhr.responseText != null) {
       var tmp = xhr.responseText;
       if (tmp == "0") {
        window.location.reload();
       } else {
        alert("File does not exist");
       }
      }
     }
    });
   }
  </script>
 </body>
</html>
