function E(e){return (typeof(e) == 'string') ? document.getElementById(e) : e;}
var userAgent = navigator.userAgent.toLowerCase();   
// Figure out what browser is being used   
jQuery.browser = {   
version: (userAgent.match( /.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/ ) || [0,'0'])[1],   
safari: /webkit/.test( userAgent ),   
opera: /opera/.test( userAgent ),   
msie: /msie/.test( userAgent ) && !/opera/.test( userAgent ),   
mozilla: /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent ),
ipad: /ipad/.test( userAgent ),
itouch: /itouch/.test( userAgent ) 
};

/* Decryption algorithm add by lyy in 20130324 start */
var MAX_CIPHER = 126;
var KEY_CIPHER = 11;              // key
function cipher(sourceChar, Key)  //�����㷨
{
    return String.fromCharCode((parseInt(sourceChar.charCodeAt()) + Key) % MAX + 1);
}
function decipher(sourceChar, Key)    
{
    return String.fromCharCode((parseInt(sourceChar.charCodeAt()) - 1 - Key + MAX_CIPHER) % MAX_CIPHER);
}
function data_encryption(src, dst)
{
    var i = 0;
	var source_size = 0;
	
	source_size = src.length;
	for (i=0; i<source_size; i++)
	{
	    dst[i] = cipher(src[i], KEY_CIPHER);
	}
}
function data_decryption(src, dst)
{
    var i = 0;
	var source_size = 0;
	
	source_size = src.length;
	for (i=0; i<source_size; i++)
	{
	    dst[i] = decipher(src[i], KEY_CIPHER);
	}
}
function data_decryption_js(src)
{
    var i = 0;
	var source_size = 0;
	var array_src = new Array();   // solve ie bug
	var array_dst = new Array();
	
	var dst = "";
	
	source_size = src.length;
	for (i=0; i<source_size; i++)
	{
	    array_src[i] = src.charAt(i);
	}
	
	data_decryption(array_src, array_dst);

	source_size = array_dst.length;
	for (i=0; i<source_size; i++)
	{
	    dst = dst + array_dst[i];
	}
	
	return dst;
}
/* Decryption algorithm add by lyy in 20130324 end */


function lc_wrong_color(id)
{
//	$("#"+id).css("background-color","#FFC");  
    if (typeof(g_mobile_device_p)!="undefined")  // solve lc_alert_p hou, F5 fresh page and page will luanxu
    {
	    $("#"+id).parent().addClass("wrong_color_alert");  
	//	$("#"+id).parent().attr("background-color", "#FFFFCC"); 
	}
	else
	{
        $("#"+id).addClass("wrong_color_alert");  
	}
}
function lc_mousedown_fade(id)
{
	$("#"+id).mousedown(function(){
//	$("#"+id).css("background-color","#FFF");  // by lyy in 20120927
    if (typeof(g_mobile_device_p)!="undefined")  // solve lc_alert_p hou, F5 fresh page and page will luanxu
    {
	    $("#"+id).parent().removeClass("wrong_color_alert");  
	}
	else
	{
        $("#"+id).removeClass("wrong_color_alert");
	}
	});
} 

function capchar(evt){
/*	var charCode = (evt.which) ? evt.which : event.keyCode   // zhushi by lyy in 20121108
      // if not a digit or arrow key abort
	if ( charCode == 13) 
	{
		var browser=navigator.appName
		var b_version=navigator.appVersion
		var version=parseFloat(b_version)

		if (browser=="Microsoft Internet Explorer")
		{
				return false;	 
		}
		else if (browser == "Opera")//forbiden the oprea submit the form 
		{
			$("*").blur();
			return false;	
		}
		else
		{
			evt.preventDefault();
		}
	}   */
}

function webpage_submit(web_form)
{
	if(checkForm())
	{
		$('#user_all').hide();
		$('#div_process_bar').show();
		if ($.browser.opera)//for opera11 load image unfinish
		{
			setTimeout("E("+web_form+").submit()",500);
		}
		else
		{
			E(web_form).submit();
		}
	}
}

function getParameter(paraStr, url)
{
	var result = ""; 
	var str = "&" + url.split("?")[1];
	var paraName = paraStr + "=";
	if(str.indexOf("&"+paraName)!=-1)
	{
		if(str.substring(str.indexOf(paraName),str.length).indexOf("&")!=-1)
		{
			var TmpStr=str.substring(str.indexOf(paraName),str.length);
			result=TmpStr.substr(TmpStr.indexOf(paraName),TmpStr.indexOf("&")-TmpStr.indexOf(paraName));   
		} 
		else
		{   
			result=str.substring(str.indexOf(paraName),str.length);   
		}
	}   
	else
	{   
		result="";   
	}   
	return (result.replace("&",""));   
}

//Select, auto add options into select. 
function select_index_autoadd(option_array, option_value, select_id, nv_value)
{
	var i;
	E(select_id).options.length=0;
		
    for(i=0; i<options_item.length; i++)
	{  
		E(select_id)[i]=new Option(option_array[i], option_value[i]);
		
		if(E(select_id).options[i].value == nv_value )
		{
			E(select_id).options.selectedIndex = i;
		}
	}
}


<!-- Function for remove left space from string start  -->
function ltrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
<!-- Function for remove left space from string end  -->
<!-- Function for remove right space from string start  -->
function rtrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

<!-- Function for remove right space from string end  -->
<!-- Function for remove space from string start  -->
function trim(str, chars) {
	return ltrim(rtrim(str, chars), chars);
}

function mm_preload_images() 
{ //v3.0
	var d=document; 
	
	if(d.images)
	{ 
		if(!d.MM_p) d.MM_p=new Array();
		var i,j=d.MM_p.length,a=mm_preload_images.arguments; 
		for(i=0; i<a.length; i++)
		{
			if (a[i].indexOf("#")!=0)
			{
				d.MM_p[j]=new Image; 			
				d.MM_p[j].src=a[i];
			}
		}
	}
}


function img_find_obj(n, d) { //v4.01
//changebdr
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=img_find_obj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function img_swap_img_restore() { //v3.0
  var i,x,a=document.MM_sr; //for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc; // by wangweizhou 2011
}
function img_swap_img() { //v3.0
   var i,j=0,x,a=img_swap_img.arguments; 
   document.MM_sr=new Array;
   for(i=0;i<(a.length-2);i+=3)
 
   if ((x=img_find_obj(a[i]))!=null){
		  document.MM_sr[j++]=x; 
	   if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];
	}
}


String.prototype.HTMLEncode = function()
{  
	var temp = document.createElement ("div");  
	(temp.textContent != null) ? (temp.textContent = this) : (temp.innerText = this);  
	var output = temp.innerHTML;  
	temp = null;  
	return output;  
}  
   
String.prototype.HTMLDecode = function()
{  
	var temp = document.createElement("div");  
	temp.innerHTML = this;  
 	var output = temp.innerText || temp.textContent;  
	temp = null;  
	return output;  
}

function toBreakWord(strContent, obj, len)
{
	var i = 0;
	var strTemp="";
	var tmp_obj = obj;
	tmp_obj.innerHTML="";
	
	for(i=0;i<strContent.length;i++)
	{
	 	tmp_obj.innerHTML = tmp_obj.innerHTML + strContent.charAt(i);
	 	if(tmp_obj.scrollWidth > len)
		{
			strTemp = strTemp + "<br>" + strContent.charAt(i);
		 	tmp_obj.innerHTML = "";
		}
		else
		{
			strTemp = strTemp + strContent.charAt(i);
		}
	}
	obj.innerHTML = strTemp;
} 

function creat_select_option(select_id,option_arr_id,optipn_arr_value,select_value)
{
	var i;
	var op_str="";
	var op_str_sel="";
	for(i=0; i<option_arr_id.length; i++)
	{
		op_str = "<option id='"+option_arr_id[i]+"'value='"+optipn_arr_value[i]+"'></option>";
		op_str_sel = "<option id='"+option_arr_id[i]+"'value='"+optipn_arr_value[i]+"' selected></option>";
		
		if(optipn_arr_value[i] == select_value )
		{
			//E(select_id).options.selectedIndex = i;
			$(op_str_sel).appendTo("#"+select_id);	
		}
		else
		{
			$(op_str).appendTo("#"+select_id);	
		}
		op_str="";
		op_str_sel="";
	}
}

//add by wangweizhou
function isString(_str) 
{
   return typeof _str == "string" || ( _str && (typeof _str.substr == 'function'));
}

/**********************help and alert************************************************************/
var http_request = new Array;
var xmldoc_a = new Array;
var jsondoc_a ="";
var lock_xml_get_go = 0;
function getxml()
{

   var a=getxml.arguments; 
   var i = 0;
   for(i=0;i<(a.length);i++)
   {
	  	sent_get_xml_request(i,a);
	}
}

function sent_get_xml_request(i,a)
{
	    
		if(false == isString(a[i]))
		{
			
			if(i == 0)
			{
				jsondoc_a = a[i];
			}
			else
			{
				$.extend(true,jsondoc_a,a[i]) ;
			}
			
			if(i == (a.length-1))
			{	
				 setTimeout("init();","300");
			}
			return;
			
		}
		
	    var lrand = Math.random();
	    var ids = parseInt(lrand*1000000);
		$.ajax({
			url: a[i]+"?id="+ids,
			type: 'GET',
			dataType: 'xml',
			timeout: 5000,
			error: function(xml){
			  	sent_get_xml_request(i,a);
			  // window.location.reload(true);
				},
			success: function(xml){
			xmldoc_a[i] =  xml;
	/*		var content_select = "content_"+nvram.language;
			$(xmldoc_a[i]).find('setting').each(function(){
				var id = $(this).attr('id');
				var content = $(this).find(content_select).text();
				if(id.indexOf("help_")>-1)
				{
					init_help(id,content);
				}
				else
				{
					$("#"+id).text(content);
				}
				//$("#"+id).val(content);
			});
		*/	
			if(i == (a.length-1))
			{	
				 setTimeout("init();","300");

			}
	 
			}
		});

	
}
   
function init_help(id,help_content)
{	
	if($("#"+id).length > 0)
	{
		if($(".qtip_"+id).length > 0)
		{
		    if (id.indexOf("_dynamic") != -1)  // Solve the home page dynamically alert is empty BUG, by lyy in 20121107
			{
			    return;
			}
			else
			{
			    $(".qtip_"+id).find(".qtip-content").html(help_content);
			}
		}
		else
		{
		
			var tooltips = 'topRight';
			var targets = 'bottomLeft';
			if(nvram.language == "ar")
			{
				tooltips = 'topleft';
				targets = 'bottomright';
			}
			
			$("#"+id).qtip({
				content: help_content,
				ids:id,
				position: {
					corner: {
						tooltip: tooltips,
						target: targets
					}
				},
				style: {
					name: 'sms',
					width: {min: 110,max: 350},
					border: { color: '#555' },
					color: 'white',
					background: '#555',
					'font-size': 14
				}
			});
		}
		

	}
}

function getalert(val)
{
    var ret = val;
	
	try  // solve eval("jsondoc_a."+val) throw SyntaxError: illegal character, by lyy in 20130217
	{
		if(xmldoc_a.length == 0 && jsondoc_a !== "")
		{
			if(typeof(eval("jsondoc_a."+val)) != 'undefined')
			{
				ret = eval("jsondoc_a."+val+"."+nvram.language);
			}
			return ret;
		}
	}
	catch(e)
	{
	//    alert("err");
	}
	
	return ret;

}

function get_language_value(val)
{
	
	var ret = val;
	
	if(xmldoc_a.length == 0 && jsondoc_a !== "")
	{
		if(eval("language_op."+val) != 'undefined')
		{
			ret = eval("language_op."+val);
		}
		return ret;
	}
	
/*	 for(i=0;i<(xmldoc_a.length);i++)
	 {
		var xmldoc = xmldoc_a[i];
	
		$(xmldoc).find('language_op').each(function(){
			ret =  $(this).find(val).text();
			
		});
	  
	 }  */
	 return ret;

}


function init_language(val){
	
	
	
	if(xmldoc_a.length == 0 && jsondoc_a !== "")
	{	
		for(var a in jsondoc_a)
		{
			var id = a;
			var content =  eval("jsondoc_a[a]."+nvram.language);
			if(id.indexOf("help_")>-1)
			{
				init_help(id,content);
			}
			else
			{
				$("#"+id).text(content);
			}
		}
	
	}
/*	else
	{
		var len_t =  xmldoc_a.length;

		if(val)
		{
			len_t = 1;
		}
		
		for(i=0;i< len_t;i++){
				var content_select = "content_"+nvram.language;
				var xmldoc =xmldoc_a[i];
				$(xmldoc).find('setting').each(function(){
					var id = $(this).attr('id');
		
					var content = $(this).find(content_select).text();
					
					if(id.indexOf("help_")>-1)
					{
						init_help(id,content);
					}
					else
					{
						
						$("#"+id).text(content);
					}
					//$("#"+id).val(content);
				});
		
		}
	}*/
	
	if(val != 1)
	{
		if(typeof(init_language2)!='undefined')
		{
			init_language2();
		}
	}




}

function lc_alert(message, callback, title)
{
	if (null == title)
		title = " ";

	if (typeof(g_mobile_device_p)!="undefined")  // for phone set4.html lc_alert
	{
	    $.alerts_p.okButton = getalert("alert_ok");
		j_alert_p(getalert(message), title, callback);
	}
	else
	{
		window.top.$.alerts.okButton = getalert("alert_ok");
		window.top.j_alert(getalert(message), title, callback);
	}
}

function lc_confirm(message, callback, title)
{
	if (null == title)
		title = " ";
		
	window.top.$.alerts.okButton = getalert("alert_ok");
	window.top.$.alerts.cancelButton = getalert("alert_cencel");
	window.top.j_confirm(getalert(message), title, callback);
}

function lc_prompt(message, callback, value, title)
{
	if (null == title)
		title = " ";
	
	window.top.$.alerts.okButton = getalert("alert_ok");
	window.top.$.alerts.cancelButton = getalert("alert_cencel");
	window.top.j_prompt(getalert(message), value, title, callback);
}

function lc_info(message, title)
{
	if (null == title)
		title = " ";

	window.top.j_info(message, title);
}

function lc_hide_alert()
{
	window.top.j_hide_alert();
}


function lc_alert_p(message, callback, title)
{
	if (null == title)
		title = " ";

	$.alerts_p.okButton = getalert("alert_ok");
	$.alerts_p.okButton = getalert("cancelButton");
	j_alert_p(getalert(message), title, callback);
}

function lc_confirm_p(message, callback, title)
{
	if (null == title)
		title = " ";
		
	$.alerts_p.okButton = getalert("alert_ok");
	$.alerts_p.okButton = getalert("cancelButton");
	j_confirm_p(getalert(message), title, callback);
}
/**********************help and alert end************************************************************/

if (typeof(g_mobile_device_p)=="undefined")  // solve lc_alert_p hou, F5 fresh page and page will luanxu
{
    javascript:window.history.forward(1); 
}

// JavaScript Document
 function isIE()
{ 
    if (window.navigator.userAgent.toLowerCase().indexOf("msie")>=1) 
    {
        return true;
    } 
    else 
    {
        return false;
    } 
} 



if(!isIE())
{ //firefox innerText define
    HTMLElement.prototype.__defineGetter__("innerText", 
    function()
	{
        var anyString = "";
        var childS = this.childNodes;
        for(var i=0; i<childS.length; i++)
        { 
            if(childS[i].nodeType==1)
            {//anyString += childS[i].tagName=="BR" ? "\n" : childS[i].innerText;
                anyString += childS[i].innerText;
            }
            else if(childS[i].nodeType==3)
            {
                anyString += childS[i].nodeValue;
            }
        }
        return anyString;
    } 
    ); 
    HTMLElement.prototype.__defineSetter__("innerText", 
    function(sText)
	{
        this.textContent=sText; 
    } 
    ); 
}

/*************************************************/


function lc_reset_or_clear_new_sms_show(val)
{
    var lrand = Math.random();
	var sids= parseInt(lrand*1000000);
	
	$.ajax({
			//	url: '/cgi-bin/set_nv.cgi?nv=sms_new_msg_notify&pram=' + val,
			    url: '/cgi-bin/sms.cgi?which_cgi=ajax_set_tmp_nv&nv=sms_new_msg_notify&pram=' + val +'&sids='+sids,
				type: 'post',
				complete: function (xhr) 
				{
					
				}
		});	
	
}

function get_current_time()
{
	//get the time of pc
	var date = "";
	{
		
		var d = new Date();
		var vYear = d.getFullYear();
		if(vYear < 1900)
		{
			vYear = vYear - 1900;
		}
		else
		{
			vYear = vYear - 2000;
		}
		var vMon = d.getMonth() + 1;
		var vDay = d.getDate();
		var h = d.getHours(); 
		var m = d.getMinutes(); 
		var se = d.getSeconds(); 
//		date  =   (vDay<10 ? "0"+ vDay : vDay)+"/"+(vMon< 10 ? "0" + vMon : vMon) + " " +vYear  +" "+  (h<10 ? "0"+ h : h) +":"+ (m<10 ? "0" + m : m)  +":"+ (se<10 ? "0" +se : se);	
		date  =   vYear + "-" + (vMon< 10 ? "0" + vMon : vMon) + "-" + (vDay<10 ? "0"+ vDay : vDay) +" "+  (h<10 ? "0"+ h : h) +":"+ (m<10 ? "0" + m : m)  +":"+ (se<10 ? "0" +se : se);
		//date  =   vYear + "-" + vMon + "-" + vDay +" "+ h +":"+ m  +":"+ se;
		
	}
	return date;
	
}


/******************************
 * Session
 *****************************/
var keep_alive_id =0;
function lc_keep_alive()
{
	/* Only the parent window should keep the session */
	if (window != window.top)
		return;

		
	if(keep_alive_id == 0)
	{
		return;
	}

	 

	$.ajax({
			url: "/cgi-bin/keep_alive.cgi",
			type: "POST",
			complete : function(xhr) {
					
					var url =  location.href;
					var sid = "";
					var dex = getParameter("sid",url.replace(/%20/g," "));
					if(dex != "")
					{	
						sid = dex.split("=")[1];
					}
					/* If this session is invalid, stop keeping. */
					
					if (null != $.cookie("SID") && "" != $.cookie("SID"))
					{
						var c_sid = $.cookie("SID");
						var sid_g = c_sid.split(":");
						sid = sid_g[0].split("=")[1];
					}
					

					if(xhr.responseText != sid)
					{
						var page = "start.html?sid=" + sid;
						window.location.replace(page);
					}
					
					setTimeout("lc_keep_alive()", 3 * 1000);
			}
	});
}

var userguideURL = "/misc/TATA_User_Guide_";
function lc_switch_userguideURL(language)
{
	var  url=window.parent.location;
	url=url+'ddfdf';
	if(url.indexOf('home.html') ==-1)	
	{
	    url='../';
	}
	else
	{
		url='';	
	}
	$('#lc_div_userguide').click(function (e){window.open(url+userguideURL+language+".pdf","newwindow","");});
}


/********************make the value of input text equal to the input file *****************************/
function valcpy(source,dest)
{
	//var s_id = "#"+id;	
	var value = $("#"+source).val();
	var filename_all = value.split("\\");
	var filename	= "";
	if (!$.browser.msie)
	{
		if(filename_all.length > 1)
		{
			value = filename_all[filename_all.length-1];
		}
	}
	$('#'+dest).val(value);
}


// change image by lyy in 20130117
function change_upfile_img(lang, id)
{
    if (lang != "")
	{
	    var img = "../images/button_size_92_file_" + lang + ".png"
	    $("#"+id).css("background-image","url("+img+")");
	}
}


function swipeleftright(left_func, right_func)  // for phone ���һ����л�ҳ��
{
    $("body").bind('swiperight', function() {
        if(typeof(eval(right_func))=="function")
        {
		//    alert("right_func");
        //    eval(right_func+"()");
        }
    }).bind('swipeleft', function() {
        if(typeof(eval(left_func))=="function")
        {
        //    eval(left_func+"()");
        }
    });
}

