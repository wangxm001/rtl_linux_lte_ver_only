<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="all.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
<link rel="stylesheet" type="text/css" href="../css/globals.css">
<link href="../css/user2.css" rel="stylesheet" />
<script type="text/javascript" src="../language/js/wan.js"></script>
<script type='text/javascript' src='../ui_js/jquery.js'></script>
<script type="text/javascript" src="../ui_js/jquery.qtip.js"></script>
<script type="text/javascript" src="../js/globals.js"></script>
<script type="text/javascript" src="../js/check.js"></script>
<script type="text/javascript" src="../js/check_wan.js"></script>
<title>wan pin</title>
<script type='text/javascript'>
nvram = {
 sim_data: '<% getInfo("modemstatus"); %>', // 'pin_enable,3,10', 
 language: 'en'
};
var subimt_limitation = 0;
function init()
{
 
 get_pin_remain();
 switch (nvram.sim_data)
 {
  case "not_invalid":
   $("#modem_show_txt").html(getalert("alert_wan_not_invalid_sim"));
   $("#modem_show_txt").attr("alt","alert_wan_not_invalid_sim");
   $("#user_data").hide();
   $("#modem_shoe_div").show();
   return;
  case "no_simcard":
   $("#modem_show_txt").html(getalert("alert_wan_nosim"));
   $("#modem_show_txt").attr("alt","alert_wan_nosim");
   $("#user_data").hide();
   $("#modem_shoe_div").show();
   return;
  case "not_ready":
   $("#modem_show_txt").html(getalert("alert_wan_modem_notready"));
   $("#modem_show_txt").attr("alt","alert_wan_modem_notready");
   $("#user_data").hide();
   $("#modem_shoe_div").show();
   return;
     case "sim_ready":
   $("#user_data").hide();
   $("#pin_status_show").html(getalert("alert_wan_pin_disable"));
   $('#lock_radio').show();
   $('#unlock_radio').hide();
   $('#modify_radio').hide();
   $('#disable_radio').hide();
   $('#puk_radio').hide();
   get_connect_status();
   break;
  case "pin_disable":
   $("#user_data").show();
   $("#pin_status_show").html(getalert("alert_wan_pin_disable"));
   $('#lock_radio').show();
   $('#unlock_radio').hide();
   $('#modify_radio').hide();
   $('#disable_radio').hide();
   $('#puk_radio').hide();
   break;
  case "pin_enable":
   $("#user_data").show();
   $("#pin_status_show").html(getalert("alert_wan_pin_enable"));
   $('#lock_radio').hide();
   $('#unlock_radio').hide();
   $('#modify_radio').show();
   $('#disable_radio').show();
   $('#puk_radio').hide();
   break;
  case "need_pin":
   $("#user_data").show();
   $("#pin_status_show").html(getalert("alert_wan_pinblock"));
   $('#lock_radio').hide();
   $('#unlock_radio').show();
   $('#modify_radio').hide();
   $('#disable_radio').hide();
   $('#puk_radio').hide();
   break;
  case "need_puk":
   $("#user_data").show();
   $("#pin_status_show").html(getalert("alert_wan_pukblock"));
   $('#lock_radio').hide();
   $('#unlock_radio').hide();
   $('#modify_radio').hide();
   $('#disable_radio').hide();
   $('#puk_radio').show();
   break;
  case "puk_lock":
   $("#modem_show_txt").html(getalert("alert_wan_puklock"));
   $("#modem_show_txt").attr("alt","alert_wan_puklock");
   $("#user_data").hide();
   $("#modem_shoe_div").show();
   break;
  default:
   $("#modem_show_txt").html(getalert("alert_wan_modem_error"));
   $("#modem_show_txt").attr("alt","alert_wan_modem_error");
   $("#user_data").hide();
   $("#modem_shoe_div").show();
   return;
 }
 init_language();
 init_language2();
}
function init_language2()
{
   $("#modem_show_txt").html(getalert($("#modem_show_txt").attr("alt")));
 switch (nvram.sim_data)
 {
     case "sim_ready":
   $("#pin_status_show").html(getalert("alert_wan_pin_disable"));
   break;
  case "pin_disable":
   $("#pin_status_show").html(getalert("alert_wan_pin_disable"));
   break;
  case "pin_enable":
   $("#pin_status_show").html(getalert("alert_wan_pin_enable"));
   break;
  case "need_pin":
   $("#pin_status_show").html(getalert("alert_wan_pinblock"));
   break;
  case "need_puk":
   $("#pin_status_show").html(getalert("alert_wan_pukblock"));
   break;
  case "puk_lock":
  case "not_invalid":
  case "no_simcard":
  case "not_ready":
      break;
  default:
   return;
 }
}
</script>
</head>
<body onload="getxml(language_wan);" >
<div id="user_all" onkeydown="javascript:capchar(event)" style="padding:0 1em 0 1em;">
  <div id="user_data" style="display:none">
    <div id="user_table">
  <form action="/boaform/admin/form4GsubmitInternetPin" method="POST" name="form4gsubmitinternetpin" id="submitpinstatus">
  <input type="hidden" value="/admin/lct_internet_pin.asp" name="submit-url">
   <div class="line">
       <div class="margin">
     <div class="line">
         <div class="s-12 l-5 line_form">
                      <label class="name" id="td_pin"></label>
                  </div>
     </div>
     <div class="line">
         <div class="s-12 l-5 line_form">
                      <label class="name" id="td_status"></label>
                  </div>
         <div class="s-12 l-5 line_form">
                      <label class="name" id="pin_status_show"></label>
                  </div>
     </div>
     <div class="line">
         <div class="s-12 l-5 line_form">
                      <label class="name" id="td_pintime"></label>
                  </div>
         <div class="s-12 l-5 line_form">
                      <label class="name" id="td_span_pin_retimes"></label>
                  </div>
     <div class="line">
         <div class="s-12 l-5 line_form">
                      <label class="name" id="td_puktime"></label>
                  </div>
      <div class="s-12 l-5 line_form">
                      <label class="name" id="td_span_puk_retimes"></label>
                  </div>
     </div>
    </div>
   </div>
   <div class="line" id= "table_pin" >
       <div class="margin">
     <div class="line">
         <div class="s-12 l-5 line_form">
                      <label class="name" id="td_simcard"></label>
                  </div>
         <div style="FLOAT: left" >
       <div class="line">
           <div id="lock_radio" width=90 class="s-12 l-5 line_form" >
            <input type="radio" name="pinset" value="1" onClick="modify(0)" /><span id="div_lock"></span>
           </div>
           <div id="unlock_radio" width=90 class="s-12 l-5 line_form">
         <input type="radio" name="pinset" value="2" onClick="modify(1)" /><span id="div_unlock"></span>
           </div>
           <div id="modify_radio" width=90 class="s-12 l-5 line_form">
         <input type="radio" name="pinset" value="3" onClick="modify(2)" /><span id="div_modify"></span>
        </div>
           <div id="disable_radio" width=90 class="s-12 l-5 line_form">
         <input type="radio" name="pinset" value="4" onClick="modify(3)" /><span id="div_disabled"></span>
           </div>
           <div id="puk_radio" width=90 class="s-12 l-5 line_form">
         <input type="radio" name="pinset" value="5" onClick="modify(4)" /><span id="div_puk"></span>
           </div>
       </div>
         </div>
     </td>
     </div>
    </div>
   </div>
   <div class="line">
    <div class="margin">
     <div id="puk_tr" align="center" class="line" style="display:none">
      <div class="s-12 l-5 line_form">
       <label class="name" id="td_puk"></label>
      </div>
      <div class="s-12 l-5 line_form">
       <input id="puk" name="puk" type="password" maxlength="8" />
      </div>
     </div>
     <div id="oldpin_tr" align="center" class="line" style="display:none">
      <div class="s-12 l-5 line_form">
       <label class="name" id="td_oldpin"></label>
      </div>
      <div class="s-12 l-5 line_form">
       <input id="oldpin" name="oldpin" type="password" maxlength="8" />
      </div>
     </div>
     <div id="newpin_tr" align="center" class="line" style="display:none">
      <div class="s-12 l-5 line_form">
       <label class="name" id="td_newpin"></label>
      </div>
      <div class="s-12 l-5 line_form">
       <input id="newpin" name="newpin" type="password" maxlength="8" />
      </div>
     </div>
     <div id="confirmpin_tr" align="center" class="line" style="display:none">
      <div class="s-12 l-5 line_form">
       <label class="name" id="td_confirmpin"></label>
      </div>
      <div class="s-12 l-5 line_form">
       <input id="confirmpin" name="confirmpin" type="password" maxlength="8" />
      </div>
     </div>
    </div>
   </div>
        </form>
  <form action="/boaform/admin/form4GConnectget" method="POST" name="form4gcconnectget" id="getnetworkconnectstatus">
  </form>
  <form action="/boaform/admin/form4GInternetPin" method="POST" name="form4ginternetpin" id="internet_pin_form">
  </form>
    </div> <!-- div id="user_table" -->
    <div id="user_end" class="line">
        <div id="user_end_in" class="margin">
   <div class="s-12 l-2">
    <div class="button" onClick="cgi_submit(0);"><span id="sp_save"></span></div>
   </div>
   <div class="s-12 l-2">
    <div class="button" onclick="window.location=location;"><span id="sp_cancel"></span></div>
   </div>
        </div>
    </div>
  </div> <!-- div id="user_data" -->
  <br class="clearfloat"/>
</div> <!-- div id="user_all" -->
</div>
<div id="modem_shoe_div" align="center" style="display:none;">
    <div class="line">
  <div class="margin">
      <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
   </div>
   <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
   </div>
   <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
   </div>
   <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
   </div>
   <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
    <div class="s-12 l-10 line_form">
     <label class="name" id="modem_show_txt"></label>
    </div>
   </div>
  </div>
    </div>
</div>
<div id="div_process_bar" style="display:none">
 <div class="line">
  <div class="margin">
   <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
   </div>
   <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
   </div>
   <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
   </div>
   <div class="s-12 l-12 busy_rndcontainer" align="center">
   </div>
   <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
   </div>
   <div class="s-12 l-12 line_form busy_rndcontainer_font" align="center">
    <label class="name" id="sp_wait_process" style="color:#000"></label>
   </div>
  </div>
 </div>
</div>
<script language="javascript">
function get_pin_remain()
{
 $.ajax({
 url: '/boaform/admin/form4GInternetPin',
 data: $('#internet_pin_form').serialize(),
 type: 'POST',
 complete: function (xhr)
 {
  if (''!=xhr.responseText && xhr.responseText!=null)
  {
   var tmp = xhr.responseText.split(",");
   $("#td_span_pin_retimes").html(tmp[0]);
   $("#td_span_puk_retimes").html(tmp[1]);
  }
  else
  {
   $("#td_span_pin_retimes").html("");
   $("#td_span_puk_retimes").html("");
  }
 }
 });
}
function get_connect_status()
{
 $.ajax({
 url: '/boaform/admin/form4GConnectget',
 data: $('#getnetworkconnectstatus').serialize(),
 type: 'POST',
 complete: function (xhr)
 {
  if (''!=xhr.responseText && xhr.responseText!=null)
  {
   var tmp = xhr.responseText.split(",");
   is_connectd_status = tmp[1];
   if(tmp[1] =="2")
   {
    $("#modem_show_txt").html(getalert("alert_wan_connected"));
    $("#modem_show_txt").attr("alt","alert_wan_connected");
    $("#user_data").hide();
    $("#modem_shoe_div").show();
   }
   else if(tmp[1] =="3")
   {
    $("#modem_show_txt").html(getalert("alert_wan_connecting"));
    $("#modem_show_txt").attr("alt","alert_wan_connecting");
    $("#user_data").hide();
    $("#modem_shoe_div").show();
   }
   else if(tmp[1] =="1")
   {
    $("#modem_show_txt").html(getalert("alert_wan_connecting"));
    $("#modem_show_txt").attr("alt","alert_wan_connecting");
    $("#user_data").hide();
    $("#modem_shoe_div").show();
   }
   else 
   {
    $("#user_data").show();
    $("#modem_shoe_div").show();
   }
  }
 }
 });
}
function checkPin(val)
{
 if(val.length<4 || val.length>8)
 {
  alert(getalert("alert_pin_lengh_inviald"));
  return false;
 }
 if(!check_number(val))
 {
  alert(getalert("alert_number_inviald"));
  return false;
 }
 return true;
}
function checkPuk(val)
{
 if(val.length != 8)
 {
  alert(getalert("alert_puk_lengh_inviald"));
  return false;
 }
 if(!check_number(val))
 {
  alert(getalert("alert_number_inviald"));
  return false;
 }
 return true;
}
function init_pro_wrong_input_css()
{
    $("#oldpin").removeClass("wrong_color_alert");
 $("#newpin").removeClass("wrong_color_alert");
 $("#confirmpin").removeClass("wrong_color_alert");
 $("#puk").removeClass("wrong_color_alert");
}
function modify(val)
{
    init_pro_wrong_input_css();
 if(val == 0 || val == 1 ||val == 3)
 {
  $('#puk_tr').hide();
  $('#oldpin_tr').show();
  $('#newpin_tr').hide();
  $('#confirmpin_tr').hide();
 }
 if(val == 2)
 {
  $('#puk_tr').hide();
  $('#oldpin_tr').show();
  $('#newpin_tr').show();
  $('#confirmpin_tr').show();
 }
 else if(val == 4)
 {
  $('#puk_tr').show();
  $('#oldpin_tr').hide();
  $('#newpin_tr').show();
  $('#confirmpin_tr').show();
 }
}
function checkForm()
{
 lc_mousedown_fade(E("oldpin").id);
 lc_mousedown_fade(E("newpin").id);
 lc_mousedown_fade(E("confirmpin").id);
 lc_mousedown_fade(E("puk").id);
 if(document.form4gsubmitinternetpin.pinset[0].checked || document.form4gsubmitinternetpin.pinset[1].checked ||document.form4gsubmitinternetpin.pinset[3].checked)
 {
  if(checkPin(E("oldpin").value)==false)
  {
   lc_wrong_color(E("oldpin").id);
   return false;
  }
  else
  {
   return true;
  }
 }
 else if(document.form4gsubmitinternetpin.pinset[2].checked)
 {
  if(checkPin(E("oldpin").value)==false)
  {
   lc_wrong_color(E("oldpin").id);
   return false;
  }
  if(checkPin(E("newpin").value)==false)
  {
   lc_wrong_color(E("newpin").id);
   return false;
  }
  if(checkPin(E("confirmpin").value)==false)
  {
   lc_wrong_color(E("confirmpin").id);
   return false;
  }
  if(E("newpin").value != E("confirmpin").value)
  {
   lc_wrong_color(E("newpin").id);
   lc_wrong_color(E("confirmpin").id);
   alert(getalert("alert_pin_code_not_math"));
   return false;
  }
 }
 else if(document.form4gsubmitinternetpin.pinset[4].checked)
 {
  if(checkPuk(E("puk").value)==false)
  {
   lc_wrong_color(E("puk").id);
   return false;
  }
  if(checkPin(E("newpin").value)==false)
  {
   lc_wrong_color(E("newpin").id);
   return false;
  }
  if(checkPin(E("confirmpin").value)==false)
  {
   lc_wrong_color(E("confirmpin").id);
   return false;
  }
  if(E("newpin").value != E("confirmpin").value)
  {
   lc_wrong_color(E("newpin").id);
   lc_wrong_color(E("confirmpin").id);
   alert(getalert("alert_pin_code_not_math"));
   return false;
  }
 }
 else
 {
  return false;
 }
 if(subimt_limitation ==0)
 {
  subimt_limitation =1;
  return true;
 }
 else
 {
  return false;
 }
}
function cgi_submit(val)
{
 if(!checkForm())
 {
	window.location=location;
	return;
 }
 $.ajax({
   url: '/boaform/admin/form4GsubmitInternetPin',
   data: $('#submitpinstatus').serialize(),
   type: 'POST',
   complete: function (xhr)
   {
		window.location=location;
   }
  });
 
}
</script>
</body>
</html>
