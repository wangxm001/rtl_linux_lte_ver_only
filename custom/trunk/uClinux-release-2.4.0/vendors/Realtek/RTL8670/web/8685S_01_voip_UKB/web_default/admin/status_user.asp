<html>
<! Copyright (c) Realtek Semiconductor Corp., 2003. All Rights Reserved. ->
<head>
<link href="all.css" rel="stylesheet" type="text/css" />
<META HTTP-EQUIV=Refresh CONTENT="10; URL=status_user.asp">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<title><% multilang("68" "LANG_DEVICE_STATUS"); %></title>
<script type="text/javascript" src="share.js"></script>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="globals.js"></script>
<SCRIPT type="text/javascript" src="../ui_js/jquery.qtip.js"></SCRIPT>
<script>
var getObj = null;
function modifyClick(url)
{
 var wide=600;
 var high=400;
 if (document.all)
  var xMax = screen.width, yMax = screen.height;
 else if (document.layers)
  var xMax = window.outerWidth, yMax = window.outerHeight;
 else
    var xMax = 640, yMax=480;
 var xOffset = (xMax - wide)/2;
 var yOffset = (yMax - high)/3;
 var settings = 'width='+wide+',height='+high+',screenX='+xOffset+',screenY='+yOffset+',top='+yOffset+',left='+xOffset+', resizable=yes, toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes';
 window.open( url, 'Status_Modify', settings );
}
function get_active_curretn_apn()
{
 var lrand = Math.random();
 var sids = parseInt(lrand * 1000000);
 $.ajax({
  url: '/boaform/admin/form4Ggetactiveapn?sids=' + sids,
  type: 'GET',
  complete: function(xhr) {
   if('' != xhr.responseText && xhr.responseText != null && xhr.responseText != "err")
   {
    //net_pro_list: 'profile_name,pn_name,user_name,password,auth_type,ip_mode';
    //auth_type:auto_type:PAP,CHAP,AUTO/1 2 3
    //ip_mode:IPV4,IPV6,IPV4V6/1 2 3
    var tmpvalue = xhr.responseText.split(',');
 $('#apn_profile_name_value').html(tmpvalue[0]);
 $('#apn_apn_name_value').html(tmpvalue[1]);
 $('#apn_user_name_value').html(tmpvalue[2]);
 $('#apn_user_password_value').html(tmpvalue[3]);
    switch(tmpvalue[4])
    {
     case "1":
  $('#apn_auth_type_value').html("PAP");
      break;
     case "2":
  $('#apn_auth_type_value').html("CHAP");
      break;
     case "0":
  $('#apn_auth_type_value').html("AUTO");
      break;
     default:
  $('#apn_auth_type_value').html("");
    }
    switch(tmpvalue[5])
    {
     case "1":
  $('#apn_ip_mode_value').html("IPV4");
      break;
     case "2":
  $('#apn_ip_mode_value').html("IPV6");
      break;
     case "3":
  $('#apn_ip_mode_value').html("IPV4V6");
      break;
     default:
  $('#apn_ip_mode_value').html("");
    }
   }
   else
   {
    $('#apn_profile_name_value').html("");
 $('#apn_apn_name_value').html("");
 $('#apn_user_name_value').html("");
 $('#apn_user_password_value').html("");
 $('#apn_auth_type_value').html("");
 $('#apn_ip_mode_value').html("");
   }
  }
 });
}
function disButton(id)
{
       getObj = document.getElementById(id);
       window.setTimeout("getObj.disabled=true", 100);
 return false;
}
function on_init()
{
 // Mason Yu for IPv6
 if (!<% checkWrite("IPv6Show"); %>) {
  if (document.getElementById) // DOM3 = IE5, NS6
  {
   document.getElementById('ipv6DefaultGW').style.display = 'none';
  }
  else {
   if (document.layers == false) // IE4
   {
    document.all.ipv6DefaultGW.style.display = 'none';
   }
  }
 }
  get_active_curretn_apn();
 return true;
}
</script>
</head>
<body onLoad="on_init();">
<blockquote>
<h2><b><font color="#0dabe1"><% multilang("68" "LANG_DEVICE_STATUS"); %></font></b></h2>
<table border=0 width="500" cellspacing=0 cellpadding=0>
<tr><td><font size=2>
 <% multilang("69" "LANG_PAGE_DESC_DEVICE_STATUS_SETTING"); %>
</font></td></tr>
<tr><td><hr size=1 noshade align=top><br></td></tr>
</table>
<form action=/boaform/admin/formStatus method=POST name="status2">
<table width=400 border=0>
  <tr>
    <td width=100% colspan="2" bgcolor="#0dabe1"><font color="#FFFFFF" size=2><b><% multilang("70" "LANG_SYSTEM"); %></b></font></td>
  </tr>
  <tr bgcolor="#EEEEEE">
    <td width=40%><font size=2><b><% multilang("95" "LANG_DEVICE_NAME"); %></b></td>
    <td width=60%><font size=2>VH510B</td>
  </tr>
    <tr bgcolor="#EEEEEE">
    <td width=40%><font size=2><b><% multilang("2634" "LANG_DEVICE_DESCRIPTION"); %></b></td>
    <td width=60%><font size=2>LTE Router</td>
  </tr>
    <tr bgcolor="#EEEEEE">
    <td width=40%><font size=2><b><% multilang("2635" "LANG_DEVICE_MANUFACTURER"); %></b></td>
    <td width=60%><font size=2>Verve Connect</td>
  </tr>
  <tr bgcolor="#DDDDDD">
    <td width=40%><font size=2><b><% multilang("72" "LANG_UPTIME"); %></b></td>
    <td width=60%><font size=2><% getInfo("uptime"); %></td>
  </tr>

  <tr bgcolor="#EEEEEE">
    <td width=40%><font size=2><b><% multilang("75" "LANG_FIRMWARE_VERSION"); %></b></td>
    <td width=60%><font size=2><% getInfo("fwVersion"); %></td>
  </tr>
   <tr id='wantype' bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>WEBUI VERSION:</b></td>
    <td width=60%><font size=2><% getInfo("hidewebuiversion"); %></td>
  </tr>
  <tr bgcolor="#EEEEEE">
    <td width=40%><font size=2><b>Serial Number</b></td>
    <td width=60%><font size=2><% getInfo("rtk_serialno"); %></td>
  </tr>
  <% DSLVer(); %>
  <% cpuUtility(); %>
  <% memUtility(); %>
   <tr bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b><% multilang("80" "LANG_NAME_SERVERS"); %></b></td>
    <td width=60%><font size=2><% getNameServer(); %></td>
  </tr>
  <tr bgcolor="#DDDDDD" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>IPv4 <% multilang("81" "LANG_DEFAULT_GATEWAY"); %></b></td>
    <td width=60%><font size=2><% getDefaultGW(); %></td>
  </tr>
  <tr id='ipv6DefaultGW' bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>IPv6 <% multilang("81" "LANG_DEFAULT_GATEWAY"); %></b></td>
    <td width=60%><font size=2><% getDefaultGW_ipv6(); %></td>
  </tr>
</table>
<table width=400 border=0>

  <% DSLStatus(); %>
  <tr>
    <td width=100% colspan="2" bgcolor="#0dabe1"><font color="#FFFFFF" size=2><b><% multilang("6" "LANG_LAN"); %><% multilang("214" "LANG_CONFIGURATION"); %></b></font></td>
  </tr>
  <tr bgcolor="#EEEEEE">
    <td width=40%><font size=2><b><% multilang("85" "LANG_IP_ADDRESS"); %></b></td>
    <td width=60%><font size=2><% getInfo("lan-ip"); %></td>
  </tr>
  <tr bgcolor="#DDDDDD">
    <td width=40%><font size=2><b><% multilang("86" "LANG_SUBNET_MASK"); %></b></td>
    <td width=60%><font size=2><% getInfo("lan-subnet"); %></td>
  </tr>
  <% DHCPSrvStatus(); %>
  <tr bgcolor="#EEEEEE">
    <td width=40%><font size=2><b><% multilang("88" "LANG_MAC_ADDRESS"); %></b></td>
    <td width=60%><font size=2><% getInfo("elan-Mac"); %></td>
  </tr>
</table>
<table width=400 border=0 style="display: none;">
  <tr>
    <td width=100% colspan="2" bgcolor="#0dabe1"><font color="#FFFFFF" size=2><b>Current APN</b></font></td>
  </tr>
  <tr bgcolor="#EEEEEE" style="display:none">
    <td width=40%><font size=2><b>Profile name</b></td>
    <td width=60%><font size=2 id="apn_profile_name_value"></td>
  </tr>
  <tr bgcolor="#DDDDDD">
    <td width=40%><font size=2><b>APN name</b></td>
    <td width=60%><font size=2 id="apn_apn_name_value"></td>
  </tr>
  <tr bgcolor="#EEEEEE">
    <td width=40%><font size=2><b>User name</b></td>
    <td width=60%><font size=2 id="apn_user_name_value"></td>
  </tr>
  <tr bgcolor="#DDDDDD">
    <td width=40%><font size=2><b>User password</b></td>
    <td width=60%><font size=2 id="apn_user_password_value"></td>
  </tr>
  <tr bgcolor="#EEEEEE">
    <td width=40%><font size=2><b>Auth type</b></td>
    <td width=60%><font size=2 id="apn_auth_type_value"></td>
  </tr>
  <tr bgcolor="#DDDDDD">
    <td width=40%><font size=2><b>Ip mode</b></td>
    <td width=60%><font size=2 id="apn_ip_mode_value"></td>
  </tr>
</table>
</form>
<br>
<form action=/boaform/admin/formStatus method=POST name="status">
<table width=600 border=0 <% checkWrite("bridge-only"); %>>
 <tr>
    <td width=100% colspan=7 bgcolor="#0dabe1"><font color="#FFFFFF" size=2><b><% multilang("11" "LANG_WAN"); %><% multilang("214" "LANG_CONFIGURATION"); %></b></font></td>
  </tr>
  <% wanConfList(); %>
</table>
  <% wan3GTable(); %>
  <% wanPPTPTable(); %>
  <% wanL2TPTable(); %>
  <% wanIPIPTable(); %>
  <input type="hidden" value="/admin/status_user.asp" name="submit-url">
  <input type="submit" value="<% multilang("379" "LANG_REFRESH"); %>" name="refresh">&nbsp;&nbsp;

</form>
</blockquote>
</body>
</html>
