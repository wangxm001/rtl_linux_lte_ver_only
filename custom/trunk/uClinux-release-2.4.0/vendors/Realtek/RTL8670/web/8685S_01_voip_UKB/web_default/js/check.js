/*
	wangweizhou  check
	2010-06-1 wangweizhou

	For use with wangweizhou only.
	No part of this file may be used without permission.
*/
Array.prototype.find = function(v) {
	for (var i = 0; i < this.length; ++i)
	if (this[i] == v) return i;
	return -1;
}

Array.prototype.remove = function(v) {
	for (var i = 0; i < this.length; ++i) 
	{
		if (this[i] == v) 
		{
			this.splice(i, 1);
			return true;
		}
	}
	return false;
}

String.prototype.trim = function() {
	return this.replace(/^\s+/, '').replace(/\s+$/, '');
}

Number.prototype.pad = function(min) {
	var s = this.toString();
	while (s.length < min) s = '0' + s;
	return s;
}

Number.prototype.hex = function(min)
{
	var h = '0123456789ABCDEF';
	var n = this;
	var s = '';
	do {
		s = h.charAt(n & 15) + s;
		n = n >>> 4;
	} while ((--min > 0) || (n > 0));
	return s;
}


function check_alphanumeric(s)
{
	var patrn =/^[0-9a-zA-Z]*$/;
	if (!patrn.exec(s))
	{
		return false;
	}
	else
	{
    	return true; 		
	}

}

function check_number(val)
{
	var i=0;

	for(i=0; i<val.length; i++)
	{
		if(val.charAt(i)<'0' || val.charAt(i)>'9')
		{	
			return false;
		}
	}
	return true;
}

function is_mac_0(mac)
{
	return (mac == '00:00:00:00:00:00');
}

function check_ip(val){
	if(val.length==0)
	{
		return false;
	}
	ip = '(25[0-5]|2[0-4]\\d|1\\d\\d|\\d\\d|\\d)';
	ipdot = ip + '\\.';
	is_ipaddress = new RegExp('^'+ipdot+ipdot+ipdot+ip+'$');
	if (!is_ipaddress.test(val))
	{
		return false;
	}
	return true;
}

function check_hostname(hostname)
{
	var ret;
	var regx = /^[a-zA-Z0-9]{1,1}[a-zA-Z0-9-]{0,14}$/;
	ret = regx.test(hostname);
	if(ret == true)
	{
		if(hostname.charAt(hostname.length-1) == '-')
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	return false;
}

function to_hex(val)
{
	var h = (val-0).toString(16);
	if(h.length==1) h='0'+h;
	return h.toUpperCase();
}

function ip_to_aton(ip)
{
	var o, x, i;
// this is goofy because << mangles numbers as signed
	o = ip.split('.');
	x = '';
	for (i = 0; i < 4; ++i) x += (o[i] * 1).hex(2);
	return parseInt(x, 16);
}

function aton_to_ip(ip)
{
	return ((ip >> 24) & 255) + '.' + ((ip >> 16) & 255) + '.' + ((ip >> 8) & 255) + '.' + (ip & 255);
}

function fix_ip(ip, x)
{
	var a, n, i;
	a = ip.split('.');
	
	if (a.length != 4) return null;
	for (i = 0; i < 4; ++i) 
	{
		n = a[i] * 1;
		if ((isNaN(n)) || (n < 0) || (n > 255)) return null;
		a[i] = n;
	}
	if ((x) && ((a[3] == 0) || (a[3] == 255))) return null;
	
	return a.join('.');

}

function cmp_ip(a, b)
{
	if ((a = fixIP(a)) == null) a = '255.255.255.255';
	if ((b = fixIP(b)) == null) b = '255.255.255.255';
	return aton(a) - aton(b);
}

function cmp_int(a, b)
{
	a = parseInt(a, 10);
	b = parseInt(b, 10);
	return ((isNaN(a)) ? -0x7FFFFFFF : a) - ((isNaN(b)) ? -0x7FFFFFFF : b);
}

function cmp_date(a, b)
{
	return b.getTime() - a.getTime();
}
