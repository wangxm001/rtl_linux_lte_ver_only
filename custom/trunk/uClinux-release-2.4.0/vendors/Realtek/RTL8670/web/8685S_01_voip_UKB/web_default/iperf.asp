
<html>
<! Copyright (c) Realtek Semiconductor Corp., 2003. All Rights Reserved. ->
<head>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<META HTTP-EQUIV=Refresh CONTENT="35; URL=iperf.asp">
<title>Iperf</title>
<script type="text/javascript" src="share.js">
</script>
<script>
function skip () { this.blur(); }

function startClick()
{
	d1 = getDigit(document.formiperf.port.value, 1);
  if (d1 > 65535 || d1 < 1) {
   alert('<% multilang(LANG_INVALID_DESTINATION_PORT_NUMBER); %>');
   document.formiperf.port.focus();
   return false;
  }
  
  d1 = getDigit(document.formiperf.clientNum.value, 1);
  if (d1 > 10 || d1 < 1) {
   alert('Invalid Client threads Number');
   document.formiperf.clientNum.focus();
   return false;
  }
 document.formiperf.Status.value = 1;
}


function modeSelected()
{
 if (document.formiperf.mode.value == 1) 
 {
  document.formiperf.duration.disabled = true;
  document.formiperf.clientNum.disabled = true;
  document.formiperf.serverAddr.disabled = true;
  
 }
 else 
 {
  document.formiperf.duration.disabled = false;
  document.formiperf.clientNum.disabled = false;
  document.formiperf.serverAddr.disabled = false;
 
 }
}

</script>
</head>
<body>
<blockquote>
<h2><font color="#0dabe1">Iperf</font></h2>
<table border=0 width="500" cellspacing=0 cellpadding=0>
 <tr><td><font size=2>
 This page is used to run speed diagnostics.
 </font></td></tr>
 <tr><td><hr size=1 noshade align=top></td></tr>
</table>
<form action=/boaform/admin/formiperf method=POST name="formiperf">
 <table border=0 width="500" cellspacing=4 cellpadding=0>
 	<tr>
  <td><font size=2><b>Last Measurement Date/Time: </b></font></td>
  <td ><font size=2><% iperf_checkWrite("lastTime"); %></td>
 </tr>
 <tr>
  <td><font size=2><b>Mode: </b></font></td>
  <td><select name="mode" onChange=modeSelected()>
  	<% iperf_checkWrite("mode"); %>
  </select>&nbsp;&nbsp;</td>
 </tr>
 <tr>
  <td><font size=2><b>Server Address: </b></font></td>
  <td><input type="text" name="serverAddr" size="15" maxlength="64" value="<% iperf_checkWrite("serverAddr"); %>" ></td>
 </tr>
  <tr>
  <td><font size=2><b>Server Port: </b></font></td>
  <td><input type="text" name="port" size="15" maxlength="5" value="<% iperf_checkWrite("port"); %>" ></td>
 </tr>
  <tr>
  <td><font size=2><b>Duration Time: </b></font></td>
  <td><input type="text" name="duration" size="15" maxlength="8" value="<% iperf_checkWrite("duration"); %>" >Seconds</td>
 </tr>
  <tr>
  <td><font size=2><b>Protocol Type: </b></font></td>
  <td><select name="protocol">
  	<% iperf_checkWrite("protocol"); %>
  </select>&nbsp;&nbsp;</td>
 </tr>
  <tr>
  <td><font size=2><b>Client threads Number: </b></font></td>
  <td><input type="text" name="clientNum" size="15" maxlength="2" value="<% iperf_checkWrite("clientNum"); %>" ></td>
 </tr>
 <tr>
  <td><font size=2><b>Status: </b></font></td>
  <td ><font size=2><% iperf_checkWrite("status"); %></td>
 </tr>
 </table>
 
 <input type='submit' onClick='return  startClick()' value='<% iperf_checkWrite("btstring"); %>'>&nbsp;&nbsp;
 
 <br>
 <table border=0 width="500" cellspacing=4 cellpadding=0>
 <tr><td><hr size=1 noshade align=top></td></tr>
 <tr><td><font size=2><b>Speed Diagnostics Result:</b></font></td></tr>
 </table>
 <table border='1' width="500" >
 <tr bgcolor=#7f7f7f> <td width="20%"><font size=2><b>Uplink Speed</b></td>
 <td width="20%"><font size=2><b>Downlink Speed</b></td>
 <tr bgcolor=#b7b7b7><td><font size=2><% iperf_checkWrite("uplinkSpeed"); %>&nbsp;</td><td><font size=2><% iperf_checkWrite("downlinkSpeed"); %>&nbsp;</td>
</table>

<input type="hidden" value="1" name="Status">
<input type="hidden" value="/admin/iperf.asp" name="submit-url">

 <script>
 modeSelected();
 </script>
</form>
</blockquote>
</body>
</html>
