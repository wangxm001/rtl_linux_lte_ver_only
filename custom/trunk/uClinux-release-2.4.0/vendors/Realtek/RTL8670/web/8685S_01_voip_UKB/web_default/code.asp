<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<link href="all.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<title>[insert your title here]</title>

<script type="text/javascript">
// Framebuster script to relocate browser when MSIE bookmarks this
// page instead of the parent frameset.  Set variable relocateURL to
// the index document of your website (relative URLs are ok):
var relocateURL = "/";

if(parent.frames.length == 0) {
  if(document.images) {
    location.replace(relocateURL);
  } else {
    location = relocateURL;
  }
}
</script>
<% checkWrite("CreateMenu"); %>
<script type="text/javascript">
switch ('<% getInfo("modemstatus"); %>')
	{
		case "need_pin":
				alert("(U)SIM Card has been locked, need PIN code to unlock.");
				break;
		case "need_puk":
				alert("(U)SIM Card has been locked, need PUK code to unlock.");
				break;
		case "puk_lock":
				alert("You have tried the PUK code more than ten times.");
				break;
		default:
				break;
	}
</script>