﻿var language_wan={

"alert_ok":{
   en:"OK",
   zh:"OK"
    },
"alert_cencel":{
   en:"Cancel",
   zh:"Cancel"
    },
"alert_ipt_save":{
   en:"Save",
   zh:"Save"
    },
"alert_ipt_new":{
   en:"New",
   zh:"New"
    },
"alert_ipt_edit":{
   en:"Edit",
   zh:"Edit"
    },
"sp_delete":{
   en:"Delete",
   zh:"Delete"
    },
"alert_ipt_setdefault":{
   en:"Set Default",
   zh:"Set Default"
    },
"alert_ipt_canceldefault":{
   en:"Cancel Default",
   zh:"Cancel Default"
    },
"alert_ipt_predefault":{
   en:"Predefault",
   zh:"Predefault"
    },
"alert_refresh":{
   en:"Refresh",
   zh:"Refresh"
    },
"alert_delete_pro":{
   en:"Are you sure you want to delete this APN?",
   zh:"Are you sure you want to delete this APN?"
    },
"alert_wan_connected":{
   en:"Unable to progress this page due to your device has connected to network.",
   zh:"Unable to progress this page due to your device has connected to network."
    },
"alert_wan_connecting":{
   en:"Please disconnect the connection before setting.",
   zh:"Please disconnect the connection before setting."
    },
"alert_dial_demand_time_limit":{
   en:"The dial demand time must be 5-60.",
   zh:"The dial demand time must be 5-60."
    },
"alert_dial_demand_time_invalid":{
   en:"The dial demand time invalid.",
   zh:"The dial demand time invalid."
    },
"alert_wan_modem_notready":{
   en:"Initing, please wait...",
   zh:"Initing, please wait..."
    },
"alert_device_lock":{
   en:"Device has been locked, need code to unlock.",
   zh:"Device has been locked, need code to unlock."
    },
"alert_wan_not_invalid_sim":{
   en:"Invalid (U)SIM card",
   zh:"Invalid (U)SIM card"
    },
"alert_wan_nosim":{
   en:"No (U)SIM Card, Please insert a (U)SIM Card.",
   zh:"No (U)SIM Card, Please insert a (U)SIM Card."
    },
"alert_wan_pinblock":{
   en:"(U)SIM Card has been locked, need PIN code to unlock.",
   zh:"(U)SIM Card has been locked, need PIN code to unlock."
    },
"alert_wan_pukblock":{
   en:"(U)SIM Card has been locked, need PUK code to unlock.",
   zh:"(U)SIM Card has been locked, need PUK code to unlock."
    },
"alert_wan_puklock":{
   en:"You have tried the PUK code more than ten times.",
   zh:"You have tried the PUK code more than ten times."
    },
"alert_wan_modem_error":{
   en:"Some errors on the modem.",
   zh:"Some errors on the modem."
    },
"alert_wan_pin_disable":{
   en:"PIN Disabled",
   zh:"PIN Disabled"
    },
"alert_wan_pin_enable":{
   en:"PIN Enabled",
   zh:"PIN Enabled"
    },
"alert_wan_modem_connected":{
   en:"Unable to progress this page due to your device has connected to network.",
   zh:"Unable to progress this page due to your device has connected to network."
    },
"alert_wan_profile_new":{
   en:"New APN Account",
   zh:"New APN Account"
    },
"alert_wan_profile_edit":{
   en:"Edit APN Account",
   zh:"Edit APN Account"
    },
"alertwan_wan_profile_name_limit":{
   en:"You have inputted a double character, so the length limit to 20.",
   zh:"You have inputted a double character, so the length limit to 20."
    },
"alertwan_wan_profile_not_invalid_ip":{
   en:"Invalid IP address.",
   zh:"Invalid IP address."
    },
"alertwan_wan_profile_not_invalid_gw":{
   en:"Not a valid Gateway.",
   zh:"Not a valid Gateway."
    },
"alertwan_wan_profile_not_invalid_mask":{
   en:"Not a valid Mask.",
   zh:"Not a valid Mask."
    },
"alertwan_wan_profile_not_invalid_dns1":{
   en:"Not a valid DNS1.",
   zh:"Not a valid DNS1."
    },
"alertwan_wan_profile_not_invalid_dns2":{
   en:"Not a valid DNS2.",
   zh:"Not a valid DNS2."
    },
"alert_wan_d_ip_blank":{
   en:"The user IP,Subnet Mask,Gateway and DNS1 can not be blank at the same time.",
   zh:"The user IP,Subnet Mask,Gateway and DNS1 can not be blank at the same time."
    },
"alert_wan_update_txt":{
   en:"Update, Please wait ......",
   zh:"Update, Please wait ......"
    },
"alert_wan_no_apn":{
   en:"There is no extended apn ......",
   zh:"There is no extended apn ......"
    },
"alert_wan_profile_name_blank":{
   en:"Input a profile name please!",
   zh:"Input a profile name please!"
    },
"alert_wan_alphanumeric_limit":{
   en:"Please enter alphanumeric characters only!",
   zh:"Please enter alphanumeric characters only!"
    },
"alert_wan_profile_invalid":{
   en:"Profile name must without '&' or '>' or \".",
   zh:"Profile name must without '&' or '>' or \"."
    },
"alert_wan_usrname_invalid":{
   en:"Profile user name must without '&' or '>' or \".",
   zh:"Profile user name must without '&' or '>' or \"."
    },
"alert_wan_password_invalid":{
   en:"Profile password must without '&' or '>' or \".",
   zh:"Profile password must without '&' or '>' or \"."
    },
"alert_wan_apn_invalid":{
   en:"The valid characters: 'a'-'z', 'A'-'Z', '0'-'9', '.' and '-'",
   zh:"The valid characters: 'a'-'z', 'A'-'Z', '0'-'9', '.' and '-'"
    },
"alert_wan_tel_invalid":{
   en:"Please check the Tel number.",
   zh:"Please check the Tel number."
    },
"alert_wan_profile_all_blank":{
   en:"The user name,password and apn name can not be blank at the same time.",
   zh:"The user name,password and apn name can not be blank at the same time."
    },
"alert_wan_request_faild":{
   en:"Request faild,please refresh the page .....",
   zh:"Request faild,please refresh the page ....."
    },
"alert_wan_operating":{
   en:"Operating, Please wait ......",
   zh:"Operating, Please wait ......"
    },
"alert_device_reboot":{
   en:"The current settings will make the device restarts. Are you sure you want to continue?",
   zh:"The current settings will make the device restarts. Are you sure you want to continue?"
    },
"alert_wan_operate_faild":{
   en:"Operator faild, try again...",
   zh:"Operator faild, try again..."
    },
"alert_wan_operate_exit":{
   en:"Setup process complete, but some errors occured, setup maybe succeed.",
   zh:"Setup process complete, but some errors occured, setup maybe succeed."
    },
"alert_pin_lengh_inviald":{
   en:"The length must be between 4 and 8.",
   zh:"The length must be between 4 and 8."
    },
"alert_puk_lengh_inviald":{
   en:"The PUK length must be 8.",
   zh:"The PUK length must be 8."
    },
"alert_number_inviald":{
   en:"Invalid number.",
   zh:"Invalid number."
    },
"alert_pin_code_not_math":{
   en:"PIN codes do not match.",
   zh:"PIN codes do not match."
    },
"alert_pin_lcok_f":{
   en:"Failed to lock sim card.",
   zh:"Failed to lock sim card."
    },
"alert_pin_ucok_f":{
   en:"Failed to unlock sim card.",
   zh:"Failed to unlock sim card."
    },
"alert_pin_mlcok_f":{
   en:"Failed to modify PIN code.",
   zh:"Failed to modify PIN code."
    },
"alert_pin_dis_lcok_f":{
   en:"Failed to disable PIN lock.",
   zh:"Failed to disable PIN lock."
    },
"alert_pin_puk_f":{
   en:"Failed to PUK unlock sim card.",
   zh:"Failed to PUK unlock sim card."
    },
"alert_wan_serching_network":{
   en:"Searching network, Please wait ......",
   zh:"Searching network, Please wait ......"
    },
"alert_wan_select_forbid":{
   en:"This service can not select.",
   zh:"This service can not select."
    },
"alert_wan_serch_network_faild":{
   en:"Can not find any network or some error here, please refresh to try again.",
   zh:"Can not find any network or some error here, please refresh to try again."
    },
"alert_wan_operate_ok":{
   en:"Operated success.....",
   zh:"Operated success....."
    },
"alert_net_mut_select_net":{
   en:"You must select a network in network list.",
   zh:"You must select a network in network list."
    },
"alert_net_apn_not_exist":{
   en:"There is no default APN or the apn list is blank, and no system auto match apn, please add a default apn to apn list.",
   zh:"There is no default APN or the apn list is blank, and no system auto match apn, please add a default apn to apn list."
    },
"alert_connecting_txt":{
   en:"Connecting...",
   zh:"Connecting..."
    },
"alert_disconnecting_txt":{
   en:"Disconnecting...",
   zh:"Disconnecting..."
    },
"alert_connected_txt":{
   en:"Internet connected",
   zh:"Internet connected"
    },
"alert_disconnected_txt":{
   en:"Internet disconnected",
   zh:"Internet disconnected"
    },
"alert_try_connect":{
   en:"Internet disconnected, Please connect and try again.",
   zh:"Internet disconnected, Please connect and try again."
    },
"alert_td_network_list_available":{
   en:"Available",
   zh:"Available"
    },
"alert_td_network_list_current":{
   en:"Current",
   zh:"Current"
    },
"alert_td_network_list_forbidden":{
   en:"Forbidden",
   zh:"Forbidden"
    },
"alert_td_network_list_unknow":{
   en:"Unknow",
   zh:"Unknow"
    },
"net_select_forbid":{
   en:"This service can not select.",
   zh:"This service can not select."
    },
"td_network":{
   en:"Network Mode:",
   zh:"Network Mode:"
    },
"sp_auto_n":{
   en:"Auto",
   zh:"Auto"
    },
"sp_3G":{
   en:"3G Mode",
   zh:"3G Mode"
    },
"sp_2G":{
   en:"2G Mode",
   zh:"2G Mode"
    },
"sp_3Gonly":{
   en:"3G Only",
   zh:"3G Only"
    },
"sp_2Gonly":{
   en:"2G Only",
   zh:"2G Only"
    },
"sp_4Gonly":{
   en:"LTE Only",
   zh:"LTE Only"
    },
"sp_4G3G2G":{
   en:"4G/3G/2G Mode",
   zh:"4G/3G/2G Mode"
    },
"sp_4G3G":{
   en:"4G/3G Mode",
   zh:"4G/3G Mode"
    },
"sp_4G":{
   en:"4G Mode",
   zh:"4G Mode"
    },
"sp_3G2G":{
   en:"3G/2G Mode",
   zh:"3G/2G Mode"
    },
"td_network_4G3G2G":{
   en:"Network Preferred:",
   zh:"Network Preferred:"
    },
"sp_4G3G2G_4G":{
   en:"4G Preferred",
   zh:"4G Preferred"
    },
"sp_4G3G2G_3G2G":{
   en:"3G/2G Preferred",
   zh:"3G/2G Preferred"
    },
"td_netselect":{
   en:"Network Selection:",
   zh:"Network Selection:"
    },
"sp_auto":{
   en:"Automatic",
   zh:"Automatic"
    },
"sp_manual":{
   en:"Manual",
   zh:"Manual"
    },
"td_operator":{
   en:"Operator",
   zh:"Operator"
    },
"td_server":{
   en:"Service",
   zh:"Service"
    },
"td_status_1":{
   en:"Status",
   zh:"Status"
    },
"sp_refresh":{
   en:"Search",
   zh:"Search"
    },
"sp_selected":{
   en:"Selected",
   zh:"Selected"
    },
"td_connect_mode":{
   en:"Connect Mode:",
   zh:"Connect Mode:"
    },
"sp_connect_mode_auto":{
   en:"Auto",
   zh:"Auto"
    },
"sp_connect_mode_manual":{
   en:"Manual",
   zh:"Manual"
    },
"td_dial_mode":{
   en:"Dial Mode:",
   zh:"Dial Mode:"
    },
"sp_keepalive":{
   en:"Keep Alive",
   zh:"Keep Alive"
    },
"sp_demand":{
   en:"Dial Demand",
   zh:"Dial Demand"
    },
"sp_minustes":{
   en:"Minutes",
   zh:"Minutes"
    },
"td_connecroam":{
   en:"Roaming:",
   zh:"Roaming:"
    },
"sp_roma_off":{
   en:"Off",
   zh:"Off"
    },
"sp_roma_on":{
   en:"On",
   zh:"On"
    },
"td_device_lock":{
   en:"Device Lock:",
   zh:"Device Lock:"
    },
"sp_device_lock_on":{
   en:"Lock",
   zh:"Lock"
    },
"sp_device_lock_off":{
   en:"Unlock",
   zh:"Unlock"
    },
"sp_device_lock_modify":{
   en:"Modify",
   zh:"Modify"
    },
"td_device_password":{
   en:"Password:",
   zh:"Password:"
    },
"td_device_new_password":{
   en:"New Password:",
   zh:"New Password:"
    },
"td_pin":{
   en:"PIN Info",
   zh:"PIN Info"
    },
"td_status":{
   en:"PIN Status:",
   zh:"PIN Status:"
    },
"td_pintime":{
   en:"PIN Retain Times:",
   zh:"PIN Retain Times:"
    },
"td_puktime":{
   en:"PUK Retain Times:",
   zh:"PUK Retain Times:"
    },
"td_simcard":{
   en:"PIN Operation:",
   zh:"PIN Operation:"
    },
"div_lock":{
   en:"Lock",
   zh:"Lock"
    },
"div_unlock":{
   en:"Unlock",
   zh:"Unlock"
    },
"div_modify":{
   en:"Modify",
   zh:"Modify"
    },
"div_disabled":{
   en:"Disabled",
   zh:"Disabled"
    },
"div_puk":{
   en:"PUK",
   zh:"PUK"
    },
"td_puk":{
   en:"PUK:",
   zh:"PUK:"
    },
"td_oldpin":{
   en:"PIN:",
   zh:"PIN:"
    },
"td_newpin":{
   en:"New PIN:",
   zh:"New PIN:"
    },
"td_confirmpin":{
   en:"Confirm PIN:",
   zh:"Confirm PIN:"
    },
"sp_back":{
   en:"Back",
   zh:"Back"
    },
"sp_default_apn":{
   en:"Set The Default APN:",
   zh:"Set The Default APN:"
    },
"td_profilename":{
   en:"*Profile Name:",
   zh:"*Profile Name:"
    },
"td_username":{
   en:"User Name:",
   zh:"User Name:"
    },
"td_password":{
   en:"Password:",
   zh:"Password:"
    },
"td_apn":{
   en:"APN:",
   zh:"APN:"
    },
"td_tel":{
   en:"Dial-up Number:",
   zh:"Dial-up Number:"
    },
"td_apntype":{
   en:"Auth. Type:",
   zh:"Auth. Type:"
    },
"td_apnnetwork":{
   en:"Network Mode:",
   zh:"Network Mode:"
    },
"td_connectmodem":{
   en:"Connect Mode:",
   zh:"Connect Mode:"
    },
"sp_menu":{
   en:"Manual",
   zh:"Manual"
    },
"td_profilelist":{
   en:"Profile List:",
   zh:"Profile List:"
    },
"td_default":{
   en:"Default",
   zh:"Default"
    },
"td_profile":{
   en:"Profile",
   zh:"Profile"
    },
"td_profilename":{
   en:"*Profile Name:",
   zh:"*Profile Name:"
    },
"td_username":{
   en:"User Name:",
   zh:"User Name:"
    },
"td_password":{
   en:"Password:",
   zh:"Password:"
    },
"td_tel":{
   en:"Dial-up Number:",
   zh:"Dial-up Number:"
    },
"td_apn":{
   en:"APN:",
   zh:"APN:"
    },
"td_apntype":{
   en:"Auth. Type:",
   zh:"Auth. Type:"
    },
"td_apnnetwork":{
   en:"Network Mode:",
   zh:"Network Mode:"
    },
"sp_save":{
   en:"Save",
   zh:"Save"
    },
"ipt_search":{
   en:"Search",
   zh:"Search"
    },
"sp_cancel":{
   en:"Cancel",
   zh:"Cancel"
    },
"td_setdefault":{
   en:"Set Default:",
   zh:"Set Default:"
    },
"td_duration":{
   en:"Duration:",
   zh:"Duration:"
    },
"td_sent":{
   en:"Sent",
   zh:"Sent"
    },
"td_receive":{
   en:"Received",
   zh:"Received"
    },
"wait_process":{
   en:"Please wait ......",
   zh:"Please wait ......"
    },
"help_networkmodem":{
   en:"In 'Network mode' setting, you should select the rule for registering the network.",
   zh:"In 'Network mode' setting, you should select the rule for registering the network."
    },
"help_connect_mode":{
   en:"Connect Mode allows administrators to select the way to connect with Internet. There are two options can be selected: 'Auto' and 'Manual'. If 'Auto' is selected, the configuration can be applied after restart. In other words, the router will automatic connect to network when all conditions are met.",
   zh:"Connect Mode allows administrators to select the way to connect with Internet. There are two options can be selected: 'Auto' and 'Manual'. If 'Auto' is selected, the configuration can be applied after restart. In other words, the router will automatic connect to network when all conditions are met."
    },
"help_dial_mode":{
   en:"Keep Alive' mode is used to detect available APN or default APN and connect automatically without any intervention. In 'Dial Demand' mode, the router will disconnected if there is no data transfer requirements from WAN during the set time (10 minutes Default ).",
   zh:"Keep Alive' mode is used to detect available APN or default APN and connect automatically without any intervention. In 'Dial Demand' mode, the router will disconnected if there is no data transfer requirements from WAN during the set time (10 minutes Default )."
    },
"help_dialmode":{
   en:"Dial Mode is a function of 3G mode, which allows administrators to select the way to connect with Internet. There are two options can be selected: 'Auto' and 'Manual'. If 'Auto' is selected, the configuration can be applied after restart. In other words, the device will automatic connect to network when all conditions are met.",
   zh:"Dial Mode is a function of 3G mode, which allows administrators to select the way to connect with Internet. There are two options can be selected: 'Auto' and 'Manual'. If 'Auto' is selected, the configuration can be applied after restart. In other words, the device will automatic connect to network when all conditions are met."
    },
"help_net_profile":{
   en:"Default APN  (Access Point Name) profile and other choices will be shown in the Profile list. The number of APN list limits to 10.You can set other profile as default configuration, edit or delete existing inactive profile and create a new account.",
   zh:"Default APN  (Access Point Name) profile and other choices will be shown in the Profile list. The number of APN list limits to 10.You can set other profile as default configuration, edit or delete existing inactive profile and create a new account."
    },
"help_net_searching":{
   en:"The default Network Selection mode is 'Automatic'. When click 'Manual', a network list will be activated, containing all searched networks. Having selected an available network from the list, device will register to the selected network.",
   zh:"The default Network Selection mode is 'Automatic'. When click 'Manual', a network list will be activated, containing all searched networks. Having selected an available network from the list, device will register to the selected network."
    },
"help_wan_pin":{
   en:"The default PIN code is provided by operator or service provider. User can reassign it. So user can lock the (U)SIM card with pin code to protect the device. And unlock the sim card with pin code which user has setted. If the PIN code is entered incorrectly multiple times, your device may request a PUK code. The PUK code is an 8-digit code provided by operator or service provider. If users don't want to use the pin code, please select 'Disabled' and click 'save' button.",
   zh:"The default PIN code is provided by operator or service provider. User can reassign it. So user can lock the (U)SIM card with pin code to protect the device. And unlock the sim card with pin code which user has setted. If the PIN code is entered incorrectly multiple times, your device may request a PUK code. The PUK code is an 8-digit code provided by operator or service provider. If users don't want to use the pin code, please select 'Disabled' and click 'save' button."
    },
"td_apnconmode":{
   en:"Connect Mode:",
   zh:"Connect Mode:"
    },
"td_connection":{
   en:"Connection",
   zh:"Connection"
    },
"sp_connected_button":{
   en:"Connect",
   zh:"Connect"
    },
"title_connect_button":{
   en:"Connect",
   zh:"Connect"
    },
"title_disconnect_button":{
   en:"Disconnect",
   zh:"Disconnect"
    },
"title_disconnect_button_disconnecting":{
   en:"Connecting",
   zh:"Connecting"
    },
"title_disconnect_button_connecting":{
   en:"Disconnecting",
   zh:"Disconnecting"
    },
"sp_wait_process":{
   en:"Please wait ......",
   zh:"Please wait ......"
    },
"alert_wan_not_connected":{
   en:"Please connect network!",
   zh:"Please connect network!"
    },
"alert_sim_not_found":{
   en:"SIM Not Found",
   zh:"SIM Not Found"
    },
"title_current_type":{
   en:"Type",
   zh:"Type"
    },
"title_current_volume":{
   en:"Current Volume",
   zh:"Current Volume"
    },
"title_current_receive":{
   en:"Receive",
   zh:"Receive"
    },
"title_current_send":{
   en:"Send",
   zh:"Send"
    },
"title_current_duration":{
   en:"Duration",
   zh:"Duration"
    },
"current_statistic_txt":{
   en:"Volume statistics provided here are approximate.For accurate statistics and details of charges refer to your bills.",
   zh:"Volume statistics provided here are approximate.For accurate statistics and details of charges refer to your bills."
    },
"opt_apn_auto_match":{
   en:"--Auto Match--",
   zh:"--Auto Match--"
    },
"opt_apn_create":{
   en:"--Creat New Default--",
   zh:"--Creat New Default--"
    },
"null":{
   en:"null",
   zh:"null"
    }

};
