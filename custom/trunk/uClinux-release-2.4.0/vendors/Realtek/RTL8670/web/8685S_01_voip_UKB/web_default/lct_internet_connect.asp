<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="all.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
<link rel="stylesheet" type="text/css" href="../css/globals.css">
<link href="../css/user2.css" rel="stylesheet" />
<script type="text/javascript" src="../language/js/wan.js"></script>
<script type="text/javascript" src="../ui_js/jquery.js"></script>
<script type="text/javascript" src="../js/globals.js"></script>
<SCRIPT type="text/javascript" src="../ui_js/jquery.qtip.js"></SCRIPT>
<script type="text/javascript" src="../js/form.js"></script>
<script type="text/javascript" src="../js/check_wan.js"></script>
<title>Inter connect</title>
<script type='text/javascript'>
nvram = {
	net_pro_list: 'relish,1,0,cd,123456,wwww.baidu.com,0',
 	sim_data: '<% getInfo("modemstatus"); %>',
 	language: 'en'
};
var apn_nunmber = 30;
var wether_has_default_apn = 0;
var separator_in = String.fromCharCode(38); // &
var separator_out = String.fromCharCode(62); // > 
var apn_separator_num = 10;
var flag_setdefault = 1;
var profile_name_max_length = (64/3 - 1);
function init()
{
	switch (nvram.sim_data)
	{
		case "not_invalid":
			$("#modem_show_txt").html(getalert("alert_wan_not_invalid_sim"));
			$("#modem_show_txt").attr("alt","alert_wan_not_invalid_sim");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			return;
		case "no_simcard":
			$("#modem_show_txt").html(getalert("alert_wan_nosim"));
			$("#modem_show_txt").attr("alt","alert_wan_nosim");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			return;
		case "not_ready":
			$("#modem_show_txt").html(getalert("alert_wan_modem_notready"));
			$("#modem_show_txt").attr("alt","alert_wan_modem_notready");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			return;
		case "sim_ready":
		case "pin_disable":
		case "pin_enable":
			$("#user_data").hide();
			 get_connect_status();
			break;
		case "need_pin":
			$("#modem_show_txt").html(getalert("alert_wan_pinblock"));
			$("#modem_show_txt").attr("alt","alert_wan_pinblock");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			break;
		case "need_puk":
			$("#modem_show_txt").html(getalert("alert_wan_pukblock"));
			$("#modem_show_txt").attr("alt","alert_wan_pukblock");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			break;	
		case "puk_lock":
			$("#modem_show_txt").html(getalert("alert_wan_puklock"));
			$("#modem_show_txt").attr("alt","alert_wan_puklock");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			break;
		default:
			$("#modem_show_txt").html(getalert("alert_wan_modem_error"));
			$("#modem_show_txt").attr("alt","alert_wan_modem_error");
			$("#user_data").hide();
			$("#modem_shoe_div").show();
			return;		
	}
    init_language();
}
</script>
</head>
<body onload="getxml(language_wan);">
<div id="user_all" onkeydown="javascript:capchar(event)" style="padding:0 1em 0 1em;">
<div id="user_data" style="display:none">
<div id="user_table" >
<form action="/boaform/admin/form4GConfapn" method="POST" name="form4gconfapn" id="form4gconfapn">
<input type="hidden" value="/admin/lct_internet_connect.asp" name="submit-url">
<input type="hidden" id="ipt_default_apn_list" name="ipt_default_apn_list" value="">
<input type="hidden" id="ipt_apn_list" name="ipt_apn_list" value="">
<input type="hidden" id="ipt_apn_default_new_id" name="ipt_apn_default_new_id" value="-1">
<input type="hidden" id="ipt_apn_default_old_id" name="ipt_apn_default_old_id" value="-1">
<input type="hidden" id="ipt_apn_default_old_network" name="ipt_apn_default_old_network" value="3">
<input type="hidden" id="ipt_apn_send_msg" name="ipt_apn_send_msg" value="1">
<div class="line">
    <div class="s-12 l-4 line_form">
        <label class="name" id="sp_default_apn"></label>
    </div>
    <div class="s-12 l-4 line_form">
        <select name="net_apn_list" id="net_apn_list" onchange="net_apn_sel_change()">
            <option value="-2" selected="selected">--Auto match--</option>
            <option value="-1" id="opt_apn_create">--Creat new default--</option>
        </select>
    </div>
</div>
<div id="tb_apn" name="tb_apn" style="display:none;">
    <div class="line">
  <div class="s-12 l-4 line_form">
   <label class="name" id="td_profilename"></label>
  </div>
  <div class="s-12 l-4 line_form">
   <div>
    <input id="net_connect_profile_name" name="net_connect_profile_name" type="text" maxlength="64" data-clear-btn="true" />
   </div>
  </div>
 </div>
 <div class="line">
  <div class="s-12 l-4 line_form">
   <label class="name" id="td_username"></label>
  </div>
  <div class="s-12 l-4 line_form">
   <div>
    <input id="net_connect_user_name" name="net_connect_user_name" type="text" maxlength="30" data-clear-btn="true" />
   </div>
  </div>
 </div>
 <div class="line">
  <div class="s-12 l-4 line_form">
   <label class="name" id="td_password"></label>
  </div>
  <div class="s-12 l-4 line_form">
   <div>
    <input id="net_connect_password" name="net_connect_password" type="text" maxlength="30" data-clear-btn="true" />
   </div>
  </div>
    </div>
 <div class="line">
  <div class="s-12 l-4 line_form">
   <label class="name" id="td_apn" ></label>
  </div>
  <div class="s-12 l-4 line_form">
   <div>
    <input id="net_connect_apn" name="net_connect_apn" type="text" maxlength="30" data-clear-btn="true" />
   </div>
  </div>
    </div>
 <div class="line">
  <div class="s-12 l-4 line_form">
   <label class="name" id="td_tel" ></label>
  </div>
  <div class="s-12 l-4 line_form">
   <div>
    <input id="net_connect_tel" name="net_connect_tel" type="text" maxlength="30" data-clear-btn="true" />
   </div>
  </div>
 </div>
 <div class="line">
  <div class="s-12 l-4 line_form">
   <label class="name" id="td_apntype"></label>
  </div>
  <div class="s-12 l-4 line_form">
   <div>
    <select name="net_connect_auth" id="net_connect_auth" >
     <option value="1">PAP </option>
     <option value="2">CHAP </option>
     <option value="0" selected="selected">AUTO</option>
    </select>
   </div>
  </div>
 </div>
 <div class="line" style="display:none">
  <div class="s-12 l-4 line_form" >
   <label align="left" class="name" id="td_apnnetwork"></label>
  </div>
  <div class="s-12 l-4 line_form" >
   <div align="left" >
    <select name="apn_network" id="apn_network" >
     <option value="1">4G </option>
     <option value="2">3G/2G </option>
     <option value="3" selected="selected" >4G/3G/2G </option>
    </select>
   </div>
  </div>
 </div>
 <div class="line">
  <div class="s-12 l-4 line_form" >
   <label class="name" id="td_apnconmode"></label>
  </div>
  <div class="s-12 l-4 line_form" >
   <div></div>
   <div >
    <select name="apn_conmode" id="apn_conmode">
     <option value="1">IPv4 </option>
     <option value="2">IPv6 </option>
     <option value="3" selected="selected">IPv4/v6 </option>
    </select>
   </div>
  </div>
 </div>
 </div>
</form>
 <form action="/boaform/admin/form4GConnectgetstatus" method="POST" name="form4gcconnectgetstatus" id="getnetworkconnectstatus">
  </form>
   <form action="/boaform/admin/form4Ggetapn" method="POST" name="form4ggetapn" id="4ggetapnlist">
  </form>
 <div id="hide_div_show" style="position:absolute; background-color:#000; width:100%; height:100%; z-index:10001; top:0px; left: 0px; opacity: 0.65; -moz-opacity: 0.65; filter:progid:DXImageTransform.Microsoft.alpha(style=2,opacity=20,finishOpacity=60);-ms-filter:'progid:DXImageTransform.Microsoft.Alpha(style=2,opacity=20,finishOpacity=60)'; display:none" >
 </div>
 <div id="busy_rndcontainer" align="center" style="display:none">
  <div class="line">
   <div class="margin">
    <div class="line" >
        <div class="s-12 l-4 line_form" >
      <label class="name" id="busy_show_txt"></label>
     </div>
     <div class="s-12 l-2" id="busy_show_picture">
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
 <div class="line">
  <div class="margin">
  <div class="s-12 l-2">
   <div class="button" onClick="apn_save()"><span id="sp_save"></span></div>
  </div>
  <div class="s-12 l-2">
   <div class="button" onclick="window.location=location;"><span id="sp_cancel"></span></div>
  </div>
  <div class="s-12 l-2" id="apn_delete" style="display:none;">
   <div class="button" onClick="delete_pro_confirm()"><span id="sp_delete"></span></div>
  </div>
  </div>
 </div>
</div>
<br class="clearfloat"/>
</div>
<div id="modem_shoe_div" align="center" style="display:none;">
    <div class="line">
  <div class="margin">
   <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
   </div>
   <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
   </div>
   <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
   </div>
   <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
   </div>
   <div class="line">
    <div class="s-12 l-12 line_form">
     <label class="name" id="sp_info_s"></label>
    </div>
   </div>
   <div class="line">
    <div class="s-12 l-1">
     <label class="name">&nbsp;</label>
    </div>
    <div class="s-12 l-10 line_form">
     <label class="name" id="modem_show_txt"></label>
    </div>
   </div>
  </div>
    </div>
</div>
<div id="div_process_bar" align="center" style="display:none">
 <div class="line">
  <div class="margin">
   <div class="s-12 l-4 line_form">
    <label class="name" id="sp_wait_process"></label>
   </div>
   <div class="s-12 l-2 center">
   </div>
  </div>
 </div>
</div>
<script type="text/javascript" language="javascript">

function get_connect_status()
{
 $.ajax({
 url: '/boaform/admin/form4GConnectgetstatus',
 data: $('#getnetworkconnectstatus').serialize(),
 type: 'POST',
 complete: function (xhr)
 {
  if (''!=xhr.responseText && xhr.responseText!=null)
  {
   var tmp = xhr.responseText.split(",");
   is_connectd_status = tmp[1];
   if(tmp[1] =="2")
   {
    $("#modem_show_txt").html(getalert("alert_wan_connected"));
    $("#modem_show_txt").attr("alt","alert_wan_connected");
    $("#user_data").hide();
    $("#modem_shoe_div").show();
   }
   else if(tmp[1] =="3")
   {
    $("#modem_show_txt").html(getalert("alert_wan_connecting"));
    $("#modem_show_txt").attr("alt","alert_wan_connecting");
    $("#user_data").hide();
    $("#modem_shoe_div").show();
   }
   else if(tmp[1] =="1")
   {
    $("#modem_show_txt").html(getalert("alert_wan_connecting"));
    $("#modem_show_txt").attr("alt","alert_wan_connecting");
    $("#user_data").hide();
    $("#modem_shoe_div").show();
   }
   else 
   {
    $("#user_data").show();
    $("#modem_shoe_div").show();
   }
  }
 }
 });
}


function delete_pro_confirm()
{

 var s = nvram.net_pro_list.split(separator_out);
 var selset_index = $("#net_apn_list").val();
 var t_tmp = "";
 var s_tmp = "";
 var end_index = "";
 var num = -1;
 for (var i = 0; i < s.length; ++i)
 {
  var t = s[i].split(separator_in);
  if (t.length == apn_separator_num)
  {
   if(t[2] == selset_index)
   {
    num = selset_index;
   }
   else
   {
    if(num == -1)
    {
     s_tmp += s[i] + separator_out;
    }
    else
    {
     t_tmp = t[0] + separator_in + t[1] + separator_in + num++ + separator_in + t[3] + separator_in + t[4] + separator_in + t[5] + separator_in + t[6]+ separator_in + t[7] + separator_in + t[8] + separator_in + t[9];
     s_tmp += t_tmp + separator_out;
    }
   }
  }
 }
 document.form4gconfapn.ipt_apn_list.value = s_tmp;
 document.form4gconfapn.ipt_apn_send_msg.value = "0";
  $.ajax({
  url: '/boaform/admin/form4GConfapn',
  data: $('#form4gconfapn').serialize(),
  type: 'POST',
  complete: function (xhr)
  {
   window.location.reload();
  }
  });
}

function get_apn_list()
{
  var lrand = Math.random();
  var sids = parseInt(lrand * 1000000);
 $.ajax({
 url: '/boaform/admin/form4Ggetapn?sids=' + sids,
 type: 'GET',
 complete: function (xhr)
 {
  	if (''!=xhr.responseText && xhr.responseText!=null)
  	{
  		nvram.net_pro_list = xhr.responseText;
        init_apn();
 	}
 }
 });
}

get_apn_list();

function init_apn()
{
 if(nvram.net_pro_list != "")
 {
  var s = nvram.net_pro_list.split(separator_out);
  var tmp_select = "";
  var count_apn = 0;
  var default_apn_index = "-2";
  E("ipt_apn_default_new_id").value = "-1";
  E("ipt_apn_default_old_id").value = "-1";
  E("ipt_apn_default_old_network").value = "3";
  for (var i = 0; i < s.length; ++i)
  {
   var t = s[i].split(separator_in);
   if (t.length == apn_separator_num)
   {
    if("1" == t[1])
    {
     E("net_connect_profile_name").value = t[0];
     E("net_connect_user_name").value = t[3];
     E("net_connect_password").value = t[4];
     E("net_connect_apn").value = t[5];
     E("net_connect_tel").value = t[7];
     $("#net_connect_auth").val(t[6]);
     $("#apn_network").val(t[8]);
     $("#apn_conmode").val(t[9]);
     wether_has_default_apn = 1;
     default_apn_index = t[2];
     E("ipt_apn_default_new_id").value = t[2];
     E("ipt_apn_default_old_id").value = t[2];
     E("ipt_apn_default_old_network").value = t[8];
     
     {
      $("#net_connect_profile_name").removeAttr("disabled");
      $("#net_connect_user_name").removeAttr("disabled");
      $("#net_connect_password").removeAttr("disabled");
      $("#net_connect_apn").removeAttr("disabled");
      $("#net_connect_tel").removeAttr("disabled");
      $("#net_connect_auth").removeAttr("disabled");
      $("#apn_network").removeAttr("disabled");
      $("#apn_conmode").removeAttr("disabled");
     }
    }
    count_apn++;
    tmp_select += "<option value="+t[2]+">"+t[0]+"</option>";
   }
  }
  if (wether_has_default_apn == 1)
  {
      $("#tb_apn").fadeIn(500);
  }
  if (count_apn >= apn_nunmber)
  {
      $("#opt_apn_create").remove();
  }
  $("#net_apn_list").append(tmp_select);
  $("#net_apn_list").val(default_apn_index);
 }
 else
 {
     $("#net_apn_list").val("-2");
 }
}
function net_apn_sel_change()
{
    var index_sel = $("#net_apn_list").val();
 $("#apn_delete").css("display","none");
 if (index_sel == "-2")
 {
     $("#tb_apn").fadeOut(200);
 }
 else if (index_sel == "-1")
 {
     $("#tb_apn").fadeIn(500);
  $("#net_connect_profile_name").removeAttr("disabled");
  $("#net_connect_user_name").removeAttr("disabled");
  $("#net_connect_password").removeAttr("disabled");
  $("#net_connect_apn").removeAttr("disabled");
  $("#net_connect_tel").removeAttr("disabled");
  $("#net_connect_auth").removeAttr("disabled");
  $("#apn_network").removeAttr("disabled");
  $("#apn_conmode").removeAttr("disabled");
  E("net_connect_profile_name").value = '';
  E("net_connect_user_name").value = '';
  E("net_connect_password").value = '';
  E("net_connect_apn").value = '';
  E("net_connect_tel").value = '';
  $("#net_connect_auth").val('4');
  $("#apn_network").val("3");
  $("#apn_conmode").val("3");
 }
 else
 {
     $("#tb_apn").fadeIn(500);
  var s = nvram.net_pro_list.split(separator_out);
  for (var i = 0; i < s.length; ++i)
  {
   var t = s[i].split(separator_in);
   if (t.length == apn_separator_num)
   {
    if(index_sel == t[2])
    {
     E("net_connect_profile_name").value = t[0];
     E("net_connect_user_name").value = t[3];
     E("net_connect_password").value = t[4];
     E("net_connect_apn").value = t[5];
     E("net_connect_tel").value = t[7];
     $("#net_connect_auth").val(t[6]);
     $("#apn_network").val(t[8]);
     $("#apn_conmode").val(t[9]);
     $("#apn_delete").css("display","block");
     $("#net_connect_profile_name").removeAttr("disabled");
     $("#net_connect_user_name").removeAttr("disabled");
     $("#net_connect_password").removeAttr("disabled");
     $("#net_connect_apn").removeAttr("disabled");
     $("#net_connect_tel").removeAttr("disabled");
     $("#net_connect_auth").removeAttr("disabled");
     $("#apn_network").removeAttr("disabled");
     $("#apn_conmode").removeAttr("disabled");
     if("1" == t[1])
     {
      $("#apn_delete").css("display","none");
     }
    }
   }
  }
 }
}
function auto_match_apn_in_list()
{
    var s_tmp = "";
    if (wether_has_default_apn == 1)
 {
     var s = nvram.net_pro_list.split(separator_out);
  var t_tmp = "";
  for (var i = 0; i < s.length; ++i)
  {
   var t = s[i].split(separator_in);
   if (t.length == apn_separator_num)
   {
    if(t[1] == "1")
    {
     t_tmp = t[0] + separator_in + "0" +separator_in+ t[2] +separator_in+ t[3] +separator_in+ t[4] +separator_in+ t[5] +separator_in+ t[6] +separator_in+ t[7] +separator_in+ t[8] +separator_in+ t[9];
     s_tmp += t_tmp +separator_out;
    }
    else
    {
     s_tmp += s[i] +separator_out;
    }
   }
  }
 }
 else
 {
     s_tmp = nvram.net_pro_list;
 }
	document.form4gconfapn.ipt_apn_list.value = s_tmp;
	document.form4gconfapn.ipt_default_apn_list.value = "";
	document.form4gconfapn.ipt_apn_default_new_id.value = "-1";
}
function add_apnto_list()
{
 var t_tmp = "";
 var s_tmp = "";
 var end_index = 0;
 if(nvram.net_pro_list != "")
 {
  var s = nvram.net_pro_list.split(separator_out);
  end_index = s.length-1;
  for (var i = 0; i < s.length; ++i)
  {
   var t = s[i].split(separator_in);
   if (t.length == apn_separator_num)
   {
    if(t[1] == "1")
    {
     t_tmp = t[0] +separator_in+ "0" +separator_in+ t[2] +separator_in+ t[3] +separator_in+ t[4] +separator_in+ t[5] +separator_in+ t[6] +separator_in+ t[7] +separator_in+ t[8] +separator_in+ t[9];
     s_tmp += t_tmp +separator_out;
    }
    else
    {
     s_tmp += s[i] +separator_out;
    }
   }
  }
 }
 s_tmp += E("net_connect_profile_name").value +separator_in+ "1" +separator_in+ end_index+separator_in+ E("net_connect_user_name").value +separator_in+ E("net_connect_password").value + separator_in + E("net_connect_apn").value + separator_in + document.form4gconfapn.net_connect_auth.value + separator_in + E("net_connect_tel").value + separator_in + document.form4gconfapn.apn_network.value + separator_in + document.form4gconfapn.apn_conmode.value + separator_out;
	document.form4gconfapn.ipt_apn_list.value = s_tmp;
	document.form4gconfapn.ipt_default_apn_list.value  = E("net_connect_profile_name").value + separator_in + "1" + separator_in + end_index+ separator_in + E("net_connect_user_name").value + separator_in + E("net_connect_password").value + separator_in + E("net_connect_apn").value + separator_in + document.form4gconfapn.net_connect_auth.value + separator_in + E("net_connect_tel").value + separator_in + document.form4gconfapn.apn_network.value + separator_in + document.form4gconfapn.apn_conmode.value ;
	document.form4gconfapn.ipt_apn_default_new_id.value = end_index;
}
function edit_apn_in_list()
{
 var s = nvram.net_pro_list.split(separator_out);
 var selset_index = $("#net_apn_list").val();
 var t_tmp = "";
 var s_tmp = "";
 var end_index = "";
 for (var i = 0; i < s.length; ++i)
 {
  var t = s[i].split(separator_in);
  if (t.length == apn_separator_num)
  {
   if(t[2] == selset_index)
   {
    s_tmp += E("net_connect_profile_name").value + separator_in + "1" + separator_in + selset_index+ separator_in + E("net_connect_user_name").value + separator_in + E("net_connect_password").value + separator_in + E("net_connect_apn").value + separator_in + document.form4gconfapn.net_connect_auth.value + separator_in + E("net_connect_tel").value + separator_in + document.form4gconfapn.apn_network.value + separator_in + document.form4gconfapn.apn_conmode.value + separator_out;
    E("ipt_default_apn_list").value = E("net_connect_profile_name").value + separator_in + "1" + separator_in + selset_index+ separator_in + E("net_connect_user_name").value + separator_in + E("net_connect_password").value + separator_in + E("net_connect_apn").value + separator_in + document.form4gconfapn.net_connect_auth.value + separator_in + E("net_connect_tel").value + separator_in + document.form4gconfapn.apn_network.value + separator_in + document.form4gconfapn.apn_conmode.value + separator_out;
    E("ipt_apn_default_new_id").value = selset_index;
   }
   else
   {
    if(t[1] == "1")
    {
     t_tmp = t[0] + separator_in + "0" +separator_in+ t[2] +separator_in+ t[3] +separator_in+ t[4] +separator_in+ t[5] +separator_in+ t[6] +separator_in+ t[7] +separator_in+ t[8] +separator_in+ t[9];
     s_tmp += t_tmp +separator_out;
    }
    else
    {
     s_tmp += s[i] +separator_out;
    }
   }
  }
 }
	document.form4gconfapn.ipt_apn_list.value = s_tmp;
}
function check_setup_apn()
{
 if(E("net_connect_profile_name").value.length == 0)
 {
  lc_mousedown_fade(E("net_connect_profile_name").id);
  lc_wrong_color(E("net_connect_profile_name").id);
  alert(getalert('alert_wan_profile_name_blank'));
  return false;
 }
 if(!check_APN_profile_length(E("net_connect_profile_name").value))
 {
     if (E("net_connect_profile_name").value.length > profile_name_max_length)
  {
   lc_mousedown_fade(E("net_connect_profile_name").id);
   lc_wrong_color(E("net_connect_profile_name").id);
   alert(getalert('alertwan_wan_profile_name_limit'));
   return false;
  }
 }
 if(!check_APN_username(E("net_connect_profile_name").value))
 {
  lc_wrong_color(E("net_connect_profile_name").id);
  alert(getalert('alert_wan_profile_invalid'));
  return false;
 }
 if(!check_APN_profile_length(E("net_connect_user_name").value))
 {
  lc_mousedown_fade(E("net_connect_user_name").id);
  lc_wrong_color(E("net_connect_user_name").id);
  alert(getalert('alert_wan_alphanumeric_limit'));
  return false;
 }
 if(!check_APN_username(E("net_connect_user_name").value))
 {
  lc_wrong_color(E("net_connect_user_name").id);
  alert(getalert('alert_wan_usrname_invalid'));
  return false;
 }
 if(!check_APN_profile_length(E("net_connect_password").value))
 {
  lc_mousedown_fade(E("net_connect_password").id);
  lc_wrong_color(E("net_connect_password").id);
  alert(getalert('alert_wan_alphanumeric_limit'));
  return false;
 }
 if(!check_APN_username(E("net_connect_password").value))
 {
  lc_wrong_color(E("net_connect_password").id);
  alert(getalert('alert_wan_password_invalid'));
  return false;
 }
 if(!check_APN_apn(E("net_connect_apn").value))
 {
  lc_mousedown_fade(E("net_connect_apn").id);
  lc_wrong_color(E("net_connect_apn").id);
  alert(getalert('alert_wan_apn_invalid'));
  return false;
 }
 if (!is_phone_number(E("net_connect_tel").value))
 {
  lc_mousedown_fade(E("net_connect_tel").id);
  lc_wrong_color(E("net_connect_tel").id);
  alert(getalert('alert_wan_tel_invalid'));
  return false;
 }
 if(E("net_connect_password").value.length == 0 && E("net_connect_user_name").value.length == 0 && E("net_connect_tel").value.length == 0 && E("net_connect_apn").value.length == 0)
 {
  alert(getalert('alert_wan_profile_all_blank'));
  return false;
 }
 var index_sel = $("#net_apn_list").val();
 if(index_sel == "-1")
 {
  add_apnto_list();
 }
 else
 {
  edit_apn_in_list();
 }
 return true;
}
var options = {
 beforeSubmit: showRequest,
 success: showResponse
};
function which_ajax_submit(which_form)
{
 $('#'+which_form).ajaxForm(options);
 $('#'+which_form).submit();
}
function showRequest(formData, jqForm, options)
{
 $("#hide_div_show").show();
 $("#busy_show_txt").html(getalert("alert_wan_operating"));
 $("#busy_show_picture").fadeIn(2);
 $("#busy_rndcontainer").fadeIn(2)
 var queryString = $.param(formData);
 return true;
}
function showResponse(responseText, statusText)
{
    if(responseText == "ok")
 {
  $("#busy_show_txt").html(getalert("alert_wan_update_txt"));
 }
 else if(responseText == "error")
 {
  $("#busy_show_txt").html(getalert("alert_wan_operating"));
  $("#busy_show_picture").fadeOut(2);
 }
 else
 {
  alert(responseText);
  $("#busy_show_txt").html(getalert("alert_wan_operate_exit"));
  $("#busy_show_picture").fadeOut(2);
 }
 window.location.reload();
}
function apn_save()
{
    var index_sel = $("#net_apn_list").val();
 if (index_sel == "-2")
 {
  auto_match_apn_in_list();
 }
 else if(!check_setup_apn())
 {
  return false;
 }
 $.ajax({
  url: '/boaform/admin/form4GConfapn',
  data: $('#form4gconfapn').serialize(),
  type: 'POST',
  complete: function (xhr)
  {
   window.location.reload();
  }
  });
}
</script>
</body>
</html>
