<html>
<! Copyright (c) Realtek Semiconductor Corp., 2003. All Rights Reserved. ->
<head>
<META HTTP-EQUIV=Refresh CONTENT="10; URL=lct_lte_info.asp">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<title><% multilang(LANG_LTE_SETTINGS); %></title>
<script type="text/javascript" src="share.js">
</script>
<script src='../ui_js/jquery.js' type='text/javascript'></script>
<SCRIPT type="text/javascript" src="../ui_js/jquery.qtip.js"></SCRIPT>
<script>
var getObj = null;
var pow = 0;
nvram = {
	rsrp_value : '<% getInfo("rsrp"); %>',
	rsrq_value : '<% getInfo("rsrq"); %>',
    cinr0_value : '<% getInfo("cinr0"); %>',
	cinr1_value : '<% getInfo("cinr1"); %>',
	sinr0_value : '<% getInfo("sinr0"); %>',
	sinr1_value : '<% getInfo("sinr1"); %>',
    rsrp0_value : '<% getInfo("rsrp0"); %>',
	rsrp1_value : '<% getInfo("rsrp1"); %>',
    tx_power_value : '<% getInfo("lte_tx_power"); %>',
    bandwidth_value : '<% getInfo("lte_bandwidth"); %>',
    dl_value : '<% getInfo("lte_dl"); %>',
    ul_value : '<% getInfo("lte_ul"); %>'
};


function on_init()
{
	// Mason Yu for IPv6
	get_statuslte();
	return true;
}
function get_statuslte()
{
	var lrand = Math.random();
	var sids = parseInt(lrand*1000000);
	$.ajax({
		url: '/boaform/admin/form4GConfget?sids='+sids,
		data: $('#statuslte').serialize(),
		type: 'GET',
		complete: function (xhr) 
		{	
			//alert(xhr.responseText);
			if (''!=xhr.responseText && xhr.responseText!=null)
			    {
					var tmp = xhr.responseText.split(",");
					
				    if(tmp[1] =="0")
					{
					    $('#state_lte_status').html("DETACH");
					}
					else if(tmp[1] =="2")
					{
					    $('#state_lte_status').html("ATTACH");
					}
					else
					{
						$('#state_lte_status').html("");
					}

				}
		}
	}); 
  /*  if(nvram.rsrp_value == "err")
	{
		$('#state_lte_rsrp').html("");
	}
	else
	{
		//pow = nvram.rsrp_value/100;
		$('#state_lte_rsrp').html("-"+nvram.rsrp_value+"dBm");
	} */
    if(nvram.rsrq_value == "err")
	{
		$('#state_lte_rsrq').html("");
	}
	else
	{
		pow = nvram.rsrq_value;
		$('#state_lte_rsrq').html(nvram.rsrq_value+"dB");
	}

    if(nvram.cinr0_value == "err")
	{
		$('#state_lte_cinr0').html("");
	}
	else
	{
		pow = nvram.cinr0_value;
		$('#state_lte_cinr0').html(pow+"dB");
	} 
    if(nvram.cinr1_value == "err")
	{
		$('#state_lte_cinr1').html("");
	}
	else
	{
		pow = nvram.cinr1_value;
		$('#state_lte_cinr1').html(pow+"dB");
	} 

    if(nvram.sinr0_value == "err")
	{
		$('#state_lte_sinr0').html("");
	}
	else
	{
		pow = nvram.sinr0_value;
		$('#state_lte_sinr0').html(pow+"dB");
	}
    if(nvram.sinr1_value == "err")
	{
		$('#state_lte_sinr1').html("");
	}
	else
	{
		pow = nvram.sinr1_value;
		$('#state_lte_sinr1').html(pow+"dB");
	}
    
    if(nvram.rsrp0_value == "err")
	{
		$('#state_lte_rsrp0').html("");
	}
	else
	{
		pow = nvram.rsrp0_value;
		$('#state_lte_rsrp0').html(pow+"dBm");
	}
    if(nvram.rsrp1_value == "err")
	{
		$('#state_lte_rsrp1').html("");
	}
	else
	{
		pow = nvram.rsrp1_value;
		$('#state_lte_rsrp1').html(pow+"dBm");
	}
  
    if(nvram.tx_power_value == "err")
	{
		$('#state_lte_tx').html("");
	}
	else
	{
		$('#state_lte_tx').html(nvram.tx_power_value+"dBm");
	}

    if(nvram.bandwidth_value == "err")
	{
		$('#state_lte_bandwidth').html("");
	}
	else
	{
		$('#state_lte_bandwidth').html(nvram.bandwidth_value+"kHz");
	}
    if(nvram.dl_value == "err")
	{
		$('#state_lte_dl').html("");
	}
	else
	{
		$('#state_lte_dl').html(nvram.dl_value+"kHz");
	}
    if(nvram.ul_value == "err")
	{
		$('#state_lte_ul').html("");
	}
	else
	{
		$('#state_lte_ul').html(nvram.ul_value +"kHz");
	}

}
</script>
</head>
<body onLoad="on_init();">
<blockquote>

<h2><b><font color="#0dabe1"><% multilang(LANG_LTE_SETTINGS); %></font></b></h2>

<table border=0 width="500" cellspacing=0 cellpadding=0>
<tr><td><hr size=1 noshade align=top><br></td></tr>
</table>

<form action=/boaform/admin/formStatus method=POST name="status2">
<table width=400 border=0>
  <tr>
    <td width=100% colspan="2" bgcolor="#0dabe1"><font color="#FFFFFF" size=2><b>LTE INFO</b></font></td>
  </tr>
    <tr id='band' bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>Band:</b></td>
    <td width=60%><font size=2 id="state_lte_band">42,43</td>
  </tr>
  <!--tr id='rssi' bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>RSRP:</b></td>
    <td width=60%><font size=2 id="state_lte_rsrp"></td>
  </tr-->
   <tr id='operatename' bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>Operatorname:</b></td>
    <td width=60%><font size=2><% getInfo("operatename"); %></td>
  </tr>
  <tr id='modem' bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>Modem Status:</b></td>
    <td width=60%><font size=2><% getInfo("modemstatus"); %></td>
  </tr>
  <tr id='networktype' bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>Network type:</b></td>
    <td width=60%><font size=2><% getInfo("networktype"); %></td>
  </tr>
   <tr id='roaming' bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>Roaming Status:</b></td>
    <td width=60%><font size=2><% getInfo("roamingstatus"); %></td>
  </tr>
   <tr id='imei' bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>IMEI:</b></td>
    <td width=60%><font size=2><% getInfo("imei"); %></td>
  </tr>
   <tr id='state' bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>STATE:</b></td>
    <td width=60%><font size=2 id="state_lte_status"></td>
  </tr>
  <tr id='imsi' bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>IMSI:</b></td>
    <td width=60%><font size=2><% getInfo("imsi"); %></td>
  </tr>
  <tr bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>RSRQ:</b></td>
    <td width=60%><font size=2 id="state_lte_rsrq"></td>
  </tr>
  <tr bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>CINR0:</b></td>
    <td width=60%><font size=2 id="state_lte_cinr0"></td>
  </tr>
  <tr  bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>CINR1:</b></td>
    <td width=60%><font size=2 id="state_lte_cinr1"></td>
  </tr>
  <tr bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>SINR0:</b></td>
    <td width=60%><font size=2 id="state_lte_sinr0"></td>
  </tr>
  <tr bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>SINR1:</b></td>
    <td width=60%><font size=2 id="state_lte_sinr1"></td>
  </tr>
  <tr bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>Cell ID:</b></td>
    <td width=60%><font size=2 ><% getInfo("lte_cellid"); %></td>
  </tr>
   <tr bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>PCI:</b></td>
    <td width=60%><font size=2 ><% getInfo("lte_pci"); %></td>
  </tr>
  <tr id='rsrp0' bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>RSRP0:</b></td>
    <td width=60%><font size=2 id="state_lte_rsrp0"></td>
  </tr>
   <tr id='rsrp1' bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>RSRP1:</b></td>
    <td width=60%><font size=2 id="state_lte_rsrp1"></td>
  </tr>
  <tr bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>TX Power:</b></td>
    <td width=60%><font size=2 id="state_lte_tx"></td>
  </tr>
   <tr bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>Bandwidth:</b></td>
    <td width=60%><font size=2 id="state_lte_bandwidth"></td>
  </tr>
  <tr bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>DL Frequency:</b></td>
    <td width=60%><font size=2 id="state_lte_dl"></td>
  </tr>
   <tr bgcolor="#EEEEEE" <% checkWrite("bridge-only"); %>>
    <td width=40%><font size=2><b>UL Frequency:</b></td>
    <td width=60%><font size=2 id="state_lte_ul"></td>
  </tr>
</table>

</form>
<br>
<form action=/boaform/admin/form4GConfget method=GET name="statuslte" id="statuslte">
</form>

</blockquote>

</body>

</html>
