<html>
<! Copyright (c) Realtek Semiconductor Corp., 2003. All Rights Reserved. ->
<head>
<link href="all.css" rel="stylesheet" type="text/css" />
<META  CONTENT="10; URL=hideversion.asp">
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<title></title>
<script type="text/javascript" src="share.js">
</script>
<link rel="stylesheet" type="text/css" href="../css/globals.css">
<link href="../css/user2.css" rel="stylesheet" />
<script src='../ui_js/jquery.js' type='text/javascript'></script>
<script type="text/javascript"  src="../js/globals.js"></script>
<SCRIPT type="text/javascript" src="../ui_js/jquery.qtip.js"></SCRIPT>
<script>

</script>
</head>
<body >
<blockquote>
<form action="/boaform/admin/formHideversion" method="POST" name="formhideversion">
<table width=400 border=0>
	<tr bgcolor="#EEEEEE">
    <td width=40%><font size=2><b>Router_version:</b></td>
    <td width=60%><font size=2></td>
  </tr>
  <tr bgcolor="#EEEEEE">
    <td width=40%><font size=2><b>External_version:</b></td>
    <td width=60%><font size=2><% getInfo("hideexternalversion"); %></td>
  </tr>
  <tr bgcolor="#DDDDDD">
    <td width=40%><font size=2><b>Internal_version:</b></td>
    <td width=60%><font size=2><% getInfo("hideinternalversion"); %></td>
  </tr>
  <tr bgcolor="#EEEEEE" style="display:none">
    <td width=40%><font size=2><b>Hardware_version:</b></td>
    <td width=60%><font size=2><% getInfo("hidehardwareversion"); %></td>
  </tr>
  <tr bgcolor="#EEEEEE" style="display:none">
    <td width=40%><font size=2><b>Product_variant:</b></td>
    <td width=60%><font size=2><% getInfo("hideproductvariant"); %></td>
  </tr>
  <tr bgcolor="#EEEEEE" style="display:none">
    <td width=40%><font size=2><b>SKU:</b></td>
    <td width=60%><font size=2><% getInfo("hideskuversion"); %></td>
  </tr>
   <tr bgcolor="#EEEEEE">
    <td width=40%><font size=2><b>Modem_version:</b></td>
    <td width=60%><font size=2></td>
  </tr>
  <tr bgcolor="#DDDDDD">
    <td width=40%><font size=2><b>SoftwareVersion:</b></td>
    <td width=60% id="td_softwareversion"></td>
  </tr>
  <tr bgcolor="#EEEEEE" >
    <td width=40%><font size=2><b>InnerVersion:</b></td>
    <td width=60% id="td_innerversion"></td>
  </tr>
</table>
 <div class="line" >
		<div class="margin">
        	<div class="s-12 l-4 line_form">		   
						<label class="name" >OPEN DEBUGLOG BUTTON:</label>
			</div>
		    <div class="s-12 l-2">
                <div class="button" onClick="debug_log_button();"><span id="sp">DEBUGLOG</span></div>
            </div>
		</div>
    </div>
</form>
<form action="/boaform/admin/form4Ghideversion" method="GET" name="form4ghideversion">
</form>
<form action="/boaform/admin/form4GLOGDebug" method="POST" name="form4glogdebug" id="form4glogdebug">
</form>
<br>
</blockquote>
<script>
get_pin_remain();
function get_pin_remain()
{

 $.ajax({
 url: '/boaform/admin/form4Ghideversion',
 type: 'GET',
 complete: function (xhr)
 {
  if (''!=xhr.responseText && xhr.responseText!=null)
  {
   var tmp = xhr.responseText.split("\r\n");
   var tmpvalue = tmp[0].split(":");
   var tmpvalue2 = tmp[1].split(":");
   $("#td_softwareversion").html(tmpvalue[1]);
   $("#td_innerversion").html(tmpvalue2[1]);
  }
  else
  {
   $("#td_softwareversion").html("");
   $("#td_innerversion").html("");
  }
 }
 });
}

function debug_log_button()
{

 $.ajax({
 url: '/boaform/admin/form4GLOGDebug',
 data: $('#form4glogdebug').serialize(),
 type: 'POST',
 complete: function (xhr)
 {
 	alert("Open debuglog success");
 	window.location.reload();
 }
 });
}

</script>
</body>
</body>
</html>


