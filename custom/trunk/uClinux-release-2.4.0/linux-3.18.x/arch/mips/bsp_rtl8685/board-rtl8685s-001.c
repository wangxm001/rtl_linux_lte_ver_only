#include <linux/kconfig.h>
#include <linux/module.h>
#include <linux/kernel.h>

#include "led-generic.h"
#include "pushbutton.h"
#include "bspgpio.h"
#include <bspchip.h>

#define LOW_ACTIVE	1
#if LOW_ACTIVE

#define GPIO_SET(w, op)  do { \
	gpioConfig(w, GPIO_FUNC_OUTPUT); \
	if (LED_ON==op) gpioClear(w); \
	else gpioSet(w); \
} while (0);

#define GPIO_READ(w) (!gpioRead(w))
		
#else

#define GPIO_SET(w, op)  do { \
	gpioConfig(w, GPIO_FUNC_OUTPUT); \
	if (LED_ON==op) gpioSet(w); \
	else gpioClear(w); \
} while (0);

#define GPIO_READ(w) (gpioRead(w))

#endif

void PCIE_reset_pin(int *reset){	
  	*reset = GPIO_D_2;
}
EXPORT_SYMBOL(PCIE_reset_pin);

#ifdef CONFIG_USE_PCIE_SLOT_1
void PCIE1_reset_pin(int *reset){
        *reset = GPIO_E_4;
}
EXPORT_SYMBOL(PCIE1_reset_pin);
#endif

static void board_01_handle_set(int which, int op) {
	//printk("%s: led %d op %d\n", __FUNCTION__, which, op);
	switch (which) {
	case LED_DSL:	
		GPIO_SET(GPIO_D_5, op);
		break;
	case LED_INTERNET_GREEN:	
		GPIO_SET(GPIO_D_3, op);
		break;
	case LED_INTERNET_RED:	
		GPIO_SET(GPIO_D_1, op);
		break;
	case LED_INTERNET_BLUE:	
		GPIO_SET(GPIO_D_0, op);
		break;
#ifdef CONFIG_RTL8672_SW_USB_LED
	case LED_USB_0: 
		GPIO_SET(GPIO_E_5, op);
		break;
#endif
#if 1
	case LED_SIM: 
		GPIO_SET(GPIO_E_5, op);
		break;
	case LED_LTE_SIGNAL0: 
		GPIO_SET(GPIO_F_0, op);
		break;
	case LED_LTE_SIGNAL1: 
		GPIO_SET(GPIO_F_1, op);
		break;
	case LED_LTE_SIGNAL2: 
		GPIO_SET(GPIO_D_4, op);
		break;
	case LED_LTE_SIGNAL3: 
		GPIO_SET(GPIO_E_7, op);
		break;
#endif
#if defined (CONFIG_RTL8192CD) || defined (CONFIG_RTL8192CD_MODULE)
	case LED_WPS_GREEN:
	case LED_WPS_YELLOW:
	    GPIO_SET(RTL8192CD_GPIO_4, op); 
#endif
	    break;
	case LED_ALL:
		GPIO_SET(RTL8192CD_GPIO_5, op);  /*wifi*/
		GPIO_SET(RTL8192CD_GPIO_2, op);  /*fxs*/
		GPIO_SET(GPIO_B_3, op);  /*LAN1*/
		GPIO_SET(GPIO_B_4, op);  /*LAN2*/
		GPIO_SET(GPIO_B_5, op);  /*LAN3*/
		GPIO_SET(GPIO_B_6, op);  /*LAN4*/
		GPIO_SET(GPIO_B_7, op);  /*WAN*/
		GPIO_SET(GPIO_E_5, op);      /*sim*/ 
		GPIO_SET(GPIO_F_0, op);
		GPIO_SET(GPIO_F_1, op);
		GPIO_SET(GPIO_D_4, op);
		GPIO_SET(GPIO_E_7, op);
		GPIO_SET(GPIO_D_1, op);
		break;
	default:
		led_handle_set(which, op);
	}
}
#include <linux/delay.h>
static __init void board_01_handle_init(void) {
	board_01_handle_set(LED_POWER_GREEN, LED_OFF);
	board_01_handle_set(LED_POWER_RED, LED_ON);
	board_01_handle_set(LED_DSL, LED_OFF);
	board_01_handle_set(LED_INTERNET_GREEN, LED_OFF);
	board_01_handle_set(LED_INTERNET_RED, LED_OFF);
	board_01_handle_set(LED_INTERNET_BLUE, LED_OFF);
	board_01_handle_set(LED_SIM, LED_OFF);
#if defined (CONFIG_RTL8192CD) || defined (CONFIG_RTL8192CD_MODULE)
	board_01_handle_set(LED_WPS_GREEN, LED_OFF);
	board_01_handle_set(LED_WPS_RED, LED_OFF);
	board_01_handle_set(LED_WPS_YELLOW, LED_OFF);
#endif
#ifdef CONFIG_RTL8672_SW_USB_LED
	board_01_handle_set(LED_USB_1, LED_OFF);
	board_01_handle_set(LED_USB_0, LED_OFF);
#endif
};

static struct led_operations board_01_operation = {
	.name = "board_01",
	//.handle_init = board_01_handle_init,
	.handle_set = board_01_handle_set,
};


static void __init board_01_pb_init(void) {

	/* Use GPIO_H_4 */
	REG32(BSP_MISC_CR_BASE) &= ~(1<<12);
	
	/* Use GPIO_E_5 */
	REG32(BSP_MISC_CR_BASE) &= ~(1<<24);

	/* Use GPIO_D_5 */
	REG32(BSP_MISC_CR_BASE) |= (1<<26);

	/* Config GPIO_H_4 */
	gpioConfig(GPIO_H_4, GPIO_FUNC_INPUT);
	gpioSet(GPIO_H_4);
};

char button_flag;

static int board_01_pb_is_pushed(int which) {

  int ret = 0;
  
	switch(which) {
#if defined (CONFIG_RTL8192CD) || defined (CONFIG_RTL8192CD_MODULE)
	case PB_WPS:
		ret =  GPIO_READ(GPIO_H_7);
		if(ret)
		{
			button_flag = '1';
		}
		break;
#endif		
	case PB_RESET:
		ret =  GPIO_READ(RTL8192CD_GPIO_7);
		if(ret)
		{
			button_flag = '2';
		}
		break;
	case PB_WIFISW:
		ret =  GPIO_READ(GPIO_H_4);
		if(ret)
		{
			button_flag = '3';
		}
		break;

	}
	return ret;
}

static struct pushbutton_operations board_01_pb_op = {
	//.handle_init = board_01_pb_init,
	.handle_is_pushed = board_01_pb_is_pushed,
};

static int __init board_01_led_init(void) {
	#ifndef CONFIG_JTAG_USABLE
	REG32(BSP_MISC_CR_BASE) &= ~(BSP_JTAG_SEL); // JTAG is mux with GPIO_D
	#endif
	
	#ifdef CONFIG_MTD_NAND
	//Note::bit 27, SWLED3 will affect nand flash read control
	//REG32(BSP_PINMUX1) |= ((1 << 29)|(1 << 5)) ; // Enable 176p SWLED0-3, and LED4			
	#else

	/* Enable SWLED0-4 */
	REG32(BSP_PINMUX1) |= ((1 << 0) | (1 << 1) | (1 << 2) | (1 << 3) | (1 << 4)) ; 
	#endif
	board_01_handle_init();
	board_01_pb_init();
	led_register_operations(&board_01_operation);
	pb_register_operations(&board_01_pb_op);
	
	GPIO_SET(GPIO_E_4, LED_OFF);    /*GPIOE4 enable the Power of LTE*/
	GPIO_SET(GPIO_E_3, LED_OFF);    /*GPIOE3 enable the SLC of CPU*/
	
	return 0;
}


static void __exit board_01_led_exit(void) {
}


arch_initcall(board_01_led_init);
//arch_initcall(board_01_led_exit);



MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("GPIO driver for Reload default");



