/*
 * Copyright 2006, Realtek Semiconductor Corp.
 *
 * arch/rlx/rlxocp/prom.c
 *   Early initialization code for the RLX OCP Platform
 *
 * Tony Wu (tonywu@realtek.com.tw)
 * Nov. 7, 2006
 */
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <asm/setup.h>
#include <asm/bootinfo.h>
#include <asm/addrspace.h>
#include <asm/page.h>
#include <asm/cpu.h>
#include "bspcpu.h"
#include "bspchip.h"
#include "prom.h"

extern char arcs_cmdline[];

#ifdef CONFIG_EARLY_PRINTK
static int promcons_output __initdata = 0;
                                                                                                    
void unregister_prom_console(void)
{
    if (promcons_output) {
        promcons_output = 0;
    }
}
                                                                                                    
void disable_early_printk(void)
    __attribute__ ((alias("unregister_prom_console")));
#endif
                                                                                                    

const char *get_system_type(void)
{
    return "RTL8672";
}

#define CONFIG_BSP_AUTOMEM
#ifdef CONFIG_BSP_AUTOMEM
u_long __init bsp_getmemsize(void)
{
	unsigned int dcr, value, row, col, bank, busw;
	
	printk("Get memory size on MCR\n");
	dcr = REG32(BSP_MC_DCR);
	
	// bus width
	value = (dcr>>24 & 0x03);
	busw = 1 << (3+value); // 8, 16, 32
	
	// row counts
	value = (dcr>>20 & 0x0f);
	row = 1 << (11+value); // 2K, 4K, .., 64K
	
	// column counts
	value = (dcr>>16 & 0x0f);
	col = 1 << (8+value); // 256, 512, .., 4K
	
	// band counts
	value = (dcr>>28 & 0x03);
	bank = 1 << (1+value); // 2,4,8
	
	//printk("busw=0x%x row=0x%x col=0x%x bank=0x%x\n", busw, row, col, bank);
	return (busw*row*col*bank)/8;
}
#endif

void __init bsp_prom_meminit(void)
{
    char *ptr;
    u_long mem_size;

    #ifdef CONFIG_BSP_AUTOMEM
    char cl_tail[CL_SIZE];
    unsigned int real_mem;
    #endif
   /* Check the command line first for a memsize directive */
   ptr = strstr(arcs_cmdline, "mem=");

   #ifdef CONFIG_BSP_AUTOMEM
   //printk("arcs_cmdline=%s ptr=%s\n", arcs_cmdline, ptr);
   if (ptr) {
      if (!strnicmp(ptr + 4, "auto", 4)) {
         // auto-detect memory size
         real_mem = bsp_getmemsize();
         //printk("real_mem=%x\n", real_mem);
         strlcpy(cl_tail, ptr+8, sizeof(cl_tail));
         if (real_mem >= 0x100000 && real_mem <= 0x40000000) {
            real_mem >>= 20;
            //printk("real_mem=%dM\n", real_mem);
            sprintf(ptr, "mem=%dM%s", real_mem, cl_tail);
         }
         else {
            printk("Error! memory size 0x%x not support, default to 32M.\n", real_mem);
            sprintf(ptr, "mem=32M%s", cl_tail);
         }
      }
      //printk("arcs_cmdline=%s ptr=%s\n", arcs_cmdline, ptr);
      mem_size = memparse(ptr + 4, &ptr);
   }
   else
      mem_size = cpu_mem_size;  /* Default to 32MB */
   #else
   if (ptr)
      mem_size = memparse(ptr + 4, &ptr);
   else
      mem_size = cpu_mem_size;  /* Default to 32MB */
   #endif

   /*
    * call <add_memory_region> to register boot_mem_map
    * add_memory_region(base, size, type);
    * type: BOOT_MEM_RAM, BOOT_MEM_ROM_DATA or BOOT_MEM_RESERVED
    */
    add_memory_region(0, mem_size, BOOT_MEM_RAM);
}

void __init bsp_free_prom_memory(void)
{
  return;
}


//set system clock and spi control registers according chip type
unsigned int BSP_MHZ, BSP_SYSCLK;
unsigned int SOC_ID , SOC_ID_EXT , SOC_BOND_ID;
unsigned int GPIO_CTRL_0, GPIO_CTRL_1, GPIO_CTRL_2, GPIO_CTRL_4;
/*ccwei*/
#ifdef CONFIG_RTL8672_SPI_FLASH
extern unsigned int SFCR, SFCSR, SFDR;
#elif defined(CONFIG_MTD_NAND)
unsigned int SFCR, SFCSR, SFDR;
#endif
/*end-ccwei*/

#ifdef CONFIG_RTL8685
static void rtl8685_chip_detect(void){

	unsigned int cmu_ctlr_val =0;
	unsigned int cmu_div = 1;
	unsigned int src_oc_mhz=0, lx_clk_div_offset=0;
	unsigned int lx_clk_div=0, lx_freq_mhz=0;

	cmu_ctlr_val = REG32(BSP_CMU_CTRL);
	if(((cmu_ctlr_val & SYSREG_CMUCTLR_CMU_MD_MASK)>> SYSREG_CMUCTLR_CMU_MD_FD_S)\
			 == SYSREG_CMUCTLR_CMU_MD_MANUALLY_SEL){

		cmu_div = 1 << ((cmu_ctlr_val & SYSREG_CMUCTLR_CMU_LX0_FREQ_DIV_MASK) >>\
			   SYSREG_CMUCTLR_CMU_LX0_FREQ_DIV_FD_S);
	}

	/* Only 25MHz Xtal */
	src_oc_mhz = 25;
	lx_clk_div_offset = 6;

	/* Calculate LX clock */
	lx_clk_div = REG32(BSP_LX_PLL_SEL_REG) + lx_clk_div_offset;
	lx_freq_mhz = (src_oc_mhz * (lx_clk_div + 2))/2;

	BSP_MHZ = lx_freq_mhz/cmu_div;
	BSP_SYSCLK = BSP_MHZ*1000*1000;
	//read soc id
	WRITE_MEM32(CHIP_ID_REG, 0xa0000000);
	SOC_ID=(REG32(CHIP_ID_REG)& 0xffff);
	if(SOC_ID==0x0371){
		GPIO_CTRL_0 = 0xbb000100;
		GPIO_CTRL_1 = 0xbb00010c;
		GPIO_CTRL_2 = 0xbb000118;
		GPIO_CTRL_4 = 0xbb0001d4;
	} else {
		GPIO_CTRL_0 = 0xBB0000D8;
		GPIO_CTRL_1 = 0xBB0000E4;
		GPIO_CTRL_2 = 0xBB0000F0;
		GPIO_CTRL_4 = 0xBB0000FC;
	}
	WRITE_MEM32(BOND_CHIP_MODE,0xb0000000); //enable bound id display.
	SOC_BOND_ID=(REG32(BOND_CHIP_MODE)& 0xff);
	SOC_ID_EXT=((REG32(BOND_CHIP_MODE)& 0x0000ff00)>>8);

#if defined(CONFIG_RTL8672_SPI_FLASH) || defined(CONFIG_MTD_NAND)
	/* Set Flash controller base address */
  	SFCR = 0xB8001200;
        SFCSR = 0xB8001208;
        SFDR = 0xB800120C;
#endif
	return;
}
#endif

#ifdef CONFIG_RTL8685S
static void rtl8685s_chip_detect(void){

	
	unsigned int cmu_ctlr_val =0;
	unsigned int cmu_div = 1, cmu_mode, cmu_busy;
	unsigned int src_oc_mhz=0, lx_clk_div_offset=0;
	unsigned int lx_clk_div=0, lx_freq_mhz=0;

	cmu_ctlr_val = REG32(BSP_CMU_CTRL);
	cmu_mode = ((cmu_ctlr_val & SYSREG_CMUCTLR_CMU_MD_MASK)>>\
						SYSREG_CMUCTLR_CMU_MD_FD_S);	
	cmu_busy = (REG32(BSP_CMU_LXBR_REG) >> SYSREG_CMULXBR_LXP_BUSY_S) & 0x1;
	
	if(cmu_mode != SYSREG_CMUCTLR_CMU_MD_DISABLED_SEL){
		if(cmu_busy && (cmu_mode== SYSREG_CMUCTLR_CMU_MD_DYNAMIC_SEL)){
			/* 
-				Two conditions let CMU busy bit work :
-				1. In CMU dynamic mode 
-				2. busy bit was set as 1
-			*/
			cmu_div =1;			
		}else{
			cmu_div = 1 << ((cmu_ctlr_val & SYSREG_CMUCTLR_CMU_LXP_FREQ_DIV_MASK) >> \
				SYSREG_CMUCTLR_CMU_LXP_FREQ_DIV_FD_S);
			
			if(IS_RLE0705){
				if(cmu_div==1) cmu_div=2;
			}
		}	
	}

	/* Only 25MHz Xtal */
	lx_clk_div_offset = 2;
	
	/* Calculate LX clock */
	lx_clk_div = ((REG32(BSP_LX_PLL_SEL_REG) & SYSREG_LXPLLSEL_LXPLL_DIV_MASK) >>\
			SYSREG_LXPLLSEL_LXPLL_DIV_S)+ lx_clk_div_offset;
	lx_freq_mhz =(1000/lx_clk_div);

	BSP_MHZ = lx_freq_mhz/cmu_div;
	BSP_SYSCLK = BSP_MHZ*1000*1000;

	/* Choose the old SPIF controller */
	//REG32(BSP_PIN_STATUS) |= 0x200;

#if defined(CONFIG_RTL8672_SPI_FLASH) || defined(CONFIG_MTD_NAND)
	/* Set Flash controller base address */
	SFCR = 0xB8001200;
	SFCSR = 0xB8001208;
	SFDR = 0xB800120C;
#endif

	return;
}
#endif /* CONFIG_RTL8685S */
 
u32 bsp_spif_mmio_copy(void* to, loff_t from, u32 len) {
	u32 to_copy = len;	

	if (from >= BSP_SPI_MMIO_SIZE) 
		return 0;
	if ((from + len) > BSP_SPI_MMIO_SIZE) {		
		to_copy = BSP_SPI_MMIO_SIZE - from;
	}
	
	memcpy((void*)to, (void *)(BSP_SPI_MMIO_BASE+(u32)from), to_copy);
	
	return to_copy;
}

void bsp_spif_mmio_inv(u32 from, u32 len) {
	dma_cache_inv(BSP_SPI_MMIO_BASE+from, len);
}
 
/* Do basic initialization */
void __init bsp_init(void)
{
#ifdef CONFIG_RTL8685
	rtl8685_chip_detect();
#elif defined(CONFIG_RTL8685S)
	rtl8685s_chip_detect();
	REG32(0xb8000008) = 0; // disable bus-timeout
#endif
	prom_console_init();
	bsp_prom_meminit();
}

